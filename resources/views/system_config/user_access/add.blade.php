<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row" id="frm_input_name">
                    <div class="col-md-6">
                        <label class="control-label">{{ __('page.name')}}</label>
                            <input type="text" class="form-control form-control-xs" name="employee_name" id="employee_name" disabled value="{{ $array['info']->name }}">
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">{{ __('page.username') }}</label>
                                <input type="text" class="form-control form-control-xs" name="username" id="username" value="{{ $array['info']->username }}">
                    </div>
                </div>
                <hr>
            </div>
        </div>
        
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <label><b>Access Controls for Pre Evaluation Building Permit Application</b></label>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-lg-12">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_1" name="access[]" value="1">
                    <label class="custom-control-label" for="access_1">
                        <b>Assessor</b> (Assessment of documents)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_2" name="access[]" value="2">
                    <label class="custom-control-label" for="access_2">
                        <b>Administrator</b> (Recommendation for evaluation)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_3" name="access[]" value="3">
                    <label class="custom-control-label" for="access_3">
                        <b>Evaluators</b> (Checking of documents)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_4" name="access[]" value="4">
                    <label class="custom-control-label" for="access_4">
                        <b>Licensing Officer</b> (IPER Report)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_5" name="access[]" value="5">
                    <label class="custom-control-label" for="access_5">
                        <b>Administrator</b> (For recommendation of application)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_6" name="access[]" value="6">
                    <label class="custom-control-label" for="access_6">
                        <b>Assessor</b> (For approval of pre evalution)
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <label><b>Access Controls for Building Permit Application</b></label>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-lg-12">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_7" name="access[]" value="7">
                    <label class="custom-control-label" for="access_7">
                        <b>Assessor</b> (Assessment of documents)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_8" name="access[]" value="8">
                    <label class="custom-control-label" for="access_8">
                        <b>Assessor</b> (Personnel to Send All the Documents)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_9" name="access[]" value="9">
                    <label class="custom-control-label" for="access_9">
                        <b>Assessor</b> (Personnel to Receive and Upload FSEC)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_10" name="access[]" value="10">
                    <label class="custom-control-label" for="access_10">
                        <b>Assessor</b> (Personnel to Receive OTC Payment and Upload Receipt)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_11" name="access[]" value="11">
                    <label class="custom-control-label" for="access_11">
                        <b>OBO Assessor</b> (Reviewer of recommended application)
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_12" name="access[]" value="12">
                    <label class="custom-control-label" for="access_12">
                        <b>Approver</b> (Approval of application and creating payment request)
                    </label>
                </div>

                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_13" name="access[]" value="13">
                    <label class="custom-control-label" for="access_13">
                        <b>OBO Assessor</b> (Acceptance of Payment and Issuance of Permit)
                    </label>
                </div>

                <!-- <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="access_16" name="access[]" value="16">
                    <label class="custom-control-label" for="access_16">
                        <b>Assessor</b> (For issuance of application permit)
                    </label>
                </div> -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        //get_application('{{$option}}', {{ $id }});

        @php
            if($array['info']->position)
            {
                $user_access = explode('|', $array['info']->position);
                foreach($user_access as $key => $value)
                {
                    echo '$("#access_'.$value.'").prop("checked", true);';
                }    
            }
            
        @endphp

        $("#save_btn").click(function() {
            var access_obj = '';
            $("[id*='access_']").each(function () {
                if($(this).is(':checked'))
                {
                    access_obj += $(this).val() + '|';
                }
            });
            var frm, text, title, success, emp_id;

            frm = document.querySelector('#edit_form');


            title = "{{ __('page.edit_'.$option) }}";
            text = "{{ __('page.update_this') }}";
            success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";

            const swal_continue = alert_continue(title, text);
            swal_continue.then((result) => {
                if(result.value){
                    clearErrors();

                    var formData = new FormData();

                    formData.append('user_access', access_obj);
                    formData.append('id', {{$array['id']}});
                    formData.append('username', $("#username").val());
                    
                    axios.post(frm.action, formData)
                    .then((response) => {
                        const swal_success = alert_success(success);
                        swal_success.then((value) => {
                            clearErrors();
                            window.location.href="{{ $cancel_url }}";
                        });
                    })
                    .catch((error) => {
                        const errors = error.response.data.errors;

                        if(typeof(errors) == 'string')
                        {
                            alert_warning(errors);
                        }
                        else
                        {
                            alert_warning("{{ __('page.check_inputs') }}");

                            const firstItem = Object.keys(errors)[0];
                            const firstItemDOM = document.getElementById(firstItem);
                            const firstErrorMessage = errors[firstItem][0];

                            firstItemDOM.scrollIntoView();
                            showErrors(firstItem, firstErrorMessage, firstItemDOM, ['user_status', 'name']);
                        }
                    });
                }
            })
        });
    });

    function get_application(option, id)
    {
        axios.get("{{ url('get-application') }}/" + option + "/" + id)
        .then(function(response){
            var info = response.data.data.info;
            $("#employee_name").val(info.name);
            $("#username").val(info.username);
        })
        .catch(function(error){
           
        }) 
    }
</script>