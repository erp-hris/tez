@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.form-css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    @php 
        $data = [
        'option' => $module, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $module.'.'.$option.'.table',
        ];
    @endphp

    @include('others.main_content', $data)
@endsection

@section('scripts')
    @include('layouts.auth-partials.form-scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
            App.dataTables();
            load_datables('#application_tbl', '{{$json_url}}', {!! json_encode($columns) !!});
        });
        function reset_pw(id)
        {
            const swal_continue = alert_continue('Reset Password', 'Are you sure you want to continue?');
            swal_continue.then((result) => {
                if(result.value){
                    Swal.fire({
                        title: 'Processing...',
                        customClass: 'content-actions-center',
                        buttonsStyling: true,
                        allowOutsideClick: false,
                        onOpen: function() {
                            swal.showLoading();
                            
                            axios.post("{{url('/reset-password/')}}" + "/" + id)
                            .then((response) => {
                                Swal.fire({
                                    text: "Reset Successfully.",
                                    type: 'success',
                                    customClass: 'content-text-center',
                                    confirmButtonClass: 'btn',
                                });
                            })
                            .catch((error) => {
                                Swal.fire({
                                    text: error.response.data.errors,
                                    type: 'warning',
                                    customClass: 'content-text-center',
                                    showConfirmButton: false
                                });
                            });
                        }
                    });
                }
            });
        }
    </script>
@endsection
