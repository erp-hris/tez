                
                <table id="application_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                    <thead>
                        <tr class="text-center">
                            <th style="width: 10%;">{{ __('page.action') }}</th>
                            <th style="width: 30%;">Employee Name</th>
                            <th style="width: 30%;">Position</th>
                            <th style="width: 30%;">Type of Permit</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>