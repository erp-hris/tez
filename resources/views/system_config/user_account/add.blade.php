<div class="row">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <label>First Name</label>
                <input type="text" class="form-control form-control-xs" name="first_name" id="first_name">
            </div>
            <div class="col-md-6">
                <label>Last Name</label>
                <input type="text" class="form-control form-control-xs" name="last_name" id="last_name">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <label>Middle Name</label>
                <input type="text" class="form-control form-control-xs" name="middle_name" id="middle_name">
            </div>
            <div class="col-md-6">
                <label>Email</label>
                <input type="text" class="form-control form-control-xs" name="email" id="email">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <label>Username</label>
                <input type="text" class="form-control form-control-xs" name="username" id="username">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <label>Position</label>
                <select class="select2 select2-xs" id="position" name="position">
                    <option value="0">Administrator</option>
                    <option value="2">Assessor</option>
                    <option value="3">Evaluator</option>
                    <option value="4">Recommendator</option>
                    <option value="5">Approver</option>
                </select>
            </div>
            <div class="col-md-6">
                <label>Permit Type</label>
                <select class="select2 select2-xs" id="permit_type" name="permit_type">
                    <option value="1">Business Permit</option>
                    <option value="2">Location Permit</option>
                    <option value="3">Building Permit</option>
                    <option value="4">Occupancy Permit</option>
                </select>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        
        $('#save_btn').click(function()
        {
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "{{ __('page.add_new_application') }}",
                text: "{{ __('page.apply_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post(frm.action, {
                        @foreach($array['fields'] as $key => $value)
                        {{ $value }} : $("#{{ $value }}").val(),
                        @endforeach
                        @if(isset($array['id']))
                        id: {{$array['id']}},
                        @endif
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Account has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{ url($module.'/'.$option) }}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });

    function get_application(option, id)
    {
        axios.get("{{ url('get-application') }}/" + option + "/" + id)
        .then(function(response){
            console.log(response);
            @foreach($array['fields'] as $key => $value)
            $("#{{$value}}").val(response.data.data.{{$value}});
            @endforeach
            /*$("#leave_id").val(response.data.data.leave_id).trigger('change');
            $("#signatory1").val(response.data.data.signatory1).trigger('change');
            $("#signatory2").val(response.data.data.signatory2).trigger('change');
            $("#signatory3").val(response.data.data.signatory3).trigger('change');
            $("#force_leave").val(response.data.data.force_leave).trigger('change');
            $("#start_date").val(response.data.data.start_date);
            $("#end_date").val(response.data.data.end_date);
            $("#leave_location").val(response.data.data.leave_location);
            $("#filed_date").val(response.data.data.filed_date);
            $("#sick_leave_reason").val(response.data.data.sick_leave_reason);
            $("#employee_name").val(response.data.data.employee_name);*/
        })
        .catch(function(error){
           
        }) 
    }
</script>