@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
    @include('layouts.auth-partials.form-css')
@endsection

@section('content')
    
    @php 
        $data = [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_footer' => 'yes',
        'isSave' => 'yes', 
        'cancel_url' => url($module.'/'.$option),
        ];

        
    @endphp
    
    @include('others.main_content', $data)

    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' => url($module.'/'.$option.'/store'), 'frm_id' => 'add_form'])
@endsection

@section('scripts')

@yield('additional-scripts')
        
    <script type="text/javascript">
        $(document).ready(function(){
        });
    </script>
@endsection