<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

    <style type="text/css">

        html { margin: 10px}
        body {
            font-family: cambria;
            font-size: 9pt;
            color: #000000;
            background: white;
            margin: 0;
        }

        th {
            vertical-align: top;
        }

        td {
            padding: 2px;
            font-size: 8pt;
            vertical-align: top;
        }

        .has-table-border {
           border: 2px solid black; 
        }

        .page-header, .page-header-space {
            height: 140px;
        }

        .page-footer, .page-footer-space {
        }

        table {
            table-layout: fixed;
        }

        thead {display: table-header-group;} 
        tfoot {display: table-footer-group;}

        .page-footer {
            display: block;
            position: fixed;
            width: 100%;
            bottom: 100px;
        }

        .page-header {
            display: block;
            position: fixed;
            top: 0;
            width: 100%;
        }

        .centered-address {
            display: inline-block;
            position: absolute;
            top: 3%;
            left: 59%;
            font-size: 9px;
            text-align: right;
            font-style: Gotham Book !important;
        }

        .centered-contact {
            display: inline-block;
            position: absolute;
            left:81%;
            font-size: 9px;
            font-style: Gotham Book !important;
            text-align: left;
        }

        #hide_when_print {
            display: none;
        }

        .remove-border {
            border-right:none;
            border-left:none;
            border-bottom:none;
            border-top:none;
        }

        .only_left_right_border {
            border-left:2px solid;
            border-right:2px solid;
            border-bottom:none;
            border-top:none;
        }

        img {
            border:0;
        }

        input[type=checkbox] {
            transform: scale(1.5);
            -ms-transform: scale(1.5);
            -webkit-transform: scale(1.5);
            padding: 2px;
        }
    </style>
</head>
<body>
    <div class="page-header">
        <img src="{{ asset('img/sr_header_1.jpg') }}" width="100%" height="160" style="display:inline-block;" />
        <div class="centered-address" data-html="true">
            <b>6th & 7th Floors, Tower 1 <br>
            Double Dragon Plaza <br>
            Double Dragon Meridian Park <br>
            Macapagal Avenue corner <br>
            Edsa Extension<br>
            1302 Bay Area, Pasay City<br></b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 4.8%;">
            <b>(+632) 8249-5982</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 6.8%;color:#4285f4">
            &nbsp;<b>traveltax.helpdesk@tieza.gov.ph</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 8.8%;">
            &nbsp;<b>www.tieza.gov.ph</b>
        </div>     
    </div>

    <div class="page-footer">
        <img src="{{ asset('img/footer.png') }}" width="100%" height="80px">
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="page">
                    <div class="page-header-space"></div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <br>
                            <br>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td class="text-center" colspan="10"> 
                                            <h3>Travel Tax Department</h3>
                                            <h5>PRIVILEGE ADMINISTRATION DIVISION</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9"> 
                                            TIEZA Online Form 353
                                        </td>
                                        <td colspan="1"> 
                                            Revised 2020
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" colspan="10"> 
                                            <h5>APPLICATION FOR ONLINE TRAVEL TAX REFUND</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7"> 
                                            &nbsp;
                                        </td>
                                        <td colspan="1"> 
                                            <h6>AR Number</h6>
                                        </td>
                                        <td colspan="2"> 
                                            <h6>ONREF-2020-0001</h6>
                                        </td>
                                    </tr>   
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="2">NAME OF PASSENGER</td>
                                        <td class="has-table-border" colspan="8">&nbsp;{{ $ta->ffull_name }}</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border" colspan="2" rowspan="2">AIRLINE TICKET NUMBER/S</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                        <td class="has-table-border" colspan="2" rowspan="3">DATE ISSUED</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border" colspan="2">SALES REPORT NO.</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border" colspan="2">AMOUNT OF TRAVEL TAX</td>
                                        <td class="has-table-border" colspan="3">{{ $ta->travel_tax_amount }}</td>
                                        <td class="has-table-border" colspan="2">AMOUNT OF REFUND</td>
                                        <td class="has-table-border" colspan="3">{{ $ta->refund_amount }}</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border" colspan="2">TIEZA OFFICIAL RECEIPT NO.</td>
                                        <td class="has-table-border" colspan="3">{{ $ta->tieza_official_receipt_no }}</td>
                                        <td class="has-table-border" colspan="2">DATE OF PAYMENT</td>
                                        <td class="has-table-border" colspan="3">{{ date('m/d/Y', strtotime($ta->payment_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="only_left_right_border" colspan="10" >REASON FOR REFUND</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center remove-border" colspan="1" style="border-left:2px solid;"> &nbsp;</td>
                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '1' ? 'checked' : '' }}>&nbsp;&nbsp;EXEMPTION</td>

                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '2' ? 'checked' : '' }}>&nbsp;&nbsp;REDUCED RATE</td>

                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '3' ? 'checked' : '' }}>&nbsp;&nbsp;NON-COVERAGE</td>
                                        <td colspan="3"  style="border-right:2px solid;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center remove-border" colspan="1"  style="border-left:2px solid;"> &nbsp;</td>
                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '4' ? 'checked' : '' }}>&nbsp;&nbsp;UNUSED TICKET</td>

                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '5' ? 'checked' : '' }}>&nbsp;&nbsp;DOUBLE PAYMENT</td>

                                        <td class="remove-border" colspan="5"  style="border-right:2px solid;"><input type="checkbox" {{ $ta->reason_refund == '6' ? 'checked' : '' }}>&nbsp;&nbsp;OTHERS <u>{{ $ta->reason_refund == '6' ? ": ".$ta->reason_refund_others : ''  }}</u></td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="10">NOTES: > Applications for travel tax refunds should be filed within two (2) years from the date of payment.<br> > The travel tax paid to the airline is due for remittance to TIEZA on ___________________.</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="10">This is to certify that the above-mentioned information are true and correct and that the travel tax collected on the above-mentioned ticket has not been previously refunded.</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="5">SUBMITTED BY:<br>___________________________________<br><br>___________________________________<br>(Name and signature of passenger)<br>Address: _____________________________________<br>_____________________________________________<br>Contact No: __________________________________<br>Email Address : _______________________________</td>


                                        <td class="has-table-border text-center" colspan="5">CERTIFIED BY:<br>___________________________________<br>(Name and signature of airline representative)<br>___________________________________<br>(Position)<br>Carrier _____________________________<br>Date _________________________<br><br><b>NOTE: FOR UNUSED TICKET ONLY</b></td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="10">PROCESSING OF ONLINE REFUND APPLICATION</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">PROCESS</td>
                                        <td class="has-table-border text-center" colspan="2">DATE</td>
                                        <td class="has-table-border text-center" colspan="3">POINT PERSON</td>
                                        <td class="has-table-border text-center" colspan="2">REMARKS</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Receipt/Evaluation of Application</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Review/Recommendation for Payment</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Print/ Collate of Documents</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Verification of Travel Tax Collection</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Preparation of Disbursement Voucher</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Certification for Payment</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Transmittal of Voucher to Accounting Division</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        
                    </div> 
                </div>
            </div>
        </div>
        
    </div>
</body>

</html>