<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

    <style type="text/css">

        html { margin: 10px}
        body {
            font-family: cambria;
            font-size: 10pt;
            color: #000000;
            background: white;
            margin: 0;
        }

        th {
            vertical-align: top;
        }

        td {
            padding: 2px;
            font-size: 9pt;
            vertical-align: top;
        }

        .has-table-border {
           border: 2px solid black; 
        }

        .page-header, .page-header-space {
            height: 140px;
        }

        .page-footer, .page-footer-space {
        }

        table {
            table-layout: fixed;
        }

        thead {display: table-header-group;} 
        tfoot {display: table-footer-group;}

        .page-footer {
            display: block;
            position: fixed;
            width: 100%;
            bottom: 100px;
        }

        .page-header {
            display: block;
            position: fixed;
            top: 0;
            width: 100%;
        }

        .centered-address {
            display: inline-block;
            position: absolute;
            top: 3%;
            left: 59%;
            font-size: 9px;
            text-align: right;
            font-style: Gotham Book !important;
        }

        .centered-contact {
            display: inline-block;
            position: absolute;
            left:81%;
            font-size: 9px;
            font-style: Gotham Book !important;
            text-align: left;
        }

        #hide_when_print {
            display: none;
        }

        .remove-border {
            border-right:none;
            border-left:none;
            border-bottom:none;
            border-top:none;
        }

        img {
            border:0;
        }

        input[type=checkbox] {
            transform: scale(1.5);
            -ms-transform: scale(1.5);
            -webkit-transform: scale(1.5);
            padding: 2px;
        }
    </style>
</head>
<body>
    <div class="page-header">
        <img src="{{ asset('img/sr_header_1.jpg') }}" width="100%" height="160" style="display:inline-block;" />
        <div class="centered-address" data-html="true">
            <b>6th & 7th Floors, Tower 1 <br>
            Double Dragon Plaza <br>
            Double Dragon Meridian Park <br>
            Macapagal Avenue corner <br>
            Edsa Extension<br>
            1302 Bay Area, Pasay City<br></b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 4.8%;">
            <b>(+632) 8249-5982</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 6.8%;color:#4285f4">
            &nbsp;<b>traveltax.helpdesk@tieza.gov.ph</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 8.8%;">
            &nbsp;<b>www.tieza.gov.ph</b>
        </div>     
    </div>

    <div class="page-footer">
        <img src="{{ asset('img/footer.png') }}" width="100%" height="80px">
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="page">
                    <div class="page-header-space"></div>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <br>
                            <br>
                            <h3>Travel Tax Department</h3>
                            PRIVILEGE ADMINISTRATION DIVISION
                            <br>
                            <br>
                            ACKNOWLEDGEMENT RECEIPT FOR ONLINE TRAVEL TAX REFUND APPLICATION
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <table width="100%">
                                <tbody>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="2">NAME OF PASSENGER</td>
                                        <td class="has-table-border" colspan="8">&nbsp;{{ $ta->ffull_name }}</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="2" rowspan="2">PAYABLE TO<br>(FOR MINOR/ QUALIFIED PAYEE)</td>
                                        <td class="has-table-border" colspan="8" rowspan="2">&nbsp;{{ $ta->is_minor == 1 ? $ta->guardian_ffull_name : $ta->ffull_name }}</td>
                                    </tr>
                                    <tr>&nbsp;</tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="2">DATE SUBMITTED</td>
                                        <td class="has-table-border" colspan="3">&nbsp;{{ date('m/d/Y', strtotime($ta->date_application)) }}</td>
                                        <td class="has-table-border text-center" colspan="2" >AR NUMBER</td>
                                        <td class="has-table-border" colspan="3">&nbsp;{{ $ta->ar_no }}</td>
                                    </tr>
                                    <tr>
                                        <td class="remove-border" colspan="10"  style="border-left:2px solid;border-right:2px solid;">REASON FOR REFUND</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center remove-border" colspan="1" style="border-left:2px solid;"> &nbsp;</td>
                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '1' ? 'checked' : '' }}>&nbsp;&nbsp;EXEMPTION</td>

                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '2' ? 'checked' : '' }}>&nbsp;&nbsp;REDUCED RATE</td>

                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '3' ? 'checked' : '' }}>&nbsp;&nbsp;NON-COVERAGE</td>
                                        <td colspan="3" style="border-right:2px solid;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center remove-border" colspan="1" style="border-left:2px solid;"> &nbsp;</td>
                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '4' ? 'checked' : '' }}>&nbsp;&nbsp;UNUSED TICKET</td>

                                        <td class="remove-border" colspan="2"><input type="checkbox" {{ $ta->reason_refund == '5' ? 'checked' : '' }}>&nbsp;&nbsp;DOUBLE PAYMENT</td>

                                        <td class="remove-border" colspan="5" style="border-right:2px solid;"><input type="checkbox" {{ $ta->reason_refund == '6' ? 'checked' : '' }}>&nbsp;&nbsp;OTHERS <u>{{ $ta->reason_refund == '6' ? ": ".$ta->reason_refund_others : ''  }}</u></td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border text-center" colspan="10">CHECKLIST OF DOCUMENTARY REQUIREMENT ATTACHED</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_passport > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Clear copy of passport/s</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_airline > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Airline Ticket/ Confirmation Number</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_tieza_official > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Clear copy of TIEZA Official Receipt - ( ) Passenger / white copy ( ) Airline / pink copy</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_marriage > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Clear copy of Marriage Contract</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_birth_certificate > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Clear copy of Birth Certificate</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_ofw_certificate > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Clear copy of  Overseas Employment Certificate (OEC) of OFW</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_proof_permanent > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Clear copy of proof of Permanent Residency (eg green card, carta soggiorno)</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_travel_authority > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Clear copy of Travel Order / Travel Authority / appropriate certification: _______</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_passport_applicable_arrival > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Page/s of passport showing the applicable date of arrival and the Philippine Immigration status</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_letter_request > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Letter Request: ______</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox" {{ $count_affidavit_name_descrepancy > 0 ? 'checked' : '' }}>&nbsp;&nbsp;Affidavit of Name Discrepancy</td>
                                    </tr>
                                    <tr>
                                        <td class="has-table-border" colspan="10"><input type="checkbox">&nbsp;&nbsp;Others: ______</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <b><u>IMPORTANT NOTES:</u></b>
                                            <br>
                                            1. Applications for refund made after two (2) years from date of payment shall not be allowed.
                                            <br>
                                            2. Travel Tax collected on ticket must be remitted to TIEZA before proceeding with the refund.
                                        </td>
                                        <td colspan="1" class="text-center">&nbsp;</td>
                                        <td colspan="4" class="text-center">
                                            <span class="" style="position: relative;right: 30px;">
                                                <br>
                                                RECEIVED AND EVALUATED BY:
                                                <br>
                                                <br>
                                                <b><u>{{ $ta->supervisor_ffull_name }}</u></b>
                                                <br>
                                                (Name and signature of Travel Tax Personnel)
                                                <br>
                                                Date {{ date('m/d/Y') }}
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        
                    </div> 
                </div>
            </div>
        </div>
        
    </div>
</body>

</html>