@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
    @include('layouts.auth-partials.form-css')
@endsection

@section('content')
    @php 
        $data = [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_footer' => 'yes', 
        'isPreview' => 'yes',
        'cancel_url' => $application_url.'/'.$option,
        ];
    @endphp

    @include('others.main_content', $data)

    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' => $application_url.'/'.$option.'/print', 'frm_id' => 'print_form'])
@endsection

@section('scripts')
    @yield('additional-scripts')
    @include('layouts.auth-partials.datatables-scripts')
    @include('layouts.auth-partials.form-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
        });
    </script>
@endsection