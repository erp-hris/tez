@extends('layouts.master-auth')
@section('css')
@include('layouts.auth-partials.datatables-css')
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/css/jquery.niftymodals.css') }}"/>
<style>
    
    .tracking li {
        width: 2em;
        height: 2em;
        text-align: center;
        line-height: 2em;
        border-radius: 1em;
        background: #1a53ff;
        margin: 0 1em;
        display: inline-block;
        color: white;
        position: relative;
    }

    .tracking li::before{
        content: '';
        position: absolute;
        top: .9em;
        left: -4em;
        width: 4em;
        height: .2em;
        background: #1a53ff;
        z-index: -1;
    }



    .tracking li:first-child::before {
        display: none;
    }

    .tracking .active {
        background: #1a53ff;
    }

    .tracking .active ~ li {
        background: #ccd9ff;
    }

    .tracking .active ~ li::before {
        background: #ccd9ff;
    }


</style>
@endsection

@section('content')
    @php
        if($pre_eval)
        {
            $status = $pre_eval->status;    
        }
        else
        {
            $status = '';
        }
        
        if ($status == 1)
        {
            $status = 'For Submission';
        }
        elseif ($status == 2)
        {
            $status = 'For Assessment';
        }
        elseif ($status == 3)
        {
            $status = 'For Approval of Assessment';
            //On Administrator
        }
        elseif ($status == 4)
        {
            $status = 'For Evaluation';
        }
        elseif ($status == 5)
        {
            $status = 'For Review of Administrator';
            //Back to Administrator
        }
        elseif ($status == 6)
        {
            $status = 'Back to Assessor';
        }
        elseif ($status == 7)
        {
            $status = 'For Building Permit Application';
        }
        elseif ($status == 8)
        {
            $status = 'Incomplete';
        }
    @endphp
    <div class="row" id="dashboard_canvas">
        <div class="col-4">
            <div class="row" style="border: 1px dashed black; padding: 10px; background: lightblue; color: black; font-size: 15pt; text-align: center; text-transform: uppercase;">
                <div class="col-lg-12">
                    {{ $status }}
                </div>
            </div>
        </div>
    </div> 
    
@endsection


@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
    @yield('additional-scripts')
   
    <script type="text/javascript">
       $(document).ready(function () {
            App.init();
            App.dataTables(); 
            
       });
    </script>
@endsection