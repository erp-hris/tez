@extends('layouts.master-auth')
@section('css')
@include('layouts.auth-partials.datatables-css')
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/css/jquery.niftymodals.css') }}"/>
<style>
    .data-info {
        color: black;
        text-align: right;
    }
    .data-info:hover {
        cursor: pointer;
        transition: 0.5s;
        font-weight: 600;
    }
    .card-body-tieza-bg-color {
        box-shadow: 5px 5px 0px gray;
    }
    .card-header-tieza-bg-color {
        box-shadow: 5px 5px 0px gray;   
        color: black !important;
    }
    h1{
        font-family:'Arial Black' !important;
        color:rgb(6,10,57) !important;
    }
    #dashboard_canvas {
        color: black !important;
    }
    #dashboard_canvas a:visited {
        color: black !important;
    }
    #dashboard_canvas a:active {
        color: black !important;
    }

</style>
@endsection

@section('content')
    <div class="row" id="dashboard_canvas">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header card-header-divider">
                            Locational Permit
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    On process
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Assessment
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Evaluation
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Recommendation
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Approval
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Payment
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Issuance
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    Issued
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header card-header-divider">
                            Business Permit
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    On process
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Assessment
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Evaluation
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Recommendation
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Approval
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Payment
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Issuance
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    Issued
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header card-header-divider">
                            Building Permit
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    On process
                                </div>
                                <div class="col-lg-5 text-right">
                                    {{ $building_permit_rpt['process'] }}
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Assessment
                                </div>
                                <div class="col-lg-5 text-right">
                                    {{ $building_permit_rpt['assessment'] }}
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Evaluation
                                </div>
                                <div class="col-lg-5 text-right">
                                    {{ $building_permit_rpt['evaluation'] }}
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Recommendation
                                </div>
                                <div class="col-lg-5 text-right">
                                    {{ $building_permit_rpt['recommendation'] }}
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Approval
                                </div>
                                <div class="col-lg-5 text-right">
                                    {{ $building_permit_rpt['approval'] }}
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Payment
                                </div>
                                <div class="col-lg-5 text-right">
                                    {{ $building_permit_rpt['payment'] }}
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Issuance
                                </div>
                                <div class="col-lg-5 text-right">
                                    {{ $building_permit_rpt['issuance'] }}
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    Issued
                                </div>
                                <div class="col-lg-5 text-right">
                                    {{ $building_permit_rpt['issued'] }}
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header card-header-divider">
                            Occupancy Permit
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    On process
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Assessment
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Evaluation
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Recommendation
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Approval
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Payment
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Issuance
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    Issued
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header card-header-divider">
                            Annual Building Inpection Certificate
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    On process
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Assessment
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Evaluation
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Recommendation
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Approval
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Payment
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    For Issuance
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-5">
                                    Issued
                                </div>
                                <div class="col-lg-5 text-right">
                                    0
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    
@endsection


@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
    @yield('additional-scripts')
   
    <script type="text/javascript">
       $(document).ready(function () {
        App.init();
        App.dataTables(); 
       });
    </script>
@endsection