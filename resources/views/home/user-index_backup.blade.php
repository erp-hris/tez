@extends('layouts.master-auth')
@section('css')
@include('layouts.auth-partials.datatables-css')
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/css/jquery.niftymodals.css') }}"/>
<style>
    
    .tracking li {
        width: 2em;
        height: 2em;
        text-align: center;
        line-height: 2em;
        border-radius: 1em;
        background: #1a53ff;
        margin: 0 1em;
        display: inline-block;
        color: white;
        position: relative;
    }

    .tracking li::before{
        content: '';
        position: absolute;
        top: .9em;
        left: -4em;
        width: 4em;
        height: .2em;
        background: #1a53ff;
        z-index: -1;
    }



    .tracking li:first-child::before {
        display: none;
    }

    .tracking .active {
        background: #1a53ff;
    }

    .tracking .active ~ li {
        background: #ccd9ff;
    }

    .tracking .active ~ li::before {
        background: #ccd9ff;
    }


</style>
@endsection

@section('content')
    <div class="row" id="dashboard_canvas" style="visibility: hidden;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @php
                $status_arr = ['','On Process', 'For Assessment', 'For Evaluation', 'For Recommendation', 'For Approval', 'For Payment', 'Receiving of Payment', 'Issued'];

                $remarks_arr = ['','On Process', 'Assessment Remarks', 'Evaluation Remarks', 'Recommendation Remarks', 'Approval Remarks', 'Payment Remarks', 'Receiving of Payment Remarks', 'Issuance Remarks'];

                function change_date_format($date)
                {
                    if($date)
                    {
                        return date('m/d/Y h:i A', strtotime($date));
                    }
                    else
                    {
                        return false;
                    }
                }

                function compute_date($date_1 , $date_2 , $differenceFormat = '%a' ) 
                {

                    $datetime1 = date_create($date_1);
                    $datetime2 = date_create($date_2);
                    $interval = date_diff($datetime1, $datetime2);
                    return $interval->format($differenceFormat);
                }

                function check_dates($date1, $date2)
                {
                    $chk_date1 = date('Y-m-d', strtotime($date1));
                    $chk_date2 = date('Y-m-d', strtotime($date2));

                    $date1 = strtotime($date1);
                    $date2 = strtotime($date2);

                    if($date1 && $date2)
                    {
                        echo secondsToTime($date2 - $date1);    
                    }
                    else
                    {
                        echo '';
                    }

                    
                }

                function secondsToTime($seconds) {
                    $dtF = new \DateTime('@0');
                    $dtT = new \DateTime("@$seconds");
                    return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
                }

                $filed_date         = '';
                $assessed_date      = '';
                $evaluated_date     = '';
                $recommended_date   = '';
                $approved_date      = '';
                $payment_date       = '';
                $issued_date        = '';
                $date_of_issuance   = '';
                $status             = 0;


                if(!empty($permit_record))
                {
                    $filed_date         = $permit_record->filed_date;
                    $assessed_date      = $permit_record->assessed_date;
                    $evaluated_date     = $permit_record->evaluated_date;
                    $recommended_date   = $permit_record->recommended_date;
                    $approved_date      = $permit_record->approved_date;
                    $payment_date       = $permit_record->payment_date;
                    $issued_date        = $permit_record->issued_date;
                    $date_of_issuance   = $permit_record->date_of_issuance;
                    $status             = $permit_record->status;    
                }
            @endphp
            @if(!empty($permit_record))
            <br><br>
            <div class="row tracking">
                <div class="col-md-12">
                    <ul style="font-size: 25pt;">

                        @if($status == 1 || $status == 2)
                        <li class="active">1</li>
                        @else
                        <li>1</li>
                        @endif

                        @if($status == 3)
                        <li class="active">2</li>
                        @else
                        <li>2</li>
                        @endif

                        @if($status == 4)
                        <li class="active">3</li>
                        @else
                        <li>3</li>
                        @endif

                        @if($status == 5)
                        <li class="active">4</li>
                        @else
                        <li>4</li>
                        @endif

                        @if($status == 6)
                        <li class="active">5</li>
                        @else
                        <li>5</li>
                        @endif

                        @if($status == 7)
                        <li class="active">6</li>
                        @else
                        <li>6</li>
                        @endif

                        @if($status == 8)
                        <li class="active">7</li>
                        @else
                        <li>7</li>
                        @endif

                        
                    </ul>  
                </div>
            </div>
            <br><br>
            <div class="card card-border-color card-border-color-default">
                <div class="card-header card-header-divider">
                    <div class="row">
                        <div class="col-md-6">
                            Track My Record
                        </div>
                        <div class="col-md-6">
                            Current Status: <u><b>{{ $status_arr[$status] }}</b></u>
                            <b></b>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="font-size: 10pt; font-weight: 400;">
                    <div class="row">
                        <div class="col-md-6">
                            Permit Type: <b><u>Building Permit</u></b>
                        </div>
                        <div class="col-md-6">
                            Filed Date: <b><u>{{ change_date_format($filed_date) }}</u></b>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-2">
                            Assessment Date:
                            <br>
                            <b><u>{{ change_date_format($assessed_date) }}</u></b>
                        </div>
                        <div class="col-md-2">
                            Evaluation Date:
                            <br>
                            <b><u>{{ change_date_format($evaluated_date) }}</u></b>
                            <br>
                            Processing Time
                            <br>
                            <b>
                                {{ check_dates($assessed_date, $evaluated_date) }}
                            </b>
                        </div>
                        <div class="col-md-2">
                            Recommendation Date:
                            <br>
                            <b>
                                <u>{{ change_date_format($recommended_date) }}</u>
                            </b>
                            <br>
                            Processing Time
                            <br>
                            <b>
                                {{ check_dates($evaluated_date, $recommended_date) }}
                            </b>
                        </div>
                        <div class="col-md-2">
                            Approval Date:
                            <br>
                            <b><u>{{ change_date_format($approved_date) }}</u></b>
                            <br>
                            Processing Time
                            <br>
                            <b>
                                {{ check_dates($recommended_date, $approved_date) }}
                            </b>


                            
                        </div>
                        <div class="col-md-2">
                            Payment Date:
                            <br>
                            <b><u>{{ change_date_format($payment_date) }}</u></b>

                            <br>
                            Processing Time
                            <br>
                            <b>
                                {{ check_dates($approved_date, $payment_date) }}
                            </b>
                        </div>
                        <div class="col-md-2">
                            Issuance Date:
                            <br>
                            <b><u>{{ change_date_format($date_of_issuance) }}</u></b>

                            <br>
                            Processing Time
                            <br>
                            <b>
                                {{ check_dates($payment_date, $date_of_issuance) }}
                            </b>
                        </div>

                    </div>
                    <hr>
                    @foreach($permit_remarks as $key => $value)
                    @if($value->remarks)
                    <div class="row">
                        <div class="col-md-12">
                            <b>{{ $remarks_arr[$value->status] }}:</b>
                            <br>
                            {{ $value->remarks }}
                        </div>
                    </div>
                    <hr>
                    @endif
                    @endforeach
                    <br><br><br>
                </div>
            </div>

            @else
            <h1>Welcome {{Auth::user()->name}}</h1>
            @endif
        </div>
    </div> 
    
@endsection


@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
    @yield('additional-scripts')
   
    <script type="text/javascript">
       $(document).ready(function () {
            App.init();
            App.dataTables(); 
            
       });
    </script>
@endsection