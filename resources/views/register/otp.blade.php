@extends('layouts.master-guest')

@section('modal')

<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/dist/html/assets/css/app.css') }}"/>

@endsection

@section('css')
  
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/sweetalert2/sweetalert2.min.css') }}"/>

<style type="text/css">
        .event-background{
            background: url("../img/bg-tiezadarker.png");
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
        }
        .card 
        {
            box-shadow: 0 0 10px 0 rgba(100, 100, 100, 0.75);
        }

    </style>
@endsection

@section('content')
@include('others.form_request', ['frm_method' => 'POST', 'frm_action' => url('/validate-otp'), 'frm_id' => 'save_form'])
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center text-dark h-25"><h2 class="mb-4">Online Permitting System<br>San Vicente Flagship Tourism Enterprise Zone</h2></div>
        </div>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card-border-color-primary">
                    <div class="card card-border-color card-border-color-primary">
                        <div class="card-header">
                            <span class="splash-title">Please enter the OTP sent to your email <b><u>{{ $email }}</u></b> .</span>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 text-center">
                                    <h5>One Time Pin</h5>
                                    <input type="text" class="form-control text-center" id="otp" name="otp" style="border: 1px solid black; letter-spacing: 10px;" maxlength="6">
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-12 text-center">
                                    <button type="button" class="btn btn-success btn-xl" id="verify">
                                        Verify OTP
                                    </button>
                                    <button type="button" class="btn btn-secondary btn-xl" id="resend">
                                        Back to Login
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection

@section('scripts')
@include('layouts.auth-partials.scripts')
@yield('scripts')
@yield('additional-scripts')
<script src="{{ asset('beagle-v1.7.1/dist/html/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>

   
    <script type="text/javascript">
        $(document).ready(function () {
            $("#verify").click(function () {
                /*if($("#otp").val() == '12345')
                {
                    success = "Email Verified";
                    const swal_success = alert_success(success, 2000);
                        swal_success.then((value) => {
                            clearInputs();
                            //var id = response.data;
                            window.location.href="{{ url('/') }}";
                        });
                }*/

                var frm, text, title, success,  formData, formAction;
                frm = document.querySelector('#save_form');

                formData = new FormData();

                formData.append("user_id",{{ $user_id }});
                formData.append("otp", $("#otp").val());

                axios.post(frm.action, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then((response) => {
                    if(response.data)
                    {
                        const swal_success = alert_success("Account Verified", 2000);
                        swal_success.then((value) => {
                            clearInputs();
                            window.location.href="{{ url('/') }}";
                        }); 
                    }
                    else
                    {
                        alert_warning("Invalid OTP");
                    }
                    

                })
                .catch((error) => {
                    const errors = error.response.data.errors;
                    if(typeof(errors) == 'string')
                    {
                        alert_warning(errors);
                    }
                    else
                    {
                        const firstItem = Object.keys(errors)[0];

                        const split_firstItem = firstItem.split('.');
                        const firstItemSplit = split_firstItem[0];

                        const firstItemDOM = document.getElementById(firstItemSplit);
                        const firstErrorMessage = errors[firstItem][0];

                        firstItemDOM.scrollIntoView();

                        alert_warning("{{ __('page.check_inputs') }}", 1500);

                        showErrors(firstItemSplit, firstErrorMessage, firstItemDOM, ['applicant_name', 'country_designation', 'airlines_name', 'type_applicant', 'assigned_processor', 'status', 'reason_denied', 'assigned_supervisor']);
                    }
                });
            });
        });
    </script>
@endsection