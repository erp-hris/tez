@extends('layouts.master-guest')

@section('modal')

<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/dist/html/assets/css/app.css') }}"/>

@endsection

@section('css')
  
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/sweetalert2/sweetalert2.min.css') }}"/>

<style type="text/css">
        .event-background{
            background: url("img/bg-tiezadarker.png");
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
        }
        .card 
        {
            box-shadow: 0 0 10px 0 rgba(100, 100, 100, 0.75);
        }

    </style>
@endsection

@section('content')
@include('others.form_request', ['frm_method' => 'POST', 'frm_action' => url('/store-registration'), 'frm_id' => 'save_form'])
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center text-dark h-25"><h2 class="mb-4">Online Permitting System<br>San Vicente Flagship Tourism Enterprise Zone</h2></div>
        </div>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card-border-color-primary">
                    <div class="card card-border-color card-border-color-primary">
                        <div class="card-header">
                            <span class="splash-title">Please enter your user information.</span>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>First Name</label>
                                    <input type="text" class="form-control form-control-xs" name="first_name" id="first_name" style="text-transform: uppercase;">
                                </div>
                                <div class="col-md-6">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control form-control-xs" name="last_name" id="last_name" style="text-transform: uppercase;">
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-6">
                                    <label>Middle Name</label>
                                    <input type="text" class="form-control form-control-xs" name="middle_name" id="middle_name" style="text-transform: uppercase;">
                                </div>
                                <div class="col-md-6">
                                    <label>Email</label>
                                    <input type="text" class="form-control form-control-xs" name="email" id="email">
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-6">
                                    <label>Username</label>
                                    <input type="text" class="form-control form-control-xs" name="username" id="username">
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control form-control-xs" name="password" id="password">
                                </div>
                                <div class="col-md-6">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control form-control-xs" name="password2" id="password2">
                                </div>
                            </div>
                            <hr>
                            <div class="row margin-top">
                                <div class="col-md-6 text-center">
                                    <button type="button" class="btn btn-secondary btn-xl" id="register">
                                        Register
                                    </button>
                                </div>
                                <div class="col-md-6 text-center">
                                    <a class="btn btn-primary btn-xl" href="{{ url('/login') }}">
                                        Already Have an Account?
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection

@section('scripts')
@include('layouts.auth-partials.scripts')
@yield('scripts')
@yield('additional-scripts')
<script src="{{ asset('beagle-v1.7.1/dist/html/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer></script>
   
        <script type="text/javascript">
            $("#register").click(function () {
                var first_name = $("#first_name").val();
                var last_name = $("#last_name").val();
                var middle_name = $("#middle_name").val();
                var username = $("#username").val();
                var password = $("#password").val();
                var password2 = $("#password2").val();
                var email = $("#email").val();

                if(first_name == '') $("#first_name").addClass('is-invalid');
                if(last_name == '') $("#last_name").addClass('is-invalid');
                if(email == '') $("#email").addClass('is-invalid');
                if(username == '') $("#username").addClass('is-invalid');
                if(password == '') $("#password").addClass('is-invalid');
                if(password2 == '') $("#password2").addClass('is-invalid');


                if(first_name && last_name && username && password && password2)
                {
                    $("#first_name").removeClass('is-invalid');
                    $("#last_name").removeClass('is-invalid');
                    $("#email").removeClass('is-invalid');
                    $("#username").removeClass('is-invalid');
                    $("#password").removeClass('is-invalid');
                    $("#password2").removeClass('is-invalid');

                    if(password != password2)
                    {
                        Swal.fire({
                        type: 'error',
                        title: '',
                        text: 'Password does not match.',
                        confirmButtonText: `OK`
                        });
                    }
                    else
                    {
                        const swal_continue = alert_continue("Submit Registration", "Are you sure you want to submit this?");
                        swal_continue.then((result) => {
                       
                            if(result.value)
                            { 
                                var frm, text, title, success,  formData, formAction;
                                frm = document.querySelector('#save_form');

                                formData = new FormData();

                                formData.append("email",email);
                                formData.append("password", password);
                                formData.append("username", username);
                                formData.append("first_name", first_name);
                                formData.append("last_name", last_name);
                                formData.append("middle_name", middle_name);

                                axios.post(frm.action, formData, {
                                    headers: {
                                        'Content-Type': 'multipart/form-data'
                                    }
                                })
                                .then((response) => {
                                    if(response.data.data == 'exist')
                                    {
                                        Swal.fire({
                                        type: 'error',
                                        title: '',
                                        text: 'Username already used.',
                                        confirmButtonText: `OK`
                                        });
                                        return false;
                                    }
                                    success = "{{ __('page.submit_successfully', ['attribute' => 'Registration']) }}";
                                    const swal_success = alert_success(success, 2000);
                                        swal_success.then((value) => {
                                            clearInputs();
                                            var id = response.data;
                                            window.location.href="{{ url('/otp') }}" + "/" + id;
                                            //window.location.href="{{ url('/login') }}";
                                        });

                                })
                                .catch((error) => {
                                    const errors = error.response.data.errors;
                                    if(typeof(errors) == 'string')
                                    {
                                        alert_warning(errors);
                                    }
                                    else
                                    {
                                        const firstItem = Object.keys(errors)[0];

                                        const split_firstItem = firstItem.split('.');
                                        const firstItemSplit = split_firstItem[0];

                                        const firstItemDOM = document.getElementById(firstItemSplit);
                                        const firstErrorMessage = errors[firstItem][0];

                                        firstItemDOM.scrollIntoView();

                                        alert_warning("{{ __('page.check_inputs') }}", 1500);

                                        showErrors(firstItemSplit, firstErrorMessage, firstItemDOM, ['applicant_name', 'country_designation', 'airlines_name', 'type_applicant', 'assigned_processor', 'status', 'reason_denied', 'assigned_supervisor']);
                                    }
                                });
                            }
                        });
                    }
                }
            });
        function submit()
        {
            var email = $('#email').val();
            var username = $('#username').val();
            var password = $('#cpassword').val();
            var confirm_password = $('#confirm_password').val();
            var agreement = $('#agreement').is(":checked");
            if(email == "") $( "#email" ).addClass( "is-invalid" );
            if(username == "") $( "#username" ).addClass( "is-invalid" );
            if(password == "") $( "#cpassword" ).addClass( "is-invalid" );
            if(confirm_password == "") $( "#confirm_password" ).addClass( "is-invalid" );
            if(!agreement) $( "#agreement" ).addClass( "is-invalid" );
         
            if(email && password && confirm_password && agreement) 
            {
                $('#email').removeClass("is-invalid");
                $('#cpassword').removeClass("is-invalid");
                $('#confirm_password').removeClass("is-invalid");
                $('#agreement').removeClass("is-invalid");
                $('#username').removeClass("is-invalid");
              
                if(password != confirm_password)
                {
                    Swal.fire({
                    type: 'error',
                    title: '',
                    text: 'Password does not match.',
                    confirmButtonText: `OK`
                    });
                }
                
                else {

                    const swal_continue = alert_continue("Submit Registration", "Are you sure you want to submit this?");
                    swal_continue.then((result) => {
                       
                        if(result.value)
                        { 
                            var frm, text, title, success,  formData, formAction;
                            formAction = "{{ url('/register') }}";
                            frm = document.querySelector('#save_form');

                            formData = new FormData();

                            formData.append("email",email);
                            formData.append("password", password);
                            formData.append("username", username);

                            axios.post(formAction, formData, {
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((response) => {
                                success = "{{ __('page.submit_successfully', ['attribute' => 'Registration']) }}";
                                const swal_success = alert_success(success, 2000);
                                    swal_success.then((value) => {
                                     clearInputs();
                                     grecaptcha.reset();
                                     $('#modal1 .close').click();
                                    });

                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;
                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                                else
                                {
                                    const firstItem = Object.keys(errors)[0];

                                    const split_firstItem = firstItem.split('.');
                                    const firstItemSplit = split_firstItem[0];

                                    const firstItemDOM = document.getElementById(firstItemSplit);
                                    const firstErrorMessage = errors[firstItem][0];

                                    firstItemDOM.scrollIntoView();

                                    alert_warning("{{ __('page.check_inputs') }}", 1500);

                                    showErrors(firstItemSplit, firstErrorMessage, firstItemDOM, ['applicant_name', 'country_designation', 'airlines_name', 'type_applicant', 'assigned_processor', 'status', 'reason_denied', 'assigned_supervisor']);
                                }
                            });
                        
                        }
                    });
                }
            }
           
        }

       
    </script>
@endsection