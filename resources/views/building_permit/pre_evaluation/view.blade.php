@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
    @include('layouts.auth-partials.form-css')
@endsection
<style type="text/css">
    .hr {
        margin-top: 20px;
        border: none;
        height: 3px;
        /* Set the hr color */
        color: black; /* old IE */
        background-color: black; /* Modern Browsers */
    }
</style>

@section('content')
    @php 
        $data = [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_footer' => 'yes',
        'cancel_url' => url($module.'/'.$option),
        ];
    @endphp
    @include('others.main_content', $data)

    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' => url($module.'/'.$option.'/upload'), 'frm_id' => 'add_form'])
@endsection

@section('scripts')
@include('layouts.auth-partials.form-scripts')
@include('layouts.auth-partials.datatables-scripts')

@yield('additional-scripts')
        
    <script type="text/javascript">
        $(document).ready(function(){
        });
    </script>
@endsection