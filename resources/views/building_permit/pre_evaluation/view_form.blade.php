
<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-header card-header-divider">
                Pre Evaluation Form
            </div>
            <div class="card-body">
                <div class="row margin-top">
                    <div class="col-md-3">
                        <h5>Type of Application for Building Permit</h5>
                        <select class="select2 select2-xs" id="application_type" name="application_type" disabled>
                            <option value="0">{{ __('page.please_select') }}</option>
                            <option value="1">New</option>
                            <option value="2">Renewal</option>
                            <option value="3">Amendatory</option>
                        </select>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-md-3">
                        <h5>
                            Last Name
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="last_name" name="last_name" disabled>
                    </div>
                    <div class="col-md-3">
                        <h5>
                            First Name
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="first_name" name="first_name" disabled>
                    </div>
                    <div class="col-md-3">
                        <h5>
                            M.I.
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="middle_name" name="middle_name" disabled>
                    </div>
                    <div class="col-md-3">
                        <h5>
                            TIN
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="tin" name="tin" disabled>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-md-4">
                        <h5>
                            Project Name
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="project_name" name="project_name" disabled>
                    </div>
                    <div class="col-md-4">
                        <h5>
                            Enterprise Name
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="enterprise_name" name="enterprise_name" disabled>
                    </div>
                    <div class="col-md-4">
                        <h5>
                            Form of Ownership
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="ownership_form" name="ownership_form" disabled>
                    </div>
                    
                </div>
                <div class="hr"></div>
                <div class="row margin-top">
                    <div class="col-md-3">
                        <h5>
                            ADDRESS
                        </h5>
                    </div>
                    <div class="col-md-3">
                        <h5>
                            House No.
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="house_number" name="house_number" disabled>
                    </div>
                    <div class="col-md-3">
                        <h5>
                            Street
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="street" name="street" disabled>
                    </div>
                    <div class="col-md-3">
                        <h5>
                            Barangay
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="barangay" name="barangay" disabled>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3">
                        <h5>
                            City / Municipality
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="city" name="city"disabled>
                    </div>
                    <div class="col-md-3">
                        <h5>
                            Zip Code
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="zip_code" name="zip_code" disabled>
                    </div>
                    <div class="col-md-3">
                        <h5>
                            Contact No.
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="contact_number" name="contact_number" disabled>
                    </div>
                </div>
                <div class="hr"></div>
                <br><br>
                <hr>
                <h3>TCT Informations</h3>
                <hr>
                <div id="tct_canvas">
                </div>
                <div id="extra_tct"></div>
                <div class="row margin-top">
                    <div class="col-md-12 text-right">
                        
                        <!-- <button type="button" class="btn btn-success" id="btn_add_tct">
                            <i class="mdi mdi-plus"></i>
                            Add Row
                        </button> -->
                        <!-- <button type="button" class="btn btn-danger" id="btn_remove_tct">
                            <i class="mdi mdi-plus"></i>
                            Remove Row
                        </button> -->
                        <input type="hidden" id="tct_count" name="" value="1">
                    </div>
                </div>
                
                @if(Auth::user()->viewer != 1)
                <div class="hr"></div>
                <div class="row margin-top">
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-space btn-secondary" id="view_pre_eval_reqs">
                            View Pre Evaluation Requirements
                        </button>
                    </div>
                </div>
                @php
                    $record = $array['record'];
                    $status = $record->status;
                    $phase_label = '';
                @endphp
                <div class="hr"></div>
                <div class="row">
                    <div class="col-md-12">
                        <h5>Assessor Remarks</h5>
                        <textarea class="form-control" rows="5" id="assessed_remarks" style="resize: none;"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h5>Initial Plan Evaluation Report</h5>
                        <input type="file" class="form-control form-control-xs" name="iper" id="iper">
                    </div>
                    <div class="col-md-4">
                        <h5>Transmittal Letter</h5>
                        <input type="file" class="form-control form-control-xs" name="transmittal_letter" id="transmittal_letter">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h5>Supplemental Initial Plan Evaluation Report</h5>
                        <input type="file" class="form-control form-control-xs" name="supplemental_iper" id="supplemental_iper">
                    </div>
                    <div class="col-md-4">
                        <h5>Supplemental Transmittal Letter</h5>
                        <input type="file" class="form-control form-control-xs" name="supplemental_transmittal_letter" id="supplemental_transmittal_letter">
                    </div>
                </div>
                <div class="hr"></div>
                <div class="row margin-top">
                    <div class="col-12 text-right">
                        @if($phase_label)
                        <button type="button" class="btn btn-space btn-primary" id="pass_pre_eval" onclick="pass_pre_eval('{{ $option }}',{{ $array['id'] }}, {{ $status }});">
                            <i class="mdi mdi-arrow-right"></i>
                            {{$phase_label}}
                        </button>
                        @endif
                        @if($status == 2 || $status == 3 || $status == 6)
                        <button type="button" class="btn btn-space btn-primary" id="notify_client_for_evaluation">
                            <i class="mdi mdi-notifications"></i>
                            Notify Client for Evaluation
                        </button>
                        <button type="button" class="btn btn-space btn-primary" id="pass_for_building_application">
                            <i class="mdi mdi-check"></i>
                            Passed for Building Permit Application
                        </button>
                        <button type="button" class="btn btn-space btn-danger" id="return_to_client">
                            <i class="mdi mdi-close"></i>
                            Return to Client (Incomplete Reqs)
                        </button>
                        <button type="button" class="btn btn-space btn-danger" id="return_to_client_supplemental">
                            <i class="mdi mdi-close"></i>
                            Return to Client (For Supplemental Docs)
                        </button>
                        @endif
                        <button type="button" class="btn btn-space btn-warning" onclick="window.location='{{url($module.'/'.$option)}}'">
                            <i class="mdi mdi-arrow-left"></i>
                            Back
                        </button>
                    </div>
                </div>
                @else
                <div class="hr"></div>
                <div class="row margin-top">
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-space btn-secondary" id="view_pre_eval_reqs">
                            View Pre Evaluation Requirements
                        </button>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="{{ url('/building_permit/pre_evaluation') }}" class="btn btn-space btn-danger"> 
                            BACK
                        </a>
                    </div>
                </div>
                @endif
            </div>
        </div>   
    </div>
</div>

@section('additional-scripts')

@endsection
<script type="text/javascript">
        $(document).ready(function () {
            App.formElements();
            get_application('{{ $option }}', {{ $array['id'] }});
            $("#view_pre_eval_reqs").click(function () {
                var href = 'view-pre-eval-reqs';
                window.open(href, '_blank');
            });
            $("#return_to_client").click(function () {
                var option = 'pre_evaluation';
                var status = 8;
                var id = {{ $array['id'] }};
                var frm = document.querySelector('#add_form');
                Swal.fire({
                    title: "Return to client this Pre Evaluation for Building Permit",
                    html: "Do you want to return this <b>Pre Evaluation Application for Building Permit</b>?",
                    confirmButtonText: 'Proceed',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-secondary',
                    closeButtonClass: 'btn btn-secondary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-danger'
                }).then((isSave) => {
                    if (isSave.value) {
                        swal.showLoading();
                        var formData = new FormData();

                        formData.append('status', status);
                        formData.append('assessed_remarks', $("#assessed_remarks").val());

                        axios.post("{{ url('user') }}/" + option + "/" + id  + "/approved-pre-evaluation", formData, {
                            headers: {
                              'Content-Type': 'multipart/form-data'
                            }
                        })
                        .then((response) => {
                            console.log(response);
                            var timerInterval = 0;
                            Swal.fire({
                                title: "The Application is processed!",
                                html: 'I will close in <strong></strong> seconds.',
                                timer: 1000,
                                customClass: 'content-actions-center',
                                buttonsStyling: true,
                                onOpen: function() {
                                    swal.showLoading();
                                    timerInterval = setInterval(function () {
                                        swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                    }, 100);
                                },
                                onClose: function() {
                                    clearInterval(timerInterval);
                                }
                            }).then(function (result) {
                                if ( result.dismiss === swal.DismissReason.timer ) {
                                    window.location.href="{{url($module.'/'.$option)}}";
                                }
                            });
                        })
                        .catch((error) => {
                            
                        });
                    }
                });
                return false;
            });

            $("#return_to_client_supplemental").click(function () {
                var option = 'pre_evaluation';
                var status = 9;
                var id = {{ $array['id'] }};
                var frm = document.querySelector('#add_form');
                Swal.fire({
                    title: "Transmit IPER and Transmittal Letter to Client?",
                    html: "Do you want to transmit this?",
                    confirmButtonText: 'Proceed',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-secondary',
                    closeButtonClass: 'btn btn-secondary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-danger'
                }).then((isSave) => {
                    if (isSave.value) {
                        swal.showLoading();
                        var formData = new FormData();

                        var iper = document.querySelector("#iper");
                        if(iper)
                        {
                            formData.append("iper", iper.files[0]);
                        }

                        var transmittal_letter = document.querySelector("#transmittal_letter");
                        if(transmittal_letter)
                        {
                            formData.append("transmittal_letter", transmittal_letter.files[0]);
                        }

                        /*var supplemental_iper = document.querySelector("#supplemental_iper");
                        if(supplemental_iper)
                        {
                            formData.append("supplemental_iper", supplemental_iper.files[0]);
                        }

                        var supplemental_transmittal_letter = document.querySelector("#supplemental_transmittal_letter");
                        if(supplemental_transmittal_letter)
                        {
                            formData.append("supplemental_transmittal_letter", supplemental_transmittal_letter.files[0]);
                        }*/

                        formData.append('status', status);
                        formData.append('assessed_remarks', $("#assessed_remarks").val());

                        axios.post("{{ url('user') }}/" + option + "/" + id  + "/approved-pre-evaluation", formData, {
                            headers: {
                              'Content-Type': 'multipart/form-data'
                            }
                        })
                        .then((response) => {
                            console.log(response);
                            var timerInterval = 0;
                            Swal.fire({
                                title: "Transmitted IPER and Transmittal Letter!",
                                html: 'I will close in <strong></strong> seconds.',
                                timer: 1000,
                                customClass: 'content-actions-center',
                                buttonsStyling: true,
                                onOpen: function() {
                                    swal.showLoading();
                                    timerInterval = setInterval(function () {
                                        swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                    }, 100);
                                },
                                onClose: function() {
                                    clearInterval(timerInterval);
                                }
                            }).then(function (result) {
                                if ( result.dismiss === swal.DismissReason.timer ) {
                                    window.location.href="{{url($module.'/'.$option)}}";
                                }
                            });
                        })
                        .catch((error) => {
                            
                        });
                    }
                });
                return false;
            });

            $("#pass_for_building_application").click(function () {
                var option = 'pre_evaluation';
                var status = 4;
                var id = {{ $array['id'] }};
                var frm = document.querySelector('#add_form');
                Swal.fire({
                    title: "Passed for Building Permit Application?",
                    html: "Do you want to pass this?",
                    confirmButtonText: 'Proceed',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-secondary',
                    closeButtonClass: 'btn btn-secondary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-danger'
                }).then((isSave) => {
                    if (isSave.value) {
                        swal.showLoading();
                        var formData = new FormData();

                        var supplemental_iper = document.querySelector("#supplemental_iper");
                        if(supplemental_iper)
                        {
                            formData.append("supplemental_iper", supplemental_iper.files[0]);
                        }

                        var supplemental_transmittal_letter = document.querySelector("#supplemental_transmittal_letter");
                        if(supplemental_transmittal_letter)
                        {
                            formData.append("supplemental_transmittal_letter", supplemental_transmittal_letter.files[0]);
                        }

                        formData.append('status', status);
                        formData.append('assessed_remarks', $("#assessed_remarks").val());

                        axios.post("{{ url('user') }}/" + option + "/" + id  + "/approved-pre-evaluation", formData, {
                            headers: {
                              'Content-Type': 'multipart/form-data'
                            }
                        })
                        .then((response) => {
                            console.log(response);
                            var timerInterval = 0;
                            Swal.fire({
                                title: "Transmitted IPER and Transmittal Letter!",
                                html: 'I will close in <strong></strong> seconds.',
                                timer: 1000,
                                customClass: 'content-actions-center',
                                buttonsStyling: true,
                                onOpen: function() {
                                    swal.showLoading();
                                    timerInterval = setInterval(function () {
                                        swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                    }, 100);
                                },
                                onClose: function() {
                                    clearInterval(timerInterval);
                                }
                            }).then(function (result) {
                                if ( result.dismiss === swal.DismissReason.timer ) {
                                    window.location.href="{{url($module.'/'.$option)}}";
                                }
                            });
                        })
                        .catch((error) => {
                            
                        });
                    }
                });
                return false;
            });

            $("#notify_client_for_evaluation").click(function () {
                var option = 'pre_evaluation';
                var status = 3;
                var id = {{ $array['id'] }};
                var frm = document.querySelector('#add_form');
                Swal.fire({
                    title: "Notify Client for Evaluation?",
                    html: "Do you want to pass this?",
                    confirmButtonText: 'Proceed',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-secondary',
                    closeButtonClass: 'btn btn-secondary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-danger'
                }).then((isSave) => {
                    if (isSave.value) {
                        swal.showLoading();
                        var formData = new FormData();

                        formData.append('status', status);
                        formData.append('assessed_remarks', $("#assessed_remarks").val());

                        axios.post("{{ url('user') }}/" + option + "/" + id  + "/approved-pre-evaluation", formData, {
                            headers: {
                              'Content-Type': 'multipart/form-data'
                            }
                        })
                        .then((response) => {
                            console.log(response);
                            var timerInterval = 0;
                            Swal.fire({
                                title: "Client Has Been Notified.",
                                html: 'I will close in <strong></strong> seconds.',
                                timer: 1000,
                                customClass: 'content-actions-center',
                                buttonsStyling: true,
                                onOpen: function() {
                                    swal.showLoading();
                                    timerInterval = setInterval(function () {
                                        swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                    }, 100);
                                },
                                onClose: function() {
                                    clearInterval(timerInterval);
                                }
                            }).then(function (result) {
                                if ( result.dismiss === swal.DismissReason.timer ) {
                                    window.location.href="{{url($module.'/'.$option)}}";
                                }
                            });
                        })
                        .catch((error) => {
                            
                        });
                    }
                });
                return false;
            });

            var tct_str = '';

            @foreach($array['tct_informations'] as $tct_key => $tct_val)
                tct_str += '<div class="hr"></div><div class="row margin-top"><div class="col-3"><h5>Land Ownership</h5><select class="select2 select2-xs" disabled><option value="">TITLED</option><option value="">ALIENABLE AND DISPOSABLE LAND</option><option value="">FOREST LAND</option></select></div><div class="col-md-3"><h5>Lot No.</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->lot_number}}" readonly></div><div class="col-md-3"><h5>Blk No.</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->block_no}}" readonly></div><div class="col-md-3"><h5>TCT No.</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->tct_no}}" readonly></div></div><div class="row margin-top"><div class="col-md-3"><h5>Tax Dec No.</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->tax_declaration_no}}" readonly></div><div class="col-md-3"><h5>Street</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->construction_street}}" readonly></div><div class="col-md-3"><h5>Barangay</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->construction_barangay}}" readonly></div><div class="col-md-3"><h5>City / Municipality</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->construction_city}}" readonly></div></div><div class="row margin-top"><div class="col-md-3"><h5>Land Title</h5><input type="file" class="form-control form-control-xs" name="" disabled></div><div class="col-md-3"><h5>DENR Certificate</h5><input type="file" class="form-control form-control-xs" name="" disabled></div><div class="col-md-3"><h5>Barangay Certificate</h5><input type="file" class="form-control form-control-xs" name="" disabled></div><div class="col-md-3"><h5>FLAgT</h5><input type="file" class="form-control form-control-xs" name="" disabled></div></div>';
            @endforeach
            $("#tct_canvas").html(tct_str);
        });
        function get_application(option, id)
        {
            $("#for_approval").hide();
            $("#for_receipt").hide();
            axios.get("{{ url('get-application') }}/" + option + "/" + id)
            .then(function(response){
                console.log(response);
                $("#last_name").val(response.data.data.pre_eval.last_name);
                $("#first_name").val(response.data.data.pre_eval.first_name);
                $("#middle_name").val(response.data.data.pre_eval.middle_name);
                $("#tin").val(response.data.data.pre_eval.tin);
                $("#project_name").val(response.data.data.pre_eval.project_name);
                $("#enterprise_name").val(response.data.data.pre_eval.enterprise_name);
                $("#ownership_form").val(response.data.data.pre_eval.ownership_form);
                $("#house_number").val(response.data.data.pre_eval.house_number);
                $("#street").val(response.data.data.pre_eval.street);
                $("#barangay").val(response.data.data.pre_eval.barangay);
                $("#city").val(response.data.data.pre_eval.city);
                $("#zip_code").val(response.data.data.pre_eval.zip_code);
                $("#contact_number").val(response.data.data.pre_eval.contact_number);
                $("#application_type").val(response.data.data.pre_eval.application_type).trigger('change');
                
            })
            .catch(function(error){
               
            }) 
        }
        function pass_pre_eval(option, id, status)
        {
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "Pass this Pre Evaluation for Building Permit",
                html: "Do you want to pass this <b>Pre Evaluation Application for Building Permit</b>?",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    swal.showLoading();
                    var formData = new FormData();

                    var transmittal_letter = document.querySelector("#transmittal_letter");
                    if(transmittal_letter)
                    {
                        formData.append("transmittal_letter", transmittal_letter.files[0]);
                    }

                    var iper = document.querySelector("#iper");
                    if(iper)
                    {
                        formData.append("iper", iper.files[0]);
                    }

                    var noncompliant_letter = document.querySelector("#noncompliant_letter");
                    if(noncompliant_letter)
                    {
                        formData.append("noncompliant_letter", noncompliant_letter.files[0]);
                    }

                    

                    formData.append('status', status);
                    formData.append('assessed_remarks', $("#assessed_remarks").val());

                    formData.append('confirmed_remarks', $("#confirmed_remarks").val());

                    formData.append('evaluation_remarks', $("#evaluation_remarks").val());

                    formData.append('accepted_remarks', $("#accepted_remarks").val());

                    

                    
                    


                    axios.post("{{ url('user') }}/" + option + "/" + id  + "/approved-pre-evaluation", formData, {
                        headers: {
                          'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application is processed!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{url($module.'/'.$option)}}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        }
        function approve_pre_eval(option, id)
        {
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "Assessment Complete",
                html: "Endorse to Evaluators?",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    swal.showLoading();
                    var formData = new FormData();

                    var transmittal_letter = document.querySelector("#transmittal_letter");
                    if(transmittal_letter)
                    {
                        formData.append("transmittal_letter", transmittal_letter.files[0]);
                    }

                    var iper = document.querySelector("#iper");
                    if(iper)
                    {
                        formData.append("iper", iper.files[0]);
                    }


                    axios.post("{{ url('user') }}/" + option + "/" + id  + "/approved-pre-evaluation", formData, {
                        headers: {
                          'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application is approved!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{url($module.'/'.$option)}}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        }
</script>