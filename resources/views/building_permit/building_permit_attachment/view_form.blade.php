<div class="row">
    <div class="col-lg-12">
        <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>Documentary Requirements for TIEZA BUILDING CLEARANCE</h4>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-8">
                Files
            </div>
            <div class="col-lg-4">
                New File
            </div>
        </div>
        @php
            $idx = $array['id'];
        @endphp
        @foreach($array['files'] as $key => $value)
        <div class="row margin-top">
            <div class="col-md-8">
                {{ ($key+1) }}. <b>{{ $value->name }}</b>
            </div>
            <div class="col-md-4">
                <input type="file" class="form-control form-control-xs" name="file_{{ $value->id }}" id="file_{{ $value->id }}" onblur="save_attachment('{{ $value->id }}', '{{ $idx }}');">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <table border="1" class="table table-striped table-hover table-fw-widget" id="table_{{ $value->id }}">
                    <thead>
                        <tr class="text-center">
                            <th style="width: 20%;">Action</th>
                            <th>File Name</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <hr>
        @endforeach
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();

        @foreach($array['files'] as $key => $value)
            load_datables(
            '#table_{{ $value->id }}',
            "{{ url('building_permit/'.$value->id.'/'.$array['id'].'/datatables') }}",
            {!!
                json_encode(
                    array(
                        ['data' => 'action', 'sortable' => false],
                        ['data' => 'original_name']
                    )
                )
            !!}, null);
            {{ usleep(75000) }}
        @endforeach
        $("[id*='save_']").each(function () {
            $(this).click(function () {
                var obj = $(this).attr('id').split("_");
                var idx = obj[1];
                var id  = obj[2];   
                var frm = document.querySelector('#add_form');
                Swal.fire({
                    title: "Upload Attachment",
                    text: "{{ __('page.add_this') }}",
                    confirmButtonText: 'Proceed',
                    confirmButtonClass: 'btn btn-primary',
                    closeButtonClass: 'btn btn-secondary',
                    cancelButtonClass: 'btn btn-secondary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-primary'
                }).then((isSave) => {
                    if (isSave.value) {
                        var formData = new FormData();
                        var file = document.querySelector('#file_' + idx);

                        formData.append("record_id", id);
                        formData.append('file_id', idx);
                        formData.append("file", file.files[0]);

                        axios.post(frm.action, formData, {
                            headers: {
                              'Content-Type': 'multipart/form-data'
                            }
                        })
                        .then((response) => {
                            var file_to_show = response.data.file_name;
                            var str = "<a href='{{ url('/tieza/building_permit') }}" + '/' + id + '/' + file_to_show + "' target='_blank'>"+ file_to_show +"</a>";   
                            $("#attachment_" + idx + '_' + id).html(str);
                            var timerInterval = 0;
                            Swal.fire({
                                title: "The Attachment has been added successfully!",
                                html: 'I will close in <strong></strong> seconds.',
                                timer: 1000,
                                customClass: 'content-actions-center',
                                buttonsStyling: true,
                                onOpen: function() {
                                    swal.showLoading();
                                    timerInterval = setInterval(function () {
                                        swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                    }, 100);
                                },
                                onClose: function() {
                                    clearInterval(timerInterval);
                                }
                            }).then(function (result) {
                                if ( result.dismiss === swal.DismissReason.timer ) {
                                    
                                }
                            });
                        })
                        .catch((error) => {
                            
                        });
                    }
                });
                return false;
            });
        });
    });

    function save_attachment(id, idx)
    {
        if($("#file_" + id).val())
        {
            var frm = document.querySelector('#add_form');

            var formData = new FormData();
            var file = document.querySelector('#file_' + id);

            formData.append("record_id", idx);
            formData.append('file_id', id);
            formData.append("file", file.files[0]);

            axios.post(frm.action, formData, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                Swal.fire({
                    title: "Uploaded",
                    html: 'I will close in <strong></strong> seconds.',
                    timer: 300,
                    customClass: 'content-actions-center',
                    buttonsStyling: true,
                    onOpen: function() {
                        swal.showLoading();
                        timerInterval = setInterval(function () {
                            swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                        }, 100);
                    },
                    onClose: function() {
                        clearInterval(timerInterval);
                        $("#file_" + id).val("");
                        load_datables(
                        '#table_' + id,
                        "{{ url('building_permit/')}}" + '/' + id + '/' + idx + '/datatables',
                        {!!
                            json_encode(
                                array(
                                    ['data' => 'action', 'sortable' => false],
                                    ['data' => 'original_name']
                                )
                            )
                        !!}, null);
                    }
                }).then(function (result) {
                    if ( result.dismiss === swal.DismissReason.timer ) {
                        
                    }
                });
            })
            .catch((error) => {
                
            });
            
        }
        
    }


    function delete_attachment(id, file_id)
    {
        Swal.fire({
            title: "Deleting Attachment",
            text: "{{ __('page.delete_this') }}",
            confirmButtonText: 'Yes',
            confirmButtonClass: 'btn btn-danger',
            closeButtonClass: 'btn btn-secondary',
            cancelButtonClass: 'btn btn-secondary',
            showCloseButton: true,
            showCancelButton: true,
            customClass: 'colored-header colored-header-danger'
        }).then((isSave) => {
            if (isSave.value) {
                axios.post("{{ url('building_permit/attachments') }}/" + id  + "/delete")
                    .then((response) => {
                    var timerInterval = 0;
                    Swal.fire({
                        title: "The Attachment has been deleted successfully!",
                        html: 'I will close in <strong></strong> seconds.',
                        timer: 1000,
                        customClass: 'content-actions-center',
                        buttonsStyling: true,
                        onOpen: function() {
                            swal.showLoading();
                            timerInterval = setInterval(function () {
                                swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                            }, 100);
                        },
                        onClose: function() {
                            clearInterval(timerInterval);
                            load_datables(
                            '#table_' + file_id,
                            "{{ url('building_permit/')}}" + '/' + file_id + '/' + {!!$idx!!} + '/datatables',
                            {!!
                                json_encode(
                                    array(
                                        ['data' => 'action', 'sortable' => false],
                                        ['data' => 'original_name']
                                    )
                                )
                            !!}, null);
                        }
                    }).then(function (result) {
                        if ( result.dismiss === swal.DismissReason.timer ) {
                            
                        }
                    });
                })
                .catch((error) => {
                    
                });
            }
        });
        return false;
    }

  
   
  </script>
@endsection