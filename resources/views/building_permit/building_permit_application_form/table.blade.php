                
                <table id="application_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                    <thead>
                        <tr class="text-center">
                            <th style="width: 10%;">{{ __('page.action') }}</th>
                            <th style="width: 15%;">Date Filed</th>
                            <th style="width: 15%;">Date Issued</th>
                            <th style="width: 25%;">Applicant Name</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
<div class="modal fade" id="fsec_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Uploading FSEC</h3>
                <button class="close" type="button" data-dismiss="modal" aria-hidden="true">
                    <span class="mdi mdi-close"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>Upload FSEC</h4>
                        <input type="file" class="form-control form-control-xs" name="fsec_file" id="fsec_file">
                        <input type="hidden" id="building_permit_id" name="building_permit_id" value="0">
                    </div>
                </div>
                <div class="mt-8"> 
                    <button class="btn btn-space btn-primary" type="button" id="btn-upload">Upload</button>
                    <button class="btn btn-space btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
@include('others.form_request', ['frm_method' => 'POST', 'frm_action' => url($module.'/'.$option.'/upload-fsec'), 'frm_id' => 'upload_form'])