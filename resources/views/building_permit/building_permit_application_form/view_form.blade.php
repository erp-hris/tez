
<div class="row">
    <div class="col-md-12">
        @include('building_permit.building_permit_application_form.add')
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-lg-12">
        <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>Documentary Requirements for TIEZA BUILDING CLEARANCE</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                Files
            </div>
            <div class="col-lg-4">
                Click to Download Files
            </div>
        </div>
        @foreach($array['attachments'] as $nkey => $nvalue)
        <div class="row margin-top">
            <div class="col-md-8">
                {{ ($nkey+1) }}. {{ $nvalue->file_name }}
            </div>
            <div class="col-md-4">
                <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$nvalue->attachment_name) }}" target="_blank" style="color: black;" download class="btn btn-success" download>
                    <i class="icon icon-left mdi mdi-download"></i>
                     Download
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$nvalue->attachment_name) }}" target="_blank" style="color: black;"  class="btn btn-success">
                    <i class="icon icon-left mdi mdi-eye"></i>
                     View File
                </a>
            </div>
        </div>
        @endforeach
    </div>
</div>
<br><br>
@if(Auth::user()->viewer != 1)
    @if($array['pre_eval']->status == 7 && Auth::user()->level == 1)
    <div class="row" id="receipt_canvas">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
                <div class="col-md-12">
                    <h4>Payment Form</h4>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-6">
                    <button type="button" class="btn btn-success" style="color: black;" id="btn_proceed">
                        GO TO PAYMENT
                    </button>
                    
                    <input type="hidden" value="9" id="status" name="status">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control form-control-xs" id="payment_receipt" name="payment_receipt" value="receipt.png">
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-primary" style="color: black;" id="btn_proceed">
                        PAYMENT
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif


    @if($array['pre_eval']->status == 2)
    <div class="row">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
                <div class="col-md-12">
                    <h4>Assessor Remarks</h4>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12">
                    <textarea class="form-control" rows="5" id="remarks" style="resize: none;"></textarea>
                    <input type="hidden" value="3" id="status" name="status">

                </div>
            </div>

            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-danger btn-return" style="color: black;" id="return_to_client">
                        RETURN TO CLIENT
                    </button>
                    <button type="button" class="btn btn-primary" style="color: white;" id="btn_proceed">
                        <!-- PROCEED TO ADMINISTRATOR -->
                        APPROVE ASSESSMENT
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($array['pre_eval']->status == 3)
    <div class="row">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
                <div class="col-md-12">
                    <h4>Assessor Remarks</h4>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12">
                    <textarea class="form-control" rows="3" id="remarks" style="resize: none;"></textarea>
                    <input type="hidden" value="4" id="status" name="status">

                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-4">
                    <h5>Final Plan Evaluation Report</h5>
                    <input type="file" name="fer" id="fer" class="form-control form-control-xs">
                </div>
                <div class="col-md-4">
                    <h5>Transmittal Letter</h5>
                    <input type="file" name="transmittal_letter" id="transmittal_letter" class="form-control form-control-xs">
                </div>
                
            </div>
            <div class="row margin-top">
                <div class="col-md-4">
                    <h5>Supplemental Final Plan Evaluation Report</h5>
                    <input type="file" name="supplemental_fer" id="supplemental_fer" class="form-control form-control-xs">
                </div>
                <div class="col-md-4">
                    <h5>Transmittal Letter (Supplemental)</h5>
                    <input type="file" name="supplemental_transmittal_letter" id="supplemental_transmittal_letter" class="form-control form-control-xs">
                </div>
                
                <div class="col-md-4">
                    <h5>Order Payment</h5>
                    <input type="file" name="order_payment" id="order_payment" class="form-control form-control-xs">
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-4">
                    <h5>Endorsement to BFP</h5>
                    <input type="file" name="endorsement_to_bfp" id="endorsement_to_bfp" class="form-control form-control-xs">
                </div>
            </div>

            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-danger" style="color: black;" id="return_for_supp_reqs">
                        RETURN TO CLIENT (For Supplemental Requirements)
                    </button>
                    <button type="button" class="btn btn-primary" style="color: white;" id="notify_fsec">
                        <!-- PROCEED TO EVALUATOR -->
                        NOTIFY CLIENT FOR FSEC
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($array['pre_eval']->status == 4)
    <div class="row">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
                <div class="col-md-12">
                    <h4>Assessor Remarks</h4>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12">
                    <textarea class="form-control" rows="5" id="remarks" style="resize: none;"></textarea>
                    <input type="hidden" value="5" id="status" name="status">
                </div>
            </div>
            
            <hr>
            <div class="row margin-top">
                <div class="col-md-4">
                    <h5>FSEC</h5>
                    <input type="file" name="fsec_file" id="fsec_file" class="form-control form-control-xs">
                </div>
                <div class="col-md-4">
                    <h5>FSEC No.</h5>
                    <input type="text" name="fsec_no" id="fsec_no" class="form-control form-control-xs">
                </div>
                <div class="col-md-4">
                    <h5>Endorsement Letter to OBO</h5>
                    <input type="file" name="obo_endorsement_letter" id="obo_endorsement_letter" class="form-control form-control-xs">
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    <!-- <button type="button" class="btn btn-danger btn-return" style="color: black;">
                        RETURN TO CLIENT
                    </button> -->
                    <!-- <button type="button" class="btn btn-warning btn-notify" style="color: black;">
                        NOTIFY CLIENT FOR FSEC
                    </button> -->
                    <button type="button" class="btn btn-primary" style="color: black;" id="btn_proceed">
                        ENDORSED TO OBO
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($array['pre_eval']->status == 5)
    <div class="row">
        <div class="col-lg-12">
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <strong>Final Evaluation Report</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->final_eval_report) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->final_eval_report) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Supplemental Final Evaluation Report</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->supplemental_final_eval_report) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->supplemental_final_eval_report) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Transmittal Letter</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->transmittal_letter) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->transmittal_letter) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Transmittal Letter (Supplemental)</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->supplemental_transmittal_letter) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->supplemental_transmittal_letter) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-3">
                    <strong>Endorsement Letter to BFP</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->endorsement_to_bfp) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->endorsement_to_bfp) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Order of Payment</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->order_payment) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->order_payment) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Fire Safety Evaluation Clearance (FSEC)</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->fsec) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->fsec) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Endorsement to OBO</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->obo_endorsement_letter) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->obo_endorsement_letter) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
            </div>
            <div class="row margin-top" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
                <div class="col-md-12">
                    <h4>Reviewer's Remark/s</h4>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12">
                    <textarea class="form-control" rows="3" id="remarks" style="resize: none;"></textarea>
                    <input type="hidden" value="6" id="status" name="status">
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-4">
                    <h5>Authority To Accept Payment (ATAP):</h5>
                    <input type="file" name="atap" id="atap" class="form-control form-control-xs">
                </div>
                <div class="col-md-4">
                    <h5>Total Amount:</h5>
                    <input type="text" name="amount" id="amount" class="form-control form-control-xs">
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    <!-- <button type="button" class="btn btn-danger btn-return" style="color: black;">
                        RETURN TO CLIENT
                    </button> -->
                    <button type="button" class="btn btn-primary" style="color: black;" id="btn_proceed_to_approval">
                        PROCEED TO APPROVAL
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($array['pre_eval']->status == 6)
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-3">
                    <strong>Final Evaluation Report</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->final_eval_report) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->final_eval_report) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Supplemental Final Evaluation Report</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->supplemental_final_eval_report) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->supplemental_final_eval_report) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Transmittal Letter</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->transmittal_letter) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->transmittal_letter) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Transmittal Letter (Supplemental)</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->supplemental_transmittal_letter) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->supplemental_transmittal_letter) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-3">
                    <strong>Endorsement Letter to BFP</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->endorsement_to_bfp) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->endorsement_to_bfp) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Order of Payment</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->order_payment) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->order_payment) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Fire Safety Evaluation Clearance (FSEC)</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->fsec) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->fsec) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-3">
                    <strong>Endorsement to OBO</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->obo_endorsement_letter) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->obo_endorsement_letter) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-3">
                    <strong>Authority To Accept Payment</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->atap) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->atap) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-6">
                    <h4>
                    <strong>Application Amount</strong>:
                    <b><u>P {{ number_format($array['pre_eval']->amount, 2) }}</u></b>
                    </h4>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12">
                    <h4>Building Official's Remark/s</h4>
                    <textarea class="form-control" rows="3" id="remarks" style="resize: none;"></textarea>
                    <input type="hidden" value="7" id="status" name="status">
                </div>
            </div>
            
            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    
                    <button type="button" class="btn btn-primary" style="color: black;" id="btn_proceed">
                        NOTIFY CLIENT FOR PAYMENT
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($array['pre_eval']->status == 7)
    <!-- <div class="row">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
                <div class="col-md-12">
                    <h4>Approver Remarks</h4>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12">
                    <textarea class="form-control" rows="5" id="remarks" style="resize: none;"></textarea>
                    <input type="hidden" value="8" id="status" name="status">
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    
                    <button type="button" class="btn btn-primary" style="color: black;" id="btn_proceed">
                        NOTIFY CLIENT FOR PAYMENT
                    </button>
                </div>
            </div>
        </div>
    </div> -->
    @endif



    @if($array['pre_eval']->status == 7)
    <div class="row" id="for_approval">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
                <div class="col-md-12">
                    <h4>Payment Receipt</h4>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-4">
                    <h5>Payment Receipt</h5>
                    <input type="file" name="payment_receipt" id="payment_receipt" class="form-control form-control-xs">
                </div>
                <div class="col-md-4">
                    <h5>OR #:</h5>
                    <input type="text" name="or_number" id="or_number" class="form-control form-control-xs">
                </div>
                <div class="col-md-4">
                    <h5>OR Date:</h5>
                    <input type="date" name="or_date" id="or_date" class="form-control form-control-xs">
                </div>
            </div>

            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-primary" style="color: black;" id="upload_payment_details">
                        <span style="color: white !important;"><i class="mdi mdi-upload"></i>&nbsp;&nbsp;&nbsp;UPLOAD PAYMENT DETAILS</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($array['pre_eval']->status == 8)
    <div class="row">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
                <div class="col-md-12">
                    <h4>Payment Receipt</h4>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-4">
                    <strong>Payment Receipt</strong>
                    <br>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->payment_receipt) }}" target="_blank" style="color: black;" class="btn btn-success">
                        <i class="icon icon-left mdi mdi-eye"></i>
                        View File
                    </a>
                    <a href="{{ url('tieza/building_permit/'.$array['id'].'/'.$array['pre_eval']->payment_receipt) }}" target="_blank" style="color: black;" class="btn btn-success" download>
                        <i class="icon icon-left mdi mdi-download"></i>
                        Download
                    </a>
                </div>
                <div class="col-md-4">
                    <h4>
                    <strong>OR #</strong>:
                    <b><u>{{$array['pre_eval']->or_number}}</u></b>
                    </h4>
                </div>
                <div class="col-md-4">
                    <h4>
                    <strong>OR Date</strong>:
                    <b><u>{{ date('F d, Y', strtotime($array['pre_eval']->or_date)) }}</u></b>
                    </h4>
                </div>
            </div>
            <input type="hidden" value="9" id="status" name="status">
            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-primary" style="color: black;" id="btn_proceed">
                        NOTIFY SVFTEZ OFFICE FOR ISSUANCE OF BUILDING PERMIT
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($array['pre_eval']->status == 9)
    <div class="row" id="for_approval">
        <div class="col-lg-12">
            
            <div class="row margin-top">
                <div class="col-md-12">
                    <textarea class="form-control" rows="5" id="remarks" style="resize: none;"></textarea>
                    <input type="hidden" value="10" id="status" name="status">
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-primary" style="color: black;" id="btn_proceed">
                        NOTIFY CLIENT FOR ISSUANCE OF BLDG PERMIT
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif
@endif


<script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        $(".form-control, .select2").prop('disabled', true);
        $("#amount").prop('disabled', false);
        $('input[type=checkbox]').attr('disabled','true');
        $("#remarks, #payment_receipt, #transmittal_letter, #fer, #order_payment, #fsec_no, #fsec_file, #obo_endorsement_letter, #supplemental_fer, #supplemental_transmittal_letter, #endorsement_to_bfp, #or_number, #or_date, #atap").prop('disabled', false);

        @php
        if(isset($array['id']))
        {
            echo 'get_application("building_permit", '.$array['id'].');';
        }
        @endphp

        $('#btn_proceed').click(function()
        {
            var status = $("#status").val();
            var swal_title = '';
            var swal_text = '';
            var result_title = '';
            if(status == 3)
            {
                swal_title = "Proceed to Administrator?";
                swal_text = "Do you want to pass to Administrator?";
                result_title = "The Application has been pass to Administrator successfully!";
            }
            else if (status == '4')
            {
                swal_title = "Notify Client to Request of FSEC?";
                swal_text = "Do you want to notify client for FSEC request?";
                result_title = "Successfully notified client!";
            }
            else if (status == '5')
            {
                swal_title = "Proceed to Approver?";
                swal_text = "Do you want to pass to Approver?";
                result_title = "The Application has been endorsed to OBO successfully!";
            }
            else if (status == '6')
            {
                
                swal_title = "Proceed to Approval?";
                swal_text = "Do you want to pass to Approver?";
                result_title = "The Application has been pass to Approver successfully!";
            }
            else if (status == '7')
            {
                swal_title = "Notify Client for Payment?";
                swal_text = "Do you want to Notify Client for Payment?";
                result_title = "successfully notify client!";
            }
            else if (status == '8')
            {
                swal_title = "Notify Client for Paymentss?";
                swal_text = "Do you want to Notify Client for Payment?";
                result_title = "successfully notify client!";
            }
            else if (status == '9')
            {
                swal_title = "Notify SVFTEZ Office";
                swal_text = "Notify SVFTEZ Office for Issuance of Building Permit?";
                result_title = "Successfully notify SVFTEZ Office!";
                //window.open('{{ url('/create_payment/'.$array['id']) }}', '_blank');
                //window.open('https://stg-sanvic-payment.tieza.gov.ph/public/' + {{ $array['id'] }} + '/make-payment');
            }
            else if (status == '10')
            {
                
                swal_title = "Issue permit?";
                swal_text = "Do you want to issue building permit?";
                result_title = "successfully notify client!";
            }
            else if (status == '11')
            {
                
                swal_title = "notify client for issuance of permit?";
                swal_text = "notify client for issuance of permit?";
                result_title = "successfully notify client!";
            }

            
            //return false;
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: swal_title,
                text: swal_text,
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post(frm.action, {
                        remarks: $("#remarks").val(),
                        status: status,
                        id: {{ $array['id'] }},
                        payment_receipt: $("#payment_receipt").val(),
                        fsec_no: $("#fsec_no").val(),
                        amount: $("#amount").val(),
                        
                    })
                    .then((response) => {
                        var timerInterval = 0;
                        Swal.fire({
                            title: result_title,
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                if(status == 4)
                                {
                                    swal.showLoading();
                                    //window.location.href="{{ url($module.'/'.$option) }}";
                                    var frm = document.querySelector('#upload_form');
                                    var formData = new FormData();
                                    var transmittal_letter = document.querySelector('#transmittal_letter');
                                    var final_eval_report = document.querySelector('#final_eval_report');
                                    var order_payment = document.querySelector('#order_payment');

                                    formData.append("transmittal_letter", transmittal_letter.files[0]);
                                    formData.append("final_eval_report", final_eval_report.files[0]);
                                    formData.append("order_payment", order_payment.files[0]);
                                    formData.append("id", {{ $array['id'] }});

                                    axios.post(frm.action, formData, {
                                        headers: {
                                          'Content-Type': 'multipart/form-data'
                                        }
                                    })
                                    .then((response) => {
                                        Swal.fire({
                                            title: "Uploaded",
                                            html: 'I will close in <strong></strong> seconds.',
                                            timer: 1000,
                                            customClass: 'content-actions-center',
                                            buttonsStyling: true,
                                            onOpen: function() {
                                                swal.showLoading();
                                                timerInterval = setInterval(function () {
                                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                                }, 100);
                                            },
                                            onClose: function() {
                                                clearInterval(timerInterval);
                                                location.reload();
                                            }
                                        }).then(function (result) {
                                            if ( result.dismiss === swal.DismissReason.timer ) {
                                                window.location.href="{{ url($module.'/'.$option) }}";
                                            }
                                        });
                                    })
                                    .catch((error) => {
                                        
                                    });
                                }
                                else if (status == 5)
                                {
                                    swal.showLoading();
                                    var frm = document.querySelector('#upload_fsec');

                                    var formData = new FormData();
                                    var fsec_file = document.querySelector('#fsec_file');

                                    formData.append("fsec_file", fsec_file.files[0]);

                                    var obo_endorsement_letter = document.querySelector('#obo_endorsement_letter');

                                    formData.append("obo_endorsement_letter", obo_endorsement_letter.files[0]);
                                    
                                    formData.append("building_permit_id", {{ $array['id'] }});

                                    axios.post(frm.action, formData, {
                                        headers: {
                                          'Content-Type': 'multipart/form-data'
                                        }
                                    })
                                    .then((response) => {
                                        Swal.fire({
                                            title: "Uploaded",
                                            html: 'I will close in <strong></strong> seconds.',
                                            timer: 1000,
                                            customClass: 'content-actions-center',
                                            buttonsStyling: true,
                                            onOpen: function() {
                                                swal.showLoading();
                                                timerInterval = setInterval(function () {
                                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                                }, 100);
                                            },
                                            onClose: function() {
                                                clearInterval(timerInterval);
                                                location.reload();
                                            }
                                        }).then(function (result) {
                                            if ( result.dismiss === swal.DismissReason.timer ) {
                                               window.location.href="{{ url($module.'/'.$option) }}";
                                            }
                                        });
                                    })
                                    .catch((error) => {
                                        
                                    });
                                }
                                else
                                {
                                    window.location.href="{{ url($module.'/'.$option) }}";
                                }
                                
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
        $(".btn-notify").click(function () { 
            var frm = document.querySelector('#upload_form');

            Swal.fire({
                title: "Notify Client for FSEC?",
                text: "Do you want to notify client for FSEC?",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    swal.showLoading();
                    Swal.fire({
                        title: 'Saving...',
                        customClass: 'content-actions-center',
                        buttonsStyling: true,
                        allowOutsideClick: false,
                        onOpen: function() {
                            swal.showLoading();
                            //$(this).prop('disabled', true);

                            var formData = new FormData();
                            var transmittal_letter = document.querySelector('#transmittal_letter');
                            var final_eval_report = document.querySelector('#final_eval_report');
                            var order_payment = document.querySelector('#order_payment');

                            formData.append("transmittal_letter", transmittal_letter.files[0]);
                            formData.append("final_eval_report", final_eval_report.files[0]);
                            formData.append("order_payment", order_payment.files[0]);
                            formData.append("id", {{ $array['id'] }});

                            axios.post(frm.action, formData, {
                                headers: {
                                  'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((response) => {
                                Swal.fire({
                                    title: "Uploaded",
                                    html: 'I will close in <strong></strong> seconds.',
                                    timer: 1000,
                                    customClass: 'content-actions-center',
                                    buttonsStyling: true,
                                    onOpen: function() {
                                        swal.showLoading();
                                        timerInterval = setInterval(function () {
                                            swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                        }, 100);
                                    },
                                    onClose: function() {
                                        clearInterval(timerInterval);
                                        location.reload();
                                    }
                                }).then(function (result) {
                                    if ( result.dismiss === swal.DismissReason.timer ) {
                                        
                                    }
                                });
                            })
                            .catch((error) => {
                                
                            });
                        }
                    });
                    
                }
            });
            return false;
        });
        $(".btn-return").click(function () {
            var status = $("#status").val();
            var swal_title = '';
            var swal_text = '';
            var result_title = '';
            swal_title = "Return Application of Building Permit?";
            swal_text = "Do you want to return this application?";
            result_title = "The Application has been returned!";
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: swal_title,
                text: swal_text,
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    swal.showLoading();
                    axios.post('{{ url($module.'/'.$option.'/return-application') }}', {
                        remarks: $("#remarks").val(),
                        status: 8,
                        id: {{ $array['id'] }},
                    })
                    .then((response) => {
                        var timerInterval = 0;
                        Swal.fire({
                            title: result_title,
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{ url($module.'/'.$option) }}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
        $('#btn_issue').click(function()
        {
            var status = $("#status").val();
            var swal_title = '';
            var swal_text = '';
            var result_title = '';
            swal_title = "Issue Building Permit?";
            swal_text = "Do you want to issue building permit?";
            result_title = "The Building has been issued successfully!";
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: swal_title,
                text: swal_text,
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    swal.showLoading();
                    axios.post(frm.action, {
                        remarks: $("#remarks").val(),
                        status: 8,
                        id: {{ $array['id'] }},
                    })
                    .then((response) => {
                        var timerInterval = 0;
                        Swal.fire({
                            title: result_title,
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{ url($module.'/'.$option) }}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
        $("#return_to_client").click(function () {
            var option = 'building_permit_application';
            var status = 8;
            var id = {{ $array['id'] }};
            var frm = document.querySelector('#upload_form');
            Swal.fire({
                title: "Return to Client",
                html: "Do you want to return this to client?",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-danger'
            }).then((isSave) => {
                if (isSave.value) {

                    swal.showLoading();
                    var formData = new FormData();

                    formData.append('status', status);
                    formData.append('remarks', $("#remarks").val());

                    axios.post("{{ url('building_permit') }}/" + option + "/" + id  + "/return-to-client", formData, {
                        headers: {
                          'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application is returned to Client!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{url($module.'/'.$option)}}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
        $("#return_for_supp_reqs").click(function () {
            var option = 'building_permit_application';
            var status = 8;
            var id = {{ $array['id'] }};
            var frm = document.querySelector('#upload_form');
            Swal.fire({
                title: "Return to Client",
                html: "Do you want to return this to client?",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-danger'
            }).then((isSave) => {
                if (isSave.value) {
                    swal.showLoading();
                    var formData = new FormData();

                    formData.append('status', status);

                    var fer = document.querySelector('#fer');

                    formData.append("fer", fer.files[0]);

                    var transmittal_letter = document.querySelector('#transmittal_letter');
                    formData.append("transmittal_letter", transmittal_letter.files[0]);


                    axios.post("{{ url('building_permit') }}/" + option + "/" + id  + "/upload-evaluation-attachments", formData, {
                        headers: {
                          'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application is processed!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{url($module.'/'.$option)}}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });

        $("#notify_fsec").click(function () {
            var option = 'building_permit_application';
            var status = 8;
            var id = {{ $array['id'] }};
            var frm = document.querySelector('#upload_form');
            Swal.fire({
                title: "Upload Attachments",
                html: "Do you want to upload this attachment for <b>Building Permit Application</b>?",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-danger'
            }).then((isSave) => {
                if (isSave.value) {
                    swal.showLoading();
                    var formData = new FormData();

                    formData.append('status', status);

                    var supplemental_fer = document.querySelector('#supplemental_fer');

                    formData.append("supplemental_fer", supplemental_fer.files[0]);

                    var supplemental_transmittal_letter = document.querySelector('#supplemental_transmittal_letter');
                    formData.append("supplemental_transmittal_letter", supplemental_transmittal_letter.files[0]);

                    var order_payment = document.querySelector('#order_payment');
                    formData.append("order_payment", order_payment.files[0]);

                    var endorsement_to_bfp = document.querySelector('#endorsement_to_bfp');
                    formData.append("endorsement_to_bfp", endorsement_to_bfp.files[0]);

                    var fer = document.querySelector('#fer');
                    formData.append("fer", fer.files[0]);
                    
                    var transmittal_letter = document.querySelector('#transmittal_letter');
                    formData.append("transmittal_letter", transmittal_letter.files[0]);

                    formData.append("id", id);


                    axios.post(frm.action, formData, {
                        headers: {
                          'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application is processed!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{url($module.'/'.$option)}}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });

        $("#btn_proceed_to_approval").click(function () {
            var option = 'building_permit_application';
            var status = 7;
            var id = {{ $array['id'] }};
            var frm = document.querySelector('#upload_form');
            Swal.fire({
                title: "Upload ATAP",
                html: "Do you want to upload this attachment for <b>Building Permit Application</b>?",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-danger'
            }).then((isSave) => {
                if (isSave.value) {
                    swal.showLoading();
                    var formData = new FormData();

                    formData.append('status', status);

                    var atap = document.querySelector('#atap');

                    formData.append("atap", atap.files[0]);

                    formData.append("amount", $("#amount").val());


                    axios.post("{{ url('building_permit') }}/" + option + "/" + id  + "/upload-atap", formData, {
                        headers: {
                          'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application is processed!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{url($module.'/'.$option)}}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
        $("#upload_payment_details").click(function () {
            var option = 'building_permit_application';
            var status = 8;
            var id = {{ $array['id'] }};
            var frm = document.querySelector('#upload_form');
            Swal.fire({
                title: "Upload Payment Details",
                html: "Do you want to upload this attachment for <b>Building Permit Application</b>?",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-danger'
            }).then((isSave) => {
                if (isSave.value) {
                    swal.showLoading();
                    var formData = new FormData();

                    formData.append('status', status);

                    var payment_receipt = document.querySelector('#payment_receipt');

                    formData.append("payment_receipt", payment_receipt.files[0]);

                    formData.append("or_number", $("#or_number").val());
                    formData.append("or_date", $("#or_date").val());


                    axios.post("{{ url('building_permit') }}/" + option + "/" + id  + "/upload-payment-details", formData, {
                        headers: {
                          'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application is processed!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{url($module.'/'.$option)}}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });

    });

    function get_application(option, id)
    {

        $("#for_approval").hide();
        $("#for_receipt").hide();
        axios.get("{{ url('get-application') }}/" + option + "/" + id)
        .then(function(response){
            console.log(response);
            
            @foreach($fields as $key => $value)
            $("#{{$value}}").val(response.data.data.{{$value}});
            
            
            @endforeach

            

            if(response.data.data.status == 6)
            {
                $("#receipt_canvas").show();
            }
            else
            {
                $("#receipt_canvas").hide();
            }
            if(response.data.data.status == '5')
            {
                $("#for_approval").show();
            }
            if(response.data.data.status == '7')
            {
                if(response.data.data.payment_receipt)
                {
                    $("#for_receipt").show();    
                }
            }
        })
        .catch(function(error){
           
        }) 
    }

  
   
  </script>
        
@section('additional-scripts')

@endsection