<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
    <link rel="stylesheet" href="{{ asset('beagle-assets/css/app.css') }}" type="text/css"/>
    <style type="text/css">
        .img-border
        {
            height: 150px; 
            border: 1px solid black;
        }
        body
        {
            background: white;
            font-family: cambria;
        }
        .hdr1
        {
            font-size: 14pt;
        }
        .hdr2
        {
            font-size: 18pt;
            font-family: Old English Text MT;
        }
        .hdr3
        {
            font-size: 14pt;
        }
        td 
        {
            font-size: 9pt;
            padding: 2px;
            padding-left: 5px;
        }
        .watermarked {
            background-image: url("{{ asset('img/tieza-logo-watermark.png') }}") !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;

            
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="col-md-12">
            @php
                function create_checkbox($label, $id)
                {
                    return '<input type="checkbox" name="" id="'.$id.'" disabled>&nbsp;&nbsp;&nbsp;'.$label;
                }


                $data->lot_number = '';
                $data->block_no = '';
                $data->tct_no = '';
                $data->tax_declaration_no = '';
                $data->construction_street = '';
                $data->construction_barangay = '';
                $data->construction_city = '';

                $architectural_permit = $data->architectural_permit;

            @endphp
            @include('building_permit/building_permit_application_form/form/building_permit_form')
            @include('building_permit/building_permit_application_form/form/architectural_permit_form')
            @include('building_permit/building_permit_application_form/form/structural_permit_form')
            @include('building_permit/building_permit_application_form/form/electrical_permit_form')
            @include('building_permit/building_permit_application_form/form/mechanical_permit_form')
            @include('building_permit/building_permit_application_form/form/sanitary_permit_form')
            
            @include('building_permit/building_permit_application_form/form/plumbing_permit_form')    
            @include('building_permit/building_permit_application_form/form/electronic_permit_form')
        </div>
    </div>
    @include('layouts.auth-partials.scripts')
    @yield('scripts')
</body>
<script type="text/javascript">
    $(document).ready(function () {
        @php
            if($data->application_type == 1)
            {
                echo '$("#new").prop("checked", true);';
            }
            else if($data->application_type == 2)
            {
                echo '$("#renewal").prop("checked", true);';
            }
            else if($data->application_type == 3)
            {
                echo '$("#amendatory").prop("checked", true);';
            }

            if($data->work_scope)
            {
                echo '$("#work_scope_" + '.$data->work_scope.').prop("checked", true);';
                echo '$("#2_work_scope_" + '.$data->work_scope.').prop("checked", true);';
            }
            if($data->occupancy_character)
            {
                echo '$("#occupancy_character_" + '.$data->occupancy_character.').prop("checked", true);';
            }

            if($data->architectural_permit)
            {
                $architectural_permit = $data->architectural_permit;

                $architectural_facilities = explode('|', $architectural_permit->architectural_facilities);

                foreach($architectural_facilities as $val)
                {
                    echo '$("#2_architectural_facilities_'.$val.'").prop("checked", true);';
                }


                $conformance = explode('|', $architectural_permit->conformance);

                foreach($conformance as $val)
                {
                    echo '$("#2_conformance_'.$val.'").prop("checked", true);';
                }
            }
        @endphp
    });
</script>
</html>