@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.form-css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    @php 
        $data = [
        'option' => $module, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $module.'.'.$option.'.table',
        
        ];
        /*
        'add_url' => url($module.'/'.$option.'/create'),
        */
    @endphp

    @include('others.main_content', $data)
@endsection

@section('scripts')
    @include('layouts.auth-partials.form-scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
            App.dataTables();
            load_datables('#application_tbl', '{{$json_url}}', {!! json_encode($columns) !!});

            $("#btn-upload").click(function () { 
                var frm = document.querySelector('#upload_form');

                Swal.fire({
                    title: "Upload FSEC?",
                    text: "Do you want to upload this FSEC?",
                    confirmButtonText: 'Proceed',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-secondary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-primary'
                }).then((isSave) => {
                    if (isSave.value) {
                        Swal.fire({
                            title: 'Saving...',
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            allowOutsideClick: false,
                            onOpen: function() {
                                swal.showLoading();
                                var formData = new FormData();
                                var fsec_file = document.querySelector('#fsec_file');

                                formData.append("fsec_file", fsec_file.files[0]);
                                
                                formData.append("building_permit_id", $("#building_permit_id").val());

                                axios.post(frm.action, formData, {
                                    headers: {
                                      'Content-Type': 'multipart/form-data'
                                    }
                                })
                                .then((response) => {
                                    Swal.fire({
                                        title: "Uploaded",
                                        html: 'I will close in <strong></strong> seconds.',
                                        timer: 1000,
                                        customClass: 'content-actions-center',
                                        buttonsStyling: true,
                                        onOpen: function() {
                                            swal.showLoading();
                                            timerInterval = setInterval(function () {
                                                swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                            }, 100);
                                        },
                                        onClose: function() {
                                            clearInterval(timerInterval);
                                            location.reload();
                                        }
                                    }).then(function (result) {
                                        if ( result.dismiss === swal.DismissReason.timer ) {
                                            
                                        }
                                    });
                                })
                                .catch((error) => {
                                    
                                });
                            }
                        });
                        
                    }
                });
                return false;
            });
        });
        function attach_fsec(module, id)
        {
            $("#fsec_modal").modal();
            $("#building_permit_id").val(0);
            $("#building_permit_id").val(id);
        }
        function print_form(permit, id)
        {
            var href = window.location + '/application-form/' + id + '/view-form';

            window.open(href, '_blank');
        }
        function to_assessor(permit, id)
        {   
            Swal.fire({
                title: "Pass to Assessor",
                html: "Do you want to send this to <b>Assessor</b>?<br>After Passing this to Assessor the current application will <b><u>automatically lock</u></b>, you cannot change the application once locked.",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post("{{ url('tez') }}/" + permit + "/" + id  + "/to-assess")
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                load_datables('#application_tbl', '{{$json_url}}', {!! json_encode($columns) !!});
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        }
        function show_building_permit(id)
        {
            var href = '{{ url("/report/") }}' + '/view-permit/' + id;

            window.open(href, '_blank');
        }
    </script>
@endsection
