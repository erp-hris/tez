@php
    $first_column = array(
        '1' => 'water closet',
        '2' => 'floor drain',
        '3' => 'lavatory',
        '4' => 'kitchen sink',
        '5' => 'faucet',
        '6' => 'shower head',
        '7' => 'water meter',
        '8' => 'grease trap',
        '9' => 'bath tub',
        '10' => 'slop sink',
        '11' => 'urinal',
        '12' => 'air conditioning unit',
        '13' => 'water tank/reservoir',
    );

    $second_column = array(
        '14' => 'bidette',
        '15' => 'laundry trays',
        '16' => 'dental cuspidor',
        '17' => 'drinking fountain',
        '18' => 'bar sink',
        '19' => 'soda fountain sink',
        '20' => 'laboratory sink',
        '21' => 'sterilizer',
    );
@endphp
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white; padding-top: 10px;">
            <div class="col-md-12">
                <h4>APPLICATION FOR PLUMBING PERMIT</h4>
            </div>
        </div>
        
        <div class="row margin-top">
            <div class="col-md-12">
                <table width="100%">
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-md-12">
                                    <b>
                                        FIXTURE TO BE INSTALLED
                                    </b>
                                </div>
                                
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-3 text-center">
                                            QTY
                                        </div>
                                        <div class="col-md-2">
                                            NEW<br>FIXTURE
                                        </div>
                                        <div class="col-md-2">
                                            EXISTING<br>FIXTURE
                                        </div>
                                        <div class="col-md-5">
                                            KIND OF<br>FIXTURE
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-3 text-center">
                                            QTY
                                        </div>
                                        <div class="col-md-2">
                                            NEW<br>FIXTURE
                                        </div>
                                        <div class="col-md-2">
                                            EXISTING<br>FIXTURE
                                        </div>
                                        <div class="col-md-5">
                                            KIND OF<br>FIXTURE
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-6">
                                    @foreach($first_column as $fkey => $fval)
                                    <div class="row margin-top first_column">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" id="plumbing_qty_{{$fkey}}" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1"
                                            id="plumbing_exist_{{$fkey}}">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0"
                                            id="plumbing_not_exist_{{$fkey}}">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="7_plumbing_item[]" id="plumbing_item_{{$fkey}}">&nbsp;&nbsp;&nbsp;
                                            {{ strtoupper($fval) }}
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                    
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="" id="first_column_total" value="0" readonly>
                                        </div>
                                        <div class="col-md-2">
                                            TOTAL
                                        </div>
                                        <div class="col-md-7"></div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    @foreach($second_column as $skey => $sval)
                                    <div class="row margin-top second_column">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" id="plumbing_qty_{{$skey}}" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1"
                                            id="plumbing_exist_{{$skey}}">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0"
                                            id="plumbing_not_exist_{{$skey}}">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="7_plumbing_item[]" id="plumbing_item_{{$skey}}">&nbsp;&nbsp;&nbsp;
                                            {{ strtoupper($sval) }}
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            OTHERS (Specify )
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control form-control-xs">
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control form-control-xs">
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control form-control-xs">
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            TOTAL
                                        </div>
                                        <div class="col-md-7"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-top">
                                
                                <div class="col-md-3">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;<b>WATER DISTRIBUTION SYSTEM</b>
                                </div>
                                <div class="col-md-3">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;<b>SEWAGE SYSTEM</b>
                                </div>
                                <div class="col-md-3">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;<b>SEPTIC TANK</b>
                                </div>
                                <div class="col-md-3">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;<b>STORM DRAINAGE SYSTEM</b>
                                </div>
                            </div>                            
                        </td>
                    </tr>
                    
                </table>
            </div>
        </div>
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                    DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Master Plumber
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer" name="plumbing_engineer">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_address" name="plumbing_engineer_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_prc_no" name="plumbing_engineer_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="plumbing_engineer_prc_validity" name="plumbing_engineer_prc_validity">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_ptr_no" name="plumbing_engineer_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="plumbing_engineer_ptr_issued_date" name="plumbing_engineer_ptr_issued_date">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_ptr_issued_at" name="plumbing_engineer_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_tin" name="plumbing_engineer_tin">
            </div>
        </div>
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                    SUPERVISOR / IN-CHARGE OF PLUMBING WORKS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Master Plumber
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor" name="plumbing_supervisor">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_address" name="plumbing_supervisor_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_prc_no" name="plumbing_supervisor_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="plumbing_supervisor_prc_validity" name="plumbing_supervisor_prc_validity">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_ptr_no" name="plumbing_supervisor_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="plumbing_supervisor_ptr_issued_date" name="plumbing_supervisor_ptr_issued_date">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_ptr_issued_at" name="plumbing_supervisor_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_tin" name="plumbing_supervisor_tin">
            </div>
        </div>
        @if(Auth::user()->level == 1)
        <div class="row margin-top">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-primary" id="btn_plumbing_permit">
                    <i class="icon mdi mdi-save"></i>&nbsp;&nbsp;
                    SAVE<!--  BUILDING PERMIT -->
                </button>
                <input type="hidden" name="7_id" id="7_id" value="0">
            </div>
            
        </div>
        @endif
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
         var plumbingQtyValues = [];
        $('.first_column input[name="7_plumbing_qty[]"]').each(function() {
            plumbingQtyValues.push($(this).val());
        });

        // Do something with the collected values
        console.log(plumbingQtyValues);
        $('#btn_plumbing_permit').click(function()
        {
            if(!$("#action_value").val())
            {
                if(!$("#building_permit_id").val())
                {
                    alert_warning('Please Create Building Application First');
                    return false;
                }
            }
        });
    });
</script>
