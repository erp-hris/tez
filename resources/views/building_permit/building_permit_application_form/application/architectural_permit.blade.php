
<div class="row">
	<div class="col-md-12">
		<div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white; padding-top: 10px;">
            <div class="col-md-12">
                <h4>APPLICATION FOR ARCHITECTURAL PERMIT</h4>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    <b>1. ARCHITECTURAL FACILITIES AND OTHER FEATURES PURSUANT TO BATAS PAMBANSA BILANG 344, REQUIRING CERTAIN BUILDINGS, INSTITUTIONS, ESTABLISHMENTS AND PUBLIC UTILITIES TO INSTALL FACILITIES AND OTHER DEVICES.</b>
                </h5>
            </div>
        </div>
        <div class="row">
	        <div class="col-md-3">
	            <input type="checkbox" name="2_architectural_facilities[]" value="1">&nbsp;&nbsp;
	            STAIRS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="2">&nbsp;&nbsp;
	            WALKWAYS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="3">&nbsp;&nbsp;
	            CORRIDORS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="4">&nbsp;&nbsp;
	            DOORS, ENTRANCES & THRESHOLDS
	            
	        </div>
	        <div class="col-md-3">
	            <input type="checkbox" name="2_architectural_facilities[]" value="5">&nbsp;&nbsp;
	            WASH ROOMS AND TOILETS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="6">&nbsp;&nbsp;
	            LIFTS/ELEVATORS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="7">&nbsp;&nbsp;
	            RAMPS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="8">&nbsp;&nbsp;
	            PARKING AREAS
	        </div>
	        <div class="col-md-3">
	            <input type="checkbox" name="2_architectural_facilities[]" value="9">&nbsp;&nbsp;
	            SWITCHES, CONTROLS & BUZZERS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="10">&nbsp;&nbsp;
	            HANDRAILS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="11">&nbsp;&nbsp;
	            THRESHOLDS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="12">&nbsp;&nbsp;
	            FLOOR FINISHES
	        </div>
	        <div class="col-md-3">
	            <input type="checkbox" name="2_architectural_facilities[]" value="13">&nbsp;&nbsp;
	            DRINKING FOUNTAINS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="14">&nbsp;&nbsp;
	            PUBLIC TELEPHONES
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="15">&nbsp;&nbsp;
	            SEATING ACCOMMODATIONS
	            <br>
	            <input type="checkbox" name="2_architectural_facilities[]" value="16" id="other_architectural_facilities_chk" onchange="enable_other_textbox('other_architectural_facilities_chk', '2_other_architectural_facilities')">&nbsp;&nbsp;
	            OTHERS (Specify)
                <br>
                <input type="text" class="form-control form-control-xs" name="2_other_architectural_facilities" id="2_other_architectural_facilities" disabled>
	        </div>
	    </div>
	    <div class="hr"></div>
	    <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                   	<b>2. PERCENTAGE OF SITE OCCUPANCY</b>
                </h5>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-4">
        		<h5>
        			PERCENTAGE OF BUILDING FOOTPRINT
        		</h5>
        		<input type="text" class="form-control form-control-xs" name="building_percentage" id="2_building_percentage">


        	</div>
        	<div class="col-md-4">
        		<h5>
        			PERCENTAGE OF IMPERVIOUS SURFACE AREA
        		</h5>
        		<input type="text" class="form-control form-control-xs" name="impervious_surface_area" id="2_impervious_surface_area">
        	</div>
        	<div class="col-md-4">
        		<h5>
        			PERCENTAGE OF UNPAVED SURFACE AREA
        		</h5>
        		<input type="text" class="form-control form-control-xs" name="unpaved_surface_area" id="2_unpaved_surface_area">
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-6">
        		<h5>
        			OTHERS (Specify)
        		</h5>
        		<input type="text" class="form-control form-control-xs" name="others_sit_occupancy" id="2_others_sit_occupancy">
        	</div>
        </div>
        <div class="hr"></div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                   	3. CONFORMANCE TO FIRE CODE OF THE PHILIPPINES (P.D. 1185)
                </h5>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <input type="checkbox" name="2_conformance[]" value="1">&nbsp;&nbsp;
                NUMBER AND WIDTH OF EXIT DOORS
                <br>
                <input type="checkbox" name="2_conformance[]" value="2">&nbsp;&nbsp;
                WIDTH OF CORRIDOR
                <br>
                <input type="checkbox" name="2_conformance[]" value="3">&nbsp;&nbsp;
                DISTANCE TO FIRE EXITS
                <br>
                <input type="checkbox" name="2_conformance[]" value="4">&nbsp;&nbsp;
                ACCESS TO PUBLIC STREET
            </div>
            <div class="col-md-6">
                <input type="checkbox" name="2_conformance[]" value="5">&nbsp;&nbsp;
                FIRE WALLS
                <br>
                <input type="checkbox" name="2_conformance[]" value="6">&nbsp;&nbsp;
                FIRE FIGHTING AND SAFETY FACILITIES
                <br>
                <input type="checkbox" name="2_conformance[]" value="7">&nbsp;&nbsp;
                SMOKE DETECTORS
                <br>
                <input type="checkbox" name="2_conformance[]" value="8">&nbsp;&nbsp;
                EMERGENCY LIGHTS
                <br>
                <input type="checkbox" name="2_conformance[]" id="2_conformance_chk" value="9" onchange="enable_other_textbox('2_conformance_chk', '2_other_conformance')">&nbsp;&nbsp;
                OTHERS
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6"></div>
        	<div class="col-md-6">
        		<input type="text" class="form-control form-control-xs" name="other_conformance" id="2_other_conformance" disabled>
        	</div>
        </div>

        <div class="hr"></div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                </h5>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h5>
                    Architect / Civil Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_civil_engineer" name="civil_engineer">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_civil_engineer_address" name="civil_engineer_address">
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_civil_engineer_prc_no" name="civil_engineer_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="2_civil_engineer_prc_validity" name="civil_engineer_prc_validity" onchange="check_date_validity('2_civil_engineer_prc_validity');">
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    IAPOA No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_civil_engineer_iapoa_no" name="2_civil_engineer_iapoa_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="2_civil_engineer_iapoa_no_date_issued" name="2_civil_engineer_iapoa_no_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_civil_engineer_iapoa_no_issued_at" name="2_civil_engineer_iapoa_no_issued_at">
            </div>
            
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_civil_engineer_ptr_no" name="civil_engineer_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="2_civil_engineer_ptr_date_issued" name="civil_engineer_ptr_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_civil_engineer_ptr_issued_at" name="civil_engineer_ptr_issued_at">
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_civil_engineer_tin" name="civil_engineer_tin">
            </div>
        </div>

        <div class="hr"></div>
        <div class="row margin-top">
            <div class="col-md-12">
                <button type="button" class="btn btn-space btn-success" id="architectural_sab">
                    Same as Above
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	SUPERVISOR / IN-CHARGE OF ARCHITECTURAL WORKS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Architect / Civil Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_architectural_supervisor" name="architectural_supervisor">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_architectural_supervisor_address" name="architectural_supervisor_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_architectural_supervisor_prc_no" name="architectural_supervisor_prc_no">
            </div>

            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="2_architectural_supervisor_prc_validity" name="architectural_supervisor_prc_validity" onchange="check_date_validity('2_architectural_supervisor_prc_validity');">
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    IAPOA No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_architectural_supervisor_iapoa_no" name="2_architectural_supervisor_iapoa_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="2_architectural_supervisor_date_issued" name="2_architectural_supervisor_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_architectural_supervisor_issued_at" name="2_architectural_supervisor_issued_at">
            </div>
            
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_architectural_supervisor_ptr_no" name="architectural_supervisor_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="2_architectural_supervisor_ptr_date_issued" name="architectural_supervisor_ptr_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_architectural_supervisor_ptr_issued_at" name="architectural_supervisor_ptr_issued_at">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="2_architectural_supervisor_tin" name="architectural_supervisor_tin">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-primary" id="btn_architectural_permit">
                    <i class="icon mdi mdi-save"></i>&nbsp;&nbsp;
                    SAVE<!--  ARCHITECTURAL PERMIT -->
                </button>
                <input type="hidden" name="2_id" id="2_id" value="0">
            </div>
            
        </div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn_architectural_permit').click(function()
        {
            if(!$("#action_value").val())
            {
                if(!$("#building_permit_id").val())
                {
                    alert_warning('Please Create Building Application First');
                    return false;
                }
            }
            var architectural_facilities = [];
            var conformance = [];

            $("[name='2_architectural_facilities[]']").each(function () {
                if($(this).prop('checked'))
                {
                    architectural_facilities.push($(this).val());
                }
            });

            $("[name='2_conformance[]']").each(function () {
                if($(this).prop('checked'))
                {
                    conformance.push($(this).val());
                }
            });

            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "Application for Architectural Permit",
                text: "{{ __('page.submit_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post("{{ url('architectural_permit/building_permit_application_form/store') }}", {

                        building_permit_id : $("#building_permit_id").val(),
                        id : $("#2_id").val(),

                        architectural_facilities : architectural_facilities,
                        other_architectural_facilities : $("#2_other_architectural_facilities").val(),

                        building_percentage : $("#2_building_percentage").val(),

                        impervious_surface_area : $("#2_impervious_surface_area").val(),

                        unpaved_surface_area : $("#2_unpaved_surface_area").val(),

                        others_sit_occupancy : $("#2_others_sit_occupancy").val(),

                        conformance : conformance,

                        other_conformance : $("#2_other_conformance").val(),

                        civil_engineer : $("#2_civil_engineer").val(),

                        civil_engineer_address : $("#2_civil_engineer_address").val(),

                        civil_engineer_prc_no : $("#2_civil_engineer_prc_no").val(),

                        civil_engineer_prc_validity : $("#2_civil_engineer_prc_validity").val(),

                        civil_engineer_ptr_no : $("#2_civil_engineer_ptr_no").val(),

                        civil_engineer_ptr_date_issued : $("#2_civil_engineer_ptr_date_issued").val(),

                        civil_engineer_ptr_issued_at : $("#2_civil_engineer_ptr_issued_at").val(),

                        civil_engineer_tin : $("#2_civil_engineer_tin").val(),

                        architectural_supervisor : $("#2_architectural_supervisor").val(),

                        architectural_supervisor_address : $("#2_architectural_supervisor_address").val(),

                        architectural_supervisor_prc_no : $("#2_architectural_supervisor_prc_no").val(),

                        architectural_supervisor_prc_validity : $("#2_architectural_supervisor_prc_validity").val(),

                        architectural_supervisor_ptr_no : $("#2_architectural_supervisor_ptr_no").val(),

                        architectural_supervisor_ptr_date_issued : $("#2_architectural_supervisor_ptr_date_issued").val(),

                        architectural_supervisor_ptr_issued_at : $("#2_architectural_supervisor_ptr_issued_at").val(),

                        architectural_supervisor_tin : $("#2_architectural_supervisor_tin").val(),

                    })
                    .then((response) => {
                        
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                $("#architectural_permit_form").slideUp(500);
                                alert_success('Application for Architectural Permit saved successfully');                        
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });
</script>