<div class="row">
	<div class="col-md-12">
		<div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white; padding-top: 10px;">
            <div class="col-md-12">
                <h4>APPLICATION FOR CIVIL/STRUCTURAL PERMIT</h4>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    <b>NATURE OF CIVIL/STRUCTURAL WORKS:</b>
                </h5>
            </div>
        </div>
        <div class="row">
	        <div class="col-md-4">
                <input type="checkbox" name="3_structural_works[]" value="1">&nbsp;&nbsp;
                STAKING
                <br>
                <input type="checkbox" name="3_structural_works[]" value="2">&nbsp;&nbsp;
                EXCAVATION
                <br>
                <input type="checkbox" name="3_structural_works[]" value="3">&nbsp;&nbsp;
                SOIL STABILIZATION
                <br>
                <input type="checkbox" name="3_structural_works[]" value="4">&nbsp;&nbsp;
                PILING WORK
                <br>
                <input type="checkbox" name="3_structural_works[]" value="5">&nbsp;&nbsp;
                FOUNDATION
                
            </div>
            <div class="col-md-4">
                <input type="checkbox" name="3_structural_works[]" value="6">&nbsp;&nbsp;
                ERECTION/LIFTING
                <br>
                <input type="checkbox" name="3_structural_works[]" value="7">&nbsp;&nbsp;
                CONCRETE FRAMING
                <br>
                <input type="checkbox" name="3_structural_works[]" value="8">&nbsp;&nbsp;
                STRUCTURAL STEEL FRAMING
                <br>
                <input type="checkbox" name="3_structural_works[]" value="9">&nbsp;&nbsp;
                SLABS
                <br>
                <input type="checkbox" name="3_structural_works[]" value="10">&nbsp;&nbsp;
                WALLS
                
            </div>
            <div class="col-md-4">
                <input type="checkbox" name="3_structural_works[]" value="11">&nbsp;&nbsp;
                PRESTRESS WORKS
                <br>
                <input type="checkbox" name="3_structural_works[]" value="12">&nbsp;&nbsp;
                MATERIAL TESTING
                <br>
                <input type="checkbox" name="3_structural_works[]" value="13">&nbsp;&nbsp;
                STEEL TOWERS
                <br>
                <input type="checkbox" name="3_structural_works[]" value="14">&nbsp;&nbsp;
                TANKS
                <br>
                <input type="checkbox" name="3_structural_works[]" value="16" id="other_structural_works_chk" onchange="enable_other_textbox('other_structural_works_chk', '3_other_structural_works')">&nbsp;&nbsp;
                OTHERS (Specify)
                <br>
                <input type="text" class="form-control form-control-xs" name="3_other_structural_works" id="3_other_structural_works" disabled>
            </div>
	    </div>
        <div class="hr"></div>
        
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Civil/Structural Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_engineer_name" name="3_civil_engineer_name">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_engineer_address" name="3_civil_engineer_address">
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_engineer_prc_no" name="3_civil_engineer_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="3_civil_engineer_prc_validity" name="3_civil_engineer_prc_validity" onchange="check_date_validity('3_civil_engineer_prc_validity');">
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_engineer_ptr_no" name="3_civil_engineer_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="3_civil_engineer_ptr_date_issued" name="3_civil_engineer_ptr_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_engineer_ptr_issued_at" name="3_civil_engineer_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_engineer_tin" name="3_civil_engineer_tin">
            </div>
        </div>

        <div class="hr"></div>
        <div class="row margin-top">
            <div class="col-md-12">
                <button type="button" class="btn btn-space btn-success" id="structural_sab">
                    Same as Above
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	SUPERVISOR / IN-CHARGE OF CIVIL/STRUCTURAL WORKS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Civil/Structural Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_supervisor" name="3_civil_supervisor">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_supervisor_address" name="3_civil_supervisor_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_supervisor_prc_no" name="3_civil_supervisor_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="3_civil_supervisor_prc_validity" name="3_civil_supervisor_prc_validity" onchange="check_date_validity('3_civil_supervisor_prc_validity');">
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_supervisor_ptr_no" name="3_civil_supervisor_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="3_civil_supervisor_ptr_date_issued" name="3_civil_supervisor_ptr_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_supervisor_ptr_issued_at" name="3_civil_supervisor_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="3_civil_supervisor_tin" name="3_civil_supervisor_tin">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-primary" id="btn_structural_permit">
                    <i class="icon mdi mdi-save"></i>&nbsp;&nbsp;
                    SAVE<!--  CIVIL/STRUCTURAL PERMIT -->
                </button>
                <input type="hidden" name="3_id" id="3_id" value="0">
            </div>
            
        </div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn_structural_permit').click(function()
        {
            if(!$("#action_value").val())
            {
                if(!$("#building_permit_id").val())
                {
                    alert_warning('Please Create Building Application First');
                    return false;
                }
            }
            
            var structural_works = [];
            var conformance = [];

            $("[name='3_structural_works[]']").each(function () {
                if($(this).prop('checked'))
                {
                    structural_works.push($(this).val());
                }
            });

            

            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "Application for Civil/Structural Permit",
                text: "{{ __('page.submit_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post("{{ url('structural_permit/building_permit_application_form/store') }}", {

                        building_permit_id : $("#building_permit_id").val(),
                        edit_building_permit_id : $("#edit_building_permit_id").val(),
                        id : $("#3_id").val(),

                        structural_works : structural_works,
                        
                        
                        civil_engineer_name : $("#3_civil_engineer_name").val(),
                        
                        civil_engineer_address : $("#3_civil_engineer_address").val(),
                        
                        civil_engineer_prc_no : $("#3_civil_engineer_prc_no").val(),
                        
                        civil_engineer_prc_validity : $("#3_civil_engineer_prc_validity").val(),
                        
                        civil_engineer_ptr_no : $("#3_civil_engineer_ptr_no").val(),
                        
                        civil_engineer_ptr_date_issued : $("#3_civil_engineer_ptr_date_issued").val(),
                        
                        civil_engineer_ptr_issued_at : $("#3_civil_engineer_ptr_issued_at").val(),
                        
                        civil_engineer_tin : $("#3_civil_engineer_tin").val(),
                        
                        civil_supervisor : $("#3_civil_supervisor").val(),
                        
                        civil_supervisor_address : $("#3_civil_supervisor_address").val(),
                        
                        civil_supervisor_prc_no : $("#3_civil_supervisor_prc_no").val(),
                        
                        civil_supervisor_prc_validity : $("#3_civil_supervisor_prc_validity").val(),
                        
                        civil_supervisor_ptr_no : $("#3_civil_supervisor_ptr_no").val(),
                        
                        civil_supervisor_ptr_date_issed : $("#3_civil_supervisor_ptr_date_issued").val(),
                        
                        civil_supervisor_ptr_issued_at : $("#3_civil_supervisor_ptr_issued_at").val(),
                        
                        civil_supervisor_tin : $("#3_civil_supervisor_tin").val(),
                        

                    })
                    .then((response) => {
                        
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                $("#structural_permit_form").slideUp(500);
                                alert_success('Application for Civil/Structural Permit saved successfully');                        
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });
</script>