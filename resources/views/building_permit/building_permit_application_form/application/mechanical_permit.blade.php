
<div class="row">
	<div class="col-md-12">
		<div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white; padding-top: 10px;">
            <div class="col-md-12">
                <h4>APPLICATION FOR MECHANICAL PERMIT</h4>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <input type="checkbox" name="6_mechanical_application[]" value="1">&nbsp;
                BOILER
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="2">&nbsp;
                PRESSURE VESSEL
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="3">&nbsp;
                INTERNAL COMBUSTION ENGINE
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="4">&nbsp;
                REFRIGERATION AND ICE MAKING
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="5">&nbsp;
                WINDOW TYPE AIRCONDITIONING
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="6">&nbsp;
                PACKED SPLIT TYPE AIR-CONDITIONING
                <br>
                <input type="checkbox" name="6_mechanical_application[]" id="6_mechanical_application_chk" value="7" onchange="enable_other_textbox('6_mechanical_application_chk', '6_other_mechanical_application')">&nbsp;
                OTHERS (Specify) ___________
                <br>
                <input type="text" class="form-control form-control-xs" name="6_other_mechanical_application" id="6_other_mechanical_application" disabled>
            </div>
            <div class="col-md-4">
                <input type="checkbox" name="6_mechanical_application[]" value="8">&nbsp;
                CENTRAL AIRCONDITIONING
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="9">&nbsp;
                MECHANICAL VENTILLATION
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="10">&nbsp;
                ESCALATOR
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="11">&nbsp;
                MOVING SIDEWALK
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="12">&nbsp;
                FREIGHT ELEVATOR
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="13">&nbsp;
                PASSENGER ELEVATOR
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="14">&nbsp;
                CABLE CAR
            </div>
            <div class="col-md-4">
                <input type="checkbox" name="6_mechanical_application[]" value="15">&nbsp;
                DUMBWAITER
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="16">&nbsp;
                PUMPS
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="17">&nbsp;
                COMPRESSED AIR, VACUUM, and/or INDUSTRIAL GAS
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="18">&nbsp;
                PNEUMATIC TUBES, CONVEYORS and/or MONORAILS
                <br>
                <input type="checkbox" name="6_mechanical_application[]" value="19">&nbsp;
                FUNICULAR
            </div>
        </div>
        <div class="hr"></div>
        
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Professional Mechanical Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_engineer" name="6_mechanical_engineer">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_engineer_address" name="6_mechanical_engineer_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_engineer_prc_no" name="6_mechanical_engineer_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="6_mechanical_engineer_prc_validity" name="6_mechanical_engineer_prc_validity" onchange="check_date_validity('6_mechanical_engineer_prc_validity');">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_engineer_ptr_no" name="6_mechanical_engineer_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="6_mechanical_engineer_ptr_issued_date" name="6_mechanical_engineer_ptr_issued_date">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_engineer_ptr_issued_at" name="6_mechanical_engineer_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_engineer_tin" name="6_mechanical_engineer_tin">
            </div>
        </div>
        <div class="hr"></div>
        <div class="row margin-top">
            <div class="col-md-12">
                <button type="button" class="btn btn-space btn-success" id="mechanical_sab">
                    Same as Above
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	SUPERVISOR / IN-CHARGE OF MECHANICAL WORKS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h5>
                    Select Engineer
                </h5>
                <select class="select2 select2-xs" id="6_mechanical_supervisor_classification" name="6_mechanical_supervisor_classification">
                    <option value="1">PROFESSIONAL MECHANICAL ENGINEER</option>
                    <option value="2">MECHANICAL ENGINEER</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_supervisor" name="6_mechanical_supervisor">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_supervisor_address" name="6_mechanical_supervisor_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_supervisor_prc_no" name="6_mechanical_supervisor_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="6_mechanical_supervisor_prc_validity" name="6_mechanical_supervisor_prc_validity" onchange="check_date_validity('6_mechanical_supervisor_prc_validity');">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_supervisor_ptr_no" name="6_mechanical_supervisor_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Issued Date 
                </h5>
                <input type="date" class="form-control form-control-xs" id="6_mechanical_supervisor_ptr_issued_date" name="6_mechanical_supervisor_ptr_issued_date">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_supervisor_ptr_issued_at" name="6_mechanical_supervisor_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="6_mechanical_supervisor_tin" name="6_mechanical_supervisor_tin">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-primary" id="btn_mechanical_permit">
                    <i class="icon mdi mdi-save"></i>&nbsp;&nbsp;
                    SAVE<!--  MECHANICAL PERMIT -->
                </button>
                <input type="hidden" name="6_id" id="6_id" value="0">
            </div>
            
        </div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn_mechanical_permit').click(function()
        {
            if(!$("#action_value").val())
            {
                if(!$("#building_permit_id").val())
                {
                    alert_warning('Please Create Building Application First');
                    return false;
                }
            }
            var mechanical_application = [];

            $("[name='6_mechanical_application[]']").each(function () {
                if($(this).prop('checked'))
                {
                    mechanical_application.push($(this).val());
                }
            });

            
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "Application for Mechanical Permit",
                text: "{{ __('page.submit_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post("{{ url('mechanical_permit/building_permit_application_form/store') }}", {

                        building_permit_id : $("#building_permit_id").val(),
                        id : $("#6_id").val(),

                        mechanical_application : mechanical_application,

                        other_mechanical_application : $("#6_other_mechanical_application").val(),
                        
                        mechanical_engineer : $("#6_mechanical_engineer").val(),
                        
                        mechanical_engineer_address : $("#6_mechanical_engineer_address").val(),
                        
                        mechanical_engineer_prc_no : $("#6_mechanical_engineer_prc_no").val(),
                        
                        mechanical_engineer_prc_validity : $("#6_mechanical_engineer_prc_validity").val(),
                        
                        mechanical_engineer_ptr_no : $("#6_mechanical_engineer_ptr_no").val(),
                        
                        mechanical_engineer_ptr_issued_date : $("#6_mechanical_engineer_ptr_issued_date").val(),
                        
                        mechanical_engineer_ptr_issued_at : $("#6_mechanical_engineer_ptr_issued_at").val(),
                        
                        mechanical_engineer_tin : $("#6_mechanical_engineer_tin").val(),
                        
                        mechanical_supervisor_classification : $("#6_mechanical_supervisor_classification").val(),
                        
                        mechanical_supervisor : $("#6_mechanical_supervisor").val(),
                        
                        mechanical_supervisor_address : $("#6_mechanical_supervisor_address").val(),
                        
                        mechanical_supervisor_prc_no : $("#6_mechanical_supervisor_prc_no").val(),
                        
                        mechanical_supervisor_prc_validity : $("#6_mechanical_supervisor_prc_validity").val(),
                        
                        mechanical_supervisor_ptr_no : $("#6_mechanical_supervisor_ptr_no").val(),
                        
                        mechanical_supervisor_ptr_issued_date : $("#6_mechanical_supervisor_ptr_issued_date").val(),
                        
                        mechanical_supervisor_ptr_issued_at : $("#6_mechanical_supervisor_ptr_issued_at").val(),
                        
                        mechanical_supervisor_tin : $("#6_mechanical_supervisor_tin").val(),

                    })
                    .then((response) => {
                        
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                $("#mechanical_permit_form").slideUp(500);
                                alert_success('Application for Mechanical Permit saved successfully');                      
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });
</script>