
<div class="row">
	<div class="col-md-12">
		<div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white; padding-top: 10px;">
            <div class="col-md-12">
                <h4>APPLICATION FOR SANITARY PERMIT</h4>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <input type="checkbox" name="5_sanitary_application[]" value="1">&nbsp;
                SHALLOW WELL
                <br>
                <input type="checkbox" name="5_sanitary_application[]" value="2">&nbsp;
                DEEP WELL AND PUMP SET
                <br>
                <input type="checkbox" name="5_sanitary_application[]" value="3">&nbsp;
                CITY/MUNICIPAL WATER SYSTEM
                <br>
                <input type="checkbox" name="5_sanitary_application[]" value="4">&nbsp;
                OTHERS (Specify) ___________
            </div>
            <div class="col-md-4">
                <input type="checkbox" name="5_sanitary_application[]" value="5">&nbsp;
                WASTE WATER TREATMENT PLANT
                <br>
                <input type="checkbox" name="5_sanitary_application[]" value="6">&nbsp;
                IMHOFF TANK
                <br>
                <input type="checkbox" name="5_sanitary_application[]" value="7">&nbsp;
                SANITARY SEWER CONNECTION
                <br>
                <input type="checkbox" name="5_sanitary_application[]" value="8">&nbsp;
                SUB-SURFACE SAND FILTER
                
            </div>
            <div class="col-md-4">
                <input type="checkbox" name="5_sanitary_application[]" value="9">&nbsp;
                SURFACE DRAINAGE
                <br>
                <input type="checkbox" name="5_sanitary_application[]" value="10">&nbsp;
                STREET CANAL
                <br>
                <input type="checkbox" name="5_sanitary_application[]" value="11">&nbsp;
                WATER COURSE 
                <br>
                <input type="checkbox" name="5_sanitary_application[]" id="5_sanitary_application_chk" value="12" onchange="enable_other_textbox('5_sanitary_application_chk', '5_other_sanitary_application')">&nbsp;
                OTHERS (Specify)
                <br>
                <input type="text" class="form-control form-control-xs" name="5_other_sanitary_application" id="5_other_sanitary_application" disabled>
            </div>
        </div>
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Sanitary Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_engineer" name="5_sanitary_engineer">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_engineer_address" name="5_sanitary_engineer_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_engineer_prc_no" name="5_sanitary_engineer_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="5_sanitary_engineer_prc_validity" name="5_sanitary_engineer_prc_validity" onchange="check_date_validity('5_sanitary_engineer_prc_validity');">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_engineer_ptr_no" name="5_sanitary_engineer_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="5_sanitary_engineer_ptr_date_issued" name="5_sanitary_engineer_ptr_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_engineer_ptr_issued_at" name="5_sanitary_engineer_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_engineer_tin" name="5_sanitary_engineer_tin">
            </div>
        </div>
        <div class="hr"></div>
        <div class="row margin-top">
            <div class="col-md-12">
                <button type="button" class="btn btn-space btn-success" id="sanitary_sab">
                    Same as Above
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	SUPERVISOR / IN-CHARGE OF SANITARY WORKS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Sanitary Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_supervisor" name="5_sanitary_supervisor">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_supervisor_address" name="5_sanitary_supervisor_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_supervisor_prc_no" name="5_sanitary_supervisor_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="5_sanitary_supervisor_prc_validity" name="5_sanitary_supervisor_prc_validity" onchange="check_date_validity('5_sanitary_supervisor_prc_validity');">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_supervisor_ptr_no" name="5_sanitary_supervisor_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="5_sanitary_supervisor_ptr_date_issued" name="5_sanitary_supervisor_ptr_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_supervisor_ptr_issued_at" name="5_sanitary_supervisor_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="5_sanitary_supervisor_tin" name="5_sanitary_supervisor_tin">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-primary" id="btn_sanitary_permit">
                    <i class="icon mdi mdi-save"></i>&nbsp;&nbsp;
                    SAVE<!--  SANITARY PERMIT -->
                </button>
                <input type="hidden" name="5_id" id="5_id" value="0">
            </div>
            
        </div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn_sanitary_permit').click(function()
        {
            if(!$("#action_value").val())
            {
                if(!$("#building_permit_id").val())
                {
                    alert_warning('Please Create Building Application First');
                    return false;
                }
            }
            var sanitary_application = [];
            var conformance = [];

            $("[name='5_sanitary_application[]']").each(function () {
                if($(this).prop('checked'))
                {
                    sanitary_application.push($(this).val());
                }
            });

            
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "Application for Sanitary Permit",
                text: "{{ __('page.submit_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post("{{ url('sanitary_permit/building_permit_application_form/store') }}", {

                        building_permit_id : $("#building_permit_id").val(),
                        id : $("#5_id").val(),

                        sanitary_application : sanitary_application,
                        
                        other_sanitary_application : $("#5_other_sanitary_application").val(),
                        sanitary_engineer : $("#5_sanitary_engineer").val(),
                        
                        sanitary_engineer_address : $("#5_sanitary_engineer_address").val(),
                        
                        sanitary_engineer_prc_no : $("#5_sanitary_engineer_prc_no").val(),
                        
                        sanitary_engineer_prc_validity : $("#5_sanitary_engineer_prc_validity").val(),
                        
                        sanitary_engineer_ptr_no : $("#5_sanitary_engineer_ptr_no").val(),
                        
                        sanitary_engineer_ptr_date_issued : $("#5_sanitary_engineer_ptr_date_issued").val(),
                        
                        sanitary_engineer_ptr_issued_at : $("#5_sanitary_engineer_ptr_issued_at").val(),
                        
                        sanitary_engineer_tin : $("#5_sanitary_engineer_tin").val(),
                        
                        sanitary_supervisor : $("#5_sanitary_supervisor").val(),
                        
                        sanitary_supervisor_address : $("#5_sanitary_supervisor_address").val(),
                        
                        sanitary_supervisor_prc_no : $("#5_sanitary_supervisor_prc_no").val(),
                        
                        sanitary_supervisor_prc_validity : $("#5_sanitary_supervisor_prc_validity").val(),
                        
                        sanitary_supervisor_ptr_no : $("#5_sanitary_supervisor_ptr_no").val(),
                        
                        sanitary_supervisor_ptr_date_issued : $("#5_sanitary_supervisor_ptr_date_issued").val(),
                        
                        sanitary_supervisor_ptr_issued_at : $("#5_sanitary_supervisor_ptr_issued_at").val(),
                        
                        sanitary_supervisor_tin : $("#5_sanitary_supervisor_tin").val(),
                    })
                    .then((response) => {
                        
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                $("#sanitary_permit_form").slideUp(500);
                                alert_success('Application for Sanitary Permit saved successfully');
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });
</script>