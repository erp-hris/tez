<div class="row">
    <div class="col-lg-12">
        <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white; padding-top: 10px;">
            <div class="col-md-12">
                <h4>APPLICATION FOR BUILDING PERMIT</h4>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>Type of Application for Building Permit</h5>
                <select class="select2 select2-xs" id="application_type" name="application_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">New</option>
                    <option value="2">Renewal</option>
                    <option value="3">Amendatory</option>
                </select>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    OWNER / APPLICANT
                </h5>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Last Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="last_name" name="last_name" value="{{ $last_name }}">
            </div>
            <div class="col-md-3">
                <h5>
                    First Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="first_name" name="first_name" value="{{ $first_name }}">
            </div>
            <div class="col-md-3">
                <h5>
                    M.I.
                </h5>
                <input type="text" class="form-control form-control-xs" id="middle_name" name="middle_name">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="tin" name="tin">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Enterprise Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="enterprise_name" name="enterprise_name">
            </div>
            <div class="col-md-6">
                <h5>
                    Form of Ownership
                </h5>
                <input type="text" class="form-control form-control-xs" id="ownership_form" name="ownership_form">
            </div>
            
        </div>
        <div class="hr"></div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    ADDRESS
                </h5>
            </div>
            <div class="col-md-3">
                <h5>
                    House No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="house_number" name="house_number">
            </div>
            <div class="col-md-3">
                <h5>
                    Street
                </h5>
                <input type="text" class="form-control form-control-xs" id="street" name="street">
            </div>
            <div class="col-md-3">
                <h5>
                    Barangay
                </h5>
                <input type="text" class="form-control form-control-xs" id="barangay" name="barangay">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
                <h5>
                    City / Municipality
                </h5>
                <input type="text" class="form-control form-control-xs" id="city" name="city">
            </div>
            <div class="col-md-3">
                <h5>
                    Zip Code
                </h5>
                <input type="text" class="form-control form-control-xs" id="zip_code" name="zip_code">
            </div>
            <div class="col-md-3">
                <h5>
                    Contact No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="telephone_number" name="telephone_number">
            </div>
        </div>
        
        <!-- <div id="tct_canvas">
            <div class="hr"></div>
            <div class="row margin-top">
                <div class="col-md-3">
                    <h5>
                        LOCATION OF CONSTRUCTION:
                    </h5>
                </div>
                <div class="col-md-3">
                    <h5>
                        Lot No.
                    </h5>
                    <input type="text" class="form-control form-control-xs" id="" name="lot_number[]">
                </div>
                <div class="col-md-3">
                    <h5>
                        Blk No.
                    </h5>
                    <input type="text" class="form-control form-control-xs" id="" name="block_no[]">
                </div>
                <div class="col-md-3">
                    <h5>
                        TCT No.
                    </h5>
                    <input type="text" class="form-control form-control-xs" id="" name="tct_no[]">
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-3">
                    <h5>
                        Tax Dec No.
                    </h5>
                    <input type="text" class="form-control form-control-xs" id="" name="tax_declaration_no[]">
                </div>
                <div class="col-md-3">
                    <h5>
                        Street
                    </h5>
                    <input type="text" class="form-control form-control-xs" id="" name="construction_street[]">
                </div>
                <div class="col-md-3">
                    <h5>
                        Barangay
                    </h5>
                    <input type="text" class="form-control form-control-xs" id="" name="construction_barangay[]">
                </div>
                <div class="col-md-3">
                    <h5>
                        City / Municipality
                    </h5>
                    <input type="text" class="form-control form-control-xs" id="" name="construction_city[]">
                </div>
            </div>
        </div> -->
        <div id="tct_canvas">
        </div>
        <!-- <div id="extra_tct"></div>
        <div class="row margin-top">
            <div class="col-md-12 text-right">
                
                <button type="button" class="btn btn-success" id="btn_add_tct">
                    <i class="mdi mdi-plus"></i>
                    Add Row
                </button>
                <input type="hidden" id="tct_count" name="" value="1">
            </div>
        </div> -->
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Scope of Work
                </h5>
                <select class="select2 select2-xs" id="work_scope" name="work_scope">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">NEW CONSTRUCTION
                    <option value="2">ERECTION
                    <option value="3">ADDITION
                    <option value="4">ALTERATION
                    <option value="5">RENOVATION
                    <option value="6">CONVERSION
                    <option value="7">REPAIR
                    <option value="8">MOVING
                    <option value="9">RAISING
                    <option value="10">DEMOLITION                        
                    <option value="11">ACCESSORY BUILDING/STRUCTURE
                    <option value="12">OTHERS (Specify)
                </select>
            </div>
            <div class="col-md-6">
                <h5>
                    If others, please specify
                </h5>
                <input type="text" class="form-control form-control-xs" id="other_work_scope" name="other_work_scope" disabled>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Use or Character of Occupancy
                </h5>
                <select class="select2 select2-xs" id="occupancy_character" name="occupancy_character">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">GROUP A : RESIDENTIAL, DWELLINGS</option>
                    <option value="2">GROUP B : RESIDENTIALS, HOTELS AND APARTMENTS</option>
                    <option value="3">GROUP C : EDUCATIONAL, RECREATIONAL</option>
                    <option value="4">GROUP D : INSTITUTIONAL</option>
                    <option value="5">GROUP E : BUSINESS AND MERCANTILE</option>
                    <option value="6">GROUP F : INDUSTRIAL</option>
                    <option value="7">GROUP G : INDUSTRIAL STORAGE AND HAZARDOUS</option>
                    <option value="8">GROUP H : RECREATIONAL, ASSEMBLY OCCUPANT LOAD LESS THAN 1000</option>
                    <option value="9">GROUP I : RECREATIONAL, ASSEMBLY OCCUPANT LOAD 1000 OR MORE</option>
                    <option value="10">GROUP J : AGRICULTURAL, ACCESSORY</option>
                    <option value="11">OTHERS (Specify)
                </select>
            </div>
            <div class="col-md-6">
                <h5>
                    If others, please specify
                </h5>
                <input type="text" class="form-control form-control-xs" id="other_occupancy_character" name="other_occupancy_character" disabled>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Occupancy Classified
                </h5>
                <input type="text" class="form-control form-control-xs" id="occupancy_classification" name="occupancy_classification">
            </div>
            <div class="col-md-6">
                <h5>
                    Total Estimated Cost Php
                </h5>
                <input type="text" class="form-control form-control-xs" id="estimated_cost" name="estimated_cost">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Number of Units
                </h5>
                <input type="text" class="form-control form-control-xs" id="units" name="units">
            </div>
            <div class="col-md-6">
                <h5>
                    Proposed Date of Construction
                </h5>
                <input type="date" class="form-control form-control-xs" id="construction_date" name="construction_date">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Total Floor Area (sq.m)
                </h5>
                <input type="text" class="form-control form-control-xs" id="floor_area" name="floor_area">
            </div>
            <div class="col-md-6">
                <h5>
                    Expected Date of Completion
                </h5>
                <input type="date" class="form-control form-control-xs" id="expected_completion_date" name="expected_completion_date">
            </div>
        </div>
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Architect / Civil Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="architect_name" name="architect_name">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="architect_address" name="architect_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="architect_prc_no" name="architect_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="architect_prc_validity" name="architect_prc_validity" onchange="check_date_validity('architect_prc_validity');">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="architect_ptr_no" name="architect_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="architect_ptr_issued_date" name="architect_ptr_issued_date">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="architect_id_issued_at" name="architect_id_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="architect_tin" name="architect_tin">
            </div>
        </div>
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Lot Owner's Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="lot_owner_name" name="lot_owner_name">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="lot_owner_address" name="lot_owner_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    CTC No. / Valid ID
                </h5>
                <input type="text" class="form-control form-control-xs" id="lot_owner_ctc_no" name="lot_owner_ctc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Issued Date
                </h5>
                <input type="date" class="form-control form-control-xs" id="lot_owner_ctc_issued_date" name="lot_owner_ctc_issued_date">
            </div>
            <div class="col-md-3">
                <h5>
                    Place Issued
                </h5>
                <input type="text" class="form-control form-control-xs" id="lot_owner_ctc_issued_place" name="lot_owner_ctc_issued_place">
            </div>
        </div>
        @if(Auth::user()->level == 1)
        <div class="row margin-top">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-primary" id="btn_building_permit">
                    <i class="icon mdi mdi-save"></i>&nbsp;&nbsp;
                    SAVE<!--  BUILDING PERMIT -->
                </button>
                <input type="hidden" name="1_id" id="1_id" value="0">
            </div>
            
        </div>
        @endif
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('input[type=text]').val(function () {
            //return this.value.toUpperCase();
        });
        $("#btn_remove_tct").click(function () {
            $(this).closest("#extra_tct").remove();
        });
        $("#btn_add_tct").click(function () {
            var tct_count = $("#tct_count").val();
            tct_count++;
            $("#tct_count").val(tct_count);
            var canvas = $("#tct_canvas").clone(true).find("input:text").val("").end();
            $("#extra_tct").append(canvas);
        });

        @if($array['pre_eval'])
        $("#last_name").val("{{ $array['pre_eval']->last_name }}");
        $("#last_name").val("{{ $array['pre_eval']->last_name}}");
        $("#first_name").val("{{ $array['pre_eval']->first_name}}");
        $("#middle_name").val("{{ $array['pre_eval']->middle_name}}");
        $("#tin").val("{{ $array['pre_eval']->tin}}");
        $("#project_name").val("{{ $array['pre_eval']->enterprise_name}}");
        $("#enterprise_name").val("{{ $array['pre_eval']->enterprise_name}}");
        $("#ownership_form").val("{{ $array['pre_eval']->ownership_form}}");
        $("#house_number").val("{{ $array['pre_eval']->house_number}}");
        $("#street").val("{{ $array['pre_eval']->street}}");
        $("#barangay").val("{{ $array['pre_eval']->barangay}}");
        $("#city").val("{{ $array['pre_eval']->city}}");
        $("#zip_code").val("{{ $array['pre_eval']->zip_code}}");
        $("#telephone_number").val();
        
        
        $("#application_type").val({{ $array['pre_eval']->application_type}});
        
        $("#last_name, #last_name, #first_name, #middle_name, #tin, #project_name, #enterprise_name, #ownership_form, #house_number, #street, #barangay, #city, #zip_code, #telephone_number").prop('readonly', true);
        var tct_str = '';

        @foreach($array['tct_informations'] as $tct_key => $tct_val)
            tct_str += '<div class="hr"></div><div class="row margin-top"><div class="col-3"><h5>Land Ownership</h5><select class="select2 select2-xs" disabled><option value="">TITLED</option><option value="">ALIENABLE AND DISPOSABLE LAND</option><option value="">FOREST LAND</option></select></div><div class="col-md-3"><h5>Lot No.</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->lot_number}}" readonly></div><div class="col-md-3"><h5>Blk No.</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->block_no}}" readonly></div><div class="col-md-3"><h5>TCT No.</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->tct_no}}" readonly></div></div><div class="row margin-top"><div class="col-md-3"><h5>Tax Dec No.</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->tax_declaration_no}}" readonly></div><div class="col-md-3"><h5>Street</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->construction_street}}" readonly></div><div class="col-md-3"><h5>Barangay</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->construction_barangay}}" readonly></div><div class="col-md-3"><h5>City / Municipality</h5><input type="text" class="form-control form-control-xs" id="" value="{{$tct_val->construction_city}}" readonly></div></div><div class="row margin-top"><div class="col-md-3"><h5>Land Title</h5><input type="file" class="form-control form-control-xs" name="" disabled></div><div class="col-md-3"><h5>DENR Certificate</h5><input type="file" class="form-control form-control-xs" name="" disabled></div><div class="col-md-3"><h5>Barangay Certificate</h5><input type="file" class="form-control form-control-xs" name="" disabled></div><div class="col-md-3"><h5>FLAgT</h5><input type="file" class="form-control form-control-xs" name="" disabled></div></div>';
        @endforeach
        $("#tct_canvas").html(tct_str);

        @endif
        $('#btn_building_permit').click(function()
        {
            @foreach($array['tct_fields'] as $key => $value)
            var {{ $value }} = [];
            $("input[name='{{ $value }}[]']").each(function() {
                {{ $value }}.push($(this).val());
            });
            @endforeach
            
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "Adding application for Building Permit",
                text: "Adding application for Building Permit",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post(frm.action, {
                        @foreach($array['building_permit_fields'] as $key => $value)
                        {{ $key }} : $("#{{ $value }}").val().toUpperCase(),
                        @endforeach

                        @foreach($array['tct_fields'] as $key => $value)

                        {{ $key }} : {{ $value }},
                        @endforeach

                        tct_count : $("#tct_count").val(),
                        @if(isset($array['id']))
                        id: {{$array['id']}},
                        @endif

                        id : $("#1_id").val(),
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                //window.location.href="{{ url($module.'/'.$option) }}";
                                $("#building_permit_form").slideUp(500);
                                alert_success('You may now add ancillary permits.');
                                $("#building_permit_id").val(response.data.last_insert_id);
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });
</script>