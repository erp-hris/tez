
<div class="row">
	<div class="col-md-12">
		<div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white; padding-top: 10px;">
            <div class="col-md-12">
                <h4>APPLICATION FOR ELECTRICAL PERMIT</h4>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <input type="checkbox" name="4_electrical_application[]" value="1">
                &nbsp;&nbsp;
                NEW INSTALLATION
                <br>

                <input type="checkbox" name="4_electrical_application[]" value="2">
                &nbsp;&nbsp;
                ANNUAL INSPECTION
                <br>

                <input type="checkbox" name="4_electrical_application[]" value="3">
                &nbsp;&nbsp;
                TEMPORARY
                
            </div>
            <div class="col-md-4">
                <input type="checkbox" name="4_electrical_application[]" value="4">
                &nbsp;&nbsp;
                RECONNECTION OF SERVICE ENTRANCE
                <br>

                <input type="checkbox" name="4_electrical_application[]" value="5">
                &nbsp;&nbsp;
                SEPARATION OF SERVICE ENTRANCE
                <br>

                <input type="checkbox" name="4_electrical_application[]" value="6">
                &nbsp;&nbsp;
                UPGRADING OF SERVICE ENTRANCE
                
            </div>
            <div class="col-md-4">
                <input type="checkbox" name="4_electrical_application[]" value="7">
                &nbsp;&nbsp;
                RELOCATING OF SERVICE ENTRANCE
                <br>

                <input type="checkbox" name="4_electrical_application[]" id="4_other_electrical_application_chk" value="8" onchange="enable_other_textbox('4_other_electrical_application_chk', '4_other_electrical_application')">
                &nbsp;&nbsp;
                OTHERS (Specify)
                <br>

                <input type="text" class="form-control form-control-xs" name="4_other_electrical_application" id="4_other_electrical_application" disabled>
                <br>
            </div>
        </div>
        <div class="hr"></div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    <b>SUMMARY OF ELECTRICAL LOADS/CAPACITIES APPLIED FOR</b>
                </h5>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-4">
                <h5>
                    TOTAL CONNECTED LOAD (kVA)
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_total_connection_load" name="4_total_connection_load">
            </div>
            <div class="col-md-4">
                <h5>
                    TOTAL TRANSFORMER CAPACITY (kVA)
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_total_transformer_capacity" name="4_total_transformer_capacity">
            </div>
            <div class="col-md-4">
                <h5>
                    TOTAL GENERATOR/UPS CAPACITY (kVA)
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_total_generator_capacity" name="4_total_generator_capacity">
            </div>
        </div>

        <div class="hr"></div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Professional Electrical Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_engineer" name="4_electrical_engineer">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_engineer_address" name="4_electrical_engineer_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_engineer_prc_no" name="4_electrical_engineer_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="4_electrical_engineer_prc_validity" name="4_electrical_engineer_prc_validity" onchange="check_date_validity('4_electrical_engineer_prc_validity');">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_engineer_ptr_no" name="4_electrical_engineer_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="4_electrical_engineer_ptr_date_issued" name="4_electrical_engineer_ptr_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_engineer_ptr_issued_at" name="4_electrical_engineer_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_engineer_tin" name="4_electrical_engineer_tin">
            </div>
        </div>

        <div class="hr"></div>
        <div class="row margin-top">
            <div class="col-md-12">
                <button type="button" class="btn btn-space btn-success" id="electrical_sab">
                    Same as Above
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                   	SUPERVISOR / IN-CHARGE OF ELECTRICAL WORKS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h5>
                    Select Supervisor
                </h5>
                <select class="select2 select2-xs" id="4_supervisor_classification" name="4_supervisor_classification">
                    <option value="1">PROFESSIONAL ELECTRICAL ENGINEER</option>
                    <option value="2">REGISTERED ELECTRICAL ENGINEER</option>
                    <option value="3">REGISTERED MASTER ELECTRICIAN</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h5>
                    Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_supervisor" name="4_electrical_supervisor">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_supervisor_address" name="4_electrical_supervisor_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_supervisor_prc_no" name="4_electrical_supervisor_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="4_electrical_supervisor_prc_validity" name="4_electrical_supervisor_prc_validity" onchange="check_date_validity('4_electrical_supervisor_prc_validity');">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_supervisor_ptr_no" name="4_electrical_supervisor_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="4_electrical_supervisor_ptr_date_issued" name="4_electrical_supervisor_ptr_date_issued">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_supervisor_ptr_issued_at" name="4_electrical_supervisor_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="4_electrical_supervisor_tin" name="4_electrical_supervisor_tin">
            </div>
        </div>
        
        <div class="row margin-top">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-primary" id="btn_electrical_permit">
                    <i class="icon mdi mdi-save"></i>&nbsp;&nbsp;
                    SAVE<!--  ELECTRICAL PERMIT -->
                </button>
                <input type="hidden" name="4_id" id="4_id" value="0">
            </div>
            
        </div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn_electrical_permit').click(function()
        {
            if(!$("#action_value").val())
            {
                if(!$("#building_permit_id").val())
                {
                    alert_warning('Please Create Building Application First');
                    return false;
                }
            }
            var electrical_application = [];
            var conformance = [];

            $("[name='4_electrical_application[]']").each(function () {
                if($(this).prop('checked'))
                {
                    electrical_application.push($(this).val());
                }
            });

            
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "Application for Electrical Permit",
                text: "{{ __('page.submit_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post("{{ url('electrical_permit/building_permit_application_form/store') }}", {

                        building_permit_id : $("#building_permit_id").val(),
                        id : $("#4_id").val(),

                        electrical_application : electrical_application,
                        
                        other_electrical_application : $("#4_other_electrical_application").val(),
                        total_connection_load : $("#4_total_connection_load").val(),
                        
                        total_transformer_capacity : $("#4_total_transformer_capacity").val(),
                        
                        total_generator_capacity : $("#4_total_generator_capacity").val(),
                        
                        electrical_engineer : $("#4_electrical_engineer").val(),
                        
                        electrical_engineer_address : $("#4_electrical_engineer_address").val(),
                        
                        electrical_engineer_prc_no : $("#4_electrical_engineer_prc_no").val(),
                        
                        electrical_engineer_prc_validity : $("#4_electrical_engineer_prc_validity").val(),
                        
                        electrical_engineer_ptr_no : $("#4_electrical_engineer_ptr_no").val(),
                        
                        electrical_engineer_ptr_date_issued : $("#4_electrical_engineer_ptr_date_issued").val(),
                        
                        electrical_engineer_ptr_issued_at : $("#4_electrical_engineer_ptr_issued_at").val(),
                        
                        electrical_engineer_tin : $("#4_electrical_engineer_tin").val(),
                        
                        supervisor_classification : $("#4_supervisor_classification").val(),
                        
                        electrical_supervisor : $("#4_electrical_supervisor").val(),
                        
                        electrical_supervisor_address : $("#4_electrical_supervisor_address").val(),
                        
                        electrical_supervisor_prc_no : $("#4_electrical_supervisor_prc_no").val(),
                        
                        electrical_supervisor_prc_validity : $("#4_electrical_supervisor_prc_validity").val(),
                        
                        electrical_supervisor_ptr_no : $("#4_electrical_supervisor_ptr_no").val(),
                        
                        electrical_supervisor_ptr_date_issued : $("#4_electrical_supervisor_ptr_date_issued").val(),
                        
                        electrical_supervisor_ptr_issued_at : $("#4_electrical_supervisor_ptr_issued_at").val(),
                        
                        electrical_supervisor_tin : $("#4_electrical_supervisor_tin").val(),

                    })
                    .then((response) => {
                        
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                $("#electrical_permit_form").slideUp(500);
                                alert_success('Application for Electrical Permit saved successfully');
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });
</script>