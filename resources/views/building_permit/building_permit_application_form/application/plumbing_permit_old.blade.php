@php
    $first_column = ['water closet', 'floor drain', 'lavatory', 'kitchen sink', 'faucet', 'shower head', 'water meter', 'grease trap', 'bath tub', 'slop sink', 'urinal', 'air conditioning unit', 'water tank/reservoir'];

    $second_column = ['bidette', 'laundry trays', 'dental cuspidor', 'drinking fountain', 'bar sink', 'soda fountain sink', 'laboratory sink', 'sterilizer'];
@endphp
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white; padding-top: 10px;">
            <div class="col-md-12">
                <h4>APPLICATION FOR PLUMBING PERMIT</h4>
            </div>
        </div>
        
        <div class="row margin-top">
            <div class="col-md-12">
                <table width="100%">
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-md-12">
                                    <b>
                                        FIXTURE TO BE INSTALLED
                                    </b>
                                </div>
                                
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-3 text-center">
                                            QTY
                                        </div>
                                        <div class="col-md-2">
                                            NEW<br>FIXTURE
                                        </div>
                                        <div class="col-md-2">
                                            EXISTING<br>FIXTURE
                                        </div>
                                        <div class="col-md-5">
                                            KIND OF<br>FIXTURE
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-3 text-center">
                                            QTY
                                        </div>
                                        <div class="col-md-2">
                                            NEW<br>FIXTURE
                                        </div>
                                        <div class="col-md-2">
                                            EXISTING<br>FIXTURE
                                        </div>
                                        <div class="col-md-5">
                                            KIND OF<br>FIXTURE
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-6">
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="7_plumbing_item[]">&nbsp;&nbsp;&nbsp;
                                            WATER CLOSET
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            FLOOR DRAIN
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            LAVATORY
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            KITCHEN SINK
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            FAUCET
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            SHOWER HEAD
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            WATER METER
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            GREASE TRAP
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            BATH TUB
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            SLOP SINK
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;URINAL
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            AIR CONDITIONNING UNIT
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            WATER TANK/RESERVOIR
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            TOTAL
                                        </div>
                                        <div class="col-md-7"></div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            BIDETTE
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            LAUNDRY TRAYS
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            DENTAL CUSPIDOR
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            DRINKING FOUNTAIN
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            BAR SINK
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            SODA FOUNTAIN SINK
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            LABORATORY SINK
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            STERILIZER
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                            OTHERS (Specify )
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control form-control-xs">
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control form-control-xs">
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="7_plumbing_qty[]" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" name="7_plumbing_exist[]" value="0">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control form-control-xs">
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-md-3 text-center">
                                            <input type="text" class="form-control form-control-xs text-center" name="" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            TOTAL
                                        </div>
                                        <div class="col-md-7"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-top">
                                
                                <div class="col-md-3">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;<b>WATER DISTRIBUTION SYSTEM</b>
                                </div>
                                <div class="col-md-3">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;<b>SEWAGE SYSTEM</b>
                                </div>
                                <div class="col-md-3">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;<b>SEPTIC TANK</b>
                                </div>
                                <div class="col-md-3">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;<b>STORM DRAINAGE SYSTEM</b>
                                </div>
                            </div>                            
                        </td>
                    </tr>
                    
                </table>
            </div>
        </div>
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                    DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Master Plumber
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer" name="plumbing_engineer">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_address" name="plumbing_engineer_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_prc_no" name="plumbing_engineer_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="plumbing_engineer_prc_validity" name="plumbing_engineer_prc_validity">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_ptr_no" name="plumbing_engineer_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="plumbing_engineer_ptr_issued_date" name="plumbing_engineer_ptr_issued_date">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_ptr_issued_at" name="plumbing_engineer_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_engineer_tin" name="plumbing_engineer_tin">
            </div>
        </div>
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                    SUPERVISOR / IN-CHARGE OF PLUMBING WORKS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Master Plumber
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor" name="plumbing_supervisor">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_address" name="plumbing_supervisor_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_prc_no" name="plumbing_supervisor_prc_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="plumbing_supervisor_prc_validity" name="plumbing_supervisor_prc_validity">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_ptr_no" name="plumbing_supervisor_ptr_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="plumbing_supervisor_ptr_issued_date" name="plumbing_supervisor_ptr_issued_date">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_ptr_issued_at" name="plumbing_supervisor_ptr_issued_at">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="plumbing_supervisor_tin" name="plumbing_supervisor_tin">
            </div>
        </div>
    </div>
</div>