
<div class="row">
    <div class="col-md-12">
        <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white; padding-top: 10px;">
            <div class="col-md-12">
                <h4>APPLICATION FOR ELECTRICAL PERMIT</h4>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <table width="100%">
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-md-12">
                                    <b>
                                        NATURE OF INSTALLATION WORKS/EQUIPMENT SYSTEM:
                                    </b>
                                </div>
                                
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-6">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;TELECOMMUNICATION SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;BROADCASTING SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;TELEVISION SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;INFORMATION TECHNOLOGY SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;SECURITY AND ALARM SYSTEM
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                    ELECTRONICS FIRE ALARM SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                    SOUND COMMUNICATION SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                    CENTRALIZED CLOCK SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                    SOUND SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                    ELECTRONICS CONTROL AND CONVEYOR SYSTEM
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                    ELECTRONICS COMPUTERIZED PROCESS CONTROLS AUTOMATION SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                    BUILDING AUTOMATION MANAGEMENT AND CONTROL SYSTEM
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;
                                    BUILDING WIRING UTILIZING COPPER CABLE, FIBER OPTIC CABLE OR OTHER MEDIAL ELECTRONICS SYSTEM   
                                    <br>
                                    <input type="checkbox" name="">&nbsp;&nbsp;&nbsp;ANY OTHER ELECTRONICS AND I.T. SYSTEMS, EQUIPMENT, APPARATUS, DEVICE AND/OR COMPONENT (Specify) __________________________________
                                </div>
                            </div>
                        </td>
                    </tr>
                    
                </table>
            </div>
        </div>
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                    DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Electronics Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="hr"></div>
        <div class="row">
            <div class="col-md-12">
                <h5>
                    SUPERVISOR / IN-CHARGE OF ELECTRONICS WORKS
                </h5>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Professional Electronics Engineer
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-6">
                <h5>
                    Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PRC No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Validity
                </h5>
                <input type="date" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    PTR No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Date Issued
                </h5>
                <input type="date" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Issued At
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
    </div>
</div>