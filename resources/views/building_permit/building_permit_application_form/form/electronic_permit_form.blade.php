<div class="row watermarked" style="page-break-after: always;">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        NBC FORM NO. A - 07                   
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12">
                        <img src="{{ asset('img/tieza-logo.png') }}" height="150px">
                        <br>
                        <span class="hdr3">
                            Republic of the Philippines
                        </span>
                        <br>
                        <span class="hdr1">
                            Tourism Infrastructure and Enterprise Zone Authority
                        </span>
                        <br>
                        <span class="hdr1">
                            Office of the Building Official
                        </span>
                        <br>
                        
                        <span class="hdr3">
                            <b>
                                ELECTRONICS PERMIT
                            </b>
                        </span>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-4">
                        APPLICATION NO.
                        <br>
                        |____| |____| |____| |____| |____| |____| |____| |____| |____| |____|
                    </div>
                    <div class="col-4">
                        ELP NO.
                        <br>
                        |____| |____| |____| |____| |____| |____| |____| |____| |____|
                    </div>
                    <div class="col-4">
                        BUILDING PERMIT NO.
                        <br>
                        |____| |____| |____| |____| |____| |____| |____| |____| |____|
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <b>
                            BOX 1 (TO BE ACCOMPLISHED IN PRINT BY THE OWNER/APPLICANT)
                        </b>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-3">
                                            OWNER/APPLICANT
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-3">
                                            LAST NAME
                                            <br>
                                            <br>
                                            <b>{{ $data->last_name }}</b>
                                        </div>
                                        <div class="col-4">
                                            FIRST NAME
                                            <br>
                                            <br>
                                            <b>{{ $data->first_name }}</b>
                                        </div>
                                        <div class="col-2">
                                            M.I
                                            <br>
                                            <br>
                                            <b>{{ $data->middle_name }}</b>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 25%;">
                                    <div class="row">
                                        <div class="col-12">
                                            TIN
                                            <br>
                                            <br>
                                            <b>{{ $data->tin }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 35%;">
                                    <div class="row">
                                        <div class="col-12">
                                            FOR CONSTRUCTION OWNED
                                            <br>
                                            BY AN ENTERPRISE
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            FORM OF OWNERSHIP
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            USE OR CHARACTER OF OCCUPANCY
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-2">
                                            ADDRESS:
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-1">
                                            NO.,
                                            <br>
                                            <br>
                                            <b>{{ $data->house_number }}</b>
                                        </div>
                                        <div class="col-2">
                                            STREET,
                                            <br>
                                            <br>
                                            <b>{{ $data->street }}</b>
                                        </div>
                                        <div class="col-2">
                                            BARANGAY,
                                            <br>
                                            <br>
                                            <b>{{ $data->barangay }}</b>
                                        </div>
                                        <div class="col-3">
                                            CITY/MUNICIPALITY
                                            <br>
                                            <br>
                                            <b>{{ $data->city }}</b>
                                        </div>
                                        <div class="col-2">
                                            ZIP CODE
                                            <br>
                                            <br>
                                            <b>{{ $data->zip_code }}</b>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 25%;">
                                    <div class="row">
                                        <div class="col-12">
                                            TELEPHONE NO.
                                            <br>
                                            <br>
                                            <b>{{ $data->telephone_number }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                        

                                    <div class="row">
                                        <div class="col-4">
                                            LOCATION OF CONSTRUCTION:
                                        </div>
                                        <div class="col-2">
                                            LOT NO. <u><b>{{ $data->lot_number }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            BLK NO. <u><b>{{ $data->block_no }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            TCT NO. <u><b>{{ $data->tct_no }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            TAX DEC. NO. <u><b>{{ $data->tax_declaration_no }}</b></u>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            STREET <u><b>{{ $data->construction_street }}</b></u>
                                        </div>
                                        <div class="col-4">
                                            BARANGAY <u><b>{{ $data->construction_barangay }}</b></u>
                                        </div>
                                        <div class="col-4">
                                            CITY/ MUNICIPALITY OF <u><b>{{ $data->construction_city }}</b></u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>SCOPE OF WORK</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <input type="checkbox" id="work_scope_1" name="" disabled>
                                            &nbsp;&nbsp;
                                            NEW CONSTRUCTION
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" id="work_scope_5" name="" disabled>
                                            &nbsp;&nbsp;
                                            ANNUAL INSPECTION
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" id="work_scope_11" name="" disabled>
                                            &nbsp;&nbsp;
                                            OTHERS (Specify)
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <b>
                            BOX 2 (TO BE ACCOMPLISHED BY THE DESIGN PROFESSIONAL)
                        </b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            <b>
                                                NATURE OF INSTALLATION WORKS/EQUIPMENT SYSTEM:
                                            </b>
                                        </div>
                                        
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-4">
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;TELECOMMUNICATION SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;BROADCASTING SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;TELEVISION SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;INFORMATION TECHNOLOGY SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;SECURITY AND ALARM SYSTEM
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                            ELECTRONICS FIRE ALARM SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                            SOUND COMMUNICATION SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                            CENTRALIZED CLOCK SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                            SOUND SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                            ELECTRONICS CONTROL AND CONVEYOR SYSTEM
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                            ELECTRONICS COMPUTERIZED PROCESS CONTROLS AUTOMATION SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                            BUILDING AUTOMATION MANAGEMENT AND CONTROL SYSTEM
                                            <br>
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                            BUILDING WIRING UTILIZING COPPER CABLE, FIBER OPTIC CABLE OR OTHER MEDIAL ELECTRONICS SYSTEM    
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;ANY OTHER ELECTRONICS AND I.T. SYSTEMS, EQUIPMENT, APPARATUS, DEVICE AND/OR COMPONENT (Specify) __________________________________
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row margin-top">
                                        <div class="col-12">
                                            PREPARED BY:
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 3
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 4
                                </b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <table border="2" width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col-8 text-center">
                                                    <br><br>
                                                    _________________________________
                                                    <br>
                                                    PROFESSIONAL ELECTRONICS ENGINEER
                                                    <br>
                                                    (Signed and Sealed Over Printed Name)
                                                </div>
                                                <div class="col-4">
                                                    <br><br>
                                                    Date ________
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Address:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PRC No.
                                        </td>
                                        <td>
                                            Validity
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PTR No.
                                        </td>
                                        <td>
                                            Date Issued
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Issued at
                                        </td>
                                        <td>
                                            T.I.N
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <table border="2" width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                SUPERVISOR / IN-CHARGE OF ELECTRONICS WORKS
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col-8 text-center">
                                                    <br><br>
                                                    _________________________________
                                                    <br>
                                                    PROFESSIONAL ELECTRONICS ENGINEER
                                                    <br>
                                                    (Signed and Sealed Over Printed Name)
                                                </div>
                                                <div class="col-4">
                                                    <br><br>
                                                    Date ________
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Address:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PRC No.
                                        </td>
                                        <td>
                                            Validity
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PTR No.
                                        </td>
                                        <td>
                                            Date Issued
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Issued at
                                        </td>
                                        <td>
                                            T.I.N
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 5
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 6
                                </b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>BUILDING OWNER</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <br><br><br>
                                            ________________________________
                                            <br>
                                            (Signature Over Printed Name)
                                            <br>
                                            Date_____________________________
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Address:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    C.T.C No.
                                    <br><br>
                                </td>
                                <td>
                                    Date Issued
                                    <br><br>
                                </td>
                                <td>
                                    Place Issued
                                    <br><br>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-6">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            WITH MY CONSENT:   <b> LOT OWNER</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <br><br><br>
                                            ________________________________
                                            <br>
                                            (Signature Over Printed Name)
                                            <br>
                                            Date_____________________________
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Address:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    C.T.C No.
                                    <br><br>
                                </td>
                                <td>
                                    Date Issued
                                    <br><br>
                                </td>
                                <td>
                                    Place Issued
                                    <br><br>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row watermarked" style="page-break-after: always;">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <b>
                    BOX 7 TO BE ACCOMPLISHED BY THE PERMITS AND LICENSES DIVISION
                </b>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td>
                            Received By:
                            <br><br>
                        </td>
                        <td>
                            Date:
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <b>FIVE (5) SETS OF A3 ELECTRONICS DOCUMENTS</b>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-6">
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    ELECTRONICS PLANS AND SPECIFICATIONS
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    BILLS OF MATERIALS
                                </div>
                                <div class="col-6">
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    COST ESTIMATES
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    OTHERS (Specify) ______________________________
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <b>
                    BOX 8
                </b>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td colspan="6">
                            <b>PROGRESS FLOW</b>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="2">
                            
                        </td> 
                        <td colspan="2" class="text-center">
                            IN
                        </td>
                        <td colspan="2" class="text-center">
                            OUT
                        </td>
                        <td rowspan="2" class="text-center">
                            PROCESSED BY:
                        </td> 
                    </tr>
                    <tr class="text-center">
                        <td>DATE</td>
                        <td>TIME</td>
                        <td>DATE</td>
                        <td>TIME</td>
                    </tr>
                    <tr>
                        <td>
                            ELECTRONICS
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            OTHERS (Specify)
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td style="padding-left: 20px; padding-right: 20px;">
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    ACTION TAKEN:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    PERMIT IS HEREBY ISSUED SUBJECT TO THE FOLLOWING:
                                </div>
                            </div>
                            <div class="row" style="font-size: 12pt;">
                                <div class="col-12">
                                    1.  That the proposed electronics works shall be in accordance with the electronics plans filed with this Office and in conformity with the latest Electronics Code of the Philippines, the National Building Code and its IRR. 
                                    <br><br>
                                    2.  That prior to any electronics installation, a duly accomplished prescribed <b>“Notice of Construction”</b> shall be submitted to the Office of the Building Official.
                                    <br><br>
                                    3.  That upon completion of the electronic works, the licensed supervisor/in-charge shall submit the entry to the logbook duly signed and sealed to the building official including as-built plans and other documents and shall also accomplish the Certificate of Completion stating that the electronic works conform to the provision of the Electronics Code of the Philippines, the National Building Code and its IRR.
                                    <br><br>
                                    4.  That this permit is null and void unless accompanied by the building permit.
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    PREPARED BY:
                                </div>
                            </div> 
                            <br><br>
                            <br><br><br><br><br><br><br><br><br><br><br><br>
                            <br><br><br><br><br><br><br><br><br>
                            <div class="row">
                                <div class="col-12 text-center" style="font-weight: 600; font-size: 18pt;">
                                    _______________________________
                                    <br>
                                    BUILDING OFFICIAL
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 12pt;">
                                    (Signature Over Printed Name)
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 15pt;">
                                    Date _____________
                                </div>
                            </div> 
                            <br><br><br><br><br><br><br><br><br><br><br><br>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>