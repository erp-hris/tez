<div class="row watermarked" style="page-break-after: always;">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <span>
                    NBC FORM NO. A-03
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row text-center">
                    <div class="col-12">
                        <img src="{{ asset('img/tieza-logo.png') }}" height="150px">
                        <br>
                        <span class="hdr3">
                            Republic of the Philippines
                        </span>
                        <br>
                        <span class="hdr1">
                            Tourism Infrastructure and Enterprise Zone Authority
                        </span>
                        <br>
                        <span class="hdr1">
                            Office of the Building Official
                        </span>
                        <br>
                        
                        <span class="hdr3">
                            <b>
                                ELECTRICAL PERMIT
                            </b>
                        </span>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-4">
                        APPLICATION NO.
                        <br>
                        |____| |____| |____| |____| |____| |____| |____| |____| |____| |____|
                    </div>
                    <div class="col-4">
                        E.P. NO.
                        <br>
                        |____| |____| |____| |____| |____| |____| |____| |____| |____|
                    </div>
                    <div class="col-4">
                        BUILDING PERMIT NO.
                        <br>
                        |____| |____| |____| |____| |____| |____| |____| |____| |____|
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <b>
                            BOX 1 (TO BE ACCOMPLISHED IN PRINT BY THE OWNER/APPLICANT)
                        </b>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-3">
                                            OWNER/APPLICANT
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-3">
                                            LAST NAME
                                            <br>
                                            <br>
                                            <b>{{ $data->last_name }}</b>
                                        </div>
                                        <div class="col-4">
                                            FIRST NAME
                                            <br>
                                            <br>
                                            <b>{{ $data->first_name }}</b>
                                        </div>
                                        <div class="col-2">
                                            M.I
                                            <br>
                                            <br>
                                            <b>{{ $data->middle_name }}</b>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 25%;">
                                    <div class="row">
                                        <div class="col-12">
                                            TIN
                                            <br>
                                            <br>
                                            <b>{{ $data->tin }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 35%;">
                                    <div class="row">
                                        <div class="col-12">
                                            FOR CONSTRUCTION OWNED
                                            <br>
                                            BY AN ENTERPRISE
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            FORM OF OWNERSHIP
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            USE OR CHARACTER OF OCCUPANCY
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-2">
                                            ADDRESS:
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-1">
                                            NO.,
                                            <br>
                                            <br>
                                            <b>{{ $data->house_number }}</b>
                                        </div>
                                        <div class="col-2">
                                            STREET,
                                            <br>
                                            <br>
                                            <b>{{ $data->street }}</b>
                                        </div>
                                        <div class="col-2">
                                            BARANGAY,
                                            <br>
                                            <br>
                                            <b>{{ $data->barangay }}</b>
                                        </div>
                                        <div class="col-3">
                                            CITY/MUNICIPALITY
                                            <br>
                                            <br>
                                            <b>{{ $data->city }}</b>
                                        </div>
                                        <div class="col-2">
                                            ZIP CODE
                                            <br>
                                            <br>
                                            <b>{{ $data->zip_code }}</b>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 25%;">
                                    <div class="row">
                                        <div class="col-12">
                                            TELEPHONE NO.
                                            <br>
                                            <br>
                                            <b>{{ $data->telephone_number }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-4">
                                            LOCATION OF CONSTRUCTION:
                                        </div>
                                        <div class="col-2">
                                            LOT NO. <u><b>{{ $data->lot_number }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            BLK NO. <u><b>{{ $data->block_no }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            TCT NO. <u><b>{{ $data->tct_no }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            TAX DEC. NO. <u><b>{{ $data->tax_declaration_no }}</b></u>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            STREET <u><b>{{ $data->construction_street }}</b></u>
                                        </div>
                                        <div class="col-4">
                                            BARANGAY <u><b>{{ $data->construction_barangay }}</b></u>
                                        </div>
                                        <div class="col-4">
                                            CITY/ MUNICIPALITY OF <u><b>{{ $data->construction_city }}</b></u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>SCOPE OF WORK</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <input type="checkbox" name="" disabled>
                                            &nbsp;&nbsp;
                                            NEW INSTALLATION
                                            <br>

                                            <input type="checkbox" name="" disabled>
                                            &nbsp;&nbsp;
                                            ANNUAL INSPECTION
                                            <br>

                                            <input type="checkbox" name="" disabled>
                                            &nbsp;&nbsp;
                                            TEMPORARY
                                            
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" name="" disabled>
                                            &nbsp;&nbsp;
                                            RECONNECTION OF SERVICE ENTRANCE
                                            <br>

                                            <input type="checkbox" name="" disabled>
                                            &nbsp;&nbsp;
                                            SEPARATION OF SERVICE ENTRANCE
                                            <br>

                                            <input type="checkbox" name="" disabled>
                                            &nbsp;&nbsp;
                                            UPGRADING OF SERVICE ENTRANCE
                                            
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" name="" disabled>
                                            &nbsp;&nbsp;
                                            RELOCATING OF SERVICE ENTRANCE
                                            <br>

                                            <input type="checkbox" name="" disabled>
                                            &nbsp;&nbsp;
                                            OTHERS (Specify)
                                            <br>

                                            
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" colspan="3">
                                    <b>
                                        SUMMARY OF ELECTRICAL LOADS/CAPACITIES APPLIED FOR
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    TOTAL CONNECTED LOAD
                                    <br>
                                    ____________kVa.
                                </td>
                                <td class="text-center">
                                    TOTAL TRANSFORMER CAPACITY
                                    <br>
                                    ____________kVa.
                                </td>
                                <td class="text-center">
                                    TOTAL GENERATOR/UPS CAPACITY
                                    <br>
                                    ____________kVa.
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <b>
                            BOX 2 (TO BE ACCOMPLISHED IN PRINT BY THE DESIGN PROFESSIONAL)
                        </b>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="3">
                                    <b>DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS </b>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="4" style="width: 50%;">
                                    <div class="row">
                                        <div class="col-8 text-center">
                                            <br><br><br><br>
                                            _______________________
                                            <br>
                                            <b>PROFESSIONAL ELECTRICAL ENGINEER</b>
                                            <br>
                                            (Signed and Sealed Over Printed Name)
                                        </div>
                                        <div class="col-4">
                                            <br><br><br><br>
                                            Date________________
                                        </div>
                                    </div>
                                </td>
                                <td colspan="2">
                                    Address
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%;">
                                    PRC. No.
                                </td>
                                <td style="width: 25%;">
                                    Validity
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    PTR. No.
                                </td>
                                <td>
                                    Date Issued
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Issued At
                                </td>
                                <td>
                                    T.I.N.
                                </td>
                            </tr>
                            
                        </table>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 3
                                </b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <table border="2" width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                SUPERVISOR / IN-CHARGE OF ELECTRICAL WORKS  
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col-4">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    <b>PROFESSIONAL ELECTRICAL ENGINEER</b>
                                                </div>
                                                <div class="col-4">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    <b>REGISTERED ELECTRICAL ENGINEER</b>
                                                </div>
                                                <div class="col-4">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    <b>REGISTERED MASTER ELECTRICIAN</b>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-8 text-center">
                                                    <br><br><br>
                                                    _________________________________
                                                    <br>
                                                    (Signed and Sealed Over Printed Name)
                                                </div>
                                                <div class="col-4">
                                                    <br><br><br>
                                                    Date _____________________
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            PRC No.
                                        </td>
                                        <td>
                                            Validity
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PTR No.
                                        </td>
                                        <td>
                                            Date Issued
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Issued at
                                        </td>
                                        <td>
                                            T.I.N
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Address:
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 4
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 5
                                </b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>BUILDING OWNER</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <br><br><br>
                                            ________________________________
                                            <br>
                                            (Signature Over Printed Name)
                                            <br>
                                            Date_____________________________
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Address:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    C.T.C No.
                                    <br><br>
                                </td>
                                <td>
                                    Date Issued
                                    <br><br>
                                </td>
                                <td>
                                    Place Issued
                                    <br><br>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-6">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            WITH MY CONSENT:   <b> LOT OWNER</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <br><br><br>
                                            ________________________________
                                            <br>
                                            (Signature Over Printed Name)
                                            <br>
                                            Date_____________________________
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Address:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    C.T.C No.
                                    <br><br>
                                </td>
                                <td>
                                    Date Issued
                                    <br><br>
                                </td>
                                <td>
                                    Place Issued
                                    <br><br>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row watermarked" style="page-break-after: always;">
    <div class="col-12">
        
        <div class="row">
            <div class="col-12">
                <b>
                    TO BE ACCOMPLISHED BY THE PERMITS AND LICENSES DIVISION
                    <br>
                    BOX 6 
                </b>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td>
                            Received By:
                            <br><br>
                        </td>
                        <td>
                            Date:
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <b>FIVE (5) SETS OF A3 ELECTRICAL DOCUMENTS</b>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-6">
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    ELECTRICAL PLANS AND SPECIFICATIONS
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    SPECIAL FIXTURES AND EQUIPMENT
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    PROPOSED STARTING DATE OF INSTALLATION/CONSTRUCTION 
                                </div>
                                <div class="col-6">
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    EXPECTED DATE OF COMPLETION/INSTALLATION/CONSTRUCTION 
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    OTHERS (Specify) ______________________________
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <b>
                    BOX 8
                </b>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td colspan="6">
                            <b>PROGRESS FLOW</b>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="2">
                            
                        </td> 
                        <td colspan="2" class="text-center">
                            IN
                        </td>
                        <td colspan="2" class="text-center">
                            OUT
                        </td>
                        <td rowspan="2" class="text-center">
                            PROCESSED BY:
                        </td> 
                    </tr>
                    <tr class="text-center">
                        <td>DATE</td>
                        <td>TIME</td>
                        <td>DATE</td>
                        <td>TIME</td>
                    </tr>
                    <tr>
                        <td>
                            ELECTRICAL
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td rowspan="3">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <br><br><br>
                                    _____________________
                                    (Signature Over Printed Name)
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4"></div>
                                <div class="col-8">
                                    PRC Reg No.____________
                                    <br>
                                    Validity:_______________
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            OTHERS (Specify)
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td style="padding-left: 20px; padding-right: 20px;">
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    ACTION TAKEN:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    PERMIT IS HEREBY ISSUED SUBJECT TO THE FOLLOWING:
                                </div>
                            </div>
                            <div class="row" style="font-size: 12pt;">
                                <div class="col-12">
                                    1.  That the proposed electrical works shall be in accordance with the electrical plans filed with this Office and in conformity with the provisions of the latest Philippine Electrical Codes, the National Building Code and its IRR.
                                    <br><br>
                                    2.  That prior to any electrical installation, the Owner/Permittee shall submit a duly accomplished prescribed Notice of Construction to the Office of the Building Official.
                                    <br><br>
                                    3.  That for installed electrical capacity of 200 amperes and above at 230 volts nominal and above, a specialty electrical contractor duly licensed by the Philippine Contractors Accreditation Board (PCAB) shall be required. 
                                    <br><br>
                                    4.  That a duly licensed electrical practitioner shall be in-charge of the installation, and that upon completion of the electrical works, he shall submit the entry of the logbook duly signed and sealed to the OBO including as-built plans and other documents.  He shall also accomplish the Certificate of Completion stating that the electrical works conform to the provisions of the Philippine Electrical Codes, the National Building Code and its IRR.
                                    <br><br>
                                    5.  That this permit is null and void unless accompanied by the building permit except for projects involving purely electrical works in which case only the building permit number of the existing building/structure shall be required.
                                    <br><br>
                                    6.  That a Certificate of Final Electrical Inspection (CFEI) shall be secured prior to the actual occupancy of the building.
                                    <br><br>
                                    7.  That this permit shall be posted at the door or site of work.
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    PERMIT ISSUED BY:
                                </div>
                            </div> 
                            <br><br>
                            <br><br><br><br><br><br><br>
                            
                            <div class="row">
                                <div class="col-12 text-center" style="font-weight: 600; font-size: 18pt;">
                                    _______________________________
                                    <br>
                                    BUILDING OFFICIAL
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 12pt;">
                                    (Signature Over Printed Name)
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 15pt;">
                                    Date _____________
                                </div>
                            </div> 
                            <br><br><br><br><br><br><br><br><br><br><br><br>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>