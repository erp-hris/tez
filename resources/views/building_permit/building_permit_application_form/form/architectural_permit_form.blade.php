<div class="row watermarked" style="page-break-after: always;">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        NBC FORM NO.  A - 01               
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12">
                        <img src="{{ asset('img/tieza-logo.png') }}" height="150px">
                        <br>
                        <span class="hdr3">
                            Republic of the Philippines
                        </span>
                        <br>
                        <span class="hdr1">
                            Tourism Infrastructure and Enterprise Zone Authority
                        </span>
                        <br>
                        <span class="hdr1">
                            Office of the Building Official
                        </span>
                        <br>
                        
                        <span class="hdr3">
                            <b>
                                ARCHITECTURAL PERMIT
                            </b>
                        </span>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-4">
                        APPLICATION NO.
                        <br>
                        <table border="2" width="100%">
                            <tr class="text-center">
                                @for($i = 0; $i < strlen($data->application_no); $i++)
                                <td><b>{{ $data->application_no[$i] }}</b></td>
                                @endfor
                            </tr>
                        </table>
                    </div>
                    <div class="col-4">
                        AP NO.
                        <br>
                        <table border="2" width="100%">
                            <tr class="text-center">
                                @for($i = 0; $i < 9; $i++)
                                <td><b>&nbsp;</b></td>
                                @endfor
                            </tr>
                        </table>
                    </div>
                    <div class="col-4">
                        BUILDING PERMIT NO.
                        <br>
                        <table border="2" width="100%">
                            <tr class="text-center">
                                @for($i = 0; $i < strlen($data->permit_no); $i++)
                                <td><b>{{ $data->permit_no[$i] }}</b></td>
                                @endfor
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <b>
                            BOX 1 (TO BE ACCOMPLISHED IN PRINT BY THE OWNER/APPLICANT)
                        </b>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-3">
                                            OWNER/APPLICANT
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-3">
                                            LAST NAME
                                            <br>
                                            <br>
                                            <b>{{ $data->last_name }}</b>
                                        </div>
                                        <div class="col-4">
                                            FIRST NAME
                                            <br>
                                            <br>
                                            <b>{{ $data->first_name }}</b>
                                        </div>
                                        <div class="col-2">
                                            M.I
                                            <br>
                                            <br>
                                            <b>{{ $data->middle_name }}</b>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 25%;">
                                    <div class="row">
                                        <div class="col-12">
                                            TIN
                                            <br>
                                            <br>
                                            <b>{{ $data->tin }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 35%;">
                                    <div class="row">
                                        <div class="col-12">
                                            FOR CONSTRUCTION OWNED
                                            <br>
                                            BY AN ENTERPRISE
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            FORM OF OWNERSHIP
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            USE OR CHARACTER OF OCCUPANCY
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-2">
                                            ADDRESS:
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-1">
                                            NO.,
                                            <br>
                                            <br>
                                            <b>{{ $data->house_number }}</b>
                                        </div>
                                        <div class="col-2">
                                            STREET,
                                            <br>
                                            <br>
                                            <b>{{ $data->street }}</b>
                                        </div>
                                        <div class="col-2">
                                            BARANGAY,
                                            <br>
                                            <br>
                                            <b>{{ $data->barangay }}</b>
                                        </div>
                                        <div class="col-3">
                                            CITY/MUNICIPALITY
                                            <br>
                                            <br>
                                            <b>{{ $data->city }}</b>
                                        </div>
                                        <div class="col-2">
                                            ZIP CODE
                                            <br>
                                            <br>
                                            <b>{{ $data->zip_code }}</b>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 25%;">
                                    <div class="row">
                                        <div class="col-12">
                                            TELEPHONE NO.
                                            <br>
                                            <br>
                                            <b>{{ $data->telephone_number }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-4">
                                            LOCATION OF CONSTRUCTION:
                                        </div>
                                        <div class="col-2">
                                            LOT NO. <u><b>{{ $data->lot_number }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            BLK NO. <u><b>{{ $data->block_no }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            TCT NO. <u><b>{{ $data->tct_no }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            TAX DEC. NO. <u><b>{{ $data->tax_declaration_no }}</b></u>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            STREET <u><b>{{ $data->construction_street }}</b></u>
                                        </div>
                                        <div class="col-4">
                                            BARANGAY <u><b>{{ $data->construction_barangay }}</b></u>
                                        </div>
                                        <div class="col-4">
                                            CITY/ MUNICIPALITY OF <u><b>{{ $data->construction_city }}</b></u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>SCOPE OF WORK</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <input type="checkbox" id="2_work_scope_1" name="" disabled>
                                            &nbsp;&nbsp;
                                            NEW CONSTRUCTION
                                            <br>

                                            <input type="checkbox" id="2_work_scope_2" name="" disabled>
                                            &nbsp;&nbsp;
                                            ERECTION
                                            <br>

                                            <input type="checkbox" id="2_work_scope_3" name="" disabled>
                                            &nbsp;&nbsp;
                                            ADDITION
                                            <br>

                                            <input type="checkbox" id="2_work_scope_4" name="" disabled>
                                            &nbsp;&nbsp;
                                            ALTERATION
                                            <br>
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" id="2_work_scope_5" name="" disabled>
                                            &nbsp;&nbsp;
                                            RENOVATION
                                            <br>

                                            <input type="checkbox" id="2_work_scope_6" name="" disabled>
                                            &nbsp;&nbsp;
                                            CONVERSION
                                            <br>

                                            <input type="checkbox" id="2_work_scope_7" name="" disabled>
                                            &nbsp;&nbsp;
                                            REPAIR
                                            <br>

                                            <input type="checkbox" id="2_work_scope_8" name="" disabled>
                                            &nbsp;&nbsp;
                                            MOVING
                                            <br>
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" id="2_work_scope_9" name="" disabled>
                                            &nbsp;&nbsp;
                                            RAISING
                                            <br>
                                            <input type="checkbox" id="2_work_scope_10" name="" disabled>
                                            &nbsp;&nbsp;
                                            DEMOLITION
                                            <br>
                                            <input type="checkbox" id="2_work_scope_11" name="" disabled>
                                            &nbsp;&nbsp;
                                            ACCESSORY BUILDING/STRUCTURE
                                            <br>

                                            <input type="checkbox" id="2_work_scope_12" name="" disabled>
                                            &nbsp;&nbsp;
                                            OTHERS (Specify)
                                            <br>

                                            
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <b>
                            BOX 2 (TO BE ACCOMPLISHED BY THE DESIGN PROFESSIONAL)
                        </b>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>
                                                1.  ARCHITECTURAL FACILITIES AND OTHER FEATURES PURSUANT TO BATAS PAMBANSA BILANG 344, REQUIRING CERTAIN BUILDINGS, INSTITUTIONS, ESTABLISHMENTS AND PUBLIC UTILITIES TO INSTALL FACILITIES AND OTHER DEVICES.
                                            </b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            @php
                                                echo create_checkbox('STAIRS', '2_architectural_facilities_1');
                                                echo '<br>';
                                                echo create_checkbox('WALKWAYS', '2_architectural_facilities_2');
                                                echo '<br>';
                                                echo create_checkbox('CORRIDORS', '2_architectural_facilities_3');
                                                echo '<br>';
                                                echo create_checkbox('DOORS, ENTRANCES & THRESHOLDS', '2_architectural_facilities_4');
                                            @endphp
                                        </div>
                                        <div class="col-3">
                                            @php
                                                echo create_checkbox('WASH ROOMS AND TOILETS', '2_architectural_facilities_5');
                                                echo '<br>';
                                                echo create_checkbox('LIFTS/ELEVATORS', '2_architectural_facilities_6');
                                                echo '<br>';
                                                echo create_checkbox('RAMPS', '2_architectural_facilities_7');
                                                echo '<br>';
                                                echo create_checkbox('PARKING AREAS', '2_architectural_facilities_8');
                                            @endphp
                                        </div>
                                        <div class="col-3">
                                            @php
                                                echo create_checkbox('SWITCHES, CONTROLS & BUZZERS', '2_architectural_facilities_9');
                                                echo '<br>';
                                                echo create_checkbox('HANDRAILS', '2_architectural_facilities_10');
                                                echo '<br>';
                                                echo create_checkbox('THRESHOLDS', '2_architectural_facilities_11');
                                                echo '<br>';
                                                echo create_checkbox('FLOOR FINISHES', '2_architectural_facilities_12');
                                            @endphp
                                        </div>
                                        <div class="col-3">
                                            @php
                                                echo create_checkbox('DRINKING FOUNTAINS', '2_architectural_facilities_13');
                                                echo '<br>';
                                                echo create_checkbox('PUBLIC TELEPHONES', '2_architectural_facilities_14');
                                                echo '<br>';
                                                echo create_checkbox('SEATING ACCOMMODATIONS', '2_architectural_facilities_15');
                                                echo '<br>';
                                                echo create_checkbox('OTHERS (Specify)', '2_architectural_facilities_16');
                                                echo '&nbsp;&nbsp;<u><b>'.$architectural_permit->other_conformance.'</b></u>';
                                            @endphp
                                        </div>
                                    </div>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40%;">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>2. PERCENTAGE OF SITE OCCUPANCY</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            PERCENTAGE OF BUILDING FOOTPRINT <u><b>{{ $architectural_permit->building_percentage }}</b></u>%   
                                            <br>
                                            PERCENTAGE OF IMPERVIOUS SURFACE AREA <u><b>{{ $architectural_permit->impervious_surface_area }}</b></u>% 
                                            <br>
                                            PERCENTAGE OF UNPAVED SURFACE AREA <u><b>{{ $architectural_permit->unpaved_surface_area }}</b></u>% 
                                            <br>
                                            OTHERS (Specify) <u><b>{{ $architectural_permit->others_sit_occupancy }}</b></u>


                                        </div>
                                    </div>
                                </td>
                                <td style="width: 60%;">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>3. CONFORMANCE TO FIRE CODE OF THE PHILIPPINES (P.D. 1185)</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            @php
                                                echo create_checkbox('NUMBER AND WIDTH OF EXIT DOORS', '2_conformance_1');
                                                echo '<br>';
                                                echo create_checkbox('WIDTH OF CORRIDOR', '2_conformance_2');
                                                echo '<br>';
                                                echo create_checkbox('DISTANCE TO FIRE EXITS', '2_conformance_3');
                                                echo '<br>';
                                                echo create_checkbox('ACCESS TO PUBLIC STREET', '2_conformance_4');
                                            @endphp
                                        </div>
                                        <div class="col-6">
                                            @php
                                                echo create_checkbox('FIRE WALLS', '2_conformance_5');
                                                echo '<br>';
                                                echo create_checkbox('FIRE FIGHTING AND SAFETY FACILITIES', '2_conformance_6');
                                                echo '<br>';
                                                echo create_checkbox('SMOKE DETECTORS', '2_conformance_7');
                                                echo '<br>';
                                                echo create_checkbox('EMERGENCY LIGHTS', '2_conformance_8');
                                                echo '<br>';
                                                echo create_checkbox('OTHERS', '2_conformance_9');
                                                echo '&nbsp;&nbsp;<u><b>'.$architectural_permit->other_conformance.'</b></u>';
                                            @endphp
                                        </div>
                                        
                                    </div>
                                </td>
                                    
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 3
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 4
                                </b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <table border="2" width="100%">
                                    <tr>
                                        <td colspan="6">
                                            <b>
                                                DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <br><br>
                                                    <b><u>{{ $architectural_permit->civil_engineer }}</u></b>
                                                    <br>
                                                    ARCHITECT
                                                    <br>
                                                    (Signed and Sealed Over Printed Name)
                                                    <br>
                                                    Date ________
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            Address: <b>{{ $architectural_permit->civil_engineer_address }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            PRC No. <b>{{ $architectural_permit->civil_engineer_prc_no }}
                                        </td>
                                        <td colspan="3">
                                            Validity <b>{{ $architectural_permit->civil_engineer_prc_validity }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            IAPOA No.
                                        </td>
                                        <td colspan="2">
                                            Date Issued
                                        </td>
                                        <td colspan="2">
                                            Issued At
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            PTR No. <b>{{ $architectural_permit->civil_engineer_ptr_no }}
                                        </td>
                                        <td colspan="2">
                                            Date Issued <b>{{ $architectural_permit->civil_engineer_ptr_date_issued }}
                                        </td>
                                        <td colspan="2">
                                            Issued At <b>{{ $architectural_permit->civil_engineer_ptr_issued_at }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            T.I.N <b>{{ $architectural_permit->civil_engineer_tin }}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <table border="2" width="100%">
                                    <tr>
                                        <td colspan="6">
                                            <b>
                                                SUPERVISOR / IN-CHARGE OF ARCHITECTURAL WORKS
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <br><br>
                                                    _________________________________
                                                    <br>
                                                    ARCHITECT
                                                    <br>
                                                    (Signed and Sealed Over Printed Name)
                                                    <br>
                                                    Date ________
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            Address:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            PRC No.
                                        </td>
                                        <td colspan="3">
                                            Validity
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            IAPOA No.
                                        </td>
                                        <td colspan="2">
                                            Date Issued
                                        </td>
                                        <td colspan="2">
                                            Issued At
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            PTR No.
                                        </td>
                                        <td colspan="2">
                                            Date Issued
                                        </td>
                                        <td colspan="2">
                                            Issued At
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            T.I.N
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 5
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 6
                                </b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>BUILDING OWNER</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <br><br><br>
                                            ________________________________
                                            <br>
                                            (Signature Over Printed Name)
                                            <br>
                                            Date_____________________________
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Address:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    C.T.C No.
                                    <br><br>
                                </td>
                                <td>
                                    Date Issued
                                    <br><br>
                                </td>
                                <td>
                                    Place Issued
                                    <br><br>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-6">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            WITH MY CONSENT:   <b> LOT OWNER</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <br><br><br>
                                            ________________________________
                                            <br>
                                            (Signature Over Printed Name)
                                            <br>
                                            Date_____________________________
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Address:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    C.T.C No.
                                    <br><br>
                                </td>
                                <td>
                                    Date Issued
                                    <br><br>
                                </td>
                                <td>
                                    Place Issued
                                    <br><br>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row watermarked" style="page-break-after: always;">
    <div class="col-12">
        
        <div class="row">
            <div class="col-12">
                <b>
                    TO BE ACCOMPLISHED BY THE PERMITS AND LICENSES DIVISION
                    <br>
                    BOX 7 
                </b>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td>
                            Received By:
                            <br><br>
                        </td>
                        <td>
                            Date:
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <b>FIVE (5) SETS OF A3 ARCHITECTURAL DOCUMENTS</b>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-6">
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    VICINITY MAP/LOCATION PLAN WITHIN A TWO- KILOMETER RADIUS SITE DEVELOPMENT PLAN 
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    PERSPECTIVE
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    FLOOR PLANS
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    ELEVATIONS, AT LEAST FOUR (4)
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    SECTIONS, AT LEAST TWO (2)
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    CEILING PLANS SHOWING LIGHTING FIXTURES AND DIFFUSERS
                                </div>
                                <div class="col-6">
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    DETAILS OF RAMPS, PARKING FOR THE DISABLED, STAIRS, FIRE ESCAPES, CABINETS AND PARTITIONS
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp; SITE DEVELOPMENT PLAN, SCHEDULE OF DOORS AND WINDOWS
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    SCHEDULE OF FINISHES FOR FLOORS, CEILINGS AND WALLS
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    ARCHITECTURAL INTERIOR
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    SPECIFICATIONS
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    COST ESTIMATE
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    OTHERS (Specify) ______________________________
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <b>
                    BOX 8
                </b>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td colspan="6">
                            <b>PROGRESS FLOW</b>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="2">
                            
                        </td> 
                        <td colspan="2" class="text-center">
                            IN
                        </td>
                        <td colspan="2" class="text-center">
                            OUT
                        </td>
                        <td rowspan="2" class="text-center">
                            PROCESSED BY:
                        </td> 
                    </tr>
                    <tr class="text-center">
                        <td>DATE</td>
                        <td>TIME</td>
                        <td>DATE</td>
                        <td>TIME</td>
                    </tr>
                    <tr>
                        <td>
                            ARCHITECTURAL DRAWINGS
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            SPECIFICATIONS
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            OTHERS (Specify)
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td style="padding-left: 20px; padding-right: 20px;">
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    ACTION TAKEN:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    PERMIT IS HEREBY ISSUED SUBJECT TO THE FOLLOWING:
                                </div>
                            </div>
                            <div class="row" style="font-size: 12pt;">
                                <div class="col-12">
                                    1.  That under Article 1723 of the Civil Code of the Philippines, the architect (and engineer) who drew up the plans and specifications for the building/structure is responsible for damages if within fifteen (15) years from the completion of the building/structure, the same should collapse due to defect in the plans or specifications or defects in the ground. The engineer or architect who supervises the construction shall be solidarily liable with the contractor should the edifice collapse due to defect in the construction or the use of inferior materials.
                                    <br><br>
                                    2.  That  the proposed  architectural  works  shall  be in accordance  with the architectural  plans  filed with this Office  and in conformity with the latest Architectural Code of the Philippines, the National Building Code and its IRR.
                                    <br><br>
                                    3.  That prior to any construction activity, a duly accomplished prescribed <b>“Notice of Construction”</b> shall be submitted to the Office of the Building Official. 
                                    <br><br>
                                    4.  That upon completion  of the construction, the licensed full-time inspector and supervisor/in-charge  of construction  works shall  submit  the entry  to the logbook  duly signed  and sealed  to the building  official  including  as-built  plans  and other documents, and shall also accomplish  the Certificate  of Completion  stating that the architectural  works conform  to the provision of the Architectural Code, the National Building Code and its IRR.
                                    <br><br>
                                    5.  That this permit is null and void unless accompanied by the building permit.
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    PERMIT ISSUED BY:
                                </div>
                            </div> 
                            <br><br>
                            <br><br><br><br><br><br><br><br>
                            
                            <div class="row">
                                <div class="col-12 text-center" style="font-weight: 600; font-size: 18pt;">
                                    _______________________________
                                    <br>
                                    BUILDING OFFICIAL
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 12pt;">
                                    (Signature Over Printed Name)
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 15pt;">
                                    Date _____________
                                </div>
                            </div> 
                            <br><br><br><br><br><br><br><br><br><br><br><br>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>