
<div class="row watermarked" style="page-break-after: always; ">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <span>
                    NBC FORM NO. B-01
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <div class="row">
                    <div class="col-12">
                        
                        <br>
                        <img src="{{ asset('img/tieza-logo.png') }}" height="150px">
                        <br>
                        <span class="hdr3">
                            Republic of the Philippines
                        </span>
                        <br>
                        <span class="hdr1">
                            Tourism Infrastructure and Enterprise Zone Authority
                        </span>
                        <br>
                        <span class="hdr1">
                            Office of the Building Official
                        </span>
                        <br>
                        
                        <span class="hdr3">
                            <b>
                                APPLICATION FOR BUILDING PERMIT
                            </b>
                        </span>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-4">
                        <input type="checkbox" id="new" name="" disabled>
                        &nbsp;&nbsp;&nbsp;NEW
                    </div>
                    <div class="col-4">
                        <input type="checkbox" id="renewal" name="" disabled>
                        &nbsp;&nbsp;&nbsp;RENEWAL
                    </div>
                    <div class="col-4">
                        <input type="checkbox" id="amendatory" name="" disabled>
                        &nbsp;&nbsp;&nbsp;AMENDATORY
                    </div>
                </div>
            </div>
            
        </div>
        <br>
        <div class="row">
            <div class="col-6">
                <b>
                    BOX 1 (TO BE ACCOMPLISHED IN PRINT BY THE APPLICANT) 
                </b>
            </div>
            <div class="col-3 text-right">
                APPLICATION NO.
            </div>
            <div class="col-3 text-right" style="padding-right: 20px; margin-bottom: 1px;">
                <table border="2" width="100%">
                    <tr class="text-center">
                        @for($i = 0; $i < strlen($data->application_no); $i++)
                        <td><b>{{ $data->application_no[$i] }}</b></td>
                        @endfor
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-3">
                                    OWNER/APPLICANT
                                    <br>
                                    <br>
                                </div>
                                <div class="col-3">
                                    LAST NAME
                                    <br>
                                    <br>
                                    <b>{{ $data->last_name }}</b>
                                </div>
                                <div class="col-4">
                                    FIRST NAME
                                    <br>
                                    <br>
                                    <b>{{ $data->first_name }}</b>
                                </div>
                                <div class="col-2">
                                    M.I
                                    <br>
                                    <br>
                                    <b>{{ $data->middle_name }}</b>
                                </div>
                            </div>
                        </td>
                        <td style="width: 25%;">
                            <div class="row">
                                <div class="col-12">
                                    TIN
                                    <br>
                                    <br>
                                    <b>{{ $data->tin }}</b>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 35%;">
                            <div class="row">
                                <div class="col-12">
                                    FOR CONSTRUCTION OWNED
                                    <br>
                                    BY AN ENTERPRISE
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-12">
                                    FORM OF OWNERSHIP
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-2">
                                    ADDRESS:
                                    <br>
                                    <br>
                                    <br>
                                </div>
                                <div class="col-1">
                                    NO.,
                                    <br>
                                    <br>
                                    <b>{{ $data->house_number }}</b>
                                </div>
                                <div class="col-2">
                                    STREET,
                                    <br>
                                    <br>
                                    <b>{{ $data->street }}</b>
                                </div>
                                <div class="col-2">
                                    BARANGAY,
                                    <br>
                                    <br>
                                    <b>{{ $data->barangay }}</b>
                                </div>
                                <div class="col-3">
                                    CITY/MUNICIPALITY
                                    <br>
                                    <br>
                                    <b>{{ $data->city }}</b>
                                </div>
                                <div class="col-2">
                                    ZIP CODE
                                    <br>
                                    <br>
                                    <b>{{ $data->zip_code }}</b>
                                </div>
                            </div>
                        </td>
                        <td style="width: 25%;">
                            <div class="row">
                                <div class="col-12">
                                    TELEPHONE NO.
                                    <br>
                                    <br>
                                    <b>{{ $data->telephone_number }}</b>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                                

                            <div class="row">
                                <div class="col-4">
                                    LOCATION OF CONSTRUCTION:
                                </div>
                                <div class="col-2">
                                    LOT NO. <u><b></b></u>
                                </div>
                                <div class="col-2">
                                    BLK NO. <u><b></b></u>
                                </div>
                                <div class="col-2">
                                    TCT NO. <u><b></b></u>
                                </div>
                                <div class="col-2">
                                    TAX DEC. NO. <u><b></b></u>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-4">
                                    STREET <u><b></b></u>
                                </div>
                                <div class="col-4">
                                    BARANGAY <u><b></b></u>
                                </div>
                                <div class="col-4">
                                    CITY/ MUNICIPALITY OF <u><b></b></u>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-12">
                                    <b>SCOPE OF WORK</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <input type="checkbox" id="work_scope_1" name="" disabled>
                                    &nbsp;&nbsp;
                                    NEW CONSTRUCTION
                                    <br>

                                    <input type="checkbox" id="work_scope_2" name="" disabled>
                                    &nbsp;&nbsp;
                                    ERECTION
                                    <br>

                                    <input type="checkbox" id="work_scope_3" name="" disabled>
                                    &nbsp;&nbsp;
                                    ADDITION
                                    <br>

                                    <input type="checkbox" id="work_scope_4" name="" disabled>
                                    &nbsp;&nbsp;
                                    ALTERATION
                                    <br>
                                </div>
                                <div class="col-4">
                                    <input type="checkbox" id="work_scope_5" name="" disabled>
                                    &nbsp;&nbsp;
                                    RENOVATION
                                    <br>

                                    <input type="checkbox" id="work_scope_6" name="" disabled>
                                    &nbsp;&nbsp;
                                    CONVERSION
                                    <br>

                                    <input type="checkbox" id="work_scope_7" name="" disabled>
                                    &nbsp;&nbsp;
                                    REPAIR
                                    <br>

                                    <input type="checkbox" id="work_scope_8" name="" disabled>
                                    &nbsp;&nbsp;
                                    MOVING
                                    <br>
                                </div>
                                <div class="col-4">
                                    <input type="checkbox" id="work_scope_9" name="" disabled>
                                    &nbsp;&nbsp;
                                    RAISING
                                    <br>
                                    <input type="checkbox" id="work_scope_10" name="" disabled>
                                    &nbsp;&nbsp;
                                    DEMOLITION
                                    <br>
                                    <input type="checkbox" id="work_scope_11" name="" disabled>
                                    &nbsp;&nbsp;
                                    ACCESSORY BUILDING/STRUCTURE
                                    <br>

                                    <input type="checkbox" id="work_scope_12" name="" disabled>
                                    &nbsp;&nbsp;
                                    OTHERS (Specify)
                                    <br>

                                    
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-12">
                                    <b>USE OR CHARACTER OF OCCUPANCY</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-5">
                                    <input type="checkbox" id="occupancy_character_1" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP A : RESIDENTIAL, DWELLINGS
                                    <br>

                                    <input type="checkbox" id="occupancy_character_2" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP B : RESIDENTIALS HOTELS AND APARTMENTS
                                    <br>

                                    <input type="checkbox" id="occupancy_character_3" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP C : EDUCATIONAL, RECREATIONAL
                                    <br>

                                    <input type="checkbox" id="occupancy_character_4" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP D : INSTITUTIONAL
                                    <br>

                                    <input type="checkbox" id="occupancy_character_5" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP E : BUSINESS AND MERCANTILE
                                    <br>

                                    <input type="checkbox" id="occupancy_character_6" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP F : INDUSTRIAL
                                    <br>
                                </div>
                                <div class="col-7">
                                    

                                    <input type="checkbox" id="occupancy_character_7" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP G : INDUSTRIAL STORAGE AND HAZARDOUS
                                    <br>

                                    <input type="checkbox" id="occupancy_character_8" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP H : RECREATIONAL, ASSEMBLY OCCUPANT LOAD LESS THAN 1000
                                    <br>

                                    <input type="checkbox" id="occupancy_character_9" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP I : RECREATIONAL, ASSEMBLY OCCUPANT LOAD 1000 OR MORE
                                    <br>

                                    <input type="checkbox" id="occupancy_character_10" name="" disabled>
                                    &nbsp;&nbsp;
                                    GROUP J : AGRICULTURAL, ACCESSORY
                                    <br>

                                    <input type="checkbox" id="occupancy_character_11" name="" disabled>
                                    &nbsp;&nbsp;
                                    OTHERS (Specify)
                                    <br>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-6">
                                    OCCUPANCY CLASSIFIED <u><b>{{ $data->occupancy_classification }}</b></u>
                                    <br>
                                    NUMBER OF UNITS <u><b>{{ $data->units }}</b></u>
                                    <br>
                                    TOTAL FLOOR AREA <u><b>{{ $data->floor_area }}</b></u> SQUARE METERS
                                    <br>
                                </div>
                                <div class="col-6">
                                    TOTAL ESTIMATED COST P <u><b>{{ $data->estimated_cost }}</b></u>
                                    <br>
                                    PROPOSED DATE OF CONSTRUCTION <u><b>{{ $data->construction_date }}</b></u>
                                    <br>
                                    EXPECTED DATE OF COMPLETION <u><b>{{ $data->expected_completion_date }}</b></u>
                                    <br>
                                    
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            
        </div>
        <div class="row">
            <div class="col-12">
                <b>
                    BOX 2
                </b>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-12">
                                    <b>
                                        FULL-TIME INSPECTOR AND SUPERVISOR OF CONSTRUCTION WORKS (REPRESENTING THE OWNER
                                    </b>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="4" style="width: 50%;">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <br>
                                    <b><u>{{ $data->architect_name }}</u></b>
                                    <br>
                                    ARCHITECT OR CIVIL ENGINEER
                                    <br>
                                    (Signed and Sealed Over Printed Name)
                                    <br>
                                    Date_____________________________
                                </div>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-12">
                                    Address
                                    <br>
                                    <b><u>{{ $data->architect_address }}</u></b>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            <div class="row">
                                <div class="col-12">
                                    PRC No. <b><u>{{ $data->architect_prc_no }}</u></b>
                                </div>
                            </div>
                        </td>
                        <td style="width: 25%;">
                            <div class="row">
                                <div class="col-12">
                                    Validity <b><u>{{ $data->architect_prc_validity }}</u></b>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-12">
                                    PTR No. <b><u>{{ $data->architect_ptr_no }}</u></b>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-12">
                                    Date Issued <b><u>{{ $data->architect_ptr_issued_date }}</u></b>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-12">
                                    Issued at <b><u>{{ $data->architect_id_issued_at }}</u></b>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-12">
                                    TIN <b><u>{{ $data->architect_tin }}</u></b>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <b>BOX 3</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>APPLICANT:</b>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-8 text-center">
                                            <div style="border-bottom: 1px solid black; height: 40%;">
                                                <b>{{ $data->first_name }} {{ $data->last_name }}</b>
                                            </div>
                                            (Signature Over Printed Name) 
                                        </div>
                                        <div class="col-4">
                                            Date <b>{{ date('F d, Y', time()) }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>Address</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33.33%;">
                                    CTC No
                                </td>
                                <td style="width: 33.33%;">
                                    Date Issued
                                </td>
                                <td style="width: 33.33%;">
                                    Place Issued
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <b>BOX 4</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            WITH MY CONSENT: <b>LOT OWNER</b>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-8 text-center">
                                            <div style="border-bottom: 1px solid black; height: 40%;">
                                                <b>{{ $data->lot_owner_name }}</b>
                                            </div>
                                            (Signature Over Printed Name) 
                                        </div>
                                        <div class="col-4">
                                            Date <b>{{ date('F d, Y', time()) }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>Address</b>
                                            <b>{{ $data->lot_owner_address }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33.33%;">
                                    CTC No<b> {{ $data->lot_owner_ctc_no }}</b>
                                </td>
                                <td style="width: 33.33%;">
                                    Date Issued <b>{{ $data->lot_owner_ctc_issued_date }}</b>
                                </td>
                                <td style="width: 33.33%;">
                                    Place Issued <b>{{ $data->lot_owner_ctc_issued_place }}</b>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <b>BOX 5</b>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table border="2" width="100%;">
                    <tr>
                        <td style="font-size: 10pt !important;">
                            <div class="row">
                                <div class="col-12">
                                    REPUBLIC OF THE PHILIPPINES
                                    <br>
                                    CITY/MUNICIPALITY OF (__________________________________________)
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12" style="text-indent: 5%; text-align: justify;">
                                    BEFORE ME, at the City/Municipality of _____________________________________________________ , on _________________________ personally appeared the following:
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-1">
                                    
                                </div>
                                <div class="col-3 text-center">
                                    <div style="border-bottom: 1px solid black;">&nbsp;</div>
                                    APPLICANT
                                </div>
                                <div class="col-2 text-center">
                                    <div style="border-bottom: 1px solid black;">&nbsp;</div>
                                    C.T.C. No
                                </div>
                                <div class="col-2 text-center">
                                    <div style="border-bottom: 1px solid black;">&nbsp;</div>
                                    Date Issued
                                </div>
                                <div class="col-3 text-center">
                                    <div style="border-bottom: 1px solid black;">&nbsp;</div>
                                    Place Issued
                                </div>
                                <div class="col-1">
                                    
                                </div>
                            </div>
                            <br>
                            <div class="row" style="font-size: 8pt;">
                                <div class="col-1">
                                    
                                </div>
                                <div class="col-3 text-center">
                                    <div style="border-bottom: 1px solid black;">&nbsp;</div>
                                    LICENSED ARCHITECT OR CIVIL ENGINEER
                                    <br>
                                    <span style="font-size: 6pt;">
                                        (Full-Time Inspector and Supervisor of Construction Works)
                                    </span>
                                </div>
                                <div class="col-2 text-center">
                                    <div style="border-bottom: 1px solid black;">&nbsp;</div>
                                    C.T.C. No
                                </div>
                                <div class="col-2 text-center">
                                    <div style="border-bottom: 1px solid black;">&nbsp;</div>
                                    Date Issued
                                </div>
                                <div class="col-3 text-center">
                                    <div style="border-bottom: 1px solid black;">&nbsp;</div>
                                    Place Issued
                                </div>
                                <div class="col-1">
                                    
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-12" style="text-align: justify;">
                                    whose signatures appear hereinabove, known to me to be the same persons who executed this standard prescribed form and acknowledged to me that the same is their free and voluntary act and deed.
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12" style="text-align: justify; text-indent: 5%;">
                                    WITNESS MY HAND AND SEAL on the date and place above written
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-8">
                                    Doc No.
                                    <br>
                                    Page No.
                                    <br>
                                    Book No.
                                    <br>
                                    Series of.
                                </div>
                                <div class="col-4 text-center">
                                    <br><br>
                                    _________________________________
                                    <br>
                                    NOTARY PUBLIC (Until December _____________ )
                                </div>
                            </div>
                            
                        </td>
                    </tr>
                    
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row watermarked" style="page-break-after: always;">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <b>BOX 6 (TO BE ACCOMPLISHED BY THE PERMITS AND LICENSES DIVISION)</b>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr class="text-center" style="font-weight: 600;">
                        <td>
                            ASSESSED FEES
                        </td>
                        <td>
                            ASSESSED BY
                        </td>
                        <td>
                            AMOUNT DUE
                        </td>
                        <td>
                            DATE PAID
                        </td>
                        <td>
                            O.R NUMBER
                        </td>
                        <td>
                            NSO
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp; 
                            FILLING FEE   
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;    
                            PROCESSING FEE
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;    
                            LOCATIONAL/ZONING OF LAND USE
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp; 
                            LINE AND GRADE (Geodetic)   
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                            FENCING    
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp; 
                            ARCHITECTURAL   
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                            CIVIL/STRUCTURAL    
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                            ELECTRICAL    
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                            MECHANICAL    
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;    
                            SANITARY
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;  
                            PLUMBING  
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp; 
                            ELECTRONICS   
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;  
                            INTERIOR  
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp; 
                            ONE HALF (½) OF FIRE SERVICE FUND (FSF)   
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="1" class="text-center">TOTAL</td>
                        <td colspan="5"></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <b>BOX 7 (TO BE ACCOMPLISHED BY THE BUILDING OFFICIAL)</b>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td style="padding-left: 20px; padding-right: 20px;">
                           <div class="row">
                                <div class="col-12 text-center" style="font-weight: 600; font-size: 12pt;">
                                    BUILDING PERMIT
                                </div>
                           </div> 
                           <div class="row">
                                <div class="col-8">
                                    BUILDING PERMIT NO.
                                    <br>
                                    <table border="1">
                                        <tr>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                        </tr>
                                    </table>

                                </div>
                                <div class="col-4">
                                    OFFICIAL RECEIPT NO.
                                    <br>
                                    <table border="1">
                                        <tr>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>
                           </div>
                           <div class="row margin-top">
                                <div class="col-8">
                                    DATE ISSUED
                                    <br>
                                    <table border="1">
                                        <tr>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr class="text-center">
                                            <td style="width: 30px;">M</td>
                                            <td style="width: 30px;">M</td>
                                            <td style="width: 30px;">D</td>
                                            <td style="width: 30px;">D</td>
                                            <td style="width: 30px;">Y</td>
                                            <td style="width: 30px;">Y</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-4">
                                    DATE PAID
                                    <br>
                                    <table border="1">
                                        <tr>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                            <td style="width: 30px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr class="text-center">
                                            <td style="width: 30px;">M</td>
                                            <td style="width: 30px;">M</td>
                                            <td style="width: 30px;">D</td>
                                            <td style="width: 30px;">D</td>
                                            <td style="width: 30px;">Y</td>
                                            <td style="width: 30px;">Y</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-12" style="font-size: 10pt;">
                                    <p style="text-indent: 5%;">
                                        Permit is issued to __________________________ for the proposed _________________________________ under____________________, of Group_____, located at Lot No. _____Block No. ______ OCT/TCT No. ______, _______________Street, Barangay_________________, City/Municipality of_______________________ subject to the following:
                                    </p>
                                    <div class="row margin-top">
                                        <div class="col-12">
                                            <ol>
                                                <li>
                                                    That under Article 1723 of the Civil Code of the Philippines, the engineer or architect who drew up the plans and specifications for a building/structure is liable for damages if within fifteen (15) years from the completion of the building/structure, the same should collapse due to defect in the plans or specifications or defects in the ground. The engineer or architect who supervises the construction shall be solidarily liable with the contractor should the edifice collapse due to defect in the construction or the use of inferior materials.
                                                </li>
                                                <li>
                                                    This permit shall be accompanied by the various applicable ancillary and accessory permits, plans and specifications signed and sealed by the corresponding design professionals who shall be responsible for the comprehensive and correctness of the plans in compliance to the Code and its IRR and to all applicable referral codes and professional regulatory laws.
                                                </li>
                                                <li>
                                                    That the proposed construction/erection/addition/alteration/renovation/conversion/repair/moving/demolition, etc. shall be in conformity with the provisions of the National Building Code, and its IRR.
                                                    <ol type="a">
                                                        <li>
                                                            That prior to commencement of the proposed projects and construction an actual relocation survey shall be conducted by a duly licensed Geodetic Engineer.
                                                        </li>
                                                        <li>
                                                            That before commencing the excavation the person making or causing the excavation to be made shall notify in writing the owner of adjoining property not less than ten (10) days before such excavation is to be made and show how the adjoining property should be protected.
                                                        </li>
                                                        <li>
                                                            That no person shall use or occupy a street, alley or public sidewalk for the performance of work covered by a building permit.
                                                        </li>
                                                        <li>
                                                            That no person shall perform any work on any building or structure adjacent to a public way in general use for pedestrian travel, unless the pedestrians are protected.
                                                        </li>
                                                        <li>
                                                            That the supervising Architect/Civil Engineer shall keep at the jobsite at all times a logbook of daily construction activities wherein the actual daily progress of construction including tests conducted, weather condition and other pertinent data are to be recorded, same shall be made available for scrutiny and comments by the OBO representative during the conduct of his/her inspection pursuant to Section 207 of the National Building Code.
                                                        </li>
                                                        <li>
                                                            That upon completion of the construction, the said licensed supervising Architect/Civil Engineer shall submit to the Building Official duly signed and sealed logbook, as-built plans and other documents and shall also prepare and submit a Certificate of Completion of the project stating that the construction of the building/structure conform to the provision of the Code, its IRR as well as the plans and specifications.
                                                        </li>
                                                        <li>
                                                            All such changes, modifications and alterations shall likewise be submitted to the Building Official and the subsequent amendatory permit therefor issued before any work on said changes, modifications and alterations shall be started.  The as-built plans and specifications maybe just an orderly and comprehensive compilation of all documents which include the originally submitted plans and specifications of all amendments thereto as actually built or they may be an entirely new set of plans and specifications accurately describing and/or reflecting therein the building as actually built.
                                                        </li>
                                                    </ol>
                                                </li>
                                                <li>
                                                    That no building/structure shall be used until the Building Official has issued a Certificate of Occupancy therefor as provided in the Code. However, a partial Certificate of Occupancy may be issued for the Use/Occupancy of a portion or portions of a building/structure prior to the completion of the entire building/structure.
                                                </li>
                                                <li>
                                                    That this permit shall not serve as an exemption from securing written clearances from various government authorities exercising regulatory function affecting buildings/structures.
                                                </li>
                                                <li>
                                                    When the construction is undertaken by contract, the work shall be done by a duly licensed and registered contractor pursuant to the provisions of the Contractor’s License Law (RA 4566). 
                                                </li>
                                                <li>
                                                    The Owner/Permittee shall submit a duly accomplished prescribed “<b>Notice of Construction</b>” to the Office of the Building Official prior to any construction activity.
                                                </li>
                                                <li>
                                                    The Owner/Permittee shall put a Building Permit sign which complies with the prescribed dimensions and information, which shall remain posted on the construction site for the duration of the construction. 
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    PERMIT ISSUED BY:
                                </div>
                            </div> 
                            <br><br>
                            <div class="row">
                                <div class="col-12 text-center" style="font-weight: 600; font-size: 18pt;">
                                    _______________________________
                                    <br>
                                    BUILDING OFFICIAL
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 12pt;">
                                    (Signature Over Printed Name)
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 15pt;">
                                    Date _____________
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 15pt;">
                                    Note: This permit may be canceled or revoked pursuant to sections 305 and 306 of the National Building Code.
                                </div>
                            </div> 
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>