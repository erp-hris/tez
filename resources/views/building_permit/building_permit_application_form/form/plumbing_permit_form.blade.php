<div class="row watermarked" style="page-break-after: always;">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        NBC FORM NO. A-06                 
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12">
                        <img src="{{ asset('img/tieza-logo.png') }}" height="150px">
                        <br>
                        <span class="hdr3">
                            Republic of the Philippines
                        </span>
                        <br>
                        <span class="hdr1">
                            Tourism Infrastructure and Enterprise Zone Authority
                        </span>
                        <br>
                        <span class="hdr1">
                            Office of the Building Official
                        </span>
                        <br>
                        
                        <span class="hdr3">
                            <b>
                                PLUMBING PERMIT
                            </b>
                        </span>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-4">
                        APPLICATION NO.
                        <br>
                        |____| |____| |____| |____| |____| |____| |____| |____| |____| |____|
                    </div>
                    <div class="col-4">
                        PP NO.
                        <br>
                        |____| |____| |____| |____| |____| |____| |____| |____| |____|
                    </div>
                    <div class="col-4">
                        BUILDING PERMIT NO.
                        <br>
                        |____| |____| |____| |____| |____| |____| |____| |____| |____|
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <b>
                            BOX 1 (TO BE ACCOMPLISHED IN PRINT BY THE OWNER/APPLICANT)
                        </b>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-3">
                                            OWNER/APPLICANT
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-3">
                                            LAST NAME
                                            <br>
                                            <br>
                                            <b>{{ $data->last_name }}</b>
                                        </div>
                                        <div class="col-4">
                                            FIRST NAME
                                            <br>
                                            <br>
                                            <b>{{ $data->first_name }}</b>
                                        </div>
                                        <div class="col-2">
                                            M.I
                                            <br>
                                            <br>
                                            <b>{{ $data->middle_name }}</b>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 25%;">
                                    <div class="row">
                                        <div class="col-12">
                                            TIN
                                            <br>
                                            <br>
                                            <b>{{ $data->tin }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 35%;">
                                    <div class="row">
                                        <div class="col-12">
                                            FOR CONSTRUCTION OWNED
                                            <br>
                                            BY AN ENTERPRISE
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            FORM OF OWNERSHIP
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            USE OR CHARACTER OF OCCUPANCY
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="row">
                                        <div class="col-2">
                                            ADDRESS:
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-1">
                                            NO.,
                                            <br>
                                            <br>
                                            <b>{{ $data->house_number }}</b>
                                        </div>
                                        <div class="col-2">
                                            STREET,
                                            <br>
                                            <br>
                                            <b>{{ $data->street }}</b>
                                        </div>
                                        <div class="col-2">
                                            BARANGAY,
                                            <br>
                                            <br>
                                            <b>{{ $data->barangay }}</b>
                                        </div>
                                        <div class="col-3">
                                            CITY/MUNICIPALITY
                                            <br>
                                            <br>
                                            <b>{{ $data->city }}</b>
                                        </div>
                                        <div class="col-2">
                                            ZIP CODE
                                            <br>
                                            <br>
                                            <b>{{ $data->zip_code }}</b>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 25%;">
                                    <div class="row">
                                        <div class="col-12">
                                            TELEPHONE NO.
                                            <br>
                                            <br>
                                            <b>{{ $data->telephone_number }}</b>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-4">
                                            LOCATION OF CONSTRUCTION:
                                        </div>
                                        <div class="col-2">
                                            LOT NO. <u><b>{{ $data->lot_number }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            BLK NO. <u><b>{{ $data->block_no }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            TCT NO. <u><b>{{ $data->tct_no }}</b></u>
                                        </div>
                                        <div class="col-2">
                                            TAX DEC. NO. <u><b>{{ $data->tax_declaration_no }}</b></u>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            STREET <u><b>{{ $data->construction_street }}</b></u>
                                        </div>
                                        <div class="col-4">
                                            BARANGAY <u><b>{{ $data->construction_barangay }}</b></u>
                                        </div>
                                        <div class="col-4">
                                            CITY/ MUNICIPALITY OF <u><b>{{ $data->construction_city }}</b></u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="row">
                                        <div class="col-12">
                                            <b>SCOPE OF WORK</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <input type="checkbox" id="work_scope_1" name="" disabled>
                                            &nbsp;&nbsp;
                                            NEW CONSTRUCTION
                                            <br>

                                            <input type="checkbox" id="work_scope_2" name="" disabled>
                                            &nbsp;&nbsp;
                                            ERECTION
                                            <br>

                                            <input type="checkbox" id="work_scope_3" name="" disabled>
                                            &nbsp;&nbsp;
                                            ADDITION
                                            <br>

                                            <input type="checkbox" id="work_scope_4" name="" disabled>
                                            &nbsp;&nbsp;
                                            ALTERATION
                                            <br>
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" id="work_scope_5" name="" disabled>
                                            &nbsp;&nbsp;
                                            RENOVATION
                                            <br>

                                            <input type="checkbox" id="work_scope_6" name="" disabled>
                                            &nbsp;&nbsp;
                                            CONVERSION
                                            <br>

                                            <input type="checkbox" id="work_scope_7" name="" disabled>
                                            &nbsp;&nbsp;
                                            REPAIR
                                            <br>

                                            <input type="checkbox" id="work_scope_8" name="" disabled>
                                            &nbsp;&nbsp;
                                            MOVING
                                            <br>
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" id="work_scope_9" name="" disabled>
                                            &nbsp;&nbsp;
                                            RAISING
                                            <br>
                                            <input type="checkbox" id="work_scope_10" name="" disabled>
                                            &nbsp;&nbsp;
                                            DEMOLITION
                                            <br>
                                            <input type="checkbox" id="work_scope_11" name="" disabled>
                                            &nbsp;&nbsp;
                                            ACCESSORY BUILDING/STRUCTURE
                                            <br>

                                            <input type="checkbox" id="work_scope_12" name="" disabled>
                                            &nbsp;&nbsp;
                                            OTHERS (Specify)
                                            <br>

                                            
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <b>
                            BOX 2 (TO BE ACCOMPLISHED BY THE DESIGN PROFESSIONAL)
                        </b>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-12">
                        <table border="2" width="100%">
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            <b>
                                                FIXTURES TO BE INSTALLED
                                            </b>
                                        </div>
                                        
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    QTY
                                                </div>
                                                <div class="col-2">
                                                    NEW<br>FEATURES
                                                </div>
                                                <div class="col-2">
                                                    EXISTING<br>FEATURES
                                                </div>
                                                <div class="col-5">
                                                    KIND OF<br>FEATURES
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    QTY
                                                </div>
                                                <div class="col-2">
                                                    NEW<br>FEATURES
                                                </div>
                                                <div class="col-2">
                                                    EXISTING<br>FEATURES
                                                </div>
                                                <div class="col-5">
                                                    KIND OF<br>FEATURES
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row margin-top">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    WATER CLOSET
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    FLOOR DRAIN
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    LAVATORY
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    KITCHEN SINK
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    FAUCET
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    SHOWER HEAD
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    WATER METER
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    GREASE TRAP
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    BATH TUB
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    SLOP SINK
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;URINAL
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    AIR CONDITIONNING UNIT
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    WATER TANK/RESERVOIR
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    TOTAL
                                                </div>
                                                <div class="col-7"></div>
                                            </div>
                                            <div class="row margin-top">
                                                <div class="col-1 text-center">
                                                    
                                                </div>
                                                <div class="col-6">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;<b>WATER DISTRIBUTION SYSTEM</b>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;<b>SEWAGE SYSTEM</b>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    BIDETTE
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    LAUNDRY TRAYS
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    DENTAL CUSPIDOR
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    DRINKING FOUNTAIN
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    BAR SINK
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    SODA FOUNTAIN SINK
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    LABORATORY SINK
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    STERILIZER
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    OTHERS (Specify )
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    _______________________
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    _______________________
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-2">
                                                    <input type="checkbox" name="" disabled>
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                                    _______________________
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3 text-center">
                                                    ________
                                                </div>
                                                <div class="col-2">
                                                    TOTAL
                                                </div>
                                                <div class="col-7"></div>
                                            </div>
                                            <div class="row margin-top">
                                                <div class="col-1 text-center">
                                                    
                                                </div>
                                                <div class="col-5">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;<b>SEPTIC TANK</b>
                                                </div>
                                                <div class="col-6">
                                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;<b>STORM DRAINAGE SYSTEM</b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row margin-top">
                                        <div class="col-12">
                                            Prepared by:
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 3
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <b>
                                    BOX 4
                                </b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <table border="2" width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                DESIGN PROFESSIONAL, PLANS AND SPECIFICATIONS
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <br><br>
                                                    _________________________________
                                                    <br>
                                                    MASTER PLUMBER
                                                    <br>
                                                    (Signed and Sealed Over Printed Name)
                                                    <br>
                                                    Date ________
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Address:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PRC No.
                                        </td>
                                        <td>
                                            Validity
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PTR No.
                                        </td>
                                        <td>
                                            Date Issued
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Issued at
                                        </td>
                                        <td>
                                            T.I.N
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <table border="2" width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                SUPERVISOR / IN-CHARGE OF PLUMBING WORKS
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <br><br>
                                                    _________________________________
                                                    <br>
                                                    MASTER PLUMBER
                                                    <br>
                                                    (Signed and Sealed Over Printed Name)
                                                    <br>
                                                    Date ________
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Address:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PRC No.
                                        </td>
                                        <td>
                                            Validity
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PTR No.
                                        </td>
                                        <td>
                                            Date Issued
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Issued at
                                        </td>
                                        <td>
                                            T.I.N
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<div class="row watermarked" style="page-break-after: always;">
    <div class="col-12">
        <div class="row margin-top">
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <b>
                            BOX 5
                        </b>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <b>
                            BOX 6
                        </b>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-6">
                <table border="2" width="100%">
                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-12">
                                    <b>BUILDING OWNER</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <br><br><br>
                                    ________________________________
                                    <br>
                                    (Signature Over Printed Name)
                                    <br>
                                    Date_____________________________
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Address:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            C.T.C No.
                            <br><br>
                        </td>
                        <td>
                            Date Issued
                            <br><br>
                        </td>
                        <td>
                            Place Issued
                            <br><br>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-6">
                <table border="2" width="100%">
                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-12">
                                    WITH MY CONSENT:   <b> LOT OWNER</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <br><br><br>
                                    ________________________________
                                    <br>
                                    (Signature Over Printed Name)
                                    <br>
                                    Date_____________________________
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Address:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            C.T.C No.
                            <br><br>
                        </td>
                        <td>
                            Date Issued
                            <br><br>
                        </td>
                        <td>
                            Place Issued
                            <br><br>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <b>
                    TO BE ACCOMPLISHED BY THE PERMITS AND LICENSES DIVISION
                    <br>
                    BOX 7 
                </b>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td>
                            Received By:
                            <br><br>
                        </td>
                        <td>
                            Date:
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <b>FIVE (5) SETS OF A3 PLUMBING DOCUMENTS</b>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-6">
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    PLUMBING PLANS AND SPECIFICATIONS
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    BILL OF MATERIALS
                                </div>
                                <div class="col-6">
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    COST ESTIMATES
                                    <br>
                                    <input type="checkbox" name="" disabled>&nbsp;&nbsp;&nbsp;
                                    OTHERS (Specify) ______________________________
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <b>
                    BOX 8
                </b>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td colspan="6">
                            <b>PROGRESS FLOW</b>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="2">
                            
                        </td> 
                        <td colspan="2" class="text-center">
                            IN
                        </td>
                        <td colspan="2" class="text-center">
                            OUT
                        </td>
                        <td rowspan="2" class="text-center">
                            PROCESSED BY:
                        </td> 
                    </tr>
                    <tr class="text-center">
                        <td>DATE</td>
                        <td>TIME</td>
                        <td>DATE</td>
                        <td>TIME</td>
                    </tr>
                    <tr>
                        <td>
                            RECEIVING AND RECORDING
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            PLUMBING 
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            OTHERS (Specify)
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12">
                <table border="2" width="100%">
                    <tr>
                        <td style="padding-left: 20px; padding-right: 20px;">
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    ACTION TAKEN:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    PERMIT IS HEREBY ISSUED SUBJECT TO THE FOLLOWING:
                                </div>
                            </div>
                            <div class="row" style="font-size: 12pt;">
                                <div class="col-12">
                                    1.  That the proposed plumbing works shall be in accordance with the plumbing plans filed with this Office and in conformity with the Revised Plumbing Code of the Philippines, the National Building Code and its IRR.
                                    <br><br>
                                    2.  That prior to any commencement of plumbing works, a duly accomplished prescribed <b>“Notice of Construction”</b> shall be submitted to the Office of the Building Official.
                                    <br><br>
                                    3.  That upon completion of the plumbing works, the licensed supervisor/in-charge shall submit the entry to the logbook duly signed and sealed to the building official including as-built plans and other documents and shall also accomplish the Certificate of Completion stating that the plumbing works conform to the provision of the Revised Plumbing Code, the National Building Code and its IRR. 
                                    <br><br>
                                    4.  That this permit is null and void unless accompanied by the building permit.
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-12" style="font-weight: 600; font-size: 12pt;">
                                    PERMIT ISSUED BY:
                                </div>
                            </div> 
                            <br><br>
                            <br><br><br><br><br><br><br><br><br><br><br><br>
                            
                            <div class="row">
                                <div class="col-12 text-center" style="font-weight: 600; font-size: 18pt;">
                                    _______________________________
                                    <br>
                                    BUILDING OFFICIAL
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 12pt;">
                                    (Signature Over Printed Name)
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12 text-center" style="font-size: 15pt;">
                                    Date _____________
                                </div>
                            </div> 
                            <br><br><br><br><br><br><br><br><br><br><br><br>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>