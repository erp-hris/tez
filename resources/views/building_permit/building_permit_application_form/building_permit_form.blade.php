<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
    <link rel="stylesheet" href="{{ asset('beagle-assets/css/app.css') }}" type="text/css"/>
    <script type="text/javascript" src="{{ asset('js/qrcode.min.js') }}"></script>
    <style type="text/css">
        .img-border
        {
            height: 150px; 
            border: 1px solid black;
        }
        body
        {
            background: white;
            font-family: cambria;
            font-size: 9pt;
        }
        .hdr1
        {
            font-size: 14pt;
        }
        .hdr2
        {
            font-size: 18pt;
            font-family: Old English Text MT;
        }
        .hdr3
        {
            font-size: 14pt;
        }
        td 
        {
            font-size: 9pt;
            padding: 2px;
            padding-left: 5px;
        }
        .watermarked {
            background-image: url("{{ asset('img/tieza-logo-watermark.png') }}") !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
        }
        @page { 
            size: landscape;
        }
        .border-div {
            /*border-top: 1px solid black;*/
        }

        .border-div::after {
            content: "";
            position: absolute;
            bottom: 0;
            left: 20%; /* Start from the middle */
            width: 80%; /* Half width of the div */
            height: 1px; /* Thickness of the border */
            background-color: black; /* Color of the border */
            transform: translateX(-0%); /* Adjust to move it to the right */
        }
        .border-div-2::after {
            content: "";
            position: absolute;
            bottom: 0;
            left: 10%; /* Start from the middle */
            width: 90%; /* Half width of the div */
            height: 1px; /* Thickness of the border */
            background-color: black; /* Color of the border */
            transform: translateX(-0%); /* Adjust to move it to the right */
        }
        .border-bot-black {
            border-bottom: 1px solid black;
        }
        .border-bot-white {
            border-bottom: 5px solid #fcfafa;
        }
    </style>
</head>
<body>    
    @php
        function show_detail($str)
        {
            echo '<b style="font-size: 10pt; color: blue;">'.strtoupper($str).'</b>';
        }
    @endphp
    <div class="container-fluid watermarked">
        <div class="col-12">
            <div class="row margin-top" style="border: 3px solid black; min-height: 400px; padding: 10px;">
                <div class="col-12">
                    <div class="row margin-top">
                        <div class="col-12">
                            <b>NBC FORM NO. B - 018</b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12" style="padding-left:20px; padding-right: 20px;">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <img src="{{ asset('img/header_2021.png') }}" width="20%">
                                    <br>
                                    <span style="font-size: 12pt;">
                                        <b>OFFICE OF THE BUILDING OFFICIAL</b>
                                    </span>
                                    <br>
                                    <span style="font-size: 22pt; font-family: cooper black;"><b>BUILDING PERMIT</b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6">
                                    <div class="row">
                                        @php
                                            $application_type = $data->application_type;
                                            $new = '';
                                            $renewal = '';
                                            $amendatory = '';
                                            if($application_type == 1)
                                            {
                                                $new = 'checked';
                                            }
                                            else if ($application_type == 2)
                                            {
                                                $renewal = 'checked';
                                            }
                                            else if ($application_type == 3)
                                            {
                                                $amendatory = 'checked';
                                            }
                                        @endphp
                                        <div class="col-4">
                                            <input type="checkbox" disabled name="" {{ $new }}>&nbsp;&nbsp;NEW
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" disabled name="" {{ $renewal }}>&nbsp;&nbsp;RENEWAL
                                        </div>
                                        <div class="col-4">
                                            <input type="checkbox" disabled name="" {{ $amendatory }}>&nbsp;&nbsp;AMENDATORY
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    BUILDING PERMIT NO.: 
                                </div>
                                <div class="col-6 border-bot-black">
                                    {{ show_detail($data->permit_no) }}
                                </div>
                                <div class="col-4 border-bot-black">
                                    <span class="border-bot-white">OFFICIAL RECEIPT NO.</span> {{ show_detail($data->or_number) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    DATE ISSUED: 
                                </div>
                                <div class="col-6 border-bot-black">
                                    {{ show_detail($data->issued_date) }}
                                </div>
                                <div class="col-4 border-bot-black">
                                    <span class="border-bot-white">DATE PAID:</span> {{ show_detail($data->or_date) }}
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-2">
                                    FSEC NO.: 
                                </div>
                                <div class="col-10 border-bot-black">
                                    {{ show_detail($data->fsec_no) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    DATE ISSUED:
                                </div>
                                <div class="col-10 border-bot-black">
                                    {{ show_detail($data->issued_date) }}
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-12">
                                    This <b>PERMIT</b> is issued pursuant to Sections 207, 301, 302, 303 and 304 of the National Building Code of the Philippines (PD 1096), its Revised IRR, other Referral Codes and its Terms and Conditions.
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    Project Title
                                </div>
                                <div class="col-10 border-bot-black">
                                    : {{ show_detail($data->enterprise_name) }} 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    Owner / Permittee
                                </div>
                                <div class="col-10 border-bot-black">
                                    : {{ show_detail($data->last_name) }}, {{show_detail($data->first_name)}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2" style="margin-left: -5px;">
                                    Location of Construction
                                </div>
                                <div class="col-2 border-bot-black">
                                    <span class="border-bot-white">Parcel of land lot:</span> 
                                </div>
                                <div class="col-2 border-bot-black">
                                    <span class="border-bot-white">Block:</span>
                                </div>
                                <div class="col-2 border-bot-black">
                                    <span class="border-bot-white">OCT No.:</span>
                                </div>
                                <div class="col-2 border-bot-black">
                                    <span class="border-bot-white">Street:</span> {{show_detail($data->street)}}
                                </div>
                                <div class="col-2 border-bot-black">
                                    <span class="border-bot-white">Brgy:</span> {{show_detail($data->barangay)}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    
                                </div>
                                <div class="col-5 border-bot-black">
                                    <span class="border-bot-white">City / Municipality:</span> {{show_detail($data->city)}}
                                </div>
                                <div class="col-5 border-bot-black">
                                    <span class="border-bot-white">Zip Code:</span> {{show_detail($data->zip_code)}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    Use or Character of Occupancy
                                </div>
                                <div class="col-4 border-bot-black">
                                    :
                                </div>
                                <div class="col-6 border-bot-black">
                                    and classified as:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    Scope of Work
                                </div>
                                <div class="col-10 border-bot-black">
                                    :
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    Total Project Cost
                                </div>
                                <div class="col-10 border-bot-black">
                                    : {{ show_detail('PHP '.number_format(floatval($data->estimated_cost), 2)) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    Professional In Charge of Construction
                                </div>
                                <div class="col-10 border-bot-black">
                                    : {{show_detail($data->architect_name)}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-4"></div>
                        <div class="col-4 text-center">
                            <b>PERMIT ISSUED BY:</b>
                            <br><br>
                            <img src="{{ asset('img/esign.png') }}" width="30%" style="margin-bottom: -30px;">
                            <br>
                            <u style="font-size: 12pt;"><b>ENGR. JOHN B. DOMINGO</b></u>
                            <br>
                            <b style="font-size: 10pt;">ACTING BUILDING OFFICIAL</b>
                            <br>
                            {{ date('F d, Y', time()) }}
                        </div>
                        <div class="col-4 text-center">
                            <div id="qr-code"></div>
                        </div>
                    </div>
                    <br>
                    <div class="row margin-top">
                        <div class="col-12 text-center" style="font-size: 8pt;">
                            <b>
                                THIS PERMIT MAY BE CANCELLED OR REVOKED PURSUANT TO SECTIONS 207, 305, AND 306 OF THE NATIONAL BUILDING CODE OF THE PHILIPPINES (PD 1096) AND ITS REVISED IRR.
                            </b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.auth-partials.scripts')
    @yield('scripts')
</body>
<script type="text/javascript">
    $(document).ready(function () {
        generateQR();
    });
    function generateQR() {
        var qrText = "{{ url('/report/view-permit/2') }}";
        var qrCodeDiv = document.getElementById("qr-code");

        // Clear previous QR code if any
        qrCodeDiv.innerHTML = '';

        // Generate QR code image element
        var qrImg = document.createElement("img");
        qrImg.src = "https://api.qrserver.com/v1/create-qr-code/?data=" + encodeURIComponent(qrText);
        qrImg.alt = "QR Code";
        qrImg.width = 100;
        qrImg.height = 100;

        // Append QR code image to the div
        qrCodeDiv.appendChild(qrImg);
    }
</script>
</html>