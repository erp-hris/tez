@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
    @include('layouts.auth-partials.form-css')
    <style type="text/css">
        .hr {
            margin-top: 20px;
            border: none;
            height: 3px;
            /* Set the hr color */
            color: black; /* old IE */
            background-color: black; /* Modern Browsers */
        }
        .red {
            color: red;
            font-weight: 600;
        }
        .toUpper {
            text-transform: uppercase !important;
        }
        .red_alert {
           -moz-box-shadow:    inset 0 0 5px red !important;
           -webkit-box-shadow: inset 0 0 5px red !important;
           box-shadow:         inset 0 0 5px red !important;
           border: 1px solid red !important;

        }
    </style>
@endsection
@section('content')
    
    @php 
        $data = [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_footer' => 'yes',
        'isDone' => 'yes', 
        'cancel_url' => url($module.'/'.$option),
        'done_url' => url($module.'/'.$option),
        ];

        
    @endphp

    @include('others.main_content', $data)

    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' => url($module.'/'.$option.'/store'), 'frm_id' => 'add_form'])
@endsection

@section('scripts')

@yield('additional-scripts')
        
    <script type="text/javascript">
        $(document).ready(function(){
            /*$('input[type=text]').val (function () {
                return this.value.toUpperCase();
            })*/
        });
    </script>
@endsection