@php
    if(Auth::user()->position == 1)
    {
        $first_name = Auth::user()->first_name;
        $last_name = Auth::user()->last_name;
    }
    else
    {
        $first_name = '';
        $last_name = '';
    }
@endphp
<div>
    <div class="tab-container">
        <ul class="nav nav-tabs card-nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#building_permit_tab" data-toggle="tab" role="tab" aria-selected="true">
                    <span class="icon mdi mdi-caret-right"></span>
                    &nbsp;Building
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#architectural_permit_tab" data-toggle="tab" role="tab" aria-selected="true">
                    <span class="icon mdi mdi-caret-right"></span>
                    &nbsp;Architectural
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#structural_permit_tab" data-toggle="tab" role="tab" aria-selected="true">
                    <span class="icon mdi mdi-caret-right"></span>
                    &nbsp;Civil/Structural
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#electrical_permit_tab" data-toggle="tab" role="tab" aria-selected="true">
                    <span class="icon mdi mdi-caret-right"></span>
                    &nbsp;Electrical
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#mechanical_permit_tab" data-toggle="tab" role="tab" aria-selected="true">
                    <span class="icon mdi mdi-caret-right"></span>
                    &nbsp;Mechanical
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#sanitary_permit_tab" data-toggle="tab" role="tab" aria-selected="true">
                    <span class="icon mdi mdi-caret-right"></span>
                    &nbsp;Sanitary
                </a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" href="#plumbing_permit_tab" data-toggle="tab" role="tab" aria-selected="true">
                    <span class="icon mdi mdi-caret-right"></span>
                    &nbsp;Plumbing
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#electronic_permit_tab" data-toggle="tab" role="tab" aria-selected="true">
                    <span class="icon mdi mdi-caret-right"></span>
                    &nbsp;Electronics
                </a>
            </li>
        </ul>
    </div>

    <div class="tab-content">
        <div class="tab-pane active" id="building_permit_tab" role="tabpanel">
            @include('building_permit.building_permit_application_form.application.building_permit')
        </div>
        <div class="tab-pane" id="architectural_permit_tab" role="tabpanel">
            @include('building_permit.building_permit_application_form.application.architectural_permit')
        </div>
        <div class="tab-pane" id="structural_permit_tab" role="tabpanel">
            @include('building_permit.building_permit_application_form.application.structural_permit')    
        </div>
        <div class="tab-pane" id="electrical_permit_tab" role="tabpanel">
            @include('building_permit.building_permit_application_form.application.electrical_permit')
        </div>
        <div class="tab-pane" id="sanitary_permit_tab" role="tabpanel">
            @include('building_permit.building_permit_application_form.application.sanitary_permit')
        </div>
        <div class="tab-pane" id="mechanical_permit_tab" role="tabpanel">
            @include('building_permit.building_permit_application_form.application.mechanical_permit')
        </div>
        <div class="tab-pane" id="plumbing_permit_tab" role="tabpanel">
            @include('building_permit.building_permit_application_form.application.plumbing_permit')
        </div>
        <div class="tab-pane" id="electronic_permit_tab" role="tabpanel">
            @include('building_permit.building_permit_application_form.application.electronic_permit')
        </div>
    </div>
</div>
<!-- <div class="card card-border-color card-border-color-primary" id="building_permit_canvas">
    <div class="card-header">
        <div class="row" id="building_permit_chk">
            <div class="col-md-12">
                <h4><i class="icon mdi mdi-chevron-right" id="chevron"></i> BUILDING PERMIT</h4>
            </div>
        </div>
    </div>
    <div class="card-body" id="building_permit_form">
        
    </div>
</div> -->
<!-- <div class="card card-border-color card-border-color-primary" id="architectural_permit_canvas">
    <div class="card-header">
        <div class="row" id="architectural_permit_chk">
            <div class="col-md-12">
                <h4><i class="icon mdi mdi-chevron-right" id="chevron"></i> ARCHITECTURAL PERMIT</h4>
            </div>
        </div>
    </div>
    <div class="card-body" id="architectural_permit_form">
        
    </div>
</div>


<div class="card card-border-color card-border-color-primary" id="structural_permit_canvas">
    <div class="card-header">
        <div class="row" id="structural_permit_chk">
            <div class="col-md-12">
                <h4><i class="icon mdi mdi-chevron-right" id="chevron"></i> CIVIL/STRUCTURAL PERMIT</h4>
            </div>
        </div>
    </div>
    <div class="card-body" id="structural_permit_form">
        
    </div>
</div>

<div class="card card-border-color card-border-color-primary" id="electrical_permit_canvas">
    <div class="card-header">
        <div class="row" id="electrical_permit_chk">
            <div class="col-md-12">
                <h4><i class="icon mdi mdi-chevron-right" id="chevron"></i> ELECTRICAL PERMIT</h4> 
            </div>
        </div>
    </div>
    <div class="card-body" id="electrical_permit_form">
        
    </div>
</div>


<div class="card card-border-color card-border-color-primary" id="sanitary_permit_canvas">
    <div class="card-header">
        <div class="row" id="sanitary_permit_chk">
            <div class="col-md-12">
                <h4><i class="icon mdi mdi-chevron-right" id="chevron"></i> SANITARY PERMIT</h4> 
            </div>
        </div>
    </div>
    <div class="card-body" id="sanitary_permit_form">
        
    </div>
</div>

<div class="card card-border-color card-border-color-primary" id="mechanical_permit_canvas">
    <div class="card-header">
        <div class="row" id="mechanical_permit_chk">
            <div class="col-md-12">
                <h4><i class="icon mdi mdi-chevron-right" id="chevron"></i> MECHANICAL PERMIT</h4> 
            </div>
        </div>
    </div>
    <div class="card-body" id="mechanical_permit_form">
        
    </div>
</div>

<div class="card card-border-color card-border-color-primary" id="plumbing_permit_canvas">
    <div class="card-header">
        <div class="row" id="plumbing_permit_chk">
            <div class="col-md-12">
                <h4><i class="icon mdi mdi-chevron-right" id="chevron"></i> PLUMBING PERMIT</h4> 
            </div>
        </div>
    </div>
    <div class="card-body" id="plumbing_permit_form">
        
    </div>
</div>

<div class="card card-border-color card-border-color-primary" id="electronic_permit_canvas">
    <div class="card-header">
        <div class="row" id="electronic_permit_chk">
            <div class="col-md-12">
                <h4><i class="icon mdi mdi-chevron-right" id="chevron"></i> ELECTRONICS PERMIT</h4> 
            </div>
        </div>
    </div>
    <div class="card-body" id="electronic_permit_form">
        
    </div>
</div> -->


<input type="hidden" name="building_permit_id" id="building_permit_id">
<input type="hidden" name="action_value" id="action_value" value="0">







        
@section('additional-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        var t_show = 500;
        var t_hide = 500;
        @php
        if(isset($array['id']))
        {
            $edit_value = $array['id'];
            echo 'get_application("building_permit", '.$array['id'].');';
        }
        else
        {
            $edit_value = 0;
        }
        @endphp

        /*$(".form-control").each(function () {
            $(this).keypress(function () {
                $(this).val($(this).val().toUpperCase());
            });
        });*/

        /*@if($edit_value)
        $("#action_value").val(1);
        $("[id*='btn_']").each(function () {
            var curr_label = $(this).html();
            if(curr_label.indexOf('PERMIT') > -1)
            {
                var str = '';
                str += '<i class="icon mdi mdi-save"></i>&nbsp;&nbsp;';
                str += 'UPDATE CIVIL/STRUCTURAL PERMIT';

                $(this).html(str);

                $(this).removeClass('btn-primary');
                $(this).addClass('btn-success');
            }
        });
        @endif*/
        
        
        $("#work_scope").change(function () {
            if($(this).val() == 10)
            {
                $("#other_work_scope").prop("disabled", false);
            }
            else
            {
                $("#other_work_scope").val('');
                $("#other_work_scope").prop("disabled", true);   
            }
        });
        $("#occupancy_character").change(function () {
            if($(this).val() == 11)
            {
                $("#other_occupancy_character").prop("disabled", false);
            }
            else
            {
                $("#other_occupancy_character").val('');
                $("#other_occupancy_character").prop("disabled", true);   
            }
        });
        
        /*$('#save_btn').click(function()
        {
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "{{ __('page.add_new_application') }}",
                text: "{{ __('page.apply_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    axios.post(frm.action, {
                        
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{ url($module.'/'.$option) }}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });*/
    });

    function get_application(option, id)
    {
        axios.get("{{ url('get-application') }}/" + option + "/" + id)
        .then(function(response){
            var architectural_permit = response.data.data.architectural_permit;
            var structural_permit = response.data.data.structural_permit;
            var electrical_permit = response.data.data.electrical_permit;
            var sanitary_permit = response.data.data.sanitary_permit;
            var mechanical_permit = response.data.data.mechanical_permit;
            var building_permit = response.data.data.building_permit;
            var plumbing_permit = response.data.data.plumbing_permit;
            var electronics_permit = response.data.data.electronics_permit;
            if(building_permit.fsec)
            {
                var url = "{{ url('tieza/building_permit/fsec/') }}";

                $("#fsec_canvas").html('<a href="' + url + '/' + building_permit.id +  '/' + building_permit.fsec +'" class="btn btn-space btn-success" id="fsec_file" target="__blank">FSEC FILE</a>')
            }
            else
            {

                $("#fsec_canvas").html('<b>No FSEC Yet.</b>');
            }
            if(building_permit.status == 5)
            {
                $("#for_receipt").hide();
            }

            if(building_permit)
            {
                @foreach($array['building_permit_fields'] as $key => $value)
                $("#{{ $value }}").val(building_permit.{{ $key }}).trigger('change');
                @endforeach

                $("#1_id").val(building_permit.id);
            }
            if(architectural_permit)
            {
                var architectural_facilities = architectural_permit.architectural_facilities;
                
                architectural_facilities_obj = architectural_facilities.split('|');

                $("[name='2_architectural_facilities[]']").val(architectural_facilities_obj);


                var conformance = architectural_permit.conformance;
                
                conformance_obj = conformance.split('|');

                $("[name='2_conformance[]']").val(conformance_obj);

                $("#2_id").val(architectural_permit.id);

                $("#2_other_architectural_facilities").val(architectural_permit.other_architectural_facilities);

                $("#2_building_percentage").val(architectural_permit.building_percentage);

                $("#2_impervious_surface_area").val(architectural_permit.impervious_surface_area);

                $("#2_unpaved_surface_area").val(architectural_permit.unpaved_surface_area);

                $("#2_others_sit_occupancy").val(architectural_permit.others_sit_occupancy);


                $("#2_other_conformance").val(architectural_permit.other_conformance);

                $("#2_civil_engineer").val(architectural_permit.civil_engineer);

                $("#2_civil_engineer_address").val(architectural_permit.civil_engineer_address);

                $("#2_civil_engineer_prc_no").val(architectural_permit.civil_engineer_prc_no);

                $("#2_civil_engineer_prc_validity").val(architectural_permit.civil_engineer_prc_validity);

                $("#2_civil_engineer_ptr_no").val(architectural_permit.civil_engineer_ptr_no);

                $("#2_civil_engineer_ptr_date_issued").val(architectural_permit.civil_engineer_ptr_date_issued);

                $("#2_civil_engineer_ptr_issued_at").val(architectural_permit.civil_engineer_ptr_issued_at);

                $("#2_civil_engineer_tin").val(architectural_permit.civil_engineer_tin);

                $("#2_architectural_supervisor").val(architectural_permit.architectural_supervisor);

                $("#2_architectural_supervisor_address").val(architectural_permit.architectural_supervisor_address);

                $("#2_architectural_supervisor_prc_no").val(architectural_permit.architectural_supervisor_prc_no);

                $("#2_architectural_supervisor_prc_validity").val(architectural_permit.architectural_supervisor_prc_validity);

                $("#2_architectural_supervisor_ptr_no").val(architectural_permit.architectural_supervisor_ptr_no);

                $("#2_architectural_supervisor_ptr_date_issued").val(architectural_permit.architectural_supervisor_ptr_date_issued);

                $("#2_architectural_supervisor_ptr_issued_at").val(architectural_permit.architectural_supervisor_ptr_issued_at);

                $("#2_architectural_supervisor_tin").val(architectural_permit.architectural_supervisor_tin);

                enable_other_textbox('other_architectural_facilities_chk', '2_other_architectural_facilities');

                enable_other_textbox('2_conformance_chk', '2_other_conformance');
            }
            else
            {
                $("#architectural_permit_canvas").hide();
            }

            if(structural_permit)
            {
                $("#3_id").val(structural_permit.id);

                var structural_works = structural_permit.structural_works;
                
                structural_works_obj = structural_works.split('|');

                $("[name='3_structural_works[]']").val(structural_works_obj);
                        
                $("#3_civil_engineer_name").val(structural_permit.civil_engineer_name);
                
                $("#3_civil_engineer_address").val(structural_permit.civil_engineer_address);
                
                $("#3_civil_engineer_prc_no").val(structural_permit.civil_engineer_prc_no);
                
                $("#3_civil_engineer_prc_validity").val(structural_permit.civil_engineer_prc_validity);
                
                $("#3_civil_engineer_ptr_no").val(structural_permit.civil_engineer_ptr_no);
                
                $("#3_civil_engineer_ptr_date_issued").val(structural_permit.civil_engineer_ptr_date_issued);
                
                $("#3_civil_engineer_ptr_issued_at").val(structural_permit.civil_engineer_ptr_issued_at);
                
                $("#3_civil_engineer_tin").val(structural_permit.civil_engineer_tin);
                
                $("#3_civil_supervisor").val(structural_permit.civil_supervisor);
                
                $("#3_civil_supervisor_address").val(structural_permit.civil_supervisor_address);
                
                $("#3_civil_supervisor_prc_no").val(structural_permit.civil_supervisor_prc_no);
                
                $("#3_civil_supervisor_prc_validity").val(structural_permit.civil_supervisor_prc_validity);
                
                $("#3_civil_supervisor_ptr_no").val(structural_permit.civil_supervisor_ptr_no);
                
                $("#3_civil_supervisor_ptr_date_issued").val(structural_permit.civil_supervisor_ptr_date_issed);
                
                $("#3_civil_supervisor_ptr_issued_at").val(structural_permit.civil_supervisor_ptr_issued_at);
                
                $("#3_civil_supervisor_tin").val(structural_permit.civil_supervisor_tin);
            }
            else
            {
                $("#structural_permit_canvas").hide();
            }

            if(electrical_permit)
            {
                $("#4_id").val(electrical_permit.id);

                var electrical_application = electrical_permit.electrical_application;
                
                electrical_application_obj = electrical_application.split('|');

                $("[name='4_electrical_application[]']").val(electrical_application_obj);
                        
                        
                $("#4_other_electrical_application").val(electrical_permit.other_electrical_application);

                $("#4_total_connection_load").val(electrical_permit.total_connection_load);
                
                $("#4_total_transformer_capacity").val(electrical_permit.total_transformer_capacity);
                
                $("#4_total_generator_capacity").val(electrical_permit.total_generator_capacity);
                
                $("#4_electrical_engineer").val(electrical_permit.electrical_engineer);
                
                $("#4_electrical_engineer_address").val(electrical_permit.electrical_engineer_address);
                
                $("#4_electrical_engineer_prc_no").val(electrical_permit.electrical_engineer_prc_no);
                
                $("#4_electrical_engineer_prc_validity").val(electrical_permit.electrical_engineer_prc_validity);
                
                $("#4_electrical_engineer_ptr_no").val(electrical_permit.electrical_engineer_ptr_no);
                
                $("#4_electrical_engineer_ptr_date_issued").val(electrical_permit.electrical_engineer_ptr_date_issued);
                
                $("#4_electrical_engineer_ptr_issued_at").val(electrical_permit.electrical_engineer_ptr_issued_at);
                
                $("#4_electrical_engineer_tin").val(electrical_permit.electrical_engineer_tin);
                
                $("#4_supervisor_classification").val(electrical_permit.supervisor_classification);
                
                $("#4_electrical_supervisor").val(electrical_permit.electrical_supervisor);
                
                $("#4_electrical_supervisor_address").val(electrical_permit.electrical_supervisor_address);
                
                $("#4_electrical_supervisor_prc_no").val(electrical_permit.electrical_supervisor_prc_no);
                
                $("#4_electrical_supervisor_prc_validity").val(electrical_permit.electrical_supervisor_prc_validity);
                
                $("#4_electrical_supervisor_ptr_no").val(electrical_permit.electrical_supervisor_ptr_no);
                
                $("#4_electrical_supervisor_ptr_date_issued").val(electrical_permit.electrical_supervisor_ptr_date_issued);
                
                $("#4_electrical_supervisor_ptr_issued_at").val(electrical_permit.electrical_supervisor_ptr_issued_at);
                
                $("#4_electrical_supervisor_tin").val(electrical_permit.electrical_supervisor_tin);
            }
            else
            {
                $("#electrical_permit_canvas").hide();
            }

            if(sanitary_permit)
            {
                $("#5_id").val(sanitary_permit.id);

                var sanitary_application = sanitary_permit.sanitary_application;
                
                sanitary_application_obj = sanitary_application.split('|');

                $("[name='5_sanitary_application[]']").val(sanitary_application_obj);
                        
                $("#5_other_sanitary_application").val(sanitary_permit.other_sanitary_application);

                $("#5_sanitary_engineer").val(sanitary_permit.sanitary_engineer);
                
                $("#5_sanitary_engineer_address").val(sanitary_permit.sanitary_engineer_address);
                
                $("#5_sanitary_engineer_prc_no").val(sanitary_permit.sanitary_engineer_prc_no);
                
                $("#5_sanitary_engineer_prc_validity").val(sanitary_permit.sanitary_engineer_prc_validity);
                
                $("#5_sanitary_engineer_ptr_no").val(sanitary_permit.sanitary_engineer_ptr_no);
                
                $("#5_sanitary_engineer_ptr_date_issued").val(sanitary_permit.sanitary_engineer_ptr_date_issued);
                
                $("#5_sanitary_engineer_ptr_issued_at").val(sanitary_permit.sanitary_engineer_ptr_issued_at);
                
                $("#5_sanitary_engineer_tin").val(sanitary_permit.sanitary_engineer_tin);
                
                $("#5_sanitary_supervisor").val(sanitary_permit.sanitary_supervisor);
                
                $("#5_sanitary_supervisor_address").val(sanitary_permit.sanitary_supervisor_address);
                
                $("#5_sanitary_supervisor_prc_no").val(sanitary_permit.sanitary_supervisor_prc_no);
                
                $("#5_sanitary_supervisor_prc_validity").val(sanitary_permit.sanitary_supervisor_prc_validity);
                
                $("#5_sanitary_supervisor_ptr_no").val(sanitary_permit.sanitary_supervisor_ptr_no);
                
                $("#5_sanitary_supervisor_ptr_date_issued").val(sanitary_permit.sanitary_supervisor_ptr_date_issued);
                
                $("#5_sanitary_supervisor_ptr_issued_at").val(sanitary_permit.sanitary_supervisor_ptr_issued_at);
                
                $("#5_sanitary_supervisor_tin").val(sanitary_permit.sanitary_supervisor_tin);
            }
            else
            {
                $("#sanitary_permit_canvas").hide();
            }

            if(mechanical_permit)
            {
                $("#6_id").val(mechanical_permit.id);

                var mechanical_application = mechanical_permit.mechanical_application;
                
                mechanical_application_obj = mechanical_application.split('|');

                $("[name='6_mechanical_application[]']").val(mechanical_application_obj);

                $("#6_other_mechanical_application").val(mechanical_permit.other_mechanical_application);
                
                $("#6_mechanical_engineer").val(mechanical_permit.mechanical_engineer);
                
                $("#6_mechanical_engineer_address").val(mechanical_permit.mechanical_engineer_address);
                
                $("#6_mechanical_engineer_prc_no").val(mechanical_permit.mechanical_engineer_prc_no);
                
                $("#6_mechanical_engineer_prc_validity").val(mechanical_permit.mechanical_engineer_prc_validity);
                
                $("#6_mechanical_engineer_ptr_no").val(mechanical_permit.mechanical_engineer_ptr_no);
                
                $("#6_mechanical_engineer_ptr_issued_date").val(mechanical_permit.mechanical_engineer_ptr_issued_date);
                
                $("#6_mechanical_engineer_ptr_issued_at").val(mechanical_permit.mechanical_engineer_ptr_issued_at);
                
                $("#6_mechanical_engineer_tin").val(mechanical_permit.mechanical_engineer_tin);
                
                $("#6_mechanical_supervisor_classification").val(mechanical_permit.mechanical_supervisor_classification);
                
                $("#6_mechanical_supervisor").val(mechanical_permit.mechanical_supervisor);
                
                $("#6_mechanical_supervisor_address").val(mechanical_permit.mechanical_supervisor_address);
                
                $("#6_mechanical_supervisor_prc_no").val(mechanical_permit.mechanical_supervisor_prc_no);
                
                $("#6_mechanical_supervisor_prc_validity").val(mechanical_permit.mechanical_supervisor_prc_validity);
                
                $("#6_mechanical_supervisor_ptr_no").val(mechanical_permit.mechanical_supervisor_ptr_no);
                
                $("#6_mechanical_supervisor_ptr_issued_date").val(mechanical_permit.mechanical_supervisor_ptr_issued_date);
                
                $("#6_mechanical_supervisor_ptr_issued_at").val(mechanical_permit.mechanical_supervisor_ptr_issued_at);
                
                $("#6_mechanical_supervisor_tin").val(mechanical_permit.mechanical_supervisor_tin);
            }
            else
            {
                $("#mechanical_permit_canvas").hide();
            }

            if(plumbing_permit)
            {

            }
            else
            {
                $("#plumbing_permit_canvas").hide();
            }

            if(electronics_permit)
            {

            }
            else
            {
                $("#electronic_permit_canvas").hide();
            }
            
            
        })
        .catch(function(error){
           
        }) 
    }

    function enable_other_textbox(btn_id, txtbox_id)
    {

        if ($("#" + btn_id).is(':checked'))
        {

            $("#" + txtbox_id).prop('disabled', false);
        }
        else
        {
            $("#" + txtbox_id).prop('disabled', true);
            $("#" + txtbox_id).val('');
        }
    }
    $("#architectural_sab").click(function () {
        var objects = [
            ['2_civil_engineer', '2_architectural_supervisor'],
            ['2_civil_engineer_address', '2_architectural_supervisor_address'],
            ['2_civil_engineer_prc_no', '2_architectural_supervisor_prc_no'],
            ['2_civil_engineer_prc_validity', '2_architectural_supervisor_prc_validity'],
            ['2_civil_engineer_ptr_no', '2_architectural_supervisor_ptr_no'],
            ['2_civil_engineer_ptr_date_issued', '2_architectural_supervisor_ptr_date_issued'],
            ['2_civil_engineer_ptr_issued_at', '2_architectural_supervisor_ptr_issued_at'],
            ['2_civil_engineer_tin', '2_architectural_supervisor_tin'],
            ['2_civil_engineer_iapoa_no', '2_architectural_supervisor_iapoa_no'],
            ['2_civil_engineer_iapoa_no_date_issued', '2_architectural_supervisor_date_issued'],
            ['2_civil_engineer_iapoa_no_issued_at', '2_architectural_supervisor_issued_at']
        ];
        same_as_above(objects);

    });

    $("#structural_sab").click(function () {
        var objects = [
            ['3_civil_engineer_name', '3_civil_supervisor'],
            ['3_civil_engineer_address', '3_civil_supervisor_address'],
            ['3_civil_engineer_prc_no', '3_civil_supervisor_prc_no'],
            ['3_civil_engineer_prc_validity', '3_civil_supervisor_prc_validity'],
            ['3_civil_engineer_ptr_no', '3_civil_supervisor_ptr_no'],
            ['3_civil_engineer_ptr_date_issued', '3_civil_supervisor_ptr_date_issued'],
            ['3_civil_engineer_ptr_issued_at', '3_civil_supervisor_ptr_issued_at'],
            ['3_civil_engineer_tin', '3_civil_supervisor_tin']
        ];
        same_as_above(objects);

    });

    $("#electrical_sab").click(function () {
        var objects = [
            ['4_electrical_engineer', '4_electrical_supervisor'],
            ['4_electrical_engineer_address', '4_electrical_supervisor_address'],
            ['4_electrical_engineer_prc_no', '4_electrical_supervisor_prc_no'],
            ['4_electrical_engineer_prc_validity', '4_electrical_supervisor_prc_validity'],
            ['4_electrical_engineer_ptr_no', '4_electrical_supervisor_ptr_no'],
            ['4_electrical_engineer_ptr_date_issued', '4_electrical_supervisor_ptr_date_issued'],
            ['4_electrical_engineer_ptr_issued_at', '4_electrical_supervisor_ptr_issued_at'],
            ['4_electrical_engineer_tin', '4_electrical_supervisor_tin']
        ];
        same_as_above(objects);

    });

    $("#sanitary_sab").click(function () {
        var objects = [
            ['5_sanitary_engineer', '5_sanitary_supervisor'],
            ['5_sanitary_engineer_address', '5_sanitary_supervisor_address'],
            ['5_sanitary_engineer_prc_no', '5_sanitary_supervisor_prc_no'],
            ['5_sanitary_engineer_prc_validity', '5_sanitary_supervisor_prc_validity'],
            ['5_sanitary_engineer_ptr_no', '5_sanitary_supervisor_ptr_no'],
            ['5_sanitary_engineer_ptr_date_issued', '5_sanitary_supervisor_ptr_date_issued'],
            ['5_sanitary_engineer_ptr_issued_at', '5_sanitary_supervisor_ptr_issued_at'],
            ['5_sanitary_engineer_tin', '5_sanitary_supervisor_tin']
        ];
        same_as_above(objects);

    });

    $("#mechanical_sab").click(function () {
        var objects = [
            ['6_mechanical_engineer', '6_mechanical_supervisor'],
            ['6_mechanical_engineer_address', '6_mechanical_supervisor_address'],
            ['6_mechanical_engineer_prc_no', '6_mechanical_supervisor_prc_no'],
            ['6_mechanical_engineer_prc_validity', '6_mechanical_supervisor_prc_validity'],
            ['6_mechanical_engineer_ptr_no', '6_mechanical_supervisor_ptr_no'],
            ['6_mechanical_engineer_ptr_date_issued', '6_mechanical_supervisor_ptr_date_issued'],
            ['6_mechanical_engineer_ptr_issued_at', '6_mechanical_supervisor_ptr_issued_at'],
            ['6_mechanical_engineer_tin', '6_mechanical_supervisor_tin']
        ];
        same_as_above(objects);

    });

    function same_as_above(objects)
    {
        for (var i = 0; i < objects.length; i++) {
            var above_value = $("#" + objects[i][0]).val();
            $("#" + objects[i][1]).val(above_value);
        }
    }

    function check_date_validity(object_id)
    {
        var value = $("#" + object_id).val();
        if(value)
        {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = mm + '/' + dd + '/' + yyyy;

            var curr_date = new Date(today);

            var inputted_date = new Date(value);

            if(curr_date > inputted_date)
            {
                $("#" + object_id).addClass("red_alert");
                $(this).focus();
            }
            else
            {
                $("#" + object_id).removeClass("red_alert");
            }
            
        }
    }


   
</script>
@endsection