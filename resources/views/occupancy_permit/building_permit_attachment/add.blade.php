@php
    $arr = [
        'Proof of Business Registration ',
        'Securities and Exchange Commission (SEC), if Corporation',
        'Cooperative Development Authority (CDA), if Cooperative',
        'Department of Trade and Industry (DTI), if Single    Proprietorship',
        'Registration Certificate from LGU',
        'Accreditation Certificate from DOT',
        'Certified True Copy of Articles of Incorporation and By-Laws in case of Partnership, corporation, association',
        'Occupancy Permit',
        'Proof of Right over the business location',
        'Original/Transfer Certificate of Title), if owned by the applicant',
        'Contract of Lease, if not owned by the applicant',
        'Land Tax Clearance from LGU',
        'Zoning /Locational Clearance from TIEZA',
        'Sanitary Permit/Inspection Certificate from LGU',
        'Fire Safety Insurance Certificate from BFP',
        'Social Security System Identification and Clearance',
        'Phil Health Clearance',
        'PAG-IBIG/HDMF Clearance',
        'Valid visa and Labor Permit from DOLE for non-Filipino personnel',
        'Management Structure or list of names of Employer and Employees',
        'Insurance Coverage against accidents for passenger and loss of luggage',
        'List of vehicles owned by the agency',
        'Travel Agency Management Training Certificate or equivalent',
        'MARINA Certificate of Public Conveyance (all crews are duly licensed) for sea transport',
        'DOPT Franchise for land transport',
    ];
@endphp
<div class="row">
    <div class="col-lg-12">
        <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>Documentary Requirements for TIEZA LOCATION CLEARANCE</h4>
            </div>
        </div>
        <hr>
            <h5>
                For Business
            </h5>
        <hr>
        @foreach($arr as $key => $value)
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    {{ ($key+1) }}. {{ $value }}
                </h5>
                
            </div>
            <div class="col-md-6">
                <input type="file" class="form-control form-control-xs" name="">
            </div>
        </div>
        @endforeach
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        $("#primary_selection").hide();
        $("#secondary_selection").hide();
        $("#primary_selection2").hide();
        $("#classification").change(function () {
            if($(this).val() == 1)
            {
                $("#secondary_selection").hide();
                $("#primary_selection").show();
                $("#primary_selection2").show();
            }
            else if($(this).val() == 2)
            {
                $("#primary_selection").hide();
                $("#primary_selection2").hide();
                $("#secondary_selection").show();
            }
            else
            {
                $("#primary_selection").hide();
                $("#primary_selection2").hide();
                $("#secondary_selection").hide();
            }
        });
        $("#classification1").change(function () {
            if($(this).val() == 4)
            {
                $("#classification1_1").prop("disabled", false);
            }
            else
            {
                $("#classification1_1").prop("disabled", true);
                $("#classification1_1").val(0).trigger('change');
            }
        });
        $('#save_btn').click(function()
        {
            var country_id = $('#country').val();
            var file_name = $('#file').val();
            var remarks = $('#remarks').val();
            if(country_id == "") 
            {
                $('#inv_cntry').removeClass('d-none');
                $('#country').addClass('is-invalid');
                return;
            }
            title = "Upload";
            text = "Are you sure you want to upload this data?"
            success = "Upload Successfully!";
            color = "colored-header colored-header-primary";
            button = "btn btn-primary";
            const swal_continue = alert_continue(title, text,button, color);
            swal_continue.then((result) => {
                if(result.value){
                    const formData = new FormData();
                    formData.append('country_id',country_id);
                    var file  = document.querySelector('#file');
                    formData.append('file_name', file.files[0]);
                    formData.append('remarks', remarks);
                    console.log(formData);
                    axios.post('add_residency/save',formData,{
                        headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                   
                  })
                  .then(function (response) {
                    const swal_success = alert_success(success, 1500);
                                swal_success.then((response) => {

                                location.href= "{{url('application/residency')}}";
                                console.log(response);

                                });
                    
                  })  
                  .catch((error) => {
                      const errors = error.response.data.errors;

                      if(typeof(errors) == 'string')
                      {
                          alert_warning(errors);
                      }
                      else
                      {
                          const firstItem = Object.keys(errors)[0];
                          const firstItemDOM = document.getElementById(firstItem);
                          const firstErrorMessage = errors[firstItem][0];

                          firstItemDOM.scrollIntoView();

                          alert_warning("{{ __('page.check_inputs') }}", 1500);

                          showErrors(firstItem, firstErrorMessage, firstItemDOM, ['user_level', 'user_status']);
                      }
                    });  
                }

            });

        });
    });

  
   
  </script>
@endsection