<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<body>
    Dear Ms./Mr. <b><u>{{ $client_name }}</u></b>,
    <br><br>
     
    Here is the OTP you need to activate your account in TIEZA Online Permitting System (TOPS) : <b><u>{{ $otp }}</u></b>
    <br>
    <br>
    Thank you for using the TOPS.
    <br><br>
    Regards,
    <br>
    <br>
    San Vicente Flagship TEZ Office
</body>
</html>