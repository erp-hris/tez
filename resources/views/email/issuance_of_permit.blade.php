<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
        
    </style>
</head>
<body style="font-size: 12pt;color: black;">
    Dear Mr/.Ms. <b><u>{{ $full_name }}</u></b>:
    <br>
    <br>
    <i>Greetings from the Tourism Infrastructure and Enterprise Zone Authority (TIEZA)! </i>
    <br>
    <br>
    This is to inform you that your application for Building Permit is <b>READY FOR ISSUANCE</b>. Please claim your approved documents at the SVFTEZ Office.
    <!-- <br>
    <br>
    <b>Note: Please submit the original hard copies of documentary requirements at the SVFTEZ Office upon release the approved documents</b> -->
    <br>
    <br>
    For inquiries, kindly contact us thru electronic mail at sanvicente.tez.tamd@tieza.gov.ph or at the following mobile numbers:
    <br>
    <br>
    Smart: +63 9108304148                
    <br>
    Globe: +63 9751171317
    <br><br>
    We look forward to continuing our partnership with you in working for the country’s sustainable tourism.
    <br><br>
    Regards, 
    <br>
    <br>
    <b>San Vicente Flagship TEZ Office</b> 


</body>
</html>