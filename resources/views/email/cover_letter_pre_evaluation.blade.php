<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
        body
        {
            background: url("<?php echo $message->embed(public_path('img/bg-tiezadarker.png')); ?>");
            background-position: center;
            background-size: cover;
            background-color: #eeeeee;
            height: auto;
            width: 100%;
        }
    </style>
</head>
<body>
    
    {{ date('F d, Y', time()) }}
    <br><br>

    {{ $full_name }}
    <br>
    {{ $project_name }}
    <br>
    {{ $enterprise_name }}
    <br>
    {{ $address }}

    <br><br>
    Dear Mr./Ms. {{ $full_name }}:
    <br><br>
    Greetings from the Tourism Infrastructure and Enterprise Zone Authority (TIEZA)!
    <br><br>
    Relative to your request for the pre-evaluation (initial plan evaluation) of your proposed [storey] [Use of Occupancy] in [Address], we are pleased to provide herein our Plan Evaluation Report (PER) upon conducting the review and assessment of your proposed project’s compliance with the National Building Code, San Vicente Municipal Tourism Code, and other related laws, rules, and regulations.
    <br><br>
    Should you wish to proceed with your application for a Building Permit, we kindly request the submission of the complete set of requirements and or revised plan incorporating our recommended actions. 
    <br><br>
    Thank you and we look forward to continuing our partnership with you in working for the country’s sustainable tourism. 
    <br><br>


    Very truly yours,
    <br><br>

    <img src="{{ asset('/img/email.png') }}" alt="ESIGN" width="50%">
    <br>
    ENGR. BERNARDO C. ALARILLA 
    <br>
    Administrator, San Vicente Flagship TEZ
    
</body>
</html>