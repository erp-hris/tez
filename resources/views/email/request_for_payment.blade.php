<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
        
    </style>
</head>
<body style="font-size: 12pt;color: black;">
    Dear Mr/.Ms. <b><u>{{ $full_name }}</u></b>:
    <br>
    <br>
    <i>Greetings from the Tourism Infrastructure and Enterprise Zone Authority (TIEZA)! </i>
    <br>
    <br>
    This is to inform you that your application for Building Permit has been approved. Please settle your fees amounting to <b>{{ strtoupper($amount_in_words) }} ONLY</b> (Php <b>{{ number_format($app_amount, 2) }}</b>) online via MYEG or over the counter at the San Vicente Flagship TEZ Office (SVFTEZ).
    <br>
    <br>
    You may access the online payment gateway through this link: {{ $payment_link }}
    <br>
    <br>
    For fees above PHP 50,000.00, kindly settle it through the following modes of payment:
    <br>
    1.  Thru TIEZA Cashier (6th Floor – Double Dragon)
    <br>
    2.  Thru Bank Deposits
    <p style="text-indent: 2%;">A.  DBP – Makati Branch</p>
    <p style="text-indent: 3%;">Account Name: Tourism Infrastructure and Enterprise Zone Authority</p>
    <p style="text-indent: 3%;">Current Account No.: <u>0405-018676-030</u></p>
    <p style="text-indent: 2%;">B.  Land Bank of the Phils. – Intramuros Branch</p>
    <p style="text-indent: 3%;">Account Name: Tourism Infrastructure and Enterprise Zone Authority</p>
    <p style="text-indent: 3%;">Savings Account No.: <u>0011-3424-26</u></p>
    <br>
    Note: Send a copy of the deposit slip to the San Vicente Flagship TEZ (sanvicente.tez.tamd@tieza.gov.ph) for issuance of Official Receipt.
    We look forward to continuing our partnership with you in working for the country’s sustainable tourism.  
    <br>
    <br>
    You may check or monitor the status of your submitted documents/application through the TIEZA Online Permitting System (TOPS).
    <br>
    <br>
    For inquiries, kindly contact us thru electronic mail at sanvicente.tez.tamd@tieza.gov.ph or at the following mobile numbers:
    <br>
    <br>
    Smart: +63 9108304148                
    <br>
    Globe: +63 9751171317
    <br><br>
    We look forward to continuing our partnership with you in working for the country’s sustainable tourism.
    <br><br>
    Regards, 
    <br>
    <br>
    <b>San Vicente Flagship TEZ Office</b> 


</body>
</html>