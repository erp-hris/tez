<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
        
    </style>
</head>
<body style="font-size: 12pt;color: black;">
    Dear Mr/.Ms. <b><u>{{ $full_name }}</u></b>:
    <br>
    <br>
    <i>Greetings from the Tourism Infrastructure and Enterprise Zone Authority (TIEZA)! </i>
    <br>
    <br>
    This is to inform you that your submitted building permit application through the TIEZA Online Permitting System (TOPS) has been received by this office for evaluation. The SVFTEZ technical team will be conducting a site inspection  within three (3) working days. Furthermore, you are advised to submit to the TIEZA-San Vicente Flagship TEZ (SVFTEZ) Office seven (7) sets of requirements as the per the attached checklist within three (3) working days.
    <br>
    <br>
    We look forward to continuing our partnership with you in working for the country’s sustainable tourism.  
    <br>
    <br>
    For inquiries, kindly contact us thru electronic mail at sanvicente.tez.tamd@tieza.gov.ph or at the following mobile numbers:
    <br>
    <br>
    Smart: +63 9108304148                
    <br>
    Globe: +63 9751171317
    <br><br>
    We look forward to continuing our partnership with you in working for the country’s sustainable tourism.
    <br><br>
    Regards, 
    <br>
    <br>
    <b>San Vicente Flagship TEZ Office</b> 


</body>
</html>