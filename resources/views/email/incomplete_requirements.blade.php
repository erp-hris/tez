<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
        
    </style>
</head>
<body style="font-size: 12pt;color: black;">
    Dear Mr/.Ms. <b><u>{{ $full_name }}</u></b>:
    <br>
    <br>
    <i>Greetings from the Tourism Infrastructure and Enterprise Zone Authority (TIEZA)! </i>
    <br>
    <br>
    <!-- This pertains to your submission of Building Plans and other documentary requirements through the TIEZA Online Permitting System (TOPS) for your proposed development in the San Vicente Flagship Tourism Enterprise Zone (SVFTEZ). Please be informed that this Office will not be able to proceed with the PRE-EVALUATION process due to your submission of incomplete requirements.   -->

    This pertains to your submission of Building Plans and other documentary requirements through the TIEZA Online Permitting System (TOPS) for your proposed development in the San Vicente Flagship Tourism Enterprise Zone (SVFTEZ). Please be informed that this Office will not be able to proceed with the PRE-EVALUATION process due to your submission of incomplete requirements.  
    <br>
    <br>
    You are encouraged to submit the lacking requirements through the TOPS:
    <br><br>
    <textarea rows="10" style="background: rgba(0, 0, 0, 0); border: none; outline: 0; cursor: text; resize: none; overflow: hidden; width: 100%;">{{ $requirement_list }}
    </textarea>
    <br>
    <br>
    We look forward to continuing our partnership with you in working for the country’s sustainable tourism.  
    <br>
    <br>
    For inquiries, kindly contact us thru electronic mail at sanvicente.tez.tamd@tieza.gov.ph or at the following mobile numbers:
    <br>
    <br>
    Smart: +63 9108304148                
    <br>
    Globe: +63 9751171317
    <br><br>
    We look forward to continuing our partnership with you in working for the country’s sustainable tourism.
    <br><br>
    Regards, 
    <br>
    <br>
    <b>San Vicente Flagship TEZ Office</b> 


</body>
</html>