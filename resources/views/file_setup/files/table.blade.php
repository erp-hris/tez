                
                <table id="application_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                    <thead>
                        <tr class="text-center">
                            <th style="width: 10%;">{{ __('page.action') }}</th>
                            <th style="width: 25%;">File Name</th>
                            <th style="width: 25%;">Permit Type</th>
                            <th style="width: 20%;">Is Required</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>