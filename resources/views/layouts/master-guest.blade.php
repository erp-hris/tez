<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.guest-partials.meta')
    @yield('meta')
    @include('layouts.guest-partials.css')
    @yield('css')
</head>
<body class="event-background" style="background-color: #4285f4 !important;">
    <div id="app">
    	@yield('content')
    </div>
    @include('layouts.guest-partials.scripts')
    @yield('scripts')
    <script src="{{ asset('js/custom.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            App.init();
        });
    </script>
</body>
</html>