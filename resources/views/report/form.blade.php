
<div class="row">
    <div class="col-lg-12">
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>Report List</h5>
                <select class="select2 select2-xs" id="report_name" name="report_name">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="issued-business-permit">ISSUED BUSINESS PERMIT</option>
                    <option value="issued-location-permit">ISSUED LOCATION PERMIT</option>
                    <option value="issued-occupancy-permit">ISSUED OCCUPANCY PERMIT</option>
                    <option value="issued-building-permit">ISSUED BUILDING PERMIT</option>
                    <option value="permit-application">LIST OF PERMIT APPLICATION</option>
                    <option value="business-permit">BUSINESS PERMIT</option>
                </select>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-lg-12 text-right">
                <button type="button" class="btn btn-success" id="preview">Preview Report</button>
            </div>
        </div>
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        
    });

  
   
  </script>
@endsection