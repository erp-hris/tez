@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.form-css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    @php 
        $data = [
        'option' => $module, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $module.'.form',
        ];
    @endphp

    @include('others.main_content', $data)
@endsection

@section('scripts')
    @include('layouts.auth-partials.form-scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
            App.dataTables();
            $("#preview").click(function () {
                var params = '';
                $("[class*='criteria']").each(function () {
                    var tag_name    = $(this).prop('tagName');
                    var id          = $(this).attr('id');
                    var value       = $(this).val();
                    var type        = $(this).attr('type');
                    if (type == 'checkbox')
                    {
                        if($(this).prop("checked"))
                        {
                            value = 1;
                        }
                        else
                        {
                            value = 0;
                        }
                    }
                    params += id + '=' + value + '&';
                });
                
                var href = window.location + '/view-report/' + $("#report_name").val() + '?' + params;
                window.open(href, '_blank');
            });
        });
        
    </script>
@endsection
