    
    @section('employees_css')
        <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/select2/css/select2.min.css') }}">
    @endsection

    <div class="card">
        <div class="card-header">
            <div class="row margin-top">
                <label class="control-label">Filter by</label>
                <select class="select2 select2-xs" id="filter_by">
                    <option value="">Please select option</option>
                </select>
            </div>

            <div class="row margin-top">
                <select class="select2 select2-xs" id="list_filter" name="list_filter">
                    <option value="">Please select option</option>
                </select>
            </div>

            <div class="row margin-top">
                <input type="text" class="form-control form-control-xs" name="filter_value" id="filter_value" placeholder="Search Name">
            </div>

            <div class="row margin-top">
                <a href="javascript:void(0);" class="btn btn-space btn-primary hover" id="filter_btn"><span class="mdi mdi-search"></span>&nbsp;Filter</a>
                <a href="javascript:void(0);" class="btn btn-space btn-secondary hover" id="cancel_btn"><span class="mdi mdi-close"></span>&nbsp;Cancel</a>
            </div>
        </div>  
        <div class="card-body">
                    <div class="row" style="max-height: 500px; overflow: auto;">
                        <div class="col-md-12">
                            <div id="list_administrator"></div>
                        </div>
                        
                    </div>
        </div>
        <div class="card-divider"></div>
        <div class="card-footer">
            <div id="total_administrator"></div>
        </div>        
    </div>

    @section('employees_scripts')
        <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/js/app-form-elements.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/js/app-form-masks.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
                type="text/javascript"></script>

        <script type="text/javascript">
            $(document).change(function(){
                $('input[type="checkbox"]').on('change', function() {
                    $(this).prop('checked', true);
                    $('input[type="checkbox"]').not(this).prop('checked', false);
                });
            });

            $(document).ready(function () {
                App.formElements();
                App.masks();

                load_list_administrator();
            });

            $('#filter_value').keypress(function(e){
                if(e.which == 13){//Enter key pressed
                    $('#filter_btn').click();//Trigger search button click event
                }
            });

            $('#filter_btn').click(function(){
                var filter_by = $('#filter_by').val();
                var list_filter = $('#list_filter').val();
                var filter_value = $('#filter_value').val();

                localStorage.setItem("filter_by", filter_by);
                localStorage.setItem("list_filter", list_filter);
                localStorage.setItem("filter_value", filter_value);

                filter_administrator(filter_value);
            });
    
            $('#cancel_btn').click(function(){
                load_list_administrator();
            });

            function load_list_administrator()
            {
                $("#list_administrator").empty();
                $("#total_administrator").empty();
                $('#list_filter').empty();
                $("#list_filter").append("<option value=''>Please select option</option>");

                $('#filter_by').val('').trigger('change.select2');
                $('#list_filter').val('').trigger('change.select2');
                $('#filter_value').val('');
                
                localStorage.removeItem("filter_by");
                localStorage.removeItem("list_filter");
                localStorage.removeItem("filter_value");

                axios.get("{{ url('/administrator/json') }}")
                .then(function(response){
                    const administrators = response.data.administrators;
                    const total_record = response.data.count_administrator;

                    $.each(administrators, function( key, value ) {
                        $("#list_administrator").append("<div class='custom-control custom-checkbox custom-control-inline'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_account('"+ value['id'] +"') name='employee'><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['name'] +"</label></div>")

                    });

                    $("#total_administrator").append('<label class="control-label">Total Record: '+total_record+'</label>');
                })
                .catch(function(error){
                    Swal.fire({
                    text: "Something error. Please contact administrator.",
                    type: 'warning',
                    customClass: 'content-text-center',
                    showConfirmButton: false,
                    timer: 1500
                    });  
                })
            }

            function filter_administrator(filter_value)
            {
                axios.get("{{ url('/administrator/filter/json') }}", {
                    params: {
                      filter_value : filter_value
                    }
                })
                .then(function (response) {
                    $("#list_administrator").empty()
                    $("#total_administrator").empty()

                    const administrators = response.data.administrators;
                    const total_record = response.data.count_administrator;

                    if($.trim(administrators))
                    {   
                        $.each(administrators, function( key, value ) {
                            $("#list_administrator").append("<div class='custom-control custom-checkbox custom-control-inline'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_account('"+ value['id'] +"') name='employee'><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['name'] +"</label></div>")

                        });

                        $("#total_administrator").append('<label class="control-label">Total Record: '+total_record+'</label>');
                    }
                    else
                    {
                        load_list_administrator();

                        Swal.fire({
                            text: "{{ __('page.no_record_found') }}",
                            type: 'warning',
                            customClass: 'content-text-center',
                            showConfirmButton: false,
                            timer: 1500
                        });  
                    }
                })
            }

            @include('others.page_script')
        </script>
    @endsection