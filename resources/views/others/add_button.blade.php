			
			<a href="{{ $add_url }}" class="btn btn-space btn-primary" title="{{ __('page.add_new_record') }}" id="add_record" @if(isset($is_modal)) data-toggle="modal" @endif><span class="icon icon-left mdi mdi-plus"></span>&nbsp;{{ __('page.add_new_record') }}
	        </a>