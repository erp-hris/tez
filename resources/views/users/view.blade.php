@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.form-css')
    @include('layouts.auth-partials.datatables-css')
    <style type="text/css">
        .hr {
            margin-top: 20px;
            border: none;
            height: 3px;
            /* Set the hr color */
            color: black; /* old IE */
            background-color: black; /* Modern Browsers */
        }
        .box{
            box-shadow: 2px 2px 2px gray; 
            border-radius: 10px;
        }
        .box:hover
        {
            background: #f2f2f2;
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    @include('others.main_content', [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => 'icon mdi mdi-plus-circle', 
        'has_file' => $file,
        'has_footer' => 'yes', 
        'has_frm' => 'yes',
        'cancel_url' => url('/user_building_permit/'.$option), 
        'frm_method' => 'POST',
        'frm_action' => url('/user_building_permit/'.$option.'/store'), 
        'frm_id' => 'add_form',
    ])
@endsection

@section('scripts')
    @include('layouts.auth-partials.form-scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
        });

        
    </script>
@endsection