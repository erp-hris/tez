@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.form-css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    @include(
        'others.main_content', 
        [
            'option' => $option, 
            'module' => $module,
            'title' => $option,
            'has_icon' => 'icon mdi mdi-settings', 
            'has_file' => $file
        ]
    )
@endsection

@section('scripts')
    @include('layouts.auth-partials.form-scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
        });
    </script>
@endsection
