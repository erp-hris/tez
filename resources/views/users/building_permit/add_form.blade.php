<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-header card-header-divider">
                Pre Evaluation Form
            </div>
            <div class="card-body">
                <div class="row margin-top">
                    <div class="col-md-3">
                        <h5>Type of Application for Building Permit</h5>
                        <select class="select2 select2-xs" id="application_type" name="application_type">
                            <option value="0">{{ __('page.please_select') }}</option>
                            <option value="1">New</option>
                            <option value="2">Renewal</option>
                            <option value="3">Amendatory</option>
                        </select>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-md-3">
                        <h5>
                            Last Name
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="last_name" name="last_name" value="{{ $array['user_info']->last_name }}">
                    </div>
                    <div class="col-md-3">
                        <h5>
                            First Name
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="first_name" name="first_name" value="{{ $array['user_info']->first_name }}">
                    </div>
                    <div class="col-md-3">
                        <h5>
                            M.I.
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="middle_name" name="middle_name">
                    </div>
                    <div class="col-md-3">
                        <h5>
                            TIN
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="tin" name="tin">
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-md-4">
                        <h5>
                            Project Name
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="project_name" name="project_name">
                    </div>
                    <div class="col-md-4">
                        <h5>
                            Enterprise Name
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="enterprise_name" name="enterprise_name">
                    </div>
                    <div class="col-md-4">
                        <h5>
                            Form of Ownership
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="ownership_form" name="ownership_form">
                    </div>
                    
                </div>
                <div class="hr"></div>
                <div class="row margin-top">
                    <div class="col-md-3">
                        <h5>
                            ADDRESS
                        </h5>
                    </div>
                    <div class="col-md-3">
                        <h5>
                            House No.
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="house_number" name="house_number">
                    </div>
                    <div class="col-md-3">
                        <h5>
                            Street
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="street" name="street">
                    </div>
                    <div class="col-md-3">
                        <h5>
                            Barangay
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="barangay" name="barangay">
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3">
                        <h5>
                            City / Municipality
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="city" name="city">
                    </div>
                    <div class="col-md-3">
                        <h5>
                            Zip Code
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="zip_code" name="zip_code">
                    </div>
                    <div class="col-md-3">
                        <h5>
                            Contact No.
                        </h5>
                        <input type="text" class="form-control form-control-xs" id="telephone_number" name="telephone_number">
                    </div>
                </div>
                <div id="tct_canvas">
                    <div class="hr"></div>
                    <div class="row">
                        <div class="col-md-3">
                            <h5>
                                LOCATION OF CONSTRUCTION:
                            </h5>
                        </div>
                        <div class="col-md-3">
                            <h5>
                                Lot No.
                            </h5>
                            <input type="text" class="form-control form-control-xs" id="" name="lot_number[]">
                        </div>
                        <div class="col-md-3">
                            <h5>
                                Blk No.
                            </h5>
                            <input type="text" class="form-control form-control-xs" id="" name="block_no[]">
                        </div>
                        <div class="col-md-3">
                            <h5>
                                TCT No.
                            </h5>
                            <input type="text" class="form-control form-control-xs" id="" name="tct_no[]">
                        </div>
                    </div>
                    <div class="row margin-top">
                        <div class="col-md-3">
                            <h5>
                                Tax Dec No.
                            </h5>
                            <input type="text" class="form-control form-control-xs" id="" name="tax_declaration_no[]">
                        </div>
                        <div class="col-md-3">
                            <h5>
                                Street
                            </h5>
                            <input type="text" class="form-control form-control-xs" id="" name="construction_street[]">
                        </div>
                        <div class="col-md-3">
                            <h5>
                                Barangay
                            </h5>
                            <input type="text" class="form-control form-control-xs" id="" name="construction_barangay[]">
                        </div>
                        <div class="col-md-3">
                            <h5>
                                City / Municipality Of
                            </h5>
                            <input type="text" class="form-control form-control-xs" id="" name="construction_city[]">
                        </div>
                    </div>
                </div>
                <div id="extra_tct"></div>
                <div class="row margin-top">
                    <div class="col-md-12 text-right">
                        
                        <button type="button" class="btn btn-success" id="btn_add_tct">
                            <i class="mdi mdi-plus"></i>
                            Add Row
                        </button>
                        <!-- <button type="button" class="btn btn-danger" id="btn_remove_tct">
                            <i class="mdi mdi-plus"></i>
                            Remove Row
                        </button> -->
                        <input type="hidden" id="tct_count" name="" value="1">
                    </div>
                </div>
                <div class="hr"></div>
                <div class="row margin-top">
                    <div class="col-md-4">
                        <h5>Land Document</h5>
                        <input type="file" class="form-control form-control-xs" name="land_document" id="land_document">
                    </div>
                    <div class="col-md-4">
                        <h5>Survey Plan</h5>
                        <input type="file" class="form-control form-control-xs" name="survey_plan" id="survey_plan">
                    </div>
                    <div class="col-md-4">
                        <h5>Building Plan</h5>
                        <input type="file" class="form-control form-control-xs" name="building_plan" id="building_plan">
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
@section('additional-scripts')

@endsection
<script type="text/javascript">
        $(document).ready(function () {
            $("#btn_remove_tct").click(function () {

                $(this).closest("#extra_tct").remove();
            });
            $("#btn_add_tct").click(function () {
                var tct_count = $("#tct_count").val();
                tct_count++;
                $("#tct_count").val(tct_count);
                var canvas = $("#tct_canvas").clone(true).find("input:text").val("").end();
                $("#extra_tct").append(canvas);
            });
            $("#save_btn").click(function () { 
                var frm = document.querySelector('#add_form');
                @foreach($array['tct_fields'] as $key => $value)
                var {{ $value }} = [];
                $("input[name='{{ $value }}[]']").each(function() {
                    {{ $value }}.push($(this).val());
                });
                @endforeach
                Swal.fire({
                    title: "Save Pre Evaluation",
                    text: "{{ __('page.add_this') }}",
                    confirmButtonText: 'Proceed',
                    confirmButtonClass: 'btn btn-primary',
                    closeButtonClass: 'btn btn-secondary',
                    cancelButtonClass: 'btn btn-secondary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-primary'
                }).then((isSave) => {
                    if (isSave.value) {
                        var formData = new FormData();
                        var land_document = document.querySelector("#land_document");
                        var survey_plan = document.querySelector("#survey_plan");
                        var building_plan = document.querySelector("#building_plan");
                        
                        formData.append("land_document", land_document.files[0]);
                        formData.append("survey_plan", survey_plan.files[0]);
                        formData.append("building_plan", building_plan.files[0]);
                        formData.append("application_type", $("#application_type").val());
                        formData.append("last_name", $("#last_name").val());
                        formData.append("first_name", $("#first_name").val());
                        formData.append("middle_name", $("#middle_name").val());
                        formData.append("tin", $("#tin").val());
                        formData.append("project_name", $("#project_name").val());
                        formData.append("enterprise_name", $("#enterprise_name").val());
                        formData.append("ownership_form", $("#ownership_form").val());
                        formData.append("house_number", $("#house_number").val());
                        formData.append("street", $("#street").val());
                        formData.append("barangay", $("#barangay").val());
                        formData.append("city", $("#city").val());
                        formData.append("zip_code", $("#zip_code").val());
                        formData.append("telephone_number", $("#telephone_number").val());
                        formData.append("application_type", $("#application_type").val());

                        @foreach($array['tct_fields'] as $key => $value)
                        formData.append("{{ $key }}", {{ $value }});
                        @endforeach

                        formData.append("tct_count", $("#tct_count").val());

                        axios.post(frm.action, formData, {
                            headers: {
                              'Content-Type': 'multipart/form-data'
                            }
                        })
                        .then((response) => {
                            if(response.data.exist_record)
                            {
                                Swal.fire({
                                    title: "Project Name Already Exist!",
                                    html: 'I will close in <strong></strong> seconds.',
                                    timer: 1000,
                                    customClass: 'content-actions-center',
                                    buttonsStyling: true,
                                    onOpen: function() {
                                        swal.showLoading();
                                        timerInterval = setInterval(function () {
                                            swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                        }, 100);
                                    },
                                    onClose: function() {
                                        clearInterval(timerInterval);
                                    }
                                }).then(function (result) {
                                    if ( result.dismiss === swal.DismissReason.timer ) {
                                        
                                    }
                                });
                            }
                            else
                            {
                                Swal.fire({
                                    title: "The Pre Evaluation has been added successfully!",
                                    html: 'I will close in <strong></strong> seconds.',
                                    timer: 1000,
                                    customClass: 'content-actions-center',
                                    buttonsStyling: true,
                                    onOpen: function() {
                                        swal.showLoading();
                                        timerInterval = setInterval(function () {
                                            swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                        }, 100);
                                    },
                                    onClose: function() {
                                        clearInterval(timerInterval);
                                    }
                                }).then(function (result) {
                                    if ( result.dismiss === swal.DismissReason.timer ) {
                                        window.location.href="{{ url('/user_building_permit/'.$option) }}";
                                    }
                                });
                            }
                        })
                        .catch((error) => {
                            
                        });
                    }
                });
                return false;
            });
        });
        
    </script>