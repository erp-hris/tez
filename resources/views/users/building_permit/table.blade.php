<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary card-table">
            <div class="card-header card-header-divider">
                <div class="row">
                    <div class="col-lg-6">
                        <b>{{ __('page.'.$option) }}</b>
                    </div>
                    
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table id="data_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('page.action') }}</th>
                                    <th>
                                        Filed Date
                                    </th>
                                    <th>
                                        Application Type
                                    </th>
                                    <th>
                                        Project Name
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
            <div class="card-footer">
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dataTables();
        load_datables(
            '#data_tbl',
            "{{ url('user_datatable/user_application/datatables') }}",
            {!!
                json_encode(
                    array(
                        ['data' => 'action', 'sortable' => false, 'class' => 'text-center'],
                        ['data' => 'filed_date'],
                        ['data' => 'application_type'],
                        ['data' => 'project_name'], 
                        ['data' => 'status']
                    )
                )
            !!}, null);
    });

    function create_building_permit(option, id)
    {
        window.location = "{{ url('user/building_permit/building_permit_application_form/') }}" + '/' + id + '/create';
    }
    


    
</script>