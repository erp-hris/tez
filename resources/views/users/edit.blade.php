@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.form-css')
    @include('layouts.auth-partials.datatables-css')
    <style type="text/css">
        .hr {
            margin-top: 20px;
            border: none;
            height: 3px;
            /* Set the hr color */
            color: black; /* old IE */
            background-color: black; /* Modern Browsers */
        }
    </style>
@endsection

@section('content')
    @include('others.main_content', [
        'option' => $option, 
        'title' => 'add_'.$option, 
        'has_icon' => 'icon mdi mdi-plus-circle', 
        'has_file' => $file,
        'has_footer' => 'yes', 
        'isSave' => 'yes',
        'has_frm' => 'yes',
        'cancel_url' => url('/user_building_permit/'.$option), 
        'frm_method' => 'POST',
        'frm_action' => url('/user_building_permit/'.$option.'/update'), 
        'frm_id' => 'edit_form',
    ])
@endsection

@section('scripts')
    @include('layouts.auth-partials.form-scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
        });

        
    </script>
@endsection