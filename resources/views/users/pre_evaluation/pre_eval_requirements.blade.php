<input type="hidden" id="pre_eval_id" name="pre_eval_id" value="0">
<div id="pre_eval_requirements">
    <div class="hr"></div>
    <div id="tct_canvas">
        <form role="form" method="POST" action="{{ url('/user_building_permit/pre_evalation/upload') }}" id="upload_form" >
        </form>
        <div class="hr"></div>
        @foreach($array['pre_evaluation_requirements'] as $key => $value)
        @if($value->code != 'IR' && $value->code != 'SD')
        <div class="row margin-top">
            <div class="col-12">
                <label><b>{{$key+1}}. {{ $value->name }}</b></label>
            </div>
        </div> 
        <div class="row">
            <div class="col-3">
                <input type="file" class="form-control form-control-xs" id="file_{{ $value->id }}" name="file_{{ $value->id }}">
            </div>
            <div class="col-3">
                <button type="button" class="btn btn-primary" id="upload_{{ $value->id }}">
                    <i class="mdi mdi-upload"></i>
                    Upload
                </button>
            </div>
            <div class="col-6">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" id="half_bar_{{$value->id}}">Uploading 50%</div>
                    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" id="full_bar_{{$value->id}}">Successfully Uploaded</div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
        
    </div>
    <div id="extra_tct"></div>
    <div class="row margin-top">
        <div class="col-md-12 text-right">
            <button type="button" class="btn btn-success" id="done_requirements">
                <i class="mdi mdi-check"></i>
                Done
            </button>
        </div>
    </div>
    <div class="hr"></div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("[id*='half_bar_']").hide();
        $("[id*='full_bar_']").hide();
        $("[id*='upload_']").each(function () {
            $(this).click(function () {
                var id = $(this).attr('id').split('_')[1];
                save_attachment(id, $("#pre_eval_id").val());
            });
        });
        $("#done_requirements").click(function () {
            Swal.fire({
                title: "You can now add You can now add documents as proof of your ownership/rights over the land.",
                html: 'I will close in <strong></strong> seconds.',
                timer: 1000,
                customClass: 'content-actions-center',
                buttonsStyling: true,
                onOpen: function() {
                    swal.showLoading();
                    timerInterval = setInterval(function () {
                        swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                    }, 1000);
                },
                onClose: function() {
                    clearInterval(timerInterval);
                }
            }).then(function (result) {
                if ( result.dismiss === swal.DismissReason.timer ) {
                    $("#tct_form").show();
                    $("#pre_eval_requirements").hide();
                }
            });
        });
    });
    function save_attachment(id, idx)
    {
        if($("#file_" + id).val())
        {
            $("#half_bar_" + id).show();
            $("#full_bar_" + id).hide();
            var frm = document.querySelector('#upload_form');
            var formData = new FormData();
            var file = document.querySelector('#file_' + id);

            formData.append("record_id", idx);
            formData.append('file_id', id);
            formData.append("file", file.files[0]);

            axios.post(frm.action, formData, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                Swal.fire({
                    title: "Uploaded",
                    html: 'I will close in <strong></strong> seconds.',
                    timer: 100,
                    customClass: 'content-actions-center',
                    buttonsStyling: true,
                    onOpen: function() {
                        swal.showLoading();
                        timerInterval = setInterval(function () {
                            swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                        }, 100);
                    },
                    onClose: function() {
                        clearInterval(timerInterval);
                        $("#half_bar_" + id).hide();
                        $("#full_bar_" + id).show();
                    }
                }).then(function (result) {
                    if ( result.dismiss === swal.DismissReason.timer ) {
                        
                    }
                });
            })
            .catch((error) => {
                
            });
            
        }
        
    }
</script>