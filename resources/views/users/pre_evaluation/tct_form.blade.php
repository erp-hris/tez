<div id="tct_form">
    <div class="row">
        <div class="col-lg-12">
            <table id="data_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                <thead>
                    <tr class="text-center">
                        <th style="width: 15%;">{{ __('page.action') }}</th>
                        <th>
                            Land Ownership
                        </th>
                        <th>
                            TCT No.
                        </th>
                        <th>
                            Tax Dec No.
                        </th>
                        <th>
                            Land Title
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="hr"></div>
    <div id="tct_canvas">
        <form role="form" method="POST" action="{{ url('/user_building_permit/tct_information/store') }}" id="tct_add_form" >
        </form>
        <div class="hr"></div>
        <div class="row margin-top">
            <div class="col-md-3">
                LOCATION OF CONSTRUCTION:
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <h5>Land Ownership</h5>
                <select class="select2 select2-xs" name="land_ownership" id="land_ownership">
                    <option value="0">Select Type of Ownership</option>
                    <option value="1">TITLED (Registered Land Owner)</option>
                    <option value="2">TITLED (Not Registered Land Owner)</option>
                    <option value="3">UNTITLED</option>
                    <option value="4">FOREST LAND</option>
                </select>
            </div>
            
            <div class="col-md-3">
                <h5>
                    Lot No.
                </h5>
                <input type="text" class="form-control form-control-xs" name="lot_number" id="lot_number">
            </div>
            <div class="col-md-3">
                <h5>
                    Blk No.
                </h5>
                <input type="text" class="form-control form-control-xs" name="block_no" id="block_no">
            </div>
            <div class="col-md-3">
                <h5>
                    TCT No.
                </h5>
                <input type="text" class="form-control form-control-xs" name="tct_no" id="tct_no">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Tax Dec No.
                </h5>
                <input type="text" class="form-control form-control-xs" name="tax_declaration_no" id="tax_declaration_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Street
                </h5>
                <input type="text" class="form-control form-control-xs" name="construction_street" id="construction_street">
            </div>
            <div class="col-md-3">
                <h5>
                    Barangay
                </h5>
                <input type="text" class="form-control form-control-xs" name="construction_barangay" id="construction_barangay">
            </div>
            <div class="col-md-3">
                <h5>
                    City / Municipality
                </h5>
                <input type="text" class="form-control form-control-xs" name="construction_city" id="construction_city">
            </div>
        </div>
        <div class="row margin-top">
            
            <div class="col-md-4">
                <h5>Land Title</h5>
                <input type="file" class="form-control form-control-xs" name="land_title" id="land_title">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-4">
                <h5>Tax Declaration</h5>
                <input type="file" class="form-control form-control-xs" name="tax_declaration" id="tax_declaration">
            </div>
            <div class="col-md-4">
                <h5>CENRO/DENR Certificate</h5>
                <input type="file" class="form-control form-control-xs" name="denr_certificate" id="denr_certificate">
            </div>
            <div class="col-md-4">
                <h5>Barangay Certificate</h5>
                <input type="file" class="form-control form-control-xs" name="barangay_certificate" id="barangay_certificate">
            </div>
            
        </div>
        <div class="row margin-top">
            <div class="col-md-4">
                <h5>FLAgT</h5>
                <input type="file" class="form-control form-control-xs" name="flagt" id="flagt">
            </div>
            <div class="col-md-4">
                <h5>Authorization Letter</h5>
                <input type="file" class="form-control form-control-xs" name="authorization_letter" id="authorization_letter">
            </div>
            <div class="col-md-4">
                <h5>Lease Contract</h5>
                <input type="file" class="form-control form-control-xs" name="lease_contract" id="lease_contract">
            </div>
            
        </div>
        <div class="row margin-top">
            <div class="col-md-4">
                <h5>Deed of Sale</h5>
                <input type="file" class="form-control form-control-xs" name="deed_of_sale" id="deed_of_sale">
            </div>
            <div class="col-md-4">
                <h5>Contract to Sell</h5>
                <input type="file" class="form-control form-control-xs" name="contract_to_sell" id="contract_to_sell">
            </div>
            <div class="col-md-4">
                <h5>Secretary's Certificate</h5>
                <input type="file" class="form-control form-control-xs" name="secretary_certificate" id="secretary_certificate">
            </div>
        </div>
        
    </div>
    <div id="extra_tct"></div>
    <div class="row margin-top">
        <div class="col-md-12 text-right">
            <button type="button" class="btn btn-primary" id="btn_add_tct">
                <i class="mdi mdi-save"></i>
                Save
            </button>
            <button type="button" class="btn btn-success" id="btn_done">
                <i class="mdi mdi-check"></i>
                Done
            </button>
        </div>
    </div>
    <div class="hr"></div>
</div>