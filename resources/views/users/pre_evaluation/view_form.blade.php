<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-header card-header-divider">
                <div class="row">
                    <div class="col-md-6">
                        Pre Evaluation Form
                    </div>
                    <div class="col-md-6 text-right">
                        
                    </div>
                </div>
            </div>
            <div class="card-body">
                @include('users.pre_evaluation.pre_eval_form')
                <div class="hr"></div>
                <div class="row margin-top">
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-space btn-secondary" id="view_pre_eval_reqs">
                            View Pre Evaluation Requirements
                        </button>
                    </div>
                </div>
                <div class="hr"></div>
                @php
                    $record = $array['record'];
                @endphp
                <div class="row margin-top">
                    @if($record->iper)
                    <div class="col-lg-4">
                        <b>IPER Report</b>
                        <br>
                        <a href="{{ asset('tieza/pre_evaluation_building_permit/'.$record->id.'/'.$record->iper) }}" target="_blank" class="btn btn-space btn-secondary">
                                <i class="mdi mdi-eye"></i>&nbsp;&nbsp;&nbsp;View
                        </a>
                        <a href="{{ asset('tieza/pre_evaluation_building_permit/'.$record->id.'/'.$record->iper) }}" class="btn btn-space btn-secondary" download>
                            <i class="mdi mdi-download"></i>&nbsp;&nbsp;&nbsp;Download
                        </a>
                    </div>
                    @endif
                    @if($record->transmittal_letter)
                    <div class="col-lg-4">
                        <b>Transmittal Letter</b>
                        <br>
                        <a href="{{ asset('tieza/pre_evaluation_building_permit/'.$record->id.'/'.$record->transmittal_letter) }}" target="_blank" class="btn btn-space btn-secondary">
                                <i class="mdi mdi-eye"></i>&nbsp;&nbsp;&nbsp;View
                        </a>
                        <a href="{{ asset('tieza/pre_evaluation_building_permit/'.$record->id.'/'.$record->transmittal_letter) }}" class="btn btn-space btn-secondary" download>
                            <i class="mdi mdi-download"></i>&nbsp;&nbsp;&nbsp;Download
                        </a>
                    </div>
                    @endif
                </div>
                
            </div>
        </div>   
    </div>
    
</div>
@section('additional-scripts')

@endsection
<script type="text/javascript">
    $(document).ready(function () {
        $("#view_pre_eval_reqs").click(function () {
            var href = 'view-pre-eval-reqs';
            window.open(href, '_blank');
        });
        $(".form-control").prop('disabled', true);
        get_application('pre_evaluation', {{ $id }});
    });
    function get_application(option, id)
    {
        $("#for_approval").hide();
        $("#for_receipt").hide();
        axios.get("{{ url('get-application') }}/" + option + "/" + id)
        .then(function(response){
            console.log(response);
            $("#last_name").val(response.data.data.pre_eval.last_name);
            $("#first_name").val(response.data.data.pre_eval.first_name);
            $("#middle_name").val(response.data.data.pre_eval.middle_name);
            $("#tin").val(response.data.data.pre_eval.tin);
            $("#project_name").val(response.data.data.pre_eval.project_name);
            $("#enterprise_name").val(response.data.data.pre_eval.enterprise_name);
            $("#ownership_form").val(response.data.data.pre_eval.ownership_form);
            $("#house_number").val(response.data.data.pre_eval.house_number);
            $("#street").val(response.data.data.pre_eval.street);
            $("#barangay").val(response.data.data.pre_eval.barangay);
            $("#city").val(response.data.data.pre_eval.city);
            $("#zip_code").val(response.data.data.pre_eval.zip_code);
            $("#contact_number").val(response.data.data.pre_eval.contact_number);
            $("#application_type").val(response.data.data.pre_eval.application_type);
            
        })
        .catch(function(error){
           
        }) 
    }
    
</script>