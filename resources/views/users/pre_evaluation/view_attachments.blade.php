
<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-header card-header-divider">
                <div class="row">
                    <div class="col-md-6">
                        List of Pre Evaluation Attachments
                    </div>
                    <div class="col-md-6 text-right">
                        <button class="btn btn-space btn-danger" onclick="window.close();">
                            <i class="mdi mdi-close"></i>
                            Close Tab
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach($array['attachments'] as $key => $value)
                    @php
                        $file_path = 'tieza/pre_evaluation_building_permit/pre_eval_attachments/'.$value->record_id.'/'.$value->attachment_name;

                        $remarks = $value->remarks;
                        if($remarks == 1)
                        {
                            $supplemental = 'supplemental File';
                        }
                        else
                        {
                            $supplemental = '';
                        }
                    @endphp
                    <div class="col-lg-4 box" onclick="window.open('{{ asset($file_path) }}')">
                        @php

                            $file_ext = explode('.', $value->attachment_name)[1];

                            if($file_ext == 'pdf')
                            {
                                $icon = 'pdf_icon.png';
                            }
                            else if ($file_ext == 'jpg')
                            {
                                $icon = 'img_icon.png';
                            }
                            else
                            {
                                $icon = 'file_icon.png';
                            }
                        @endphp
                        <br>
                        <b style="font-size: 10pt;">{{ $value->file_name }}{{ $supplemental ? ' ('.$supplemental.')' : '' }}</b>
                        <br><br>
                        <center>
                        @if($file_ext == 'jpg' || $file_ext == 'png')
                            <img src="{{ asset($file_path) }}" width="100px" height="100px" />
                        @else
                            <img src="{{ asset('img/'.$icon) }}" width="100px" height="100px" />
                        @endif
                        
                        </center>
                        <br>
                        <u>{{ $value->original_name }}</u>
                        @if($remarks == 1)
                        <br>
                        <b>Submitted on</b>: <u>{{ date('F d, Y h:i:s A', strtotime($value->created_at)) }}</u>
                        @endif
                        
                    </div>
                    @endforeach
                </div>
                
            </div>
        </div>   
    </div>
    
</div>
@section('additional-scripts')

@endsection
<script type="text/javascript">
    $(document).ready(function () {
        $("#view_pre_eval_reqs").click(function () {
            var href = 'view-pre-eval-reqs';
            window.open(href, '_blank');
        });
    });
    
</script>