<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary card-table">
            <div class="card-header card-header-divider">
                <!-- <div class="row">
                    <div class="col-lg-3">
                        <label>Amount</label>
                        <input type="text" class="form-control form-control-xs" name="" id="amount">
                    </div>
                    <div class="col-lg-3">
                        <label>Transaction Fee</label>
                        <input type="text" class="form-control form-control-xs" name="" id="tfee">
                    </div>
                    <div class="col-lg-3">
                        <label>Total Amount</label>
                        <input type="text" class="form-control form-control-xs" name="" id="total">
                    </div>
                </div> -->
                <div class="row margin-top">
                    <div class="col-lg-6">
                        <b>{{ __('page.'.$option) }}</b>
                    </div>
                    <div class="col-lg-6 text-right">
                        <a href="{{url('user_building_permit/'.$option.'/create')}}"
                           class="btn btn-space btn-primary">
                            <i class="icon icon-left mdi mdi-account-add"></i>
                            {{ __('page.add_new_record') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table id="data_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 15%;">{{ __('page.action') }}</th>
                                    <th>
                                        Filed Date
                                    </th>
                                    <th>
                                        Application Type
                                    </th>
                                    <th>
                                        Project Name
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
            <div class="card-footer">
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dataTables();
        load_datables(
            '#data_tbl',
            "{{ url('user_datatable/user_pre_evaluation/datatables') }}",
            {!!
                json_encode(
                    array(
                        ['data' => 'action', 'sortable' => false],
                        ['data' => 'filed_date'],
                        ['data' => 'application_type'],
                        ['data' => 'project_name'], 
                        ['data' => 'status']
                    )
                )
            !!}, null);


        $("#amount").change(function () {
            var value = parseFloat($(this).val());
            var tfee = parseFloat($("#tfee").val());
            var total = parseFloat($("#total").val());

            if(value == '') value = 0;
            if(tfee == '') tfee = 0;
            if(total == '') total = 0;

            if(value <= 10000)
            {
                $("#tfee").val('500.00');
                $("#total").val(value + 500);
            }
            else
            {
                value = value - 10000;
                tfee = value * 0.01 + 500;
                $("#tfee").val(tfee);
                $("#total").val(parseFloat($(this).val()) + tfee);
            }
        });
    });


    function remove_pre_eval(option,id)
    {
        Swal.fire({
            title: "Remove Pre Evaluation Application",
            html: "Do you want to cancel this to <b>Pre Evaluation Application</b> for Building Permit?",
            confirmButtonText: 'Proceed',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-secondary',
            closeButtonClass: 'btn btn-secondary',
            showCloseButton: true,
            showCancelButton: true,
            customClass: 'colored-header colored-header-danger'
        }).then((isSave) => {
            if (isSave.value) {
                axios.post("{{ url('user') }}/" + option + "/" + id  + "/remove-pre-eval")
                .then((response) => {
                    console.log(response);
                    var timerInterval = 0;
                    Swal.fire({
                        title: "The Application has been added cancelled!",
                        html: 'I will close in <strong></strong> seconds.',
                        timer: 1000,
                        customClass: 'content-actions-center',
                        buttonsStyling: true,
                        onOpen: function() {
                            swal.showLoading();
                            timerInterval = setInterval(function () {
                                swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                            }, 100);
                        },
                        onClose: function() {
                            clearInterval(timerInterval);
                        }
                    }).then(function (result) {
                        if ( result.dismiss === swal.DismissReason.timer ) {
                            load_datables(
                            '#data_tbl',
                            "{{ url('user_datatable/user_pre_evaluation/datatables') }}",
                            {!!
                                json_encode(
                                    array(
                                        ['data' => 'action', 'sortable' => false],
                                        ['data' => 'filed_date'],
                                        ['data' => 'application_type'],
                                        ['data' => 'project_name'], 
                                        ['data' => 'status']
                                    )
                                )
                            !!}, null);
                        }
                    });
                })
                .catch((error) => {
                    
                });
            }
        });
        return false;
    }


    function pass_pre_eval_to_assessor(option,id)
    {
        Swal.fire({
            title: "Submit for Assessment?",
            html: "Do you want to submit this for <b>Assessment</b>?",
            confirmButtonText: 'Proceed',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-secondary',
            closeButtonClass: 'btn btn-secondary',
            showCloseButton: true,
            showCancelButton: true,
            customClass: 'colored-header colored-header-primary'
        }).then((isSave) => {
            if (isSave.value) {
                axios.post("{{ url('user') }}/" + option + "/" + id  + "/pass-pre-eval-to-assessor")
                .then((response) => {
                    console.log(response);
                    var timerInterval = 0;
                    Swal.fire({
                        title: "The Application is for assessment!",
                        html: 'I will close in <strong></strong> seconds.',
                        timer: 1000,
                        customClass: 'content-actions-center',
                        buttonsStyling: true,
                        onOpen: function() {
                            swal.showLoading();
                            timerInterval = setInterval(function () {
                                swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                            }, 100);
                        },
                        onClose: function() {
                            clearInterval(timerInterval);
                        }
                    }).then(function (result) {
                        if ( result.dismiss === swal.DismissReason.timer ) {
                            load_datables(
                            '#data_tbl',
                            "{{ url('user_datatable/user_pre_evaluation/datatables') }}",
                            {!!
                                json_encode(
                                    array(
                                        ['data' => 'action', 'sortable' => false],
                                        ['data' => 'filed_date'],
                                        ['data' => 'application_type'],
                                        ['data' => 'project_name'], 
                                        ['data' => 'status']
                                    )
                                )
                            !!}, null);
                        }
                    });
                })
                .catch((error) => {
                    
                });
            }
        });
        return false;
    }


    
</script>