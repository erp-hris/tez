<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-header card-header-divider">
                <div class="row">
                    <div class="col-md-6">
                        Pre Evaluation Form
                    </div>
                    <div class="col-md-6 text-right">
                        <span style="color: red;"><b>List of requirements for proof of ownership/right over the land:</b>&nbsp;&nbsp;<i class="mdi mdi-arrow-right"></i></span>&nbsp;&nbsp;
                        <button type="button" class="btn btn-space btn-secondary btn-small" id="requirements">
                                <i class="mdi mdi-pin-help"></i> Requirements
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @include('users.pre_evaluation.pre_eval_form')
                @include('users.pre_evaluation.tct_form')
                @include('users.pre_evaluation.pre_eval_requirements')
            </div>
        </div>   
    </div>
    <div class="modal fade colored-header colored-header-success" id="requirements_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog full-width">
            <div class="modal-content">
                <div class="modal-header modal-header-colored">
                    <h3 class="modal-title">List of requirements for proof of ownership/right over the land</h3>
                    <button class="close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>

                </div>
                <div class="modal-body">
                    <div class="row margin-top">
                        <div class="col-md-4">
                            <ul>
                                <li><b>SINGLE PROPRIETOR</b></li>
                                <ol>
                                    <li>
                                        TITLED (Registered Land Owner)
                                    </li>
                                    <ul>
                                        <li>Land Title</li>
                                    </ul>
                                    <li>
                                        TITLED (Not Registered Land Owner)
                                    </li>
                                    <ul>
                                        <li>Land Title</li>
                                        <li>Authorization Letter</li>
                                        <li>Deed of Sale</li>
                                        <li>Lease Contract</li>
                                        <li>Contract to Sell</li>
                                    </ul>
                                    <li>UNTITLED (Alienable and Disposable Land)</li>
                                    <ul>
                                        <li>CENRO / DENR Certificate</li>
                                        <li>Barangay Certificate</li>
                                    </ul>
                                    <li>FOREST LAND</li>
                                    <ul>
                                        <li>FLAgT</li>
                                    </ul>
                                </ol>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul>
                                <li><b>PARTNERSHIP</b></li>
                                <ol>
                                    <li>
                                        TITLED (Registered Land Owner)
                                    </li>
                                    <ul>
                                        <li>Land Title</li>
                                    </ul>
                                    <li>
                                        TITLED (Not Registered Land Owner)
                                    </li>
                                    <ul>
                                        <li>Land Title</li>
                                        <li>Authorization Letter</li>
                                        <li>Deed of Sale</li>
                                        <li>Lease Contract</li>
                                        <li>Contract to Sell</li>
                                    </ul>
                                    <li>UNTITLED (Alienable and Disposable Land)</li>
                                    <ul>
                                        <li>CENRO / DENR Certificate</li>
                                        <li>Barangay Certificate</li>
                                    </ul>
                                    <li>FOREST LAND</li>
                                    <ul>
                                        <li>FLAgT</li>
                                    </ul>
                                </ol>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul>
                                <li><b>CORPORATION</b></li>
                                <ol>
                                    <li>
                                        TITLED (Registered Land Owner)
                                    </li>
                                    <ul>
                                        <li>Land Title</li>
                                        <li>Secretary's Certificate</li>
                                    </ul>
                                    <li>
                                        TITLED (Not Registered Land Owner)
                                    </li>
                                    <ul>
                                        <li>Land Title</li>
                                        <li>Authorization Letter</li>
                                        <li>Deed of Sale</li>
                                        <li>Lease Contract</li>
                                        <li>Contract to Sell</li>
                                        <li>Secretary's Certificate</li>
                                    </ul>
                                    <li>UNTITLED (Alienable and Disposable Land)</li>
                                    <ul>
                                        <li>CENRO / DENR Certificate</li>
                                        <li>Barangay Certificate</li>
                                        <li>Secretary's Certificate</li>
                                    </ul>
                                    <li>FOREST LAND</li>
                                    <ul>
                                        <li>FLAgT</li>
                                        <li>Secretary's Certificate</li>
                                    </ul>
                                </ol>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@section('additional-scripts')

@endsection
<script type="text/javascript">
    $(document).ready(function () {
        $("#requirements_box").hide();
        $("#tct_form").hide();
        $("#pre_eval_requirements").hide();

        $("#requirements").click(function () {
            $("#requirements_modal").modal();
        });
        
        $("#btn_remove_tct").click(function () {
            $(this).closest("#extra_tct").remove();
        });
        $("#btn_done").click(function () {
            window.location.href="{{ url('/user_building_permit/'.$option) }}";
        });
        $("#btn_add_tct").click(function () {
            /*var tct_count = $("#tct_count").val();
            tct_count++;
            $("#tct_count").val(tct_count);
            var canvas = $("#tct_canvas").clone(true).find("input:text").val("").end();
            $("#extra_tct").append(canvas);*/

            var frm = document.querySelector('#tct_add_form');
            Swal.fire({
                title: "Save Information.",
                text: "{{ __('page.add_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                closeButtonClass: 'btn btn-secondary',
                cancelButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    Swal.fire({
                        title: 'Saving...',
                        customClass: 'content-actions-center',
                        buttonsStyling: true,
                        allowOutsideClick: false,
                        onOpen: function() {
                            swal.showLoading();
                            //$(this).prop('disabled', true);
                            var formData = new FormData();

                            var land_title = document.querySelector("#land_title");

                            if(land_title)
                            {
                                formData.append("land_title", land_title.files[0]);
                            }
                            
                            var tax_declaration = document.querySelector("#tax_declaration");
                            if(tax_declaration)
                            {
                                formData.append("tax_declaration", tax_declaration.files[0]);
                            }

                            var denr_certificate = document.querySelector("#denr_certificate");
                            if(denr_certificate)
                            {
                                formData.append("denr_certificate", denr_certificate.files[0]);
                            }

                            var barangay_certificate = document.querySelector("#barangay_certificate");
                            if(barangay_certificate)
                            {
                                formData.append("barangay_certificate", barangay_certificate.files[0]);
                            }

                            var flagt = document.querySelector("#flagt");
                            if(flagt)
                            {
                                formData.append("flagt", flagt.files[0]);
                            }

                            var authorization_letter = document.querySelector("#authorization_letter");
                            if(authorization_letter)
                            {
                                formData.append("authorization_letter", authorization_letter.files[0]);
                            }

                            var lease_contract = document.querySelector("#lease_contract");
                            if(lease_contract)
                            {
                                formData.append("lease_contract", lease_contract.files[0]);
                            }

                            var deed_of_sale = document.querySelector("#deed_of_sale");
                            if(deed_of_sale)
                            {
                                formData.append("deed_of_sale", deed_of_sale.files[0]);
                            }

                            var contract_to_sell = document.querySelector("#contract_to_sell");
                            if(contract_to_sell)
                            {
                                formData.append("contract_to_sell", contract_to_sell.files[0]);
                            }

                            var secretary_certificate = document.querySelector("#secretary_certificate");
                            if(secretary_certificate)
                            {
                                formData.append("secretary_certificate", secretary_certificate.files[0]);
                            }


                            formData.append("land_ownership", $("#land_ownership").val());
                            formData.append("lot_number", $("#lot_number").val());
                            formData.append("block_no", $("#block_no").val());
                            formData.append("tct_no", $("#tct_no").val());
                            formData.append("tax_declaration_no", $("#tax_declaration_no").val());
                            formData.append("construction_street", $("#construction_street").val());
                            formData.append("construction_barangay", $("#construction_barangay").val());
                            formData.append("construction_city", $("#construction_city").val());
                            formData.append("pre_eval_id", $("#pre_eval_id").val());
                            var pre_eval_id = $("#pre_eval_id").val();
                            axios.post(frm.action, formData, {
                                headers: {
                                  'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((response) => {
                                const swal_success = alert_success('TCT Information successfully saved.');
                                swal_success.then((value) => {
                                    const add_again = alert_continue('Continue to add', 'Do you want to add again?');
                                    add_again.then((result) => {
                                        if(result.value){
                                            $('#land_ownership').val(0).trigger('change');
                                            $("#lot_number").val('');
                                            $("#block_no").val('');
                                            $("#tct_no").val('');
                                            $("#tax_declaration_no").val('');
                                            $("#construction_street").val('');
                                            $("#construction_barangay").val('');
                                            $("#construction_city").val('');
                                            $("#land_title").val('');
                                            $("#tax_declaration").val('');
                                            $("#denr_certificate").val('');
                                            $("#barangay_certificate").val('');
                                            $("#flagt").val('');
                                            $("#authorization_letter").val('');
                                            $("#lease_contract").val('');
                                            $("#deed_of_sale").val('');
                                            $("#contract_to_sell").val('');
                                            $("#secretary_certificate").val('');
                                            load_datables(
                                            '#data_tbl',
                                            "{{ url('user/tct_information/building_permit/') }}" + '/' + pre_eval_id + '/datatables',
                                            {!!
                                                json_encode(
                                                    array(
                                                        ['data' => 'action', 'sortable' => false],
                                                        ['data' => 'land_ownership'],
                                                        ['data' => 'tct_no'],
                                                        ['data' => 'tax_declaration_no'],
                                                        ['data' => 'land_title_original_name']
                                                    )
                                                )
                                            !!}, null);

                                        }
                                        else
                                        {
                                            window.location.href="{{ url('/user_building_permit/'.$option) }}";
                                        }
                                    });    
                                });
                            })
                            .catch((error) => {
                                
                            });           
                            
                        }
                    });
                }
            });
            return false;
        });
        $("#save_btn").click(function () { 
            var frm = document.querySelector('#add_form');

            Swal.fire({
                title: "Save Pre Evaluation",
                text: "{{ __('page.add_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                closeButtonClass: 'btn btn-secondary',
                cancelButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave.value) {
                    Swal.fire({
                        title: 'Saving...',
                        customClass: 'content-actions-center',
                        buttonsStyling: true,
                        allowOutsideClick: false,
                        onOpen: function() {
                            swal.showLoading();
                            //$(this).prop('disabled', true);
                            var formData = new FormData();
                            /*//var land_document = document.querySelector("#land_document");
                            var survey_plan = document.querySelector("#survey_plan");

                            if(survey_plan)
                            {
                                formData.append("survey_plan", survey_plan.files[0]);
                            }
                            
                            var architectural_plan = document.querySelector("#architectural_plan");
                            if(architectural_plan)
                            {
                                formData.append("architectural_plan", architectural_plan.files[0]);
                            }

                            var structural_plan = document.querySelector("#structural_plan");
                            if(structural_plan)
                            {
                                formData.append("structural_plan", structural_plan.files[0]);
                            }

                            var electrical_plan = document.querySelector("#electrical_plan");
                            if(electrical_plan)
                            {
                                formData.append("electrical_plan", electrical_plan.files[0]);
                            }

                            var mechanical_plan = document.querySelector("#mechanical_plan");
                            if(mechanical_plan)
                            {
                                formData.append("mechanical_plan", mechanical_plan.files[0]);
                            }

                            var sanitary_plan = document.querySelector("#sanitary_plan");
                            if(sanitary_plan)
                            {
                                formData.append("sanitary_plan", sanitary_plan.files[0]);
                            }

                            var plumbing_plan = document.querySelector("#plumbing_plan");
                            if(plumbing_plan)
                            {
                                formData.append("plumbing_plan", plumbing_plan.files[0]);
                            }

                            var electronics_plan = document.querySelector("#electronics_plan");
                            if(electronics_plan)
                            {
                                formData.append("electronics_plan", electronics_plan.files[0]);
                            }*/


                            formData.append("application_type", $("#application_type").val());
                            formData.append("last_name", $("#last_name").val());
                            formData.append("first_name", $("#first_name").val());
                            formData.append("middle_name", $("#middle_name").val());
                            formData.append("tin", $("#tin").val());
                            formData.append("project_name", $("#project_name").val());
                            formData.append("enterprise_name", $("#enterprise_name").val());
                            formData.append("ownership_form", $("#ownership_form").val());
                            formData.append("house_number", $("#house_number").val());
                            formData.append("street", $("#street").val());
                            formData.append("barangay", $("#barangay").val());
                            formData.append("city", $("#city").val());
                            formData.append("zip_code", $("#zip_code").val());
                            formData.append("contact_number", $("#contact_number").val());
                            formData.append("application_type", $("#application_type").val());

                            

                            axios.post(frm.action, formData, {
                                headers: {
                                  'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((response) => {
                                if(response.data.exist_record)
                                {
                                    Swal.fire({
                                        title: "Project Name Already Exist!",
                                        html: 'I will close in <strong></strong> seconds.',
                                        timer: 1000,
                                        customClass: 'content-actions-center',
                                        buttonsStyling: true,
                                        onOpen: function() {
                                            swal.showLoading();
                                            timerInterval = setInterval(function () {
                                                swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                            }, 300);
                                        },
                                        onClose: function() {
                                            clearInterval(timerInterval);
                                        }
                                    }).then(function (result) {
                                        if ( result.dismiss === swal.DismissReason.timer ) {
                                            
                                        }
                                    });
                                }
                                else
                                {
                                    Swal.fire({
                                        title: "The Pre Evaluation has been added successfully!",
                                        html: 'I will close in <strong></strong> seconds.',
                                        timer: 1000,
                                        customClass: 'content-actions-center',
                                        buttonsStyling: true,
                                        onOpen: function() {
                                            swal.showLoading();
                                            timerInterval = setInterval(function () {
                                                swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                            }, 300);
                                        },
                                        onClose: function() {
                                            clearInterval(timerInterval);
                                        }
                                    }).then(function (result) {
                                        if ( result.dismiss === swal.DismissReason.timer ) {
                                            var record_id = response.data.record_id;
                                            $("#pre_eval_id").val(record_id);
                                            $("#pre_eval_form").hide();
                                            $(".card-footer").empty();
                                            Swal.fire({
                                                title: "You can now add Pre Evaluation Requirements",
                                                html: 'I will close in <strong></strong> seconds.',
                                                timer: 1000,
                                                customClass: 'content-actions-center',
                                                buttonsStyling: true,
                                                onOpen: function() {
                                                    swal.showLoading();
                                                    timerInterval = setInterval(function () {
                                                        swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                                    }, 1000);
                                                },
                                                onClose: function() {
                                                    clearInterval(timerInterval);
                                                }
                                            }).then(function (result) {
                                                if ( result.dismiss === swal.DismissReason.timer ) {
                                                    $("#pre_eval_requirements").show();
                                                }
                                            });
                                            
                                            
                                            //
                                        }
                                    });
                                }
                            })
                            .catch((error) => {
                                
                            });
                        }
                    });
                    
                }
            });
            return false;
        });
    });
    
</script>