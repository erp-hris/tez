<div id="pre_eval_form">
    <div class="row margin-top">
        <div class="col-md-3">
            <h5>Type of Application for Building Permit</h5>
            <select class="select2 select2-xs" id="application_type" name="application_type">
                <option value="0">{{ __('page.please_select') }}</option>
                <option value="1">New</option>
                <option value="2">Renewal</option>
                <option value="3">Amendatory</option>
            </select>
        </div>
    </div>
    <div class="row margin-top">
        <div class="col-md-3">
            <h5>
                Last Name
            </h5>
            <input type="text" class="form-control form-control-xs" id="last_name" name="last_name" value="{{ $array['user_info']->last_name }}" readonly>
        </div>
        <div class="col-md-3">
            <h5>
                First Name
            </h5>
            <input type="text" class="form-control form-control-xs" id="first_name" name="first_name" value="{{ $array['user_info']->first_name }}" readonly>
        </div>
        <div class="col-md-3">
            <h5>
                M.I.
            </h5>
            <input type="text" class="form-control form-control-xs" id="middle_name" name="middle_name" readonly value="{{ $array['user_info']->middle_name }}">
        </div>
        <div class="col-md-3">
            <h5>
                TIN
            </h5>
            <input type="text" class="form-control form-control-xs" id="tin" name="tin">
        </div>
    </div>
    <div class="row margin-top">
        <div class="col-md-4">
            <h5>
                Project Name
            </h5>
            <input type="text" class="form-control form-control-xs" id="project_name" name="project_name">
        </div>
        <div class="col-md-4">
            <h5>
                Enterprise Name
            </h5>
            <input type="text" class="form-control form-control-xs" id="enterprise_name" name="enterprise_name">
        </div>
        <div class="col-md-4">
            <h5>
                Form of Ownership
            </h5>
            <select class="select2 select2-xs" id="ownership_form" name="ownership_form">
                <option value="">Select Form of Ownership</option>
                <option value="SINGLE PROPRIETOR">SINGLE PROPRIETOR</option>
                <option value="PARTNERSHIP">PARTNERSHIP</option>
                <option value="CORPORATION">CORPORATION</option>

            </select>
        </div>
        
    </div>
    <div class="hr"></div>
    <div class="row margin-top">
        <div class="col-md-3">
            <h5>
                ADDRESS
            </h5>
        </div>
        <div class="col-md-3">
            <h5>
                House No.
            </h5>
            <input type="text" class="form-control form-control-xs" id="house_number" name="house_number">
        </div>
        <div class="col-md-3">
            <h5>
                Street
            </h5>
            <input type="text" class="form-control form-control-xs" id="street" name="street">
        </div>
        <div class="col-md-3">
            <h5>
                Barangay
            </h5>
            <input type="text" class="form-control form-control-xs" id="barangay" name="barangay">
        </div>
    </div>
    <div class="row margin-top">
        <div class="col-md-3">
        </div>
        <div class="col-md-3">
            <h5>
                City / Municipality
            </h5>
            <input type="text" class="form-control form-control-xs" id="city" name="city">
        </div>
        <div class="col-md-3">
            <h5>
                Zip Code
            </h5>
            <input type="text" class="form-control form-control-xs" id="zip_code" name="zip_code">
        </div>
        <div class="col-md-3">
            <h5>
                Contact No.
            </h5>
            <input type="text" class="form-control form-control-xs" id="contact_number" name="contact_number">
        </div>
    </div>
    
    <!-- <div class="row margin-top">
        <div class="col-md-6">
            <h5>Survey Plan</h5>
            <input type="file" class="form-control form-control-xs" name="survey_plan" id="survey_plan">
        </div>
    </div>  
    <div class="hr"></div>
    <div class="row margin-top">
        <div class="col-md-12">
            <b>Building Plan</b>
        </div>
    </div>
    <div class="row margin-top">
        <div class="col-md-4">
            <h5>Architectural Plan</h5>
            <input type="file" class="form-control form-control-xs" name="building_plan" id="building_plan">
        </div>
        <div class="col-md-4">
            <h5>Civil/Structural Plan</h5>
            <input type="file" class="form-control form-control-xs" name="structural_plan" id="structural_plan">
        </div>
        <div class="col-md-4">
            <h5>Electrical Plan</h5>
            <input type="file" class="form-control form-control-xs" name="electrical_plan" id="electrical_plan">
        </div>
    </div>
    <div class="row margin-top">
        <div class="col-md-4">
            <h5>Mechanical Plan</h5>
            <input type="file" class="form-control form-control-xs" name="mechanical_plan" id="mechanical_plan">
        </div>
        <div class="col-md-4">
            <h5>Sanitary Plan</h5>
            <input type="file" class="form-control form-control-xs" name="sanitary_plan" id="sanitary_plan">
        </div>
        
        <div class="col-md-4">
            <h5>Plumbing Plan</h5>
            <input type="file" class="form-control form-control-xs" name="plumbing_plan" id="plumbing_plan">
        </div>
    </div>
    <div class="row margin-top">
        <div class="col-md-4">
            <h5>Electronics Plan</h5>
            <input type="file" class="form-control form-control-xs" name="electronics_plan" id="electronics_plan">
        </div>
    </div> -->
</div>