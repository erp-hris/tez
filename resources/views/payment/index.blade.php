@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
  

    @php 
        $data = [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_footer' => 'yes', 
        // 'cancel_url' => url('a')
        ];
        // $data['isSave'] = 'yes';
    @endphp

    @include('others.main_content', $data)
    


@endsection

@section('scripts')

@include('layouts.auth-partials.datatables-scripts')
@yield('additional-scripts')
        
    <script type="text/javascript">
        $(document).ready(function(){
            // $('#main-content').addClass('be-aside');
             $('#main-content').addClass('be-nosidebar-left');
            $(".be-left-sidebar").hide();
        });
    

        
     
    </script>
@endsection