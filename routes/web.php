<?php

Route::any('/', 'Auth\LoginController@showLoginForm');
Route::any('/registration', 'RegisterController@index');
Route::any('/otp/{id}', 'RegisterController@check_otp');
Route::any('/store-registration', 'RegisterController@register');
Route::any('/validate-otp', 'RegisterController@validate_otp');

Auth::routes();
Route::any('/create_payment/{id}', 'ApplicationController@create_payment');
Route::any('/reset-password/{id}', 'ApplicationController@reset_pw');
Route::any('/test/{id}', 'ApplicationController@test');


// Dashboard
Route::any('/', 'HomeController@index');

Route::any('/create_account', 'ApplicationController@create_account');
Route::any('/upload_reqs', 'ApplicationController@upload_requirements');
Route::any('/create_account', 'ApplicationController@create_account');
Route::any('/direct', 'PaymentController@direct_payment');
Route::any('/indirect', 'PaymentController@indirect_payment');


// Building Permit
//User Side
Route::any('user_pre_evaluation/{id}/view-pre-eval-reqs', 'BuildingPermitController@view_pre_eval_reqs');
Route::any('{module}/{option}/{id}/view-pre-eval-reqs', 'BuildingPermitController@view_pre_eval_reqs_admin');

Route::any('/user_building_permit/{option}', 'BuildingPermitController@index');
Route::any('/user_datatable/{option}/datatables', 'BuildingPermitController@user_datatable');
Route::any('/user_building_permit/{option}/create', 'BuildingPermitController@user_create');
Route::any('/user_building_permit/{option}/store', 'BuildingPermitController@user_store');
Route::any('/user/{option}/{id}/remove-pre-eval', 'BuildingPermitController@user_remove_pre_eval');
Route::any('/user/{option}/{id}/edit', 'BuildingPermitController@user_edit');
Route::any('/user/{option}/{id}/pass-pre-eval-to-assessor', 'BuildingPermitController@pass_pre_eval_to_assessor');
Route::any('/user_building_permit/pre_evalation/upload', 'BuildingPermitController@upload_pre_eval');

Route::any('/user/{option}/{id}/approved-pre-evaluation', 'BuildingPermitController@approved_pre_evaluation');
Route::any('user/{module}/{option}/{pre_eval_id}/create', 'ApplicationController@create_application');
Route::any('user/{module}/{option}/{pre_eval_id}/datatables', 'BuildingPermitController@list_tct_informations');
Route::any('{option}/{id}/view-record', 'BuildingPermitController@view_record');
Route::any('{id}/add-suplemental', 'BuildingPermitController@add_suplemental');






Route::any('/{module}/{option}', 'ApplicationController@index');
Route::any('/{module}/{option}/create', 'ApplicationController@create');
Route::any('/{module}/{id}/edit', 'ApplicationController@edit');
Route::any('/get-application/{option}/{id}', 'ApplicationController@get_application');
Route::any('/{module}/{option}/datatables', 'ApplicationController@datatables');
Route::any('/{module}/{option}/store', 'ApplicationController@store');
Route::any('/{module}/{option}/store-remarks', 'ApplicationController@store_remarks');
Route::any('/{module}/{option}/upload', 'ApplicationController@upload');
Route::any('/{module}/{option}/upload-attachment', 'ApplicationController@upload_file_for_fsec');
Route::any('/{module}/{option}/update', 'ApplicationController@update');

Route::any('/{module}/{option}/{id}/upload-evaluation-attachments', 'ApplicationController@upload_evaluation_attachments');
Route::any('/{module}/{option}/{id}/return-to-client', 'ApplicationController@return_to_client');

Route::any('/{module}/{option}/upload-fsec', 'ApplicationController@upload_fsec');


Route::any('/{module}/{option}/{id}/view-attachment', 'ApplicationController@view_attachment');
Route::any('/tez/{permit}/{id}/to-assess', 'ApplicationController@to_assess');

Route::any('/{module}/{option}/application-form/{id}/view-form', 'ApplicationController@view_form');

Route::any('/{module}/{file_id}/{record_id}/datatables', 'ApplicationController@get_attachment');
Route::any('/{option}/{module}/{id}/delete', 'ApplicationController@destroy');

Route::any('/report', 'ReportController@index');
Route::any('/report/view-report/{option}', 'ReportController@view_report');



Route::any('/{module}/{option}/return-application', 'ApplicationController@return_application');

Route::any('/{module}/{option}/{id}/add-suplemental-proper', 'ApplicationController@add_suplemental_proper');

Route::any('/{module}/{option}/upload-supplemental', 'ApplicationController@upload_supplemental');

Route::any('/{module}/{option}/{id}/upload-atap', 'ApplicationController@upload_atap');
Route::any('/{module}/{option}/{id}/upload-payment-details', 'ApplicationController@upload_payment_details');

Route::any('/report/view-permit/{id}', 'ApplicationController@view_permit');