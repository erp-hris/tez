<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username', 'email', 'password', 'confirm_password', 'user_type' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return Auth::user()->level == 0;
    }

    public function isUser()
    {
        return Auth::user()->level == 1;
    }

    public function getEmployeeID()
    {
        return Auth::user()->employee_id;
    }

    public function Employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
