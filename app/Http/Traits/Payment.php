<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use DB;
use Exception;

trait Payment
{
	protected function get_user_info($table, $record_id)
	{
		$data = $this->get_record($table, $record_id);
		if($data)
		{
			$user_id = $data->user_id;
			$user_data = $this->get_record('users', $user_id);
			
		}
		else
		{
			$user_data = array();
		}
		return $user_data;
	}

	protected function get_record($table, $id)
	{
		return DB::table($table)
		->where($table.'.id', '=', $id)
		->where($table.'.is_deleted', null)
		->first();
	}
}
	