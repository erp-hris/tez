<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use DB;
use Exception;

trait Application
{
	protected function list_business_permits($user_id, $evaluator, $approver, $issuer)
	{
		$query = DB::table('business_permit')
		->where('business_permit.is_deleted', null);
		if($user_id)
		{
			$query->where('user_id', '=', $user_id);
		}
		if($evaluator)
		{
			$query->where('status', '=', 2);
		}
		if($approver)
		{
			$query->where('status', '=', 3);
		}
		if($issuer)
		{
			$query->where('status', '=', 4);
		}
		return $query->get();
		
	}

	protected function list_location_permits($user_id, $evaluator, $approver, $issuer)
	{
		$query = DB::table('location_permit')
		->where('location_permit.is_deleted', null);
		if($user_id)
		{
			$query->where('user_id', '=', $user_id);
		}
		if($evaluator)
		{
			$query->where('status', '=', 2);
		}
		if($approver)
		{
			$query->where('status', '=', 3);
		}
		if($issuer)
		{
			$query->where('status', '=', 4);
		}
		return $query->get();
		
	}

	protected function list_building_permits($user_id, $position)
	{
		$query = DB::table('building_permit')
		->where('building_permit.is_deleted', null)
		->select('building_permit.*', DB::raw('CONCAT(building_permit.last_name,", ",building_permit.first_name) as applicant_name'));
		if($user_id)
		{
			$query->where('user_id', '=', $user_id);
		}
		if($position)
		{
			/*if($position != 1)
			{
				if($position == 5 || $position == 7)
				{
					$query->where('status', '=', 5)
					->orwhere('status', '=', 7);	
				}
				else
				{
					$query->where('status', '=', $position);
				}
				
			}*/
			$query->wherein('status', $position);
		}
		
		return $query->get();
		
	}
	

	protected function list_files($permit_type = null)
	{
		$query = DB::table('files')
		->where('files.is_deleted', null);
		if($permit_type)
		{
			$query->where('files.permit_type', '=', $permit_type);
		}
		
		return $query->get();
	}

	protected function get_record($table, $id)
	{
		return DB::table($table)
		->where($table.'.id', '=', $id)
		->where($table.'.is_deleted', null)
		->first();
	}

	protected function get_ancillary_permit($table, $building_permit_id)
	{
		return DB::table($table)
		->where($table.'.building_permit_id', '=', $building_permit_id)
		->where($table.'.is_deleted', null)
		->first();
	}

	protected function check_if_attachment_exist($user_id, $file_id, $record_id)
	{
		$table = 'attachments';
		return DB::table($table)
		->where($table.'.user_id', '=', $user_id)
		->where($table.'.file_id', '=', $file_id)
		->where($table.'.record_id', '=', $record_id)
		->where($table.'.is_deleted', null)
		->first();
	}

	protected function get_attachments($record_id, $permit_type)
	{
		$table = 'attachments';
		return DB::table($table)
		->where($table.'.record_id', '=', $record_id)
		->where($table.'.permit_type', '=', $permit_type)
		->where($table.'.is_deleted', null)
		->leftjoin('files', 'files.id', '=', $table.'.file_id')
		->select($table.'.*', 'files.name as file_name')
		->get();
	}

	protected function list_employees()
	{
		return DB::table('users')
		->where('users.level', '!=', 1)
		->get();
	}

	protected function get_attachment_by($file_id, $record_id)
	{
		return DB::table('attachments')
		->where('attachments.record_id', '=', $record_id)
		->where('attachments.file_id', '=', $file_id)
		->where('attachments.is_deleted', null)
		->get();	
	}

	protected function list_pre_evaluation()
	{
		$query = DB::table('pre_evaluation_building_permit as pebp')
		->where('pebp.is_deleted', null)
		/*->where('pebp.status', 2)*/
		->select('pebp.*', DB::raw('CONCAT(pebp.last_name,", ",pebp.first_name) as client_name'));
		return $query->get();
	}

	protected function get_tct_informations($pre_eval_id)
	{
		return DB::table('tct_informations')
		->where('tct_informations.pre_evaluation_id', '=', $pre_eval_id)
		->where('tct_informations.is_deleted', null)
		->get();	
	}

	protected function get_pre_evaluation($id)
	{
		$query = DB::table('pre_evaluation_building_permit as pebp')
		->where('pebp.is_deleted', null)
		->where('pebp.id', $id);
		return $query->first();
	}


	protected function get_user_info($table, $record_id)
	{
		$data = $this->get_record($table, $record_id);
		if($data)
		{
			$user_id = $data->user_id;
			$user_data = $this->get_record('users', $user_id);
			
		}
		else
		{
			$user_data = array();
		}
		return $user_data;
	}

	protected function list_pre_evaluation_by($position)
	{
		$query = DB::table('pre_evaluation_building_permit as pebp')
		->where('pebp.is_deleted', null)
		->wherein('pebp.status', $position)
		->orderBy('pebp.id', 'DESC')
		->select('pebp.*', DB::raw('CONCAT(pebp.last_name,", ",pebp.first_name) as client_name'));
		return $query->get();
	}
}