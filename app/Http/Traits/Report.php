<?php

namespace App\Http\Traits;

use DB;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;

trait Report
{
	protected $reports_pds = [
        'pds-form' => 'PDS Form',
        'service-record-form' => 'Service Record Form',
        'employee-age-and-birthday' => 'Employees Age and Birthdate',
        'employee-contact-information' => 'Employees Contact Information',
        'employee-civilstatus-by-gender' => 'Emplyoees Civil Status by Gender',
        'employee-age-bracket-by-gender' => 'Employees Age Bracket By Gender',
        'employee-government-id' => 'Employees Government IDs',
        'employee-length-of-service' => 'Employees Length of Service',
        'resigned-employees' => 'Resigned Employees',
        'hired-employees' => 'Hired Employees',
    ];

    


	protected function get_employee_by($id) {
		return DB::table('employees')
		->where('id', '=', $id)
		->first();	
	}

	protected function list_employee_by($department, $sector, $office, $unit, $division, $employment_status)
	{
		$result = DB::table('employees')
		->where('employees.deleted_at', null)
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->leftjoin('plantilla_items', 'plantilla_items.id', '=', 'employee_informations.plantilla_item_id')
		->leftjoin('positions', 'positions.id', '=', 'employee_informations.position_id')
		->leftjoin('sectors', 'sectors.id', '=', 'employee_informations.sector_id')
		->leftjoin('offices', 'offices.id', '=', 'employee_informations.office_id')
		->leftjoin('departments', 'departments.id', '=', 'employee_informations.department_id')
		->leftjoin('divisions', 'divisions.id', '=', 'employee_informations.division_id')
		->leftjoin('units', 'units.id', '=', 'employee_informations.unit_id')
		->leftjoin('employment_status', 'employment_status.id', '=', 'employee_informations.employment_status_id')
		->leftjoin('civil_status', 'civil_status.id', '=', 'employees.civil_status_id')
		->leftjoin('blood_types', 'blood_types.id', '=', 'employees.blood_type_id')
		->leftjoin('cities as residential_city', 'residential_city.id', '=', 'employees.residential_city_id')
		->leftjoin('provinces as residential_province', 'residential_province.id', '=', 'employees.residential_province_id')
		->leftjoin('cities as permanent_city', 'permanent_city.id', '=', 'employees.permanent_city_id')
		->leftjoin('provinces as permanent_province', 'permanent_province.id', '=', 'employees.permanent_province_id')
		->select('employees.*', 'residential_city.name as residential_city_name', 'residential_province.name as residential_province_name', 'permanent_city.name as permanent_city_name', 'permanent_province.name as permanent_province_name', 'civil_status.name as civil_status_name', 'blood_types.name as blood_type_name', 'positions.name AS position_name', 'sectors.name AS sector_name', 'offices.name AS office_name','departments.name AS department_name', 'divisions.name AS division_name', 'units.name AS unit_name', 'employment_status.name AS employment_status_name', 'plantilla_items.name AS plantilla_item_name', 'employee_informations.assumption_date', 'employee_informations.salary_grade', 'employee_informations.salary_step', 'employee_informations.monthly_salary as monthly_salary')
		->orderBy('last_name','asc')
		->where(function ($query) {
             $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
                   ->orwhere('employee_informations.resign_date', null)
                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
        });
        if (!empty($department))
		{
			$result->where('employee_informations.department_id', '=', $department);
		}

		if (!empty($sector))
		{
			$result->where('employee_informations.sector_id', '=', $sector);
		}

		if (!empty($office))
		{
			$result->where('employee_informations.office_id', '=', $office);
		}

		if (!empty($unit))
		{
			$result->where('employee_informations.unit_id', '=', $unit);
		}

		if (!empty($division))
		{
			$result->where('employee_informations.division_id', '=', $division);
		}

		if (!empty($employment_status))
		{
			$result->where('employee_informations.employment_status_id', '=', $employment_status);
		}

		return $result->get();
	}

	protected function list_employee($department, $sector, $office, $unit, $division, $employment_status, $type = null, $value = null, $column = null, $educ_level = null)
	{

		$result = DB::table('employees')
		->where('employees.deleted_at', null)
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->leftjoin('plantilla_items', 'plantilla_items.id', '=', 'employee_informations.plantilla_item_id')
		->leftjoin('positions', 'positions.id', '=', 'employee_informations.position_id')
		->leftjoin('sectors', 'sectors.id', '=', 'employee_informations.sector_id')
		->leftjoin('offices', 'offices.id', '=', 'employee_informations.office_id')
		->leftjoin('departments', 'departments.id', '=', 'employee_informations.department_id')
		->leftjoin('divisions', 'divisions.id', '=', 'employee_informations.division_id')
		->leftjoin('units', 'units.id', '=', 'employee_informations.unit_id')
		->leftjoin('employment_status', 'employment_status.id', '=', 'employee_informations.employment_status_id')
		
		->select('employees.*')
		->orderBy('last_name','asc')
		->where(function ($query) {
             $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
                   ->orwhere('employee_informations.resign_date', null)
                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
        });

        if (!empty($department))
		{
			$result->where('employee_informations.department_id', '=', $department);
		}

		if (!empty($sector))
		{
			$result->where('employee_informations.sector_id', '=', $sector);
		}

		if (!empty($office))
		{
			$result->where('employee_informations.office_id', '=', $office);
		}

		if (!empty($unit))
		{
			$result->where('employee_informations.unit_id', '=', $unit);
		}

		if (!empty($division))
		{
			$result->where('employee_informations.division_id', '=', $division);
		}

		if (!empty($employment_status))
		{
			$result->where('employee_informations.employment_status_id', '=', $employment_status);
		}

		if (!empty($type))
		{
			if ($type == 'training')
			{
				$table = 'employee_training';
			}
			else if ($type == 'eligibility')
			{
				$table = 'employee_eligibility';
				$type  = 'eligibilitie';
			}
			else
			{
				$table = 'employee_education';
				
			}
			$result->join($table,$table.'.employee_id', '=', 'employees.id')
			->leftjoin($type.'s', $type.'s'.'.id', '=', $table.'.'.$column)
			->where($table.'.'.$column, '=', $value)
			->where($table.'.deleted_at', null);

			if ($educ_level)
	        {
	        	$result->where('employee_education.education_level', $educ_level);
	        }

			$result->select('employees.*', $type.'s'.'.name as column_name', 'positions.name AS position_name', 'sectors.name AS sector_name', 'offices.name AS office_name','departments.name AS department_name', 'divisions.name AS division_name', 'units.name AS unit_name', 'employment_status.name AS employment_status_name', 'plantilla_items.name AS plantilla_item_name', 'employee_informations.assumption_date', 'employee_informations.salary_grade', 'employee_informations.salary_step', 'employee_informations.monthly_salary as monthly_salary', $table.'.*');

			if ($table == 'employee_education')
			{
				$result->addSelect($table.'.education_level as educ_level');
			}
		}
		return $result->get();
	}

	protected function count_employee_by_civil_status_and_gender($civil_status, $gender)
	{
		return DB::table('employees')
		->where('civil_status_id', '=', $civil_status)
		->where('gender', '=', $gender)
		->where('employees.deleted_at', null)
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->where(function ($query) {
             $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
                   ->orwhere('employee_informations.resign_date', null)
                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
        })
        ->select('employees.*', 'employee_informations.id as emp_info_id')
		->orderBy('last_name','asc')
		->orderBy('first_name','asc')
		->count();
	}

	protected function list_service_record($id)
	{	
		return DB::table('employee_work_experience')
		->where('employee_id', '=', $id)
		->where('government_service', '=', 1)
		->where('employee_work_experience.deleted_at', null)
		->leftjoin('positions', 'positions.id', '=', 'employee_work_experience.position_id')
		->leftjoin('offices', 'offices.id', '=', 'employee_work_experience.office_id')
		->leftjoin('divisions', 'divisions.id', '=', 'employee_work_experience.division_id')
		->leftjoin('units', 'units.id', '=', 'employee_work_experience.unit_id')
		->leftjoin('agencies', 'agencies.id', '=', 'employee_work_experience.agency_id')
		->leftjoin('appointment_status', 'appointment_status.id', '=', 'employee_work_experience.appointment_status_id')
		->leftjoin('employment_status', 'employment_status.id', '=', 'employee_work_experience.employment_status_id')
		->select('employee_work_experience.*', 'positions.name AS position_name', 'appointment_status.name AS appointment_status_name', 'employment_status.name AS employment_status_name', 'offices.name AS office_name', 'divisions.name AS division_name', 'units.name AS unit_name', 'agencies.name as agency_name')
		->orderBy('workexp_start_date', 'asc')
		->get();
	}

	protected function get_latest_workexp($id)
	{
		return DB::table('employee_work_experience')
		->where('employee_id', '=', $id)
		->where('workexp_present', '=', 1)
		->where('employee_work_experience.deleted_at', null)
		->leftjoin('plantilla_items', 'plantilla_items.id', '=', 'employee_work_experience.plantilla_item_id')
		->leftjoin('positions', 'positions.id', '=', 'employee_work_experience.position_id')
		->leftjoin('agencies', 'agencies.id', '=', 'employee_work_experience.agency_id')
		->leftjoin('locations', 'locations.id', '=', 'employee_work_experience.location_id')
		->leftjoin('branches', 'branches.id', '=', 'employee_work_experience.branch_id')
		->leftjoin('departments', 'departments.id', '=', 'employee_work_experience.department_id')
		->leftjoin('offices', 'offices.id', '=', 'employee_work_experience.office_id')
		->leftjoin('divisions', 'divisions.id', '=', 'employee_work_experience.division_id')
		->leftjoin('units', 'units.id', '=', 'employee_work_experience.unit_id')
		->leftjoin('employment_status', 'employment_status.id', '=', 'employee_work_experience.employment_status_id')
		->leftjoin('appointment_status', 'appointment_status.id', '=', 'employee_work_experience.appointment_status_id')
		->select('employee_work_experience.*', 'positions.name AS position_name', 'agencies.name AS agency_name', 'locations.name AS location_name', 'branches.name AS branch_name', 'departments.name AS department_name', 'employment_status.name AS employment_status_name', 'appointment_status.name AS appointment_status_name', 'offices.name AS office_name', 'divisions.name AS division_name', 'units.name AS unit_name', 'plantilla_items.name as plantilla_item_name')
		->orderBy('workexp_start_date', 'desc')
		->first();		
	}


	

	protected function list_employees_eligibilities($id)
	{
		return DB::table('employee_eligibility')
		->where('employee_eligibility.employee_id', $id)
		->where('employee_eligibility.deleted_at', null)
		->leftjoin('eligibilities', 'eligibilities.id', '=', 'employee_eligibility.eligibility_id')
		->select('employee_eligibility.*', 'eligibilities.name as eligibility_name')
		->orderby('employee_eligibility.id', 'asc')
		->get();
	}

	protected function get_data_by($table, $id)
	{
		return DB::table($table)
		->where('id', '=', $id)
		->where($table.'.deleted_at', null)
		->first();	
	}	

	protected function get_training_type($id)
	{
		$table = 'training_types';
		return DB::table($table)
		->where('id', '=', $id)
		->where($table.'.deleted_at', null)
		->select('training_types.name')
		->first();	
	}

	protected function list_employee_by_status($status)
	{
		$result = DB::table('employees')
		->where('employees.deleted_at', null)
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->leftjoin('employment_status', 'employment_status.id', '=', 'employee_informations.employment_status_id')
		->select('employees.*', 'employment_status.name as emp_status_name', 'employment_status.category_status as emp_status_category')
		->orderBy('last_name','asc')
		->orderBy('emp_status_category', 'asc')
		->where(function ($query) {
             $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
                   ->orwhere('employee_informations.resign_date', null)
                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
        });
        /*if (!empty($department))
		{
			$result->where('employee_informations.department_id', '=', $department);
		}*/
		return $result->get();
	}

	protected function list_celebrants_by($month)
	{
		if($month)
		{
			return DB::table('employees')
			->whereMonth('birthday', '=', $month)
			->where('employees.deleted_at', null)
			->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
			->leftjoin('plantilla_items', 'plantilla_items.id', '=', 'employee_informations.plantilla_item_id')
			->leftjoin('positions', 'positions.id', '=', 'employee_informations.position_id')
			->leftjoin('sectors', 'sectors.id', '=', 'employee_informations.sector_id')
			->leftjoin('offices', 'offices.id', '=', 'employee_informations.office_id')
			->leftjoin('departments', 'departments.id', '=', 'employee_informations.department_id')
			->leftjoin('divisions', 'divisions.id', '=', 'employee_informations.division_id')
			->leftjoin('units', 'units.id', '=', 'employee_informations.unit_id')
			->leftjoin('employment_status', 'employment_status.id', '=', 'employee_informations.employment_status_id')
			
			->where(function ($query) {
	             $query->where('employee_informations.resign_date', '=', '0000-00-00')
	                   ->orwhere('employee_informations.resign_date', null);
	        })
	        ->select('employees.*', 'positions.name AS position_name', 'sectors.name AS sector_name', 'offices.name AS office_name','departments.name AS department_name', 'divisions.name AS division_name', 'units.name AS unit_name', 'employment_status.name AS employment_status_name', 'plantilla_items.name AS plantilla_item_name', 'employee_informations.assumption_date', 'employee_informations.salary_grade', 'employee_informations.salary_step')
	        ->orderBy('birthday', 'asc')
			->get();
		}
		else
		{
			return DB::table('employees')
			->where('employees.deleted_at', null)
			->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
			->leftjoin('plantilla_items', 'plantilla_items.id', '=', 'employee_informations.plantilla_item_id')
			->leftjoin('positions', 'positions.id', '=', 'employee_informations.position_id')
			->leftjoin('sectors', 'sectors.id', '=', 'employee_informations.sector_id')
			->leftjoin('offices', 'offices.id', '=', 'employee_informations.office_id')
			->leftjoin('departments', 'departments.id', '=', 'employee_informations.department_id')
			->leftjoin('divisions', 'divisions.id', '=', 'employee_informations.division_id')
			->leftjoin('units', 'units.id', '=', 'employee_informations.unit_id')
			->leftjoin('employment_status', 'employment_status.id', '=', 'employee_informations.employment_status_id')
			
			->where(function ($query) {
	             $query->where('employee_informations.resign_date', '=', '0000-00-00')
	                   ->orwhere('employee_informations.resign_date', null);
	        })
	        ->select('employees.*', 'positions.name AS position_name', 'sectors.name AS sector_name', 'offices.name AS office_name','departments.name AS department_name', 'divisions.name AS division_name', 'units.name AS unit_name', 'employment_status.name AS employment_status_name', 'plantilla_items.name AS plantilla_item_name', 'employee_informations.assumption_date', 'employee_informations.salary_grade', 'employee_informations.salary_step')
	        ->orderBy('birthday', 'asc')
			->get();
		}
	}

	

	protected function list_resigned_employees_by($month, $year, $start_date = null, $end_date = null)
	{
		$result = DB::table('employees')
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->where('employees.deleted_at', null)
        ->select('employees.*', 'employee_informations.resign_date')
		->orderBy('last_name','asc');
		if ($start_date)
		{
			$result->whereBetween('employee_informations.resign_date', [$start_date, $end_date]);
		}
		else
		{
			$result->whereMonth('employee_informations.resign_date', '=', $month)
			->whereYear('employee_informations.resign_date', '=', $year);
		}
		return $result->get();
	}

	protected function list_employment_information_by($department, $sector, $office, $unit, $division, $employment_status)
	{
		$result = DB::table('employees')
		->where('employees.deleted_at', null)
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->leftjoin('plantilla_items', 'plantilla_items.id', '=', 'employee_informations.plantilla_item_id')
		->leftjoin('positions', 'positions.id', '=', 'employee_informations.position_id')
		->leftjoin('sectors', 'sectors.id', '=', 'employee_informations.sector_id')
		->leftjoin('offices', 'offices.id', '=', 'employee_informations.office_id')
		->leftjoin('departments', 'departments.id', '=', 'employee_informations.department_id')
		->leftjoin('divisions', 'divisions.id', '=', 'employee_informations.division_id')
		->leftjoin('units', 'units.id', '=', 'employee_informations.unit_id')
		->leftjoin('employment_status', 'employment_status.id', '=', 'employee_informations.employment_status_id')
		->select('employees.*', 'positions.name AS position_name', 'sectors.name AS sector_name', 'offices.name AS office_name','departments.name AS department_name', 'divisions.name AS division_name', 'units.name AS unit_name', 'employment_status.name AS employment_status_name', 'plantilla_items.name AS plantilla_item_name', 'employee_informations.assumption_date', 'employee_informations.salary_grade', 'employee_informations.salary_step', 'employee_informations.monthly_salary as monthly_salary', 'employee_informations.hired_date')
		->orderBy('last_name','asc')
		->where(function ($query) {
             $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
                   ->orwhere('employee_informations.resign_date', null)
                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
        });
        if (!empty($department))
		{
			$result->where('employee_informations.department_id', '=', $department);
		}

		if (!empty($sector))
		{
			$result->where('employee_informations.sector_id', '=', $sector);
		}

		if (!empty($office))
		{
			$result->where('employee_informations.office_id', '=', $office);
		}

		if (!empty($unit))
		{
			$result->where('employee_informations.unit_id', '=', $unit);
		}

		if (!empty($division))
		{
			$result->where('employee_informations.division_id', '=', $division);
		}

		if (!empty($employment_status))
		{
			$result->where('employee_informations.employment_status_id', '=', $employment_status);
		}

		return $result->get();
	}

	protected function keyword_finder($table, $search_string)
	{
		return DB::table($table)
		->where($table.'.name', 'like', '%'.$search_string.'%')
		->get();
	}

	protected function get_employee_list_by($table, $column, $value)
	{
		return DB::table($table)
		->where($table.'.'.$column, '=', $value)
		->leftjoin('employees', 'employees.id', '=', $table.'.employee_id')
		->select('employees.last_name', 'employees.first_name', 'employees.middle_name', 'employees.extension_name', 'employees.employee_number')
		->where($table.'.deleted_at', null)
		->get();
	}

	protected function get_report_data(
		$employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro
	)
	{
		if ($month && $year)
		{
			$num_of_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
			$end_of_the_month = $year.'-'.$month.'-'.$num_of_days;
		}
		else
		{
			$end_of_the_month = 0;
		}
		$result = DB::table('employees')
		->where('employees.deleted_at', null)
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->leftjoin('plantilla_items', 'plantilla_items.id', '=', 'employee_informations.plantilla_item_id')
		->leftjoin('positions', 'positions.id', '=', 'employee_informations.position_id')
		->leftjoin('sectors', 'sectors.id', '=', 'employee_informations.sector_id')
		->leftjoin('offices', 'offices.id', '=', 'employee_informations.office_id')
		->leftjoin('departments', 'departments.id', '=', 'employee_informations.department_id')
		->leftjoin('divisions', 'divisions.id', '=', 'employee_informations.division_id')
		->leftjoin('units', 'units.id', '=', 'employee_informations.unit_id')
		->leftjoin('employment_status', 'employment_status.id', '=', 'employee_informations.employment_status_id')
		->select('employees.*', 'positions.name AS position_name', 'sectors.name AS sector_name', 'offices.name AS office_name','departments.name AS department_name', 'divisions.name AS division_name', 'units.name AS unit_name', 'employment_status.name AS employment_status_name', 'plantilla_items.name AS plantilla_item_name', 'employee_informations.assumption_date', 'employee_informations.salary_grade', 'employee_informations.salary_step', 'employee_informations.monthly_salary as monthly_salary', 'employee_informations.resign_date', 'employee_informations.hired_date')
		->orderBy('last_name','asc');
		if ($end_of_the_month)
		{
			$result->where(function ($query) use ($end_of_the_month) {
	        $query
	        		->whereDate('employee_informations.resign_date', '>=', $end_of_the_month)
	        		->orwhere('employee_informations.resign_date', null)
	                ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
	        });
		}
		else
		{
			$result->where(function ($query) {
	        $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
	                   ->orwhere('employee_informations.resign_date', null)
	                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
	        });
		}
		if($employee_id)
		{
			$result->where('employees.id', '=', $employee_id);
		}
		if ($tro)
		{
			$tro_id = $this->get_tro_id();
			if ($tro == 1)
			{
				$result->where('employee_informations.department_id', '=', $tro_id->id);	
			}
			else
			{
				$result->where('employee_informations.department_id', '!=', $tro_id->id);	
			}
		}
		else
		{
			if (!empty($department))
			{
				$result->where('employee_informations.department_id', '=', $department);
			}	
		}

		if (!empty($sector))
		{
			$result->where('employee_informations.sector_id', '=', $sector);
		}

		if (!empty($office))
		{
			$result->where('employee_informations.office_id', '=', $office);
		}

		if (!empty($unit))
		{
			$result->where('employee_informations.unit_id', '=', $unit);
		}

		if (!empty($division))
		{
			$result->where('employee_informations.division_id', '=', $division);
		}

		if (!empty($employment_status))
		{
			$result->where('employee_informations.employment_status_id', '=', $employment_status);
		}

		return $result->get();
	}


	protected function get_master_list($employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro)
	{
		if ($month && $year)
		{
			$num_of_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
			$end_of_the_month = $year.'-'.$month.'-'.$num_of_days;
		}
		else
		{
			$end_of_the_month = 0;
		}
		$result = DB::table('employees')
		->where('employees.deleted_at', null)
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->leftjoin('plantilla_items', 'plantilla_items.id', '=', 'employee_informations.plantilla_item_id')
		->leftjoin('positions', 'positions.id', '=', 'employee_informations.position_id')
		->leftjoin('sectors', 'sectors.id', '=', 'employee_informations.sector_id')
		->leftjoin('offices', 'offices.id', '=', 'employee_informations.office_id')
		->leftjoin('departments', 'departments.id', '=', 'employee_informations.department_id')
		->leftjoin('divisions', 'divisions.id', '=', 'employee_informations.division_id')
		->leftjoin('units', 'units.id', '=', 'employee_informations.unit_id')
		->leftjoin('employment_status', 'employment_status.id', '=', 'employee_informations.employment_status_id')
		->leftjoin('civil_status', 'civil_status.id', '=', 'employees.civil_status_id')
		->leftjoin('blood_types', 'blood_types.id', '=', 'employees.blood_type_id')
		->leftjoin('cities as residential_city', 'residential_city.id', '=', 'employees.residential_city_id')
		->leftjoin('provinces as residential_province', 'residential_province.id', '=', 'employees.residential_province_id')
		->leftjoin('cities as permanent_city', 'permanent_city.id', '=', 'employees.permanent_city_id')
		->leftjoin('provinces as permanent_province', 'permanent_province.id', '=', 'employees.permanent_province_id')
		->select('employees.*', 'residential_city.name as residential_city_name', 'residential_province.name as residential_province_name', 'permanent_city.name as permanent_city_name', 'permanent_province.name as permanent_province_name', 'civil_status.name as civil_status_name', 'blood_types.name as blood_type_name', 'positions.name AS position_name', 'sectors.name AS sector_name', 'offices.name AS office_name','departments.name AS department_name', 'divisions.name AS division_name', 'units.name AS unit_name', 'employment_status.name AS employment_status_name', 'plantilla_items.name AS plantilla_item_name', 'employee_informations.assumption_date', 'employee_informations.salary_grade', 'employee_informations.salary_step', 'employee_informations.monthly_salary as monthly_salary')
		->orderBy('last_name','asc');
		if ($end_of_the_month)
		{
			$result->where(function ($query) use ($end_of_the_month) {
	        $query
	        		->whereDate('employee_informations.resign_date', '>=', $end_of_the_month)
	        		->orwhere('employee_informations.resign_date', null)
	                ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
	        });
		}
		else
		{
			$result->where(function ($query) {
	        $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
	                   ->orwhere('employee_informations.resign_date', null)
	                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
	        });
		}
		if($employee_id)
		{
			$result->where('employees.id', '=', $employee_id);
		}
		if ($tro)
		{
			$tro_id = $this->get_tro_id();
			if ($tro == 1)
			{
				$result->where('employee_informations.department_id', '=', $tro_id->id);	
			}
			else
			{
				$result->where('employee_informations.department_id', '!=', $tro_id->id);	
			}
		}
		else
		{
			if (!empty($department))
			{
				$result->where('employee_informations.department_id', '=', $department);
			}	
		}
		if (!empty($sector))
		{
			$result->where('employee_informations.sector_id', '=', $sector);
		}

		if (!empty($office))
		{
			$result->where('employee_informations.office_id', '=', $office);
		}

		if (!empty($unit))
		{
			$result->where('employee_informations.unit_id', '=', $unit);
		}

		if (!empty($division))
		{
			$result->where('employee_informations.division_id', '=', $division);
		}

		if (!empty($employment_status))
		{
			$result->where('employee_informations.employment_status_id', '=', $employment_status);
		}

		return $result->get();
	}

	protected function get_tro_id()
	{
		return DB::table('departments')
		->where('departments.deleted_at', null)
		->where('departments.code', 'TRO')
		->first();
	}

	protected function list_trainings_attended($employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro)
	{
		if ($month && $year)
		{
			$num_of_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
			$end_of_the_month = $year.'-'.$month.'-'.$num_of_days;
		}
		else
		{
			$end_of_the_month = 0;
		}

		$result = DB::table('employee_training')
		->where('employee_training.deleted_at', null)
		->join('employees', 'employees.id', '=', 'employee_training.employee_id')
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employee_training.employee_id')
		->leftjoin('trainings', 'trainings.id', '=', 'employee_training.training_id')
		->leftjoin('training_types', 'training_types.id', '=', 'employee_training.training_type')
		->select('employee_training.*', 'trainings.name as training_name', 'training_types.name as training_type_name', DB::raw('CONCAT(employees.last_name,", ",employees.first_name," ",employees.extension_name," ",employees.middle_name) AS full_name'))
		->orderby('employee_training.training_start_date', 'desc');
		/*->orderby('employee_training.str');*/
		if($employee_id)
		{
			$result->where('employee_training.employee_id', $employee_id);
		}
		if ($end_of_the_month)
		{
			$result->where(function ($query) use ($end_of_the_month) {
	        $query
	        		->whereDate('employee_informations.resign_date', '>=', $end_of_the_month)
	        		->orwhere('employee_informations.resign_date', null)
	                ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
	        });
		}
		else
		{
			$result->where(function ($query) {
	        $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
	                   ->orwhere('employee_informations.resign_date', null)
	                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
	        });
		}
		if ($tro)
		{
			$tro_id = $this->get_tro_id();
			if ($tro == 1)
			{
				$result->where('employee_informations.department_id', '=', $tro_id->id);	
			}
			else
			{
				$result->where('employee_informations.department_id', '!=', $tro_id->id);	
			}
		}
		else
		{
			if (!empty($department))
			{
				$result->where('employee_informations.department_id', '=', $department);
			}	
		}
		if (!empty($sector))
		{
			$result->where('employee_informations.sector_id', '=', $sector);
		}

		if (!empty($office))
		{
			$result->where('employee_informations.office_id', '=', $office);
		}

		if (!empty($unit))
		{
			$result->where('employee_informations.unit_id', '=', $unit);
		}

		if (!empty($division))
		{
			$result->where('employee_informations.division_id', '=', $division);
		}

		if (!empty($employment_status))
		{
			$result->where('employee_informations.employment_status_id', '=', $employment_status);
		}
		return $result->get();
	}


	protected function list_education_background($employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro)
	{

		if ($month && $year)
		{
			$num_of_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
			$end_of_the_month = $year.'-'.$month.'-'.$num_of_days;
		}
		else
		{
			$end_of_the_month = 0;
		}
		$result = DB::table('employee_education')
		->where('employee_education.deleted_at', null)
		->join('employees', 'employees.id', '=', 'employee_education.employee_id')
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employee_education.employee_id')
		->leftjoin('schools', 'schools.id', '=', 'employee_education.school_id')
		->leftjoin('courses', 'courses.id', '=', 'employee_education.course_id')
		->select('employee_education.*', 'schools.name as school_name', 'courses.name as course_name', DB::raw('CONCAT(employees.last_name,", ",employees.first_name," ",employees.extension_name," ",employees.middle_name) AS full_name'), 'employees.employee_number')
		->orderBy('employees.last_name', 'asc');

		if($employee_id)
		{
			$result->where('employee_education.employee_id', $employee_id);
		}
		if ($end_of_the_month)
		{
			$result->where(function ($query) use ($end_of_the_month) {
	        $query
	        		->whereDate('employee_informations.resign_date', '>=', $end_of_the_month)
	        		->orwhere('employee_informations.resign_date', null)
	                ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
	        });
		}
		else
		{
			$result->where(function ($query) {
	        $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
	                   ->orwhere('employee_informations.resign_date', null)
	                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
	        });
		}
		if ($tro)
		{
			$tro_id = $this->get_tro_id();
			if ($tro == 1)
			{
				$result->where('employee_informations.department_id', '=', $tro_id->id);	
			}
			else
			{
				$result->where('employee_informations.department_id', '!=', $tro_id->id);	
			}
		}
		else
		{
			if (!empty($department))
			{
				$result->where('employee_informations.department_id', '=', $department);
			}	
		}
		if (!empty($sector))
		{
			$result->where('employee_informations.sector_id', '=', $sector);
		}

		if (!empty($office))
		{
			$result->where('employee_informations.office_id', '=', $office);
		}

		if (!empty($unit))
		{
			$result->where('employee_informations.unit_id', '=', $unit);
		}

		if (!empty($division))
		{
			$result->where('employee_informations.division_id', '=', $division);
		}

		if (!empty($employment_status))
		{
			$result->where('employee_informations.employment_status_id', '=', $employment_status);
		}
		return $result->get();
	}

	protected function list_hired_employees_by(
		$employee_id, $month, $year, $department, $sector, $office, $unit, $division, $employment_status, $tro
	)
	{
		$result = DB::table('employees')
		->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
		->where('employees.deleted_at', null)
		->where(function ($query) {
             $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
                   ->orwhere('employee_informations.resign_date', null)
                   ->orwhere('employee_informations.resign_date', '=', '0000-00-00');
        })
        ->select('employees.*', 'employee_informations.hired_date')
		->orderBy('last_name','asc');

		if ($start_date)
		{
			$result->whereBetween('employee_informations.hired_date', [$start_date, $end_date]);
		}
		else
		{
			$result->whereMonth('employee_informations.hired_date', '=', $month)
			->whereYear('employee_informations.hired_date', '=', $year);
		}
		return $result->get();
	}
}