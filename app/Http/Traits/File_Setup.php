<?php

namespace App\Http\Traits;

use DB;
use Exception;

trait File_Setup
{
	protected $salary_grade = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33];

	protected $tranche = [1, 2, 3, 4, 5];

	protected $step = [0, 1, 2, 3, 4, 5, 6, 7, 8];

	protected $day = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

	protected $philhealth_below = 0.0275;

	protected $philhealth_above = 1100;

	protected $pay_period = ['semi_monthly', 'monthly'];

	protected $deduction_period = ['both', 'first_half', 'second_half'];

	protected $policy_type = ['system_generated', 'inputted'];

	protected $based_on = ['monthly_salary', 'gross_salary', 'gross_taxable']; 

	protected $default_ee_percentage = 0.09;

	protected $default_er_percentage = 0.12;

	protected $default_ec_percentage = 0.01;

	protected $tax_table_bracket = ['daily', 'weekly', 'semi_monthly', 'monthly'];

	protected $category_status = array('Non Plantilla', 'Plantilla');

	protected $signatory_pages = ['pds', 'service_record',  'attendance', 'payroll'];

	protected $educational_level = array(
	['id' => 1 , 'name' => 'Primary Level'],
	['id' => 2 , 'name' => 'Secondary Level'],
	['id' => 3, 'name' => 'Vocational / Trade Course'],
	['id' => 4 , 'name' => 'College'],
	['id' => 5 , 'name' => 'Graduate Studies']
	);

	protected function is_filesetup_option_exist($option)
	{
		if(!in_array($option, array_column($this->list_filesetups(), 'id'))) throw new Exception(trans('page.not_found', ['attribute' => strtolower(trans('page.option'))]));

		return true;
	}

	protected function list_filesetups()
	{
		return array(
            ['system' => '1', 'url' => url('/file_setup/countries'), 'id' => 'countries', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.countries')],
            ['system' => '1', 'url' => url('/file_setup/occupations'), 'id' => 'occupations', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.occupations')],
            ['system' => '1', 'url' => url('/file_setup/citizenships'), 'id' => 'citizenships', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.citizenships')],
            ['system' => '1', 'url' => url('/file_setup/cities'), 'id' => 'cities', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.cities')],
            ['system' => '1', 'url' => url('/file_setup/provinces'), 'id' => 'provinces', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.provinces')],
            ['system' => '1', 'url' => url('/file_setup/schools'), 'id' => 'schools', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.schools')],
            ['system' => '1', 'url' => url('/file_setup/courses'), 'id' => 'courses', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.courses')],
            ['system' => '1', 'url' => url('/file_setup/eligibilities'), 'id' => 'eligibilities', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.eligibilities')],
            ['system' => '1', 'url' => url('/file_setup/organizations'), 'id' => 'organizations', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.organizations')],
            ['system' => '1', 'url' => url('/file_setup/providers'), 'id' => 'providers', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.providers')],
            ['system' => '1', 'url' => url('/file_setup/agencies'), 'id' => 'agencies', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.agencies')],
            ['system' => '1', 'url' => url('/file_setup/departments'), 'id' => 'departments', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.departments')],
            ['system' => '1', 'url' => url('/file_setup/offices'), 'id' => 'offices', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.offices')],
            ['system' => '1', 'url' => url('/file_setup/branches'), 'id' => 'branches', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.branches')],
            ['system' => '1', 'url' => url('/file_setup/sections'), 'id' => 'sections', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.sections')],
            ['system' => '1', 'url' => url('/file_setup/sectors'), 'id' => 'sectors', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.sectors')],
            ['system' => '1', 'url' => url('/file_setup/locations'), 'id' => 'locations', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.locations')],
            ['system' => '1', 'url' => url('/file_setup/divisions'), 'id' => 'divisions', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.divisions')],
            ['system' => '1', 'url' => url('/file_setup/positions'), 'id' => 'positions', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.positions')],
            ['system' => '1', 'url' => url('/file_setup/trainings'), 'id' => 'trainings', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.trainings')],
            ['system' => '1', 'url' => url('/file_setup/training_types'), 'id' => 'training_types', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.training_types')],
            ['system' => '1', 'url' => url('/file_setup/appointment_status'), 'id' => 'appointment_status', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.appointment_status')],
            ['system' => '1', 'url' => url('/file_setup/employment_status'), 'id' => 'employment_status', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.employment_status')],
            ['system' => '1', 'url' => url('/file_setup/salary_grade'), 'id' => 'salary_grade', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.salary_grade')],
            ['system' => '1', 'url' => url('/file_setup/plantilla_items'), 'id' => 'plantilla_items', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.plantilla_items')],
            ['system' => '3', 'url' => url('/file_setup/benefits'), 'id' => 'benefits', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.benefits')],
            ['system' => '3', 'url' => url('/file_setup/adjustments'), 'id' => 'adjustments', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.adjustments')],
            ['system' => '3', 'url' => url('/file_setup/deductions'), 'id' => 'deductions', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.deductions')],
            ['system' => '3', 'url' => url('/file_setup/loans'), 'id' => 'loans', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.loans')],
            ['system' => '3', 'url' => url('/file_setup/responsibility_centers'), 'id' => 'responsibility_centers', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.responsibility_centers')],
            ['system' => '3', 'url' => url('/file_setup/banks'), 'id' => 'banks', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.banks')],
            ['system' => '3', 'url' => url('/file_setup/philhealth_policy'), 'id' => 'philhealth_policy', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.philhealth_policy')],
            ['system' => '3', 'url' => url('/file_setup/pagibig_policy'), 'id' => 'pagibig_policy', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.pagibig_policy')],
            ['system' => '3', 'url' => url('/file_setup/gsis_policy'), 'id' => 'gsis_policy', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.gsis_policy')],
            ['system' => '3', 'url' => url('/file_setup/wage_rate'), 'id' => 'wage_rate', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.wage_rate')],
            ['system' => '3', 'url' => url('/file_setup/tax_table'), 'id' => 'tax_table', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.tax_table')],
            ['system' => '2', 'url' => url('/file_setup/leaves'), 'id' => 'leaves', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.leaves')],
            ['system' => '2', 'url' => url('/file_setup/holidays'), 'id' => 'holidays', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.holidays')],
            ['system' => '2', 'url' => url('/file_setup/office_suspensions'), 'id' => 'office_suspensions', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.office_suspensions')],
            ['system' => '2', 'url' => url('/file_setup/work_schedules'), 'id' => 'work_schedules', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.work_schedules')],
            ['system' => '2', 'url' => url('/file_setup/absences'), 'id' => 'absences', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.absences')],
           	['system' => '1', 'url' => url('/file_setup/signatories'), 'id' => 'signatories', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.signatories')],
           	['system' => '1', 'url' => url('/file_setup/units'), 'id' => 'units', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.units')],
           	['system' => '1', 'url' => url('/file_setup/designations'), 'id' => 'designations', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.designations')],
           	['system' => '1', 'url' => url('/file_setup/assignments'), 'id' => 'assignments', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.assignments')],
           	['system' => '3', 'url' => url('/file_setup/bank_accounts'), 'id' => 'bank_accounts', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.bank_accounts')],
           	//['system' => '1', 'url' => url('/file_setup/announcements'), 'id' => 'announcements', 'icon' => 'icon mdi mdi-caret-right', 'label' => trans('page.announcements')],
        );
	}

	protected function list_assignments()
	{
		return DB::table('assignments')
		->where('assignments.deleted_at', null)
		->orderby('assignments.name')
		->get();
	}

	protected function list_announcements()
	{
		return DB::table('announcements')
		->where('announcements.deleted_at', null)
		->get();
	}
	
	protected function list_agencies()
	{
		return DB::table('agencies')
		->where('agencies.deleted_at', null)
		->orderby('agencies.name')
		->get();
	}

	protected function list_departments()
	{
		return DB::table('departments')
		->where('departments.deleted_at', null)
		->orderby('departments.name')
		->get();
	}

	protected function list_offices()
	{
		return DB::table('offices')
		->where('offices.deleted_at', null)
		->orderby('offices.name')
		->get();
	}

	protected function list_plantilla_items()
	{
		return DB::table('plantilla_items')
		->where('plantilla_items.deleted_at', null)
		->leftjoin('positions', 'positions.id', '=', 'plantilla_items.position_id')
		->select('plantilla_items.*', 'positions.name as position_name')
		->orderby('plantilla_items.name')
		->get();
	}

	protected function list_positions()
	{
		return DB::table('positions')
		->where('positions.deleted_at', null)
		->orderby('positions.name')
		->get();
	}

	protected function list_countries()
	{
		return DB::table('countries')
		->where('countries.deleted_at', null)
		->orderby('countries.name')
		->get();
	}
	protected function list_cities()
	{
		return DB::table('cities')
		->join('provinces', 'provinces.id', '=', 'cities.province_id')
		->where('cities.deleted_at', null)
		->select('cities.*', 'provinces.name as province_name')
		->orderby('cities.name')
		->get();
	}

	protected function list_occupations()
	{
		return DB::table('occupations')
		->where('occupations.deleted_at', null)
		->orderby('occupations.name')
		->get();
	}

	protected function list_designations()
	{
		return DB::table('designations')
		->where('designations.deleted_at', null)
		->orderby('designations.name')
		->get();
	}

	protected function list_citizenships()
	{
		return DB::table('citizenships')
		->where('citizenships.deleted_at', null)
		->orderby('citizenships.name')
		->get();
	}

	protected function list_provinces()
	{
		return DB::table('provinces')
		->where('provinces.deleted_at', null)
		->orderby('provinces.name')
		->get();
	}

	protected function list_schools()
	{
		return DB::table('schools')
		->where('schools.deleted_at', null)
		->orderby('schools.name')
		->get();
	}

	protected function list_courses()
	{
		return DB::table('courses')
		->where('courses.deleted_at', null)
		->whereNotIn('courses.name', ['Primary Education', 'Highschool', 'Junior Highschool', 'Senior Highschool'])
		->orderby('courses.name')
		->get();
	}

	protected function list_basic_education()
	{
		return DB::table('courses')
		->where('courses.deleted_at', null)
		->whereIn('courses.name', ['Primary Education', 'Highschool', 'Junior Highschool', 'Senior Highschool'])
		->orderby('courses.name')
		->get();
	}



	protected function list_eligibilities()
	{
		return DB::table('eligibilities')
		->where('eligibilities.deleted_at', null)
		->orderby('eligibilities.name')
		->get();
	}

	protected function list_organizations()
	{
		return DB::table('organizations')
		->where('organizations.deleted_at', null)
		->orderby('organizations.name')
		->get();
	}

	protected function list_providers()
	{
		return DB::table('providers')
		->where('providers.deleted_at', null)
		->orderby('providers.name')
		->get();
	}

	protected function list_trainings()
	{
		return DB::table('trainings')
		->where('trainings.deleted_at', null)
		->orderby('trainings.name')
		->get();
	}

	protected function list_divisions()
	{
		return DB::table('divisions')
		->where('divisions.deleted_at', null)
		->orderby('divisions.name')
		->get();
	}

	protected function list_appointment_status()
	{
		return DB::table('appointment_status')
		->leftjoin('appointment_type', 'appointment_type.id', 'appointment_status.appointment_type')
		->where('appointment_status.deleted_at', null)
		->select('appointment_status.*', 'appointment_type.appointment_type as appointment_type_lbl')
		->orderby('appointment_status.name')
		->get();
	}

	protected function list_appointment_type()
	{
		return DB::table('appointment_type')
		->get();
	}

	protected function list_employment_status($category_status = null)
	{
		$query = DB::table('employment_status')
		->where('employment_status.deleted_at', null)
		->orderby('employment_status.name');

		if($category_status)
		{
			$query->where('employment_status.category_status', $category_status);
		}
		
		return $query->get();
	}

	protected function list_salary_grade()
	{
		return DB::table('salary_grade')
		->where('salary_grade.deleted_at', null)
		->orderby('salary_grade.name')
		->get();
	}

	protected function list_branches()
	{
		return DB::table('branches')
		->where('branches.deleted_at', null)
		->orderby('branches.name')
		->get();
	}

	protected function list_locations()
	{
		return DB::table('locations')
		->where('locations.deleted_at', null)
		->orderby('locations.name')
		->get();
	}

	protected function list_sections()
	{
		return DB::table('sections')
		->where('sections.deleted_at', null)
		->orderby('sections.name')
		->get();
	}

	protected function list_units()
	{
		return DB::table('units')
		->where('units.deleted_at', null)
		->orderby('units.name')
		->get();
	}

	protected function list_taxtypes()
	{
		return DB::table('tax_types')
		->get();
	}

	protected function list_computationtypes()
	{
		return DB::table('computation_types')
		->get();
	}

	protected function list_benefits()
	{
		return DB::table('benefits')
		->where('benefits.deleted_at', null)
		->join('tax_types', 'tax_types.id', 'benefits.tax_type_id')
		->join('computation_types', 'computation_types.id', 'benefits.computation_type_id')
		->select('benefits.*', 'computation_types.name as computation_type', 'tax_types.name as tax_type')
		->orderby('benefits.name')
		->get();
	}

	protected function list_adjustments()
	{
		return DB::table('adjustments')
		->where('adjustments.deleted_at', null)
		->join('tax_types', 'tax_types.id', 'adjustments.tax_type_id')
		->select('adjustments.*', 'tax_types.name as tax_type')
		->orderby('adjustments.name')
		->get();
	}

	protected function list_payrollgroups()
	{
		return DB::table('payroll_group')
		->get();
	}

	protected function list_deductions()
	{
		return DB::table('deductions')
		->where('deductions.deleted_at', null)
		->join('tax_types', 'tax_types.id', 'deductions.tax_type_id')
		->join('payroll_group', 'payroll_group.id', 'deductions.payroll_group_id')
		->select('deductions.*', 'tax_types.name as tax_type', 'payroll_group.name as payroll_group') 
		->orderby('deductions.name')
		->get();
	} 

	protected function list_loantypes()
	{
		return DB::table('loan_types')
		->get();
	}

	protected function list_loans()
	{
		return DB::table('loans')
		->where('loans.deleted_at', null)
		->join('loan_types', 'loan_types.id', 'loans.loan_type_id')
		->select('loans.*', 'loan_types.name as loan_type')
		->orderby('loans.name')
		->get();
	}

	protected function list_responsibilitycenters()
	{
		return DB::table('responsibility_centers')
		->where('responsibility_centers.deleted_at', null)
		->orderby('responsibility_centers.name')
		->get();
	}

	protected function list_bank_accounts()
	{
		return DB::table('bank_accounts')
		->where('bank_accounts.deleted_at', null)
		->orderby('bank_accounts.name')
		->get();
	}

	protected function list_banks()
	{
		return DB::table('banks')
		->where('banks.deleted_at', null)
		->orderby('banks.name')
		->get();
	}

	protected function list_philhealthpolicy()
	{
		return DB::table('philhealth_policy')
		->where('philhealth_policy.deleted_at', null)
		->orderby('philhealth_policy.name')
		->get();
	}

	protected function list_pagibigpolicy()
	{
		return DB::table('pagibig_policy')
		->where('pagibig_policy.deleted_at', null)
		->orderby('pagibig_policy.name')
		->get();
	}

	protected function list_gsispolicy()
	{
		return DB::table('gsis_policy')
		->where('gsis_policy.deleted_at', null)
		->orderby('gsis_policy.name')
		->get();
	}

	protected function list_wagerate()
	{
		return DB::table('wage_rate')
		->where('wage_rate.deleted_at', null)
		->orderby('wage_rate.name')
		->get();
	}

	protected function list_taxtables()
	{
		return DB::table('tax_table')
		->where('tax_table.deleted_at', null)
		->orderby('tax_table.name')
		->get();
	}

	protected function list_absences()
	{
		return DB::table('absences')
		->where('absences.deleted_at', null)
		->orderby('absences.name')
		->get();
	}

	protected function list_leaves()
	{
		return DB::table('leaves')
		->where('leaves.deleted_at', null)
		->orderby('leaves.name')
		->get();
	}

	protected function list_holidays()
	{
		return DB::table('holidays')
		->where('holidays.deleted_at', null)
		->orderby('holidays.name')
		->get();
	}

	protected function list_officesuspensions()
	{
		return DB::table('office_suspensions')
		->where('office_suspensions.deleted_at', null)
		->orderby('office_suspensions.name')
		->get();
	}

	protected function list_workschedules()
	{
		return DB::table('work_schedules')
		->where('work_schedules.deleted_at', null)
		->orderby('work_schedules.name')
		->get();
	}

	protected function list_scheduletypes()
	{
		return DB::table('schedule_type')
		->get();
	}

	protected function list_sectors()
	{
		return DB::table('sectors')
		->where('sectors.deleted_at', null)
		->orderby('sectors.name')
		->get();
	}

	protected function list_signatories()
	{
		return DB::table('signatories')
		->where('signatories.deleted_at', null)
		->join('employees', 'employees.id', 'signatories.employee_id')
		->leftjoin('employee_work_experience', 'employee_work_experience.employee_id', 'employees.id')
		->where('employee_work_experience.workexp_present', '=', 1)
		->where('employee_work_experience.deleted_at', null)
		->select('employees.*', 'signatories.id as signatory_id', DB::raw('CONCAT(employees.first_name," ",employees.extension_name," ",employees.middle_name," ",employees.last_name) as full_name'), 'signatories.position', 'signatories.department', 'signatories.division')
		->get();
	}

	protected function count_city_by($id)
	{
		return DB::table('cities')->where('cities.id', '=', $id)->count();
	}

	protected function select_city_by($id)
	{
		return DB::table('cities')
		->where('cities.id', '=', $id)
		->join('provinces', 'provinces.id', '=', 'cities.province_id')
		->select('cities.*', 'provinces.name as province_name')
		->first();
	}

	protected function count_table_record($table, $id)
	{
		return DB::table($table)
		->where('id', '=', $id)
		->count();
	}

	protected function get_table_record($table, $id)
	{
		$data = DB::table($table)
		->where('id', '=', $id)
		->first();

		if($table == 'office_suspensions')
		{
			$data->start_time = date('h:i A', mktime(0, $data->start_time));
		}
		elseif($table == 'work_schedules')
		{
			foreach($this->day as $key => $val)
			{
				$time_in = $val.'_time_in';
				if($data->$time_in) $data->$time_in =  date('h:i A', mktime(0, $data->$time_in ));

				$lunch_out = $val.'_lunch_out';
                if($data->$lunch_out) $data->$lunch_out =  date('h:i A', mktime(0, $data->$lunch_out));

                $lunch_in = $val.'_lunch_in';
                if($data->$lunch_in) $data->$lunch_in =  date('h:i A', mktime(0, $data->$lunch_in));

                $time_out = $val.'_time_out';
            	if($data->$time_out) $data->$time_out =  date('h:i A', mktime(0, $data->$time_out));

            	$flexi_time = $val.'_flexi_time';
            	if($data->$flexi_time) $data->$flexi_time =   date('h:i A', mktime(0, $data->$flexi_time));
			}
		}

		return $data;
	}

	protected function check_exist_table_record($table, $id)
	{
		if($this->count_table_record($table, $id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_table_record($table, $id);
	}

	protected function count_salary_grade_by($salary_grade, $step)
	{
		return DB::table('salary_grade')
		->where('salary_grade.deleted_at', null)
		->where('salary_grade.name', $salary_grade)
		->where('salary_grade.step', $step)
		->count();
	}

	protected function get_salary_grade_by($salary_grade, $step)
	{
		return DB::table('salary_grade')
		->where('salary_grade.deleted_at', null)
		->where('salary_grade.name', $salary_grade)
		->where('salary_grade.step', $step)
		->first();
	}

	protected function check_exist_salary_grade($salary_grade, $step)
	{
		if($this->count_salary_grade_by($salary_grade, $step) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_salary_grade_by($salary_grade, $step);
	}

	protected function list_training_types()
	{
		return DB::table('training_types')
		->where('training_types.deleted_at', null)
		->get();
	}

	protected function get_course_by_name($name)
	{
		return DB::table('courses')->where('courses.name', 'like', '%'.$name.'%')->first();
	}

	protected function check_exist_plantilla_item($id)
	{
		if($this->count_table_record('plantilla_items', $id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_table_record('plantilla_items', $id);
	}

	protected function get_signatory_by($id)
	{
		return DB::table('signatories')
		->where('signatories.deleted_at', null)
		->where('signatories.id', $id)
		->join('employees', 'employees.id', '=', 'signatories.employee_id')
		->select('signatories.*', 'employees.first_name', 'employees.last_name', 'employees.extension_name', 'employees.middle_name')
		->first();
	}

}