<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use DB;
use Exception;

trait BuildingPermit
{
	protected function list_pre_evaluation($user_id = null, $status = null)
	{
		$query = DB::table('pre_evaluation_building_permit as pebp')
		->where('pebp.is_deleted', null);
		if($user_id)
		{
			$query->where('pebp.user_id', $user_id);
		}
		if($status)
		{
			$query->where('pebp.status', $status);
		}
		return $query->get();
	}

	protected function user_info($user_id)
	{
		$query = DB::table('users')
		->where('id', $user_id);
		return $query->first();
	}

	protected function check_project($project_name)
	{
		$query = DB::table('pre_evaluation_building_permit as pebp')
		->where('pebp.is_deleted', null)
		->where('pebp.project_name', $project_name);
		return $query->count();
	}

	protected function get_pre_evaluation($id)
	{
		$query = DB::table('pre_evaluation_building_permit as pebp')
		->where('pebp.is_deleted', null)
		->where('pebp.id', $id);
		return $query->first();
	}

	protected function get_tct_informations($pre_eval_id)
	{
		return DB::table('tct_informations')
		->where('tct_informations.pre_evaluation_id', '=', $pre_eval_id)
		->where('tct_informations.is_deleted', null)
		->get();	
	}

	protected function get_record($table, $id)
	{
		return DB::table($table)
		->where($table.'.id', '=', $id)
		->where($table.'.is_deleted', null)
		->first();
	}

	protected function list_files($permit_type = null)
	{
		$query = DB::table('files')
		->where('files.is_deleted', null);
		if($permit_type)
		{
			$query->where('files.permit_type', '=', $permit_type);
		}
		
		return $query->get();
	}

	protected function get_attachments($record_id, $permit_type)
	{
		$table = 'attachments';
		return DB::table($table)
		->where($table.'.record_id', '=', $record_id)
		->where($table.'.permit_type', '=', $permit_type)
		->where($table.'.is_deleted', null)
		->leftjoin('files', 'files.id', '=', $table.'.file_id')
		->select($table.'.*', 'files.name as file_name')
		->get();
	}

}