<?php

namespace App\Http\Controllers;

use App\Http\Traits\Employee;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends Controller
{
    use Employee;

    private $personal_info_attribute;

    private $fieldnames = array(
        ['field_name' => 'last_name', 'input_name' => 'last_name', 'input_type' => 'text'], 
        ['field_name' => 'first_name', 'input_name' => 'first_name', 'input_type' => 'text'], 
        ['field_name' => 'middle_name', 'input_name' => 'middle_name', 'input_type' => 'text'],
        ['field_name' => 'extension_name', 'input_name' => 'ext_name', 'input_type' => 'text'],
        ['field_name' => 'nickname', 'input_name' => 'nickname', 'input_type' => 'text'],
        ['field_name' => 'contact_number', 'input_name' => 'contact_no', 'input_type' => 'text'],
        ['field_name' => 'mobile_number', 'input_name' => 'mobile_no', 'input_type' => 'text'],
        ['field_name' => 'telephone_number', 'input_name' => 'tel_no', 'input_type' => 'text'],
        ['field_name' => 'birthday', 'input_name' => 'birth_date', 'input_type' => 'date'],
        ['field_name' => 'birth_place', 'input_name' => 'birth_place', 'input_type' => 'textarea'],
        ['field_name' => 'email_address', 'input_name' => 'email_address', 'input_type' => 'text'],
        ['field_name' => 'gender', 'input_name' => 'sex', 'input_type' => 'select'],
        ['field_name' => 'civil_status_id', 'input_name' => 'civil_status', 'input_type' => 'select'],
        ['field_name' => 'filipino', 'input_name' => 'is_filipino', 'input_type' => 'select'],
        ['field_name' => 'blood_type_id', 'input_name' => 'blood_type', 'input_type' => 'select'],
        ['field_name' => 'height', 'input_name' => 'height', 'input_type' => 'text'],
        ['field_name' => 'weight', 'input_name' => 'weight', 'input_type' => 'text'],
        ['field_name' => 'gsis', 'input_name' => 'gsis_id_no', 'input_type' => 'text'],
        ['field_name' => 'pagibig', 'input_name' => 'pagibig_id_no', 'input_type' => 'text'],
        ['field_name' => 'philhealth', 'input_name' => 'philhealth_no', 'input_type' => 'text'],
        ['field_name' => 'sss', 'input_name' => 'sss_no', 'input_type' => 'text'],
        ['field_name' => 'tin', 'input_name' => 'tin_no', 'input_type' => 'text'],
        ['field_name' => 'employee_number', 'input_name' => 'agency_emp_no', 'input_type' => 'text'],
        ['field_name' => 'residential_house_number', 'input_name' => 'residential_house_number', 'input_type' => 'text'],
        ['field_name' => 'residential_street', 'input_name' => 'residential_street', 'input_type' => 'text'],
        ['field_name' => 'residential_subdivision', 'input_name' => 'residential_subdivision', 'input_type' => 'text'],
        ['field_name' => 'residential_brgy', 'input_name' => 'residential_brgy', 'input_type' => 'text'],
        ['field_name' => 'residential_city_id', 'input_name' => 'residential_city', 'input_type' => 'select'],
        ['field_name' => 'permanent_house_number', 'input_name' => 'permanent_house_number', 'input_type' => 'text'],
        ['field_name' => 'permanent_street', 'input_name' => 'permanent_street', 'input_type' => 'text'],
        ['field_name' => 'permanent_subdivision', 'input_name' => 'permanent_subdivision', 'input_type' => 'text'],
        ['field_name' => 'permanent_brgy', 'input_name' => 'permanent_brgy', 'input_type' => 'text'],
        ['field_name' => 'permanent_city_id', 'input_name' => 'permanent_city', 'input_type' => 'select'],
        ['field_name' => 'father_last_name', 'input_name' => 'father_last_name', 'input_type' => 'text'],
        ['field_name' => 'father_first_name', 'input_name' => 'father_first_name', 'input_type' => 'text'],
        ['field_name' => 'father_middle_name', 'input_name' => 'father_middle_name', 'input_type' => 'text'],
        ['field_name' => 'father_extension_name', 'input_name' => 'father_extension_name', 'input_type' => 'text'],
        ['field_name' => 'mother_last_name', 'input_name' => 'mother_last_name', 'input_type' => 'text'],
        ['field_name' => 'mother_first_name', 'input_name' => 'mother_first_name', 'input_type' => 'text'],
        ['field_name' => 'mother_middle_name', 'input_name' => 'mother_middle_name', 'input_type' => 'text'],
        ['field_name' => 'spouse_last_name', 'input_name' => 'spouse_last_name', 'input_type' => 'text'],
        ['field_name' => 'spouse_first_name', 'input_name' => 'spouse_first_name', 'input_type' => 'text'],
        ['field_name' => 'spouse_middle_name', 'input_name' => 'spouse_middle_name', 'input_type' => 'text'],
        ['field_name' => 'spouse_extension_name', 'input_name' => 'spouse_extension_name', 'input_type' => 'text'],
        ['field_name' => 'occupation_id', 'input_name' => 'spouse_occupation', 'input_type' => 'select'],
        ['field_name' => 'employer_name', 'input_name' => 'spouse_employer_name', 'input_type' => 'text'],
        ['field_name' => 'business_address', 'input_name' => 'spouse_business_address', 'input_type' => 'text'],
        ['field_name' => 'spouse_telephone_number', 'input_name' => 'spouse_telephone_number', 'input_type' => 'text'],
    );

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'personal_info';
    
        $this->personal_info_attribute = [
        'last_name' => strtolower(trans('page.surname')),
        'first_name' => strtolower(trans('page.first_name')),
        'middle_name' => strtolower(trans('page.middle_name')),
        'extension_name' => strtolower(trans('page.ext_name')),
        'sex' => strtolower(trans('page.sex')),
        'civil_status' => strtolower(trans('page.civil_status')),
        'other_civil_status' => strtolower(trans('page.please_specify')),
        'birth_date' => strtolower(trans('page.birth_date')),
        'birth_place' => strtolower(trans('page.birth_place')),
        'height' => strtolower(trans('page.height')),
        'blood_type' => strtolower(trans('page.blood_type')),
        'gsis' => strtolower(trans('page.gsis_id_no')),
        'pagibig' => strtolower(trans('page.pagibig_id_no')),
        'philhealth' => strtolower(trans('page.philhealth_no')),
        'sss' => strtolower(trans('page.sss_no')),
        'tin' => strtolower(trans('page.tin_no')),
        'telepone_no' => strtolower(trans('page.tel_no')),
        'mobile_no' => strtolower(trans('page.mobile_no')),
        'email_address' => strtolower(trans('page.email_address')),
        'is_filipino' => strtolower(trans('page.is_filipino')),
        'dual_citizenship' => strtolower(trans('page.dual_citizenship')),
        'residential_house_no' => strtolower(trans('page.house_blk_lot_no')),
        'residential_street' => strtolower(trans('page.street')),
        'residential_subdivision' => strtolower(trans('page.subdivision')),
        'residential_barangay' => strtolower(trans('page.barangay')),
        'same_as_above' => strtolower(trans('page.same_as_above')),
        'permanent_house_no' => strtolower(trans('page.house_blk_lot_no')),
        'permanent_street' => strtolower(trans('page.street')),
        'permanent_subdivision' => strtolower(trans('page.subdivision')),
        'permanent_barangay' => strtolower(trans('page.barangay')),
        'employee_number'  => strtolower(trans('page.agency_emp_no')),
        ];
    }

    public function index($option)
    {
        $data = ['module' => $this->module, 'option' => $option];

        if($option == '201-file')
        {
            $data = array_merge($data, ['default_table_id' => 'employees_tbl', 'default_json_url' => url('/personal_info/datatables/'.$option), 'default_columns' => array(['data' => 'action', 'sortable' => false], ['data' => 'employee_number'], ['data' => 'last_name'], ['data' => 'first_name']), 'file' => 'pds-file.201-file.table']);

            $file = 'pds-file.index';
        }
        elseif($option == '201-update')
        {
            $data = array_merge($data, ['default_table_id' => 'employees_update_tbl', 'default_json_url' => url('/personal_info/datatables/'.$option), 'default_columns' => array(['data' => 'action', 'sortable' => false], ['data' => 'employee_number'], ['data' => 'field_name'], ['data' => 'old_value'], ['data' => 'new_value']), 'file' => 'pds-file.201-update.table']);

            $file = 'pds-file.index';
        }
        elseif($option == 'employee-information')
        {
            $file = 'employee-information.index';
        }
        elseif($option == 'employee-service-record')
        {
            $columns        = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'position_name'],
                ['data' => 'workexp_start_date'],
                ['data' => 'workexp_end_date']
            );
            $data = array_merge($data, ['columns' => $columns]);
            $file = 'employee-service-record.index';
        }
        elseif($option == 'reports')
        {
            $file = 'report.index';
        }

        return view($file, $data);
    }

    public function employees_datatables($option)
    {
        if($option == '201-file')
        {
            $data = $this->list_employees();

            $datatables = Datatables::of($data)
            ->addColumn('action', function($data){
                return '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>
                            </div>
                        </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        elseif($option == '201-update')
        {   
            $list_civilstatus = $this->list_civilstatus();
            $list_bloodtypes = $this->list_bloodtypes();
            $list_cities = $this->list_cities();
            $list_occupations = $this->list_occupations();

            $data = $this->list_employee_updates();

            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($option){
                return '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="javascript:void(0);" class="dropdown-item" onclick="delete_employee_update('.$data->eu_id.')">
                                    <i class="icon icon-left mdi mdi-delete"></i>&nbsp;Delete
                                </a>
                                <form
                                role="form"
                                method="POST"
                                action="'.url('/personal_info/201-update/'.$data->eu_id.'/delete').'" 
                                id="delete_emp_'.$data->eu_id.'">'.
                                csrf_field().'
                                </form>
                            </div>
                        </div>';
            })
            ->editColumn('field_name', function($data){
                if(in_array($data->field_name, array_column($this->fieldnames, 'field_name')))
                {
                    $key = array_search($data->field_name, array_column($this->fieldnames, 'field_name'));
                    $field = $this->fieldnames[$key]['input_name'];
                } 

                return trans('page.'.$field);
            })
            ->editColumn('old_value', function($data) use ($list_civilstatus, $list_bloodtypes, $list_cities, $list_occupations){
                $old_value = $data->old_value;

                if($old_value !== NULL)
                {
                    if(in_array($data->field_name, ['gender']))
                    {
                        $old_value = $old_value == 'M' ? 'Male' : 'Female'; 
                    }
                    elseif(in_array($data->field_name, ['filipino']))
                    {
                        $old_value = $old_value == '1' ? 'Yes' : 'No';
                    }
                    elseif(in_array($data->field_name, ['civil_status_id']))
                    {
                        if(in_array($old_value, ['1','2','3','4']))
                        {
                            $civil_status = $list_civilstatus->first(function($item) use ($old_value) { 
                                return $item->id == $old_value;
                            });

                            $old_value = $civil_status->name;
                        }
                        else
                        {
                            $old_value = $data->old_value;
                        }
                    }
                    elseif(in_array($data->field_name, ['blood_type_id']))
                    {
                        $blood_type = $list_bloodtypes->first(function($item) use ($old_value) { 
                                return $item->id == $old_value;
                        });

                        $old_value = $blood_type->name;
                    }
                    elseif(in_array($data->field_name, ['residential_city_id', 'permanent_city_id']))
                    {
                        $city = $list_cities->first(function($item) use ($old_value) { 
                                return $item->id == $old_value;
                        });

                        $old_value = $city->name;
                    }
                    elseif(in_array($data->field_name, ['list_occupations']))
                    {
                        $occupation = $list_occupations->first(function($item) use ($old_value) { 
                                return $item->id == $old_value;
                        });

                        $old_value = $occupation->name;
                    }
                }
            
                return $old_value;
            })
            ->editColumn('new_value', function($data) use ($list_civilstatus, $list_bloodtypes, $list_cities, $list_occupations){
                $new_value = $data->new_value;

                if($new_value !== NULL)
                {
                    if(in_array($data->field_name, ['gender']))
                    {
                        $new_value = $new_value == 'M' ? 'Male' : 'Female'; 
                    }
                    elseif(in_array($data->field_name, ['filipino']))
                    {
                        $new_value = $new_value == '1' ? 'Yes' : 'No';
                    }
                    elseif(in_array($data->field_name, ['civil_status_id']))
                    {
                        // except 5 = others

                        if(in_array($new_value, ['1','2','3','4']))
                        {
                            $civil_status = $list_civilstatus->first(function($item) use ($new_value) { 
                                return $item->id == $new_value;
                            });

                            $new_value = $civil_status->name;
                        }
                    }
                    elseif(in_array($data->field_name, ['blood_type_id']))
                    {
                        $blood_type = $list_bloodtypes->first(function($item) use ($new_value) { 
                                return $item->id == $new_value;
                        });

                        $new_value = $blood_type->name;
                    }
                    elseif(in_array($data->field_name, ['residential_city_id', 'permanent_city_id']))
                    {
                        $city = $list_cities->first(function($item) use ($new_value) { 
                                return $item->id == $new_value;
                        });

                        $new_value = $city->name;
                    }
                    elseif(in_array($data->field_name, ['list_occupations']))
                    {
                        $occupation = $list_occupations->first(function($item) use ($new_value) { 
                                return $item->id == $new_value;
                        });

                        $new_value = $occupation->name;
                    }
                }
                
                return $new_value;
            })
            ->rawColumns(['action'])
            ->make(true);
        }

        return $datatables;
    }

    public function create_personal_info($option)
    {
        if($option == '201-file')
        {
            $data = ['module' => $this->module, 'option' => '201-file', 'file' => 'pds-file.201-file.personal_info_form', 'list_cities' => $this->list_cities(), 'list_provinces' => $this->list_provinces(), 'list_bloodtypes' => $this->list_bloodtypes(), 'list_civilstatus' => $this->list_civilstatus()];

            $file = 'pds-file.create';
        }   
        elseif($option == '201-update')
        {
            $data = ['module' => $this->module, 'option' => '201-update', 'file' => 'pds-file.201-update.add_form', 'list_employees' => $this->list_employees(), 'fieldnames' => $this->fieldnames];

            $file = 'pds-file.create';
        }

        return view($file, $data);
    }

    public function store_personal_info(request $request, $option)
    {
        try
        {
            if($option == '201-file')
            {
                $validate_cities = implode(',', $this->get_value_by($this->list_cities(), 'id'));
                $validate_bloodtypes = implode(',', $this->get_value_by($this->list_bloodtypes(), 'id'));
                $validate_civilstatus = implode(',', $this->get_value_by($this->list_civilstatus(), 'id'));

                $rule = [
                'last_name' => 'required|max:255',
                'first_name' => 'required|max:255',
                'middle_name' => 'required|max:255',
                'extension_name' => 'sometimes|nullable|max:255',
                'sex' => 'in:M,F',
                'civil_status' => 'in:'.$validate_civilstatus,
                'other_civil_status' => 'sometimes|nullable|max:255',
                'birth_date' => 'required',
                'birth_place' => 'required|max:255',
                'height' => 'sometimes|nullable|max:4',
                'weight' => 'sometimes|nullable|max:4',
                'blood_type' => 'sometimes|nullable|in:'.$validate_bloodtypes,
                'gsis' => 'sometimes|nullable|max:255',
                'pagibig' => 'sometimes|nullable|max:255',
                'philhealth' => 'sometimes|nullable|max:255',
                'sss' => 'sometimes|nullable|max:255',
                'tin' => 'sometimes|nullable|max:255',
                'employee_number' => 'required|max:255',
                'telepone_no' => 'sometimes|nullable|max:255',
                'mobile_no' => 'sometimes|nullable|max:255',
                'email_address' => 'required|email',
                'is_filipino' => 'in:0,1',
                'dual_citizenship' => 'in:0,1',
                'residential_house_no' => 'required|max:255',
                'residential_street' => 'sometimes|nullable|max:255',
                'residential_subdivision' => 'sometimes|nullable|max:255',
                'residential_barangay' => 'required|max:255',
                'residential_city' => 'in:'.$validate_cities,
                'same_as_above' => 'in:0,1',
                'permanent_house_no' => 'required|max:255',
                'permanent_street' => 'sometimes|nullable|max:255',
                'permanent_subdivision' =>  'sometimes|nullable|max:255',
                'permanent_barangay' => 'required|max:255',
                'permanent_city' => 'in:'.$validate_cities,
                ];

                $this->validate_request($request->all(), $rule, $this->personal_info_attribute);

                $residential_house_no = $request->get('residential_house_no');
                $residential_street = $request->get('residential_street');
                $residential_subdivision = $request->get('residential_subdivision');
                $residential_barangay = $request->get('residential_barangay');
                $residential_city = $request->get('residential_city');
                $residential_city_info = $this->select_city_by($residential_city);

                $same_as_above = $request->get('same_as_above');

                $permanent_house_no = $request->get('permanent_house_no');
                $permanent_street = $request->get('permanent_street');
                $permanent_subdivision = $request->get('permanent_subdivision');
                $permanent_barangay = $request->get('permanent_barangay');
                $permanent_city = $request->get('permanent_city');
                $permanent_city_info =  $this->select_city_by($permanent_city);
                $permanent_province = $permanent_city_info->province_id;
                $permanent_zip_code = $permanent_city_info->zip_code;

                if($same_as_above == 1)
                {
                    $permanent_house_no = $residential_house_no;
                    $permanent_street = $residential_street;
                    $permanent_subdivision = $residential_subdivision;
                    $permanent_barangay = $residential_barangay;
                    $permanent_city = $residential_city;
                    $permanent_province = $residential_city_info->province_id;
                    $permanent_zip_code = $residential_city_info->zip_code;
                }

                $civil_status = $request->get('civil_status');
                $other_civil_status = $request->get('other_civil_status');

                if($civil_status !== 5) 
                {
                    $other_civil_status = null;
                }

                $insert_data = [
                'last_name' => strtoupper($request->get('last_name')),
                'first_name' => strtoupper($request->get('first_name')),
                'middle_name' => strtoupper($request->get('middle_name')),
                'extension_name' => strtoupper($request->get('extension_name')),
                'birthday' => date("Y-m-d", strtotime($request->get('birth_date'))),
                'birth_place' => $request->get('birth_place'),
                'email_address' => $request->get('email_address'),
                'gender' => $request->get('sex'),
                'civil_status_id' => $civil_status,
                'other_civil_status' => $other_civil_status,
                'filipino' => $request->get('is_filipino'),
                'height' => $request->get('height'),
                'weight' => $request->get('weight'),
                'blood_type_id' => $request->get('blood_type'),
                'gsis' => $request->get('gsis'),
                'pagibig' => $request->get('pagibig'),
                'philhealth' => $request->get('philhealth'),
                'sss' => $request->get('sss'),
                'tin' => $request->get('tin'),
                'employee_number' => $request->get('employee_number'),
                'telephone_number' => $request->get('telepone_no'),
                'mobile_number' => $request->get('mobile_no'),
                'residential_house_number' => $residential_house_no,
                'residential_street' => $residential_street,
                'residential_subdivision' => $residential_subdivision,
                'residential_brgy' => $residential_barangay,
                'residential_city_id' => $residential_city,
                'residential_province_id' => $residential_city_info->province_id,
                'residential_zip_code' => $residential_city_info->zip_code,
                'residential_country_id' => 1,
                'permanent_house_number' => $permanent_house_no,
                'permanent_street' => $permanent_street,
                'permanent_subdivision' => $permanent_subdivision,
                'permanent_brgy' => $permanent_barangay,
                'permanent_city_id' => $permanent_city,
                'permanent_province_id' => $permanent_province,
                'permanent_zip_code' => $permanent_zip_code,
                'permanent_country_id' => 1,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table('employees')
                ->insert($insert_data);

                $lastId = DB::getPdo()->lastInsertId();

                DB::table('users')
                ->insert([
                'name' => $request->get('employee_number'),
                'username' => $request->get('employee_number'),
                'password' => bcrypt('123456'),
                'employee_id' => $lastId,
                'level' => 2,
                'created_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
            elseif($option == '201-update')
            {
                $emp_id = $request->get('emp_id');

                if(!$employee = $this->check_exist_employee_by($emp_id)) throw new Exception(trans('page.no_record_found'));
                
                $field_name = $request->get('field_name');
            
                $new_value = $request->get('new_value');
                if($field_name == 'civil_status_id') $new_value_others = $request->get('new_value_others');
                
                $rule = array(
                'field_name' => 'required|in:'.implode(',', array_column($this->fieldnames, 'field_name'))
                );

                $update_employee = [];
                $update_employee_family = [];
                $insert_employee_updates = [];

                if(in_array($field_name, ['last_name', 'first_name', 'middle_name', 'birth_place', 'employee_number', 'residential_house_no', 'residential_barangay', 'permanent_house_no', 'permanent_barangay']))
                {
                    $rule['new_value'] = 'required|max:255';
                    $update_employee[$field_name] = strtoupper($new_value); 
                }
                elseif(in_array($field_name, ['birthday']))
                {
                    $rule['new_value'] = 'required'; // validate with regex
                    $update_employee[$field_name] = $new_value;
                }
                elseif(in_array($field_name, ['email_address']))
                {
                    $rule['new_value'] = 'required|email';
                }
                elseif(in_array($field_name, ['extension_name', 'nickname', 'height', 'weight', 'gsis', 'pagibig', 'philhealth', 'sss', 'tin', 'telephone_number', 'mobile_number', 'contact_number', 'residential_street', 'residential_subdivision', 'permanent_street', 'permanent_subdivision', 'father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                {
                    $rule['new_value'] = 'sometimes|nullable|max:255';

                    if(in_array($field_name, ['father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                    {       
                        $update_employee_family[$field_name] = strtoupper($new_value);
                    }
                    else
                    {
                        $update_employee[$field_name] = strtoupper($new_value);
                    }
                }
                elseif(in_array($field_name, ['civil_status_id']))
                {
                    $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_civilstatus(), 'id') );
                    $update_employee[$field_name] = $new_value;

                    if($new_value == '5')
                    {
                        $rule['new_value_others'] = 'required|max:255';
                        $update_employee[$field_name] = $new_value;
                        $update_employee['other_civil_status'] = $new_value_others;

                        $insert_employee_updates['new_value'] = $new_value_others;
                    } 
                }
                elseif(in_array($field_name, ['gender']))
                {
                    $rule['new_value'] = 'in:'.implode(',', config('params.sex'));
                    $update_employee[$field_name] = $new_value;
                }
                elseif(in_array($field_name, ['blood_type_id']))
                {
                    $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_bloodtypes(), 'id'));
                    $update_employee[$field_name] = $new_value;
                }
                elseif(in_array($field_name, ['filipino']))
                {
                    $rule['new_value'] = 'sometimes|nullable|in:0,1';
                    $update_employee[$field_name] = $new_value;
                }
                elseif(in_array($field_name, ['occupation_id']))
                {
                    $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_occupations(), 'id'));
                    $update_employee_family[$field_name] = $new_value;
                }
                elseif(in_array($field_name, ['spouse_telephone_number']))
                {
                    $rule['new_value'] = 'sometimes|nullable|max:255';
                    $update_employee_family['telephone_number'] = $new_value;
                }
                elseif(in_array($field_name, ['residential_city_id', 'permanent_city_id']))
                {
                    $rule['new_value'] = 'in:'.implode(',', $this->get_value_by($this->list_cities(), 'id'));
                    $update_employee[$field_name] = $new_value;
                }

                $this->validate_request($request->all(), $rule, $this->personal_info_attribute);

                $old_value = $employee->$field_name;

                if($field_name == 'civil_status_id')
                {
                    if($old_value == 5) $old_value = $employee->other_civil_status;
                }

                $insert_employee_updates = array_merge($insert_employee_updates, [
                'field_name' => $field_name,
                'employee_id' => $emp_id,
                'old_value' => $old_value,
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ]);

                if(in_array($field_name, ['residential_city_id', 'permanent_city_id']))
                {
                    $selected_city = $this->select_city_by($new_value);

                    if($field_name == 'residential_city_id')
                    {
                        $update_employee['residential_province_id'] = $selected_city->province_id;
                        $update_employee['residential_zip_code'] = $selected_city->zip_code;
                    }
                    elseif($field_name == 'permanent_city_id')
                    {
                        $update_employee['permanent_province_id'] = $selected_city->province_id;
                        $update_employee['permanent_zip_code'] = $selected_city->zip_code;
                    }
                }

                DB::beginTransaction();

                if(!empty($update_employee))
                {
                    DB::table('employees')
                    ->where('employees.id', '=', $emp_id)
                    ->update($update_employee);
                }
                
                if(!empty($update_employee_family))
                {
                    DB::table('employee_family')
                    ->where('employee_family.employee_id', '=', $emp_id)
                    ->update($update_employee_family);
                }

                if(!array_key_exists('new_value', $insert_employee_updates)) $insert_employee_updates['new_value'] = $new_value;

                DB::table('employee_updates')
                ->insert($insert_employee_updates);
  
                DB::commit();
            }
            elseif($option == 'employee-information')
            {
                // validate 

                $update_data = [
                    'hired_date' => $request->get('hired_date'),
                    'assumption_date' => $request->get('assumption_date'),
                    'resign_date' => $request->get('resign_date'),
                    'start_date' => $request->get('start_date'),
                    'end_date' => $request->get('end_date'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];

                DB::beginTransaction();

                DB::table("employee_informations")
                ->where('id', '=',$request->get('id'))
                ->update($update_data); 
                
                DB::commit();   
            }
        }
        catch(Exception $e)
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response('success', 204);
    }

    public function edit_personal_info($option, $id)
    {
        dd('test');
    }

    public function update_personal_info(request $request, $option)
    {
        try
        {
            dd('test');
        }
        catch(Exception $e)
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response('success', 204);
    }

    public function delete_personal_info(request $request, $option, $id)
    {
        try
        {
            if($option == '201-update')
            {
                if(!$employee_update = $this->check_exist_employee_update_by($id)) throw new Exception(trans('page.no_record_found'));

                $list_civilstatus = $this->list_civilstatus();

                DB::beginTransaction();

                $last_record = $this->get_latest_employee_update($employee_update->employee_id, $employee_update->field_name);

                if($id >= $last_record->id)
                {
                    if(!in_array($last_record->field_name, array_column($this->fieldnames, 'field_name'))) throw new Exception('field not existing');

                    $field_name = $last_record->field_name == 'spouse_telephone_number' ? 'telephone_number' : $last_record->field_name;

                    $old_value = $last_record->old_value;

                    if(in_array($last_record->field_name, ['father_last_name', 'father_first_name', 'father_middle_name', 'father_extension_name', 'mother_last_name', 'mother_first_name', 'mother_middle_name', 'spouse_last_name', 'spouse_first_name', 'spouse_middle_name', 'spouse_extension_name', 'employer_name', 'business_address']))
                    {
                        DB::table('employee_family')
                        ->where('employee_family.employee_id', '=', $last_record->employee_id)
                        ->update([
                        $field_name => $old_value
                        ]);
                    }
                    else
                    {   
                        $update_employee = [
                            $field_name => $old_value
                        ];
                        
                        if($field_name == 'civil_status_id')
                        {
                            if(in_array($old_value, $this->get_value_by($list_civilstatus, 'id')))
                            {
                                if($old_value !== 5) 
                                {
                                    $update_employee['other_civil_status'] = ''; 
                                }
                            }
                            else
                            {
                                $update_employee['civil_status_id'] = 5;
                                $update_employee['other_civil_status'] = $old_value;
                            }
                        }

                        DB::table('employees')
                        ->where('employees.id', '=', $last_record->employee_id)
                        ->update($update_employee);
                    }
                }

                DB::table('employee_updates')
                ->where('employee_updates.id', '=', $id)
                ->update([
                'is_deleted' => '1',
                'updated_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
        }
        catch(Exception $e)
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        return response('success', 204);
    }

    /*******************************  Employee API ***************************************/

    public function get_civilstatus()
    {
        try
        {
            $civil_status = $this->list_civilstatus();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['civil_status' => $civil_status]);
    }

    public function get_cities()
    {
        try
        {
            $city = $this->list_cities();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['city' => $city]);
    }

    public function get_city_by($id)
    {   
        try
        {
            if($this->count_city_by($id) == 0) throw new Exception();

            $city = $this->select_city_by($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['city' => $city]);
    }

    public function get_employees()
    {
        try
        {
            $employees = $this->list_employees();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employees' => $employees, 'count_employee' => count($employees)]);
    }

    public function get_bloodtypes()
    {
        try
        {
            $blood_type = $this->list_bloodtypes();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['blood_type' => $blood_type]);
    }

    public function get_occupations()
    {
        try
        {
            $occupation = $this->list_occupations();
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['occupation' => $occupation]);
    }

    public function get_employee_by($id)
    {   
        try
        {
            if($this->count_employee_by($id) == 0) throw new Exception();

            $employee = $this->get_employee_by_id($id);

            $gender = config('params.sex');

            $civil_status = $this->list_civilstatus();

            $blood_type = $this->list_bloodtypes();

            $city = $this->list_cities();

            $occupation = $this->list_occupations();

            $workexp = $this->get_latest_workexp($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee' => $employee, 'gender' => $gender, 'civil_status' => $civil_status, 'blood_type' => $blood_type, 'city' => $city, 'occupation' => $occupation, 'workexp' => $workexp]);
    }

    public function filter_employee($filter)
    {
        try
        {
            $employees = $this->filter_employee_by($filter);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employees' => $employees, 'count_employee' => count($employees)]);
    }  
}
