<?php

namespace App\Http\Controllers;

use App\Http\Traits\BuildingPermit;
use DB;
use Exception;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;
use \PDF;

class BuildingPermitController extends Controller
{
    use BuildingPermit;

    protected $location_permit_fields = [
        'proponent_name',
        'representative',
        'business_name',
        'business_line',
        'location',
        'contact_number',
        'fax_number',
        'email_add',
        'website',
        'application_type',
        'other_application',
        'project_type',
        'other_project',
        'units',
        'storey',
        'lot_area',
        'floor_area',
        'ownership_type',
        'other_ownership',
        'investment_cost',
    ];

    protected $business_permit_fields = [
        'enterprise_classification',
        'primary_enterprise_type',
        'secondary_enterprise_type',
        'tourist_transport_type',
        'application_type',
        'payment_mode',
        'tin',
        'registration_number',
        'business_type',
        'amendment_from',
        'amendment_to',
        'tax_incentive_from_govt',
        'govt_entity',
        'last_name',
        'first_name',
        'middle_name',
        'business_name',
        'trade_name',
        'business_address',
        'business_postal_code',
        'business_tel_no',
        'business_email_add',
        'business_mobile_no',
        'owner_address',
        'owner_postal_code',
        'owner_tel_no',
        'owner_email_add',
        'owner_mobile_no',
        'contact_person',
        'contact_person_number',
        'business_area',
        'total_employees',
        'residing_with_lgu',
        'lessor_full_name',
        'lessor_address',
        'lessor_contact_number',
        'lessor_email_add',
        'monthly_rental',
    ];

    protected $building_permit_fields = [
        'application_type' => 'application_type',
        'last_name' => 'last_name',
        'first_name' => 'first_name',
        'middle_name' => 'middle_name',
        'tin' => 'tin',
        'enterprise_name' => 'enterprise_name',
        'ownership_form' => 'ownership_form',
        'house_number' => 'house_number',
        'street' => 'street',
        'barangay' => 'barangay',
        'city' => 'city',
        'zip_code' => 'zip_code',
        'telephone_number' => 'telephone_number',
        'work_scope' => 'work_scope',
        'other_work_scope' => 'other_work_scope',
        'occupancy_character' => 'occupancy_character',
        'other_occupancy_character' => 'other_occupancy_character',
        'occupancy_classification' => 'occupancy_classification',
        'estimated_cost' => 'estimated_cost',
        'units' => 'units',
        'construction_date' => 'construction_date',
        'floor_area' => 'floor_area',
        'expected_completion_date' => 'expected_completion_date',
        'architect_name' => 'architect_name',
        'architect_address' => 'architect_address',
        'architect_prc_no' => 'architect_prc_no',
        'architect_prc_validity' => 'architect_prc_validity',
        'architect_ptr_no' => 'architect_ptr_no',
        'architect_ptr_issued_date' => 'architect_ptr_issued_date',
        'architect_id_issued_at' => 'architect_id_issued_at',
        'architect_tin' => 'architect_tin',
        'lot_owner_name' => 'lot_owner_name',
        'lot_owner_address' => 'lot_owner_address',
        'lot_owner_ctc_no' => 'lot_owner_ctc_no',
        'lot_owner_ctc_issued_date' => 'lot_owner_ctc_issued_date',
        'lot_owner_ctc_issued_place' => 'lot_owner_ctc_issued_place',
    ];

    protected $tct_fields = [
        'lot_number' => 'lot_number',
        'block_no' => 'block_no',        
        'tct_no' => 'tct_no',
        'tax_declaration_no' => 'tax_declaration_no',
        'construction_street' => 'construction_street',
        'construction_barangay' => 'construction_barangay',
        'construction_city' => 'construction_city'
    ];
    protected $user_account_fields = [
        'first_name',
        'last_name',
        'middle_name',
        'username',
        'position',
        'permit_type'

    ];
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'business_permit';
    }

    public function index($option)
    {
        $json_url = '';
        $columns  = '';
        if($option == 'user_pre_evaluation')
        {
            $module = 'building_permit';
            $file = 'users.pre_evaluation.table';
            $route = 'users.index';
        }
        else if($option == 'application')
        {
            $module = 'building_permit';
            $file = 'users.building_permit.table';
            $route = 'users.index';
        }
        else
        {

        }


        $icon = 'mdi mdi-plus';
    	$data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'json_url' => $json_url, 'columns' => $columns, 'file' => $file];
        return view($route, $data);
    }
    public function user_create($option)
    {
        $array = array();
        if($option == 'user_pre_evaluation') 
        {
            $module = 'users';
            $file = $module.'.pre_evaluation.add_form';
            $array = [
                'user_info' => $this->user_info(Auth::user()->id),
                'tct_fields' => $this->tct_fields,
                'pre_evaluation_requirements' => $this->list_files(5)
            ];
        }
        $icon = 'mdi mdi-plus';
        $data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'file' => $file, 'array' => $array];
        return view($module.'.create', $data);
    }

    public function user_edit($option, $id)
    {
        $array = array();
        if($option == 'user_pre_evaluation') 
        {
            $module = 'users';
            $file = $module.'.pre_evaluation.add_form';
            $array = [
                'row' => $this->get_pre_evaluation($id),
                'tct_fields' => $this->tct_fields,
            ];
        }
        $icon = 'mdi mdi-plus';
        $data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'file' => $file, 'array' => $array];
        return view($module.'.create', $data);
    }

    public function user_datatable($option)
    {

        if($option == 'user_pre_evaluation')
        {
            $data = $this->list_pre_evaluation(Auth::user()->id);
        }
        else if($option == 'user_application')
        {
            $data = $this->list_pre_evaluation(Auth::user()->id, 4);

        }
        if($option == 'user_pre_evaluation')
        {
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($option) {
                
                $edit_record = '<a href="'.url('/user/'.$option.'/'.$data->id.'/edit-record').'" class="dropdown-item">
                            <i class="icon icon-left mdi mdi-edit"></i>Edit Record
                            </a>';
                $edit_record = '';
                $view_record = '<a href="'.url('/'.$option.'/'.$data->id.'/view-record').'" class="dropdown-item">
                            <i class="icon icon-left mdi mdi-eye"></i>View Record
                            </a>';

                $pass_to_assessor = '<span class="dropdown-item" onclick="pass_pre_eval_to_assessor(\'building_permit\','.$data->id.')">
                            <i class="icon icon-left mdi mdi-skip-next"></i>Submit for Assessment
                        </span>';
                $pass_to_assessor_2 = '<span class="dropdown-item" onclick="pass_pre_eval_to_assessor(\'building_permit\','.$data->id.')">
                            <i class="icon icon-left mdi mdi-skip-next"></i>Submit for Assessment
                        </span>';

                /*$cancel_evaluation = '<span class="dropdown-item" onclick="remove_pre_eval(\'building_permit\','.$data->id.')">
                            <i class="icon icon-left mdi mdi-delete"></i>Cancel Pre Evaluation
                        </span>';*/
                $cancel_evaluation = '';
                $upload_suplemental = '<a href="'.url('/'.$data->id.'/add-suplemental').'" class="dropdown-item">
                            <i class="icon icon-left mdi mdi-upload"></i>Upload Requirements
                        </a>';
                $upload_suplemental_2 = '<a href="'.url('/'.$data->id.'/add-suplemental').'" class="dropdown-item">
                            <i class="icon icon-left mdi mdi-upload"></i>Upload Supplemental
                        </a>';
                if($data->status == 1)
                {
                    $view_record = '';
                }
                else if($data->status == 2 || $data->status == 3 || $data->status == 5 || $data->status == 7)
                {
                    $edit_record = '';
                    $pass_to_assessor = '';
                    $cancel_evaluation = '';
                }
                if($data->status != 8)
                {
                    $edit_record = '';
                    $upload_suplemental = '';
                }
                if($data->status != 9)
                {
                    $edit_record = '';
                    $upload_suplemental_2 = '';
                    $pass_to_assessor_2 = '';
                }
                if($data->status == 9)
                {
                    $pass_to_assessor = '';
                }
                return '<div cla ss="tools">
                    <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                    <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                        '.$view_record.'
                        '.$edit_record.'
                        '.$pass_to_assessor.'
                        '.$pass_to_assessor_2.'
                        '.$cancel_evaluation.'
                        '.$upload_suplemental.'
                        '.$upload_suplemental_2.'
                    </div>
                </div>';
            });
        }
        else
        {
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($option) {
                
                $str = '<button type="type" class="btn btn-secondary btn-space text-center" style="font-weight: 400;" onclick="create_building_permit(\'building_permit\','.$data->id.')">Apply for Building Permit</button>';
                return $str;
            });
        }
        

        if($option == 'user_pre_evaluation' || $option == 'user_application')
        {
            $datatables
            ->editColumn('filed_date', function($data){
                
                return $data->file_date = date('F d, Y h:i', strtotime($data->filed_date));
            });

            $datatables
            ->editColumn('application_type', function($data){
                if($data->application_type == 0)
                {
                    $data->application_type = '';
                }
                else if ($data->application_type == 1)
                {
                    $data->application_type = 'New';
                }
                else if ($data->application_type == 2)
                {
                    $data->application_type = 'Renewal';
                }
                else if ($data->application_type == 3)
                {
                    $data->application_type = 'Amendatory';
                }
                return $data->application_type;
            });

            $datatables
            ->editColumn('status', function($data){
                if ($data->status == 1)
                {
                    $data->status = 'For Submission';
                    //On Process
                }
                elseif ($data->status == 2)
                {
                    $data->status = 'For Assessment';
                }
                elseif ($data->status == 3)
                {
                    $data->status = 'On Going Pre-Evaluation';
                }
                elseif ($data->status == 4)
                {
                    $data->status = 'For Building Permit Application';
                }
                /*elseif ($data->status == 4)
                {
                    $data->status = 'For Pre Evaluation';
                }
                elseif ($data->status == 5)
                {
                    $data->status = 'For Review of Supplemental Initial Plan Evaluation Report';
                }
                elseif ($data->status == 6)
                {
                    $data->status = 'Supplemental Initial Plan Evaluation Report to Client';
                }
                elseif ($data->status == 7)
                {
                    $data->status = 'For Building Permit Application';
                }*/
                elseif ($data->status == 8)
                {
                    $data->status = 'Incomplete Requirements';
                }
                elseif ($data->status == 9)
                {
                    $data->status = 'For Submission of Supplemental Requirements';
                }
                return $data->status;
            });
        }

        return $datatables
        ->rawColumns(['action'])
        ->make(true);

    }
    

    public function user_store(Request $request, $option)
    {
        try {
            $exist_record   = 0;
            $record_id      = 0;

            if($option == 'user_pre_evaluation')
            {
                $check_if_project_exist = $this->check_project($request->get('project_name'));


                if($check_if_project_exist)
                {
                    $exist_record = 1;
                }
                else
                {
                    $token_code         = Str::random(50);
                    /*$survey_plan        = Input::File('survey_plan');
                    $structural_plan    = Input::File('structural_plan');
                    $electrical_plan    = Input::File('electrical_plan');
                    $mechanical_plan    = Input::File('mechanical_plan');
                    $sanitary_plan      = Input::File('sanitary_plan');
                    $plumbing_plan      = Input::File('plumbing_plan');
                    $electronics_plan   = Input::File('electronics_plan');*/

                    /*if($survey_plan)
                    {
                        $file_ext_survey_plan  = $survey_plan->getClientOriginalExtension();

                        $survey_plan_file      = 'survey_plan_'.$token_code.'.'.$file_ext_survey_plan;

                        $survey_plan_name      = $survey_plan->getClientOriginalName();
                    }
                    else
                    {
                        $survey_plan_file = '';
                        $survey_plan_name = '';
                    }

                    if($structural_plan)
                    {
                        $file_ext_structural_plan  = $structural_plan->getClientOriginalExtension();

                        $structural_plan_file      = 'structural_plan_'.$token_code.'.'.$file_ext_structural_plan;

                        $structural_plan_name      = $structural_plan->getClientOriginalName();
                    }
                    else
                    {
                        $structural_plan_file = '';
                        $structural_plan_name = '';
                    }


                    if($electrical_plan)
                    {
                        $file_ext_electrical_plan  = $electrical_plan->getClientOriginalExtension();

                        $electrical_plan_file      = 'electrical_plan_'.$token_code.'.'.$file_ext_electrical_plan;

                        $electrical_plan_name      = $electrical_plan->getClientOriginalName();
                    }
                    else
                    {
                        $electrical_plan_file = '';
                        $electrical_plan_name = '';
                    }

                    if($mechanical_plan)
                    {
                        $file_ext_mechanical_plan  = $mechanical_plan->getClientOriginalExtension();

                        $mechanical_plan_file      = 'mechanical_plan_'.$token_code.'.'.$file_ext_mechanical_plan;

                        $mechanical_plan_name      = $mechanical_plan->getClientOriginalName();
                    }
                    else
                    {
                        $mechanical_plan_file = '';
                        $mechanical_plan_name = '';
                    }

                    if($sanitary_plan)
                    {
                        $file_ext_sanitary_plan  = $sanitary_plan->getClientOriginalExtension();

                        $sanitary_plan_file      = 'sanitary_plan_'.$token_code.'.'.$file_ext_sanitary_plan;

                        $sanitary_plan_name      = $sanitary_plan->getClientOriginalName();
                    }
                    else
                    {
                        $sanitary_plan_file = '';
                        $sanitary_plan_name = '';
                    }

                    if($plumbing_plan)
                    {
                        $file_ext_plumbing_plan  = $plumbing_plan->getClientOriginalExtension();

                        $plumbing_plan_file      = 'plumbing_plan_'.$token_code.'.'.$file_ext_plumbing_plan;

                        $plumbing_plan_name      = $plumbing_plan->getClientOriginalName();
                    }
                    else
                    {
                        $plumbing_plan_file = '';
                        $plumbing_plan_name = '';
                    }


                    if($electronics_plan)
                    {
                        $file_ext_electronics_plan  = $electronics_plan->getClientOriginalExtension();

                        $electronics_plan_file      = 'electronics_plan_'.$token_code.'.'.$file_ext_electronics_plan;

                        $electronics_plan_name      = $electronics_plan->getClientOriginalName();
                    }
                    else
                    {
                        $electronics_plan_file = '';
                        $electronics_plan_name = '';
                    }*/
                    $insert_data = [
                        'user_id' => Auth::user()->id,
                        'filed_date' => DB::raw('now()'),
                        'application_type' => $request->get('application_type'),
                        'last_name' => strtoupper($request->get('last_name')),
                        'first_name' => strtoupper($request->get('first_name')),
                        'middle_name' => strtoupper($request->get('middle_name')),
                        'tin' => strtoupper($request->get('tin')),
                        'project_name' => strtoupper($request->get('project_name')),
                        'enterprise_name' => strtoupper($request->get('enterprise_name')),
                        'ownership_form' => strtoupper($request->get('ownership_form')),
                        'house_number' => strtoupper($request->get('house_number')),
                        'street' => strtoupper($request->get('street')),
                        'barangay' => strtoupper($request->get('barangay')),
                        'city' => strtoupper($request->get('city')),
                        'zip_code' => strtoupper($request->get('zip_code')),
                        'contact_number' => strtoupper($request->get('contact_number')),
                        'status' => 1,
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];
                    /*
                    'survey_plan' => $survey_plan_file,
                    'survey_plan_original_name' => $survey_plan_name,
                    'structural_plan' => $structural_plan_file,
                    'structural_plan_original_name' => $structural_plan_name,
                    'electrical_plan' => $electrical_plan_file,
                    'electrical_plan_original_name' => $electrical_plan_name,
                    'mechanical_plan' => $mechanical_plan_file,
                    'mechanical_plan_original_name' => $mechanical_plan_name,
                    'sanitary_plan' => $sanitary_plan_file,
                    'sanitary_plan_original_name' => $sanitary_plan_name,
                    'plumbing_plan' => $plumbing_plan_file,
                    'plumbing_plan_original_name' => $plumbing_plan_name,
                    'electronics_plan' => $electronics_plan_file,
                    'electronics_plan_original_name' => $electronics_plan_name,
                    */

                    DB::beginTransaction();

                    DB::table('pre_evaluation_building_permit')
                    ->insert($insert_data); 


                    $record_id = DB::getPdo()->lastInsertId(); 

                    $folder_path = public_path().'/tieza/pre_evaluation_building_permit/'.$record_id;

                    if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                    /*if($survey_plan_file)
                    {
                        $request->file('survey_plan')->move($folder_path, $survey_plan_file);    
                    }

                    if($structural_plan_file)
                    {
                        $request->file('structural_plan')->move($folder_path, $structural_plan_file);    
                    }

                    if($electrical_plan_file)
                    {
                        $request->file('electrical_plan')->move($folder_path, $electrical_plan_file);    
                    }

                    if($mechanical_plan_file)
                    {
                        $request->file('mechanical_plan')->move($folder_path, $mechanical_plan_file);    
                    }

                    if($sanitary_plan_file)
                    {
                        $request->file('sanitary_plan')->move($folder_path, $sanitary_plan_file);    
                    }

                    if($plumbing_plan_file)
                    {
                        $request->file('plumbing_plan')->move($folder_path, $plumbing_plan_file);    
                    }

                    if($electronics_plan_file)
                    {
                        $request->file('electronics_plan')->move($folder_path, $electronics_plan_file);    
                    }*/

                    

                    /*$tct_count = $request->get('tct_count');

                    $tct_data = [
                        'pre_evaluation_id' => $record_id,
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];
                    for ($i=1; $i <= $tct_count; $i++) { 
                        foreach ($this->tct_fields as $key => $value) {
                            $obj = $request->get($value);
                            $obj_arr = explode(",", $obj);
                            $tct_data[$key] = $obj_arr[$i - 1];
                        }
                        DB::table('tct_informations')
                        ->insert($tct_data); 
                    }*/

                    DB::commit();
                }

                
            }
            else if($option == 'tct_information')
            {
                $pre_eval_id                  = $request->get('pre_eval_id');
                $land_ownership               = $request->get('land_ownership');
                $lot_number                   = strtoupper($request->get('lot_number'));
                $block_no                     = strtoupper($request->get('block_no'));
                $tct_no                       = strtoupper($request->get('tct_no'));
                $tax_declaration_no           = strtoupper($request->get('tax_declaration_no'));
                $tax_declaration_no           = strtoupper($request->get('tax_declaration_no'));
                $construction_barangay        = strtoupper($request->get('construction_barangay'));
                $construction_city            = strtoupper($request->get('construction_city'));
                $construction_street          = strtoupper($request->get('construction_street'));


                $token_code                 = Str::random(50);
                $land_title                 = Input::File('land_title');
                $tax_declaration            = Input::File('tax_declaration');
                $denr_certificate           = Input::File('denr_certificate');
                $barangay_certificate       = Input::File('barangay_certificate');
                $flagt                      = Input::File('flagt');
                $authorization_letter       = Input::File('authorization_letter');
                $lease_contract             = Input::File('lease_contract');
                $deed_of_sale               = Input::File('deed_of_sale');
                $contract_to_sell           = Input::File('contract_to_sell');
                $secretary_certificate      = Input::File('secretary_certificate');



                if($land_title)
                {
                    $file_ext_land_title  = $land_title->getClientOriginalExtension();

                    $land_title_file      = 'land_title_'.$token_code.'.'.$file_ext_land_title;

                    $land_title_name      = $land_title->getClientOriginalName();
                }
                else
                {
                    $land_title_file = '';
                    $land_title_name = '';
                }

                if($tax_declaration)
                {
                    $file_ext_tax_declaration  = $tax_declaration->getClientOriginalExtension();

                    $tax_declaration_file      = 'tax_declaration_'.$token_code.'.'.$file_ext_tax_declaration;

                    $tax_declaration_name      = $tax_declaration->getClientOriginalName();
                }
                else
                {
                    $tax_declaration_file = '';
                    $tax_declaration_name = '';
                }

                if($denr_certificate)
                {
                    $file_ext_denr_certificate  = $denr_certificate->getClientOriginalExtension();

                    $denr_certificate_file      = 'denr_certificate_'.$token_code.'.'.$file_ext_denr_certificate;

                    $denr_certificate_name      = $denr_certificate->getClientOriginalName();
                }
                else
                {
                    $denr_certificate_file = '';
                    $denr_certificate_name = '';
                }

                if($barangay_certificate)
                {
                    $file_ext_barangay_certificate  = $barangay_certificate->getClientOriginalExtension();

                    $barangay_certificate_file      = 'barangay_certificate_'.$token_code.'.'.$file_ext_barangay_certificate;

                    $barangay_certificate_name      = $barangay_certificate->getClientOriginalName();
                }
                else
                {
                    $barangay_certificate_file = '';
                    $barangay_certificate_name = '';
                }

                if($flagt)
                {
                    $file_ext_flagt  = $flagt->getClientOriginalExtension();

                    $flagt_file      = 'flagt_'.$token_code.'.'.$file_ext_flagt;

                    $flagt_name      = $flagt->getClientOriginalName();
                }
                else
                {
                    $flagt_file = '';
                    $flagt_name = '';
                }

                if($authorization_letter)
                {
                    $file_ext_authorization_letter  = $authorization_letter->getClientOriginalExtension();

                    $authorization_letter_file      = 'authorization_letter_'.$token_code.'.'.$file_ext_authorization_letter;

                    $authorization_letter_name      = $authorization_letter->getClientOriginalName();
                }
                else
                {
                    $authorization_letter_file = '';
                    $authorization_letter_name = '';
                }

                if($lease_contract)
                {
                    $file_ext_lease_contract  = $lease_contract->getClientOriginalExtension();

                    $lease_contract_file      = 'lease_contract_'.$token_code.'.'.$file_ext_lease_contract;

                    $lease_contract_name      = $lease_contract->getClientOriginalName();
                }
                else
                {
                    $lease_contract_file = '';
                    $lease_contract_name = '';
                }

                if($deed_of_sale)
                {
                    $file_ext_deed_of_sale  = $deed_of_sale->getClientOriginalExtension();

                    $deed_of_sale_file      = 'deed_of_sale_'.$token_code.'.'.$file_ext_deed_of_sale;

                    $deed_of_sale_name      = $deed_of_sale->getClientOriginalName();
                }
                else
                {
                    $deed_of_sale_file = '';
                    $deed_of_sale_name = '';
                }

                if($contract_to_sell)
                {
                    $file_ext_contract_to_sell  = $contract_to_sell->getClientOriginalExtension();

                    $contract_to_sell_file      = 'contract_to_sell_'.$token_code.'.'.$file_ext_contract_to_sell;

                    $contract_to_sell_name      = $contract_to_sell->getClientOriginalName();
                }
                else
                {
                    $contract_to_sell_file = '';
                    $contract_to_sell_name = '';
                }

                if($secretary_certificate)
                {
                    $file_ext_secretary_certificate  = $secretary_certificate->getClientOriginalExtension();

                    $secretary_certificate_file      = 'secretary_certificate_'.$token_code.'.'.$file_ext_secretary_certificate;

                    $secretary_certificate_name      = $secretary_certificate->getClientOriginalName();
                }
                else
                {
                    $secretary_certificate_file = '';
                    $secretary_certificate_name = '';
                }


                $insert_data = [
                    'pre_evaluation_id' => $pre_eval_id,
                    'land_ownership' => $land_ownership,
                    'lot_number' => $lot_number,
                    'block_no' => $block_no,
                    'tct_no' => $tct_no,
                    'tax_declaration_no' => $tax_declaration_no,
                    'construction_street' => $construction_street,
                    'construction_barangay' => $construction_barangay,
                    'construction_city' => $construction_city,
                    'land_title' => $land_title_file,
                    'land_title_original_name' => $land_title_name,
                    'tax_declaration' => $tax_declaration_file,
                    'tax_declaration_original_name' => $tax_declaration_name,
                    'denr_certificate' => $denr_certificate_file,
                    'denr_certificate_original_name' => $denr_certificate_name,
                    'barangay_certificate' => $barangay_certificate_file,
                    'barangay_certificate_original_name' => $barangay_certificate_name,
                    'flagt' => $flagt_file,
                    'flagt_original_name' => $flagt_name,
                    'authorization_letter' => $authorization_letter_file,
                    'authorization_letter_original_name' => $authorization_letter_name,
                    'lease_contract' => $lease_contract_file,
                    'lease_contract_original_name' => $lease_contract_name,
                    'deed_of_sale' => $deed_of_sale_file,
                    'deed_of_sale_original_name' => $deed_of_sale_name,
                    'contract_to_sell' => $contract_to_sell_file,
                    'contract_to_sell_original_name' => $contract_to_sell_name,
                    'secretary_certificate' => $secretary_certificate_file,
                    'secretary_certificate_original_name' => $secretary_certificate_name,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                DB::beginTransaction();

                DB::table('tct_informations')
                ->insert($insert_data); 


                $record_id = DB::getPdo()->lastInsertId(); 

                $folder_path = public_path().'/tieza/pre_evaluation_building_permit/tct_informations/'.$record_id;

                if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                if($land_title_file)
                {
                    $request->file('land_title')->move($folder_path, $land_title_file);    
                }

                if($tax_declaration_file)
                {
                    $request->file('tax_declaration')->move($folder_path, $tax_declaration_file);    
                }

                if($denr_certificate_file)
                {
                    $request->file('denr_certificate')->move($folder_path, $denr_certificate_file);    
                }

                if($barangay_certificate_file)
                {
                    $request->file('barangay_certificate')->move($folder_path, $barangay_certificate_file);    
                }

                if($flagt_file)
                {
                    $request->file('flagt')->move($folder_path, $flagt_file);    
                }

                if($authorization_letter_file)
                {
                    $request->file('authorization_letter')->move($folder_path, $authorization_letter_file);    
                }

                if($lease_contract_file)
                {
                    $request->file('lease_contract')->move($folder_path, $lease_contract_file);    
                }

                if($deed_of_sale_file)
                {
                    $request->file('deed_of_sale')->move($folder_path, $deed_of_sale_file);    
                }

                if($contract_to_sell_file)
                {
                    $request->file('contract_to_sell')->move($folder_path, $contract_to_sell_file);    
                }

                if($secretary_certificate_file)
                {
                    $request->file('secretary_certificate')->move($folder_path, $secretary_certificate_file);    
                }

                DB::commit();
            }


            return response(['exist_record' => $exist_record, 'record_id' => $record_id], 200);
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        
        


        
    }
    public function user_remove_pre_eval($option, $id)
    {
        DB::beginTransaction();
        $data       = array();
        $success    = 0;

        try {
            
            if($option == 'building_permit')
            {
                $update_data = [
                        'status' => 5,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                ];
                $table = 'pre_evaluation_building_permit';
            }
            

            DB::table($table)
            ->where($table.'.id', $id)
            ->update($update_data); 

            DB::commit();
            $status = 200;
            $success = 1;
        } catch (Exception $e) {
            $status = 422;
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
        }

        return response(['errors' => $data, 'success' => $success], $status);
    }

    public function pass_pre_eval_to_assessor($option, $id)
    {
        DB::beginTransaction();
        $data       = array();
        $success    = 0;

        try {
            
            if($option == 'building_permit')
            {
                $update_data = [
                        'for_assessment' => DB::raw('now()'),
                        'status' => 2,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                ];
                $table = 'pre_evaluation_building_permit';
            }
            

            DB::table($table)
            ->where($table.'.id', $id)
            ->update($update_data); 

            DB::commit();
            $status = 200;
            $success = 1;

            /*$result = $this->get_record('pre_evaluation_building_permit', $id);
            $user_data = $this->user_info($result->user_id);    
            $email = $user_data->email;
            $data = [
                'title' => 'Pre Evaluation Email',
                'full_name' => $result->first_name.' '.$result->last_name,
                'email' => $email,
                'project_name' => $result->project_name,
                'enterprise_name' => $result->enterprise_name,
                'address' => $result->house_number.' '.$result->street.' '.$result->barangay.' '.$result->city, 
            ]; 
            $subject = 'TIEZA Online Permitting System Pre-Evaluation';
            Mail::send('email.pre_evaluation', $data, function($message) use ($email,$subject){
                $message->from("no-reply.tezps@tieza.gov.ph",'no-reply.tezps@tieza.gov.ph'); 
                $message->to($email)->subject($subject);
            });*/


            /*Send Email to Client*/

            /*$result = $this->get_record('pre_evaluation_building_permit', $id);
            
            $email = Auth::user()->email;
            $data = [
                'title' => 'Pre Evaluation Email',
                'content' => 'This is your Pre Evaluation Receipt.',
                'full_name' => $result->first_name.' '.$result->last_name,
                'email' => $email,
                'project_name' => $result->project_name,
                'enterprise_name' => $result->enterprise_name,
                'address' => $result->house_number.' '.$result->street.' '.$result->barangay.' '.$result->city, 
            ]; 
            $pdf_view = 'email.pre_evaluation';

            $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($pdf_view, $data)->setWarnings(false);
            $file_name = "BP_PE_".$id;
            $folder_path = storage_path('tieza/building_permit/'.$id);
            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

            $pdf->save($folder_path.'/'.$file_name.'.pdf');

            $file_path = $folder_path.'/'.$file_name.'.pdf';

            $subject = 'TIEZA';
            Mail::send('email.pre_evaluation', $data, function($message) use ($email,$file_path,$subject){
                $message->from("no-reply.traveltax@tieza.gov.ph",'no-reply.traveltax@tieza.gov.ph'); 
                $message->to($email)->subject($subject);
                $message->attach($file_path, ['as' => 'AR CERTIFICATE']);
            });*/

            /*Mail::send('email.pre_evaluation', $data, function($message) use ($email) {
                $message->to($email, 'TEZ Email')
                        ->subject('Pre Evaluation Receipt');
            });*/ 
            /*Mail::send('email.cover_letter_pre_evaluation', $data, function($message) use ($email) {
                $message->to($email, 'TEZ Email')
                        ->subject('Cover Letter for Pre Evaluation');
            });  */

        } catch (Exception $e) {
            $status = 422;
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
        }

        return response(['errors' => $data, 'success' => $success], $status);
    }

    public function approved_pre_evaluation(Request $request, $option, $id)
    {
        DB::beginTransaction();
        $data       = array();
        $success    = 0;

        try {
            
            if($option == 'pre_evaluation')
            {
                $folder_path = public_path().'/tieza/pre_evaluation_building_permit/'.$id;

                if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                $token_code          = Str::random(50);


                $transmittal_letter  = Input::File('transmittal_letter');
                if($transmittal_letter)
                {
                    $file_ext_transmittal_letter  = $transmittal_letter->getClientOriginalExtension();

                    $transmittal_letter_file      = 'transmittal_letter_'.$token_code.'.'.$file_ext_transmittal_letter;

                    $transmittal_letter_name      = $transmittal_letter->getClientOriginalName();
                }
                else
                {
                    $transmittal_letter_file = '';
                    $transmittal_letter_name = '';
                }

                $supplemental_transmittal_letter  = Input::File('supplemental_transmittal_letter');
                if($supplemental_transmittal_letter)
                {
                    $file_ext_supplemental_transmittal_letter  = $supplemental_transmittal_letter->getClientOriginalExtension();

                    $supplemental_transmittal_letter_file      = 'supplemental_transmittal_letter_'.$token_code.'.'.$file_ext_supplemental_transmittal_letter;

                    $supplemental_transmittal_letter_name      = $supplemental_transmittal_letter->getClientOriginalName();
                }
                else
                {
                    $supplemental_transmittal_letter_file = '';
                    $supplemental_transmittal_letter_name = '';
                }

                $iper  = Input::File('iper');
                if($iper)
                {
                    $file_ext_iper  = $iper->getClientOriginalExtension();

                    $iper_file      = 'iper_'.$token_code.'.'.$file_ext_iper;

                    $iper_name      = $iper->getClientOriginalName();
                }
                else
                {
                    $iper_file = '';
                    $iper_name = '';
                }

                $supplemental_iper  = Input::File('supplemental_iper');
                if($supplemental_iper)
                {
                    $file_ext_supplemental_iper  = $supplemental_iper->getClientOriginalExtension();

                    $supplemental_iper_file      = 'supplemental_iper_'.$token_code.'.'.$file_ext_supplemental_iper;

                    $supplemental_iper_name      = $supplemental_iper->getClientOriginalName();
                }
                else
                {
                    $supplemental_iper_file = '';
                    $supplemental_iper_name = '';
                }

                $status = $request->get('status');
                if($status == 3)
                {
                    $update_data = [
                            'status' => 3,
                            'assessed_by' => Auth::user()->id,
                            'to_administrator' => DB::raw('now()'),
                            'assessed_remarks' => $request->get('assessed_remarks'),
                            'updated_by' => Auth::user()->id,
                            'updated_at' => DB::raw('now()')
                    ];
                    $result = $this->get_record('pre_evaluation_building_permit', $id);
                    $user_data = $this->user_info($result->user_id);    
                    $email = $user_data->email;
                    $data = [
                        'title' => 'Pre Evaluation Email',
                        'full_name' => $result->first_name.' '.$result->last_name,
                        'email' => $email,
                        'project_name' => $result->project_name,
                        'enterprise_name' => $result->enterprise_name,
                        'address' => $result->house_number.' '.$result->street.' '.$result->barangay.' '.$result->city, 
                    ]; 
                    $subject = 'TIEZA Online Permitting System Pre-Evaluation';
                    Mail::send('email.pre_evaluation', $data, function($message) use ($email,$subject){
                        $message->from("no-reply.tezps@tieza.gov.ph",'no-reply.tezps@tieza.gov.ph'); 
                        $message->to($email)->subject($subject);
                    });
                }
                else if($status == 4)
                {
                    if($supplemental_iper_file)
                    {
                        $request->file('supplemental_iper')->move($folder_path, $supplemental_iper_file);    
                    }
                    if($supplemental_transmittal_letter_file)
                    {
                        $request->file('supplemental_transmittal_letter')->move($folder_path, $supplemental_transmittal_letter_file);    
                    }
                    $update_data = [
                            'status' => 4,
                            'assessed_by' => Auth::user()->id,
                            'returned_date' => DB::raw('now()'),
                            'supplemental_transmittal_letter' => $supplemental_transmittal_letter_file,
                            'supplemental_transmittal_letter_original_name' => $supplemental_transmittal_letter_name,
                            'supplemental_iper' => $supplemental_iper_file,
                            'supplemental_iper_original_name' => $supplemental_iper_name,
                            'updated_by' => Auth::user()->id,
                            'updated_at' => DB::raw('now()')
                    ];
                    
                    $result = $this->get_record('pre_evaluation_building_permit', $id);
                    $user_data = $this->user_info($result->user_id);    
                    $email = $user_data->email;
                    $data = [
                        'title' => 'Pre Evaluation Email',
                        'full_name' => $result->first_name.' '.$result->last_name,
                        'email' => $email,
                        'project_name' => $result->project_name,
                        'enterprise_name' => $result->enterprise_name,
                        'address' => $result->house_number.' '.$result->street.' '.$result->barangay.' '.$result->city, 
                        'requirement_list' => $request->get('assessed_remarks'),
                    ]; 

                    $supplemental_iper_path = $folder_path.'/'.$supplemental_iper_file;
                    $supplemental_transmittal_letter_path = $folder_path.'/'.$supplemental_transmittal_letter_file;


                    $subject = 'Supplemental Initial Plan Evaluation Report (SIPER) for the proposed development of Mr./Ms. '.$result->first_name.' '.$result->last_name;
                    $cl_folder_path = public_path().'/tieza/building_permit';
                    $check_list = $cl_folder_path.'/checklist.pdf';
                    Mail::send('email.supplemental_iper_transmittal_letter', $data, function($message) use ($email,$subject, $supplemental_iper_file, $supplemental_transmittal_letter_file, $folder_path, $check_list){
                        $message->from("no-reply.tezps@tieza.gov.ph",'no-reply.tezps@tieza.gov.ph'); 
                        if($supplemental_iper_file)
                        {
                            $message->attach($folder_path.'/'.$supplemental_iper_file, ['as' => 'SIPER']);
                        }
                        if($supplemental_transmittal_letter_file)
                        {
                            $message->attach($folder_path.'/'.$supplemental_transmittal_letter_file, ['as' => 'Supplemental Transmittal Letter']);
                        }
                        $message->attach($check_list, ['as' => 'Check List']);
                        $message->to($email)->subject($subject);
                    });
                }
                else if($status == 4)
                {
                    if($iper_file)
                    {
                        $request->file('iper')->move($folder_path, $iper_file);    
                    }
                    $update_data = [
                            'status' => 5,
                            'evaluated_by' => Auth::user()->id,
                            'back_to_administrator' => DB::raw('now()'),
                            'evaluation_remarks' => $request->get('evaluation_remarks'),
                            'iper' => $iper_file,
                            'iper_original_name' => $iper_name,
                            'updated_by' => Auth::user()->id,
                            'updated_at' => DB::raw('now()')
                    ];
                }
                else if($status == 5)
                {
                    if($iper_file)
                    {
                        $request->file('iper')->move($folder_path, $iper_file);    
                    }
                    $update_data = [
                            'status' => 6,
                            'accepted_by' => Auth::user()->id,
                            'back_to_assessor' => DB::raw('now()'),
                            'iper' => $iper_file,
                            'iper_original_name' => $iper_name,
                            'accepted_remarks' => $request->get('accepted_remarks'),
                            'updated_by' => Auth::user()->id,
                            'updated_at' => DB::raw('now()')
                    ];
                }
                else if($status == 6)
                {
                    if($transmittal_letter_file)
                    {
                        $request->file('transmittal_letter')->move($folder_path, $transmittal_letter_file);    
                    }

                    $update_data = [
                            'status' => 7,
                            'final_assessed_by' => Auth::user()->id,
                            'approved_date' => DB::raw('now()'),
                            'transmittal_letter' => $transmittal_letter_file,
                            'transmittal_letter_original_name' => $transmittal_letter_name,
                            'updated_by' => Auth::user()->id,
                            'updated_at' => DB::raw('now()')
                    ];

                    $result = $this->get_record('pre_evaluation_building_permit', $id);
                    $user_data = $this->user_info($result->user_id);    
                    $email = $user_data->email;
                    $data = [
                        'title' => 'Pre Evaluation Email',
                        'content' => 'This is your Pre Evaluation Receipt.',
                        'full_name' => $result->first_name.' '.$result->last_name,
                        'email' => $email,
                        'project_name' => $result->project_name,
                        'enterprise_name' => $result->enterprise_name,
                        'address' => $result->house_number.' '.$result->street.' '.$result->barangay.' '.$result->city, 
                    ]; 

                    $iper_path = $folder_path.'/'.$result->iper;
                    $transmittal_path = $folder_path.'/'.$transmittal_letter_file;
                    $subject = 'Initial Plan Evaluation Report (IPER)';
                    Mail::send('email.iper_report', $data, function($message) use ($email, $subject, $iper_path, $transmittal_path){
                        $message->from("no-reply.tezps@tieza.gov.ph",'no-reply.tezps@tieza.gov.ph'); 
                        $message->to($email)->subject($subject);
                        $message->attach($iper_path, ['as' => 'IPER']);
                        $message->attach($transmittal_path, ['as' => 'Transmittal Letter']);
                    });
                }
                else if($status == 8)
                {
                    $update_data = [
                            'status' => 8,
                            'assessed_by' => Auth::user()->id,
                            'returned_date' => DB::raw('now()'),
                            'assessed_remarks' => $request->get('assessed_remarks'),
                            'updated_by' => Auth::user()->id,
                            'updated_at' => DB::raw('now()')
                    ];
                    
                    $result = $this->get_record('pre_evaluation_building_permit', $id);
                    $user_data = $this->user_info($result->user_id);    
                    $email = $user_data->email;
                    $data = [
                        'title' => 'Pre Evaluation Email',
                        'content' => 'This is your Pre Evaluation Receipt.',
                        'full_name' => $result->first_name.' '.$result->last_name,
                        'email' => $email,
                        'project_name' => $result->project_name,
                        'enterprise_name' => $result->enterprise_name,
                        'address' => $result->house_number.' '.$result->street.' '.$result->barangay.' '.$result->city, 
                        'requirement_list' => $request->get('assessed_remarks'),
                    ]; 
                    $subject = 'Incomplete Requirements for Pre-Evaluation';
                    //noncompliant_letter
                    Mail::send('email.incomplete_requirements', $data, function($message) use ($email,$subject){
                        $message->from("no-reply.tezps@tieza.gov.ph",'no-reply.tezps@tieza.gov.ph'); 
                        $message->to($email)->subject($subject);
                    });
                }
                else if($status == 9)
                {
                    if($iper_file)
                    {
                        $request->file('iper')->move($folder_path, $iper_file);    
                    }
                    
                    if($transmittal_letter_file)
                    {
                        $request->file('transmittal_letter')->move($folder_path, $transmittal_letter_file);    
                    }
                    $update_data = [
                            'status' => 9,
                            'assessed_by' => Auth::user()->id,
                            'returned_date' => DB::raw('now()'),
                            'transmittal_letter' => $transmittal_letter_file,
                            'transmittal_letter_original_name' => $transmittal_letter_name,
                            'iper' => $iper_file,
                            'iper_original_name' => $iper_name,
                            'updated_by' => Auth::user()->id,
                            'updated_at' => DB::raw('now()')
                    ];
                    
                    $result = $this->get_record('pre_evaluation_building_permit', $id);
                    $user_data = $this->user_info($result->user_id);    
                    $email = $user_data->email;
                    $data = [
                        'title' => 'Pre Evaluation Email',
                        'full_name' => $result->first_name.' '.$result->last_name,
                        'email' => $email,
                        'project_name' => $result->project_name,
                        'enterprise_name' => $result->enterprise_name,
                        'address' => $result->house_number.' '.$result->street.' '.$result->barangay.' '.$result->city, 
                        'requirement_list' => $request->get('assessed_remarks'),
                    ]; 

                    $iper_path = $folder_path.'/'.$iper_file;
                    $transmittal_letter_path = $folder_path.'/'.$transmittal_letter_file;

                    $subject = 'Initial Plan Evaluation Report (IPER) for the proposed development of Mr./Ms.'.$result->first_name.' '.$result->last_name;


                    Mail::send('email.iper_transmittal_letter', $data, function($message) use ($email,$subject, $iper_file, $transmittal_letter_file, $folder_path){
                        $message->from("no-reply.tezps@tieza.gov.ph",'no-reply.tezps@tieza.gov.ph'); 
                        if($iper_file)
                        {
                            $message->attach($folder_path.'/'.$iper_file, ['as' => 'IPER']);
                        }
                        if($transmittal_letter_file)
                        {
                            $message->attach($folder_path.'/'.$transmittal_letter_file, ['as' => 'Transmittal Letter']);
                        }
                        $message->to($email)->subject($subject);
                    });
                }
                /*$update_data = [
                        
                        
                        'status' => 3,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                ];*/
                
                $table = 'pre_evaluation_building_permit';
                 

                DB::commit();

                

                

                /*if($noncompliant_letter_file)
                {
                    $request->file('noncompliant_letter')->move($folder_path, $noncompliant_letter_file);    
                }*/
            }
            DB::table($table)
            ->where($table.'.id', $id)
            ->update($update_data);
            $status = 200;
            $success = 1;
        } catch (Exception $e) {
            $status = 422;
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
        }

        return response(['errors' => $data, 'success' => $success], $status);
    }


    public function list_tct_informations($module, $option, $pre_eval_id)
    {
        $data = $this->get_tct_informations($pre_eval_id);
        $option = $module;
        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) {
            return '
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="button" class="btn btn-danger" onclick="delete_attachment('.$data->id.');">
                            <i class="icon icon-left mdi mdi-close"></i>
                        </button>
                        <a href="'.url('/tieza/pre_evaluation_building_permit/tct_informations').'/'.$data->id.'/'.$data->land_title.'" target="_blank" class="btn btn-success"><i class="icon icon-left mdi mdi-eye"></i></a>
                    </a>
                    </div>
                </div>
                
            ';
        });
        $datatables
        ->editColumn('land_ownership', function($data){
            if($data->land_ownership == 1)
            {
                $data->land_ownership = 'TITLED (Registered Land Owner)';
            }
            else if($data->land_ownership == 2)
            {
                $data->land_ownership = 'TITLED (Not Registered Land Owner)';
            }
            else if($data->land_ownership == 3)
            {
                $data->land_ownership = 'UNTITLED';
            }
            else if($data->land_ownership == 4)
            {
                $data->land_ownership = 'FOREST LAND';
            }
            return $data->land_ownership;
        });        
        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }

    public function upload_pre_eval(Request $request)
    {
        $id                 = $request->get('record_id');
        $user_id            = Auth::user()->id;
        $token_code         = Str::random(50);
        $file               = Input::File('file');
        $file_id            = $request->get('file_id');
        $remarks            = $request->get('remarks');
        

        if($file)
        {
            $file_ext           = $file->getClientOriginalExtension();

            $attachment_name    = $token_code.'.'.$file_ext;

            $original_name      = $file->getClientOriginalName();
        }
        else
        {
            $attachment_name = '';
            $original_name = '';
        }


        

        $insert_data = [
            'user_id' => $user_id,
            'record_id' => $id,
            'permit_type' => 5,
            'file_id' => $file_id,
            'attachment_name' => $attachment_name,
            'original_name' => $original_name,
            'remarks' => $remarks,
            'lock_file' => 1,
            'created_by' => $user_id,
            'created_at' => DB::raw('now()')
        ];
        DB::beginTransaction();

        DB::table('attachments')
        ->insert($insert_data);

        DB::commit();

        $folder_path = public_path().'/tieza/pre_evaluation_building_permit/pre_eval_attachments/'.$id;

        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

        if($file)
        {
            $request->file('file')->move($folder_path, $attachment_name);    
        }
        
    }

    public function view_record($option, $id)
    {
        $array = array();
        if($option == 'user_pre_evaluation') 
        {
            $module = 'users';
            $file = $module.'.pre_evaluation.view_form';
            $array = [
                'user_info' => $this->user_info(Auth::user()->id),
                'tct_fields' => $this->tct_fields,
                'pre_evaluation_requirements' => $this->list_files(5),
                'record' => $this->get_record('pre_evaluation_building_permit', $id)
            ];
        }
        $icon = 'mdi mdi-plus';
        $data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'file' => $file, 'array' => $array, 'id' => $id];
        return view($module.'.view', $data);
    }

    public function add_suplemental($id)
    {
        $option = 'user_pre_evaluation';
        $array = array();
        $module = 'users';
        $file = $module.'.pre_evaluation.upload_suplemental';
        $array = [
            'pre_evaluation_requirements' => $this->list_files(5),
            'record' => $this->get_record('pre_evaluation_building_permit', $id)
        ];
        $icon = 'mdi mdi-plus';
        $data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'file' => $file, 'array' => $array, 'id' => $id];
        return view($module.'.view', $data);
    }

    public function view_pre_eval_reqs($id)
    {
        $option = 'user_pre_evaluation';
        $array = array();
        if($option == 'user_pre_evaluation') 
        {
            $attachments = $this->get_attachments($id, 5);
            $module = 'users';
            $file = $module.'.pre_evaluation.view_attachments';
            $array = ['attachments' => $attachments];
        }

        $icon = 'mdi mdi-plus';
        $data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'file' => $file, 'array' => $array, 'id' => $id];
        return view($module.'.view', $data);
    }

    public function view_pre_eval_reqs_admin($option, $module, $id)
    {
        $option = 'pre_evaluation';
        $array = array();
        if($option == 'pre_evaluation') 
        {
            $attachments = $this->get_attachments($id, 5);
            $module = 'users';
            $file = $module.'.pre_evaluation.view_attachments';
            $array = ['attachments' => $attachments];
        }

        $icon = 'mdi mdi-plus';
        $data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'file' => $file, 'array' => $array, 'id' => $id];
        return view($module.'.view', $data);
    }
}

