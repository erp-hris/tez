<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

Class FileSetupController extends Controller
{
	public function datatables_file_setup($option)
    {
    	try
    	{
    		$this->is_filesetup_option_exist($option);

	        if($option == 'agencies')
	       	{
	            $data = $this->list_agencies();
	        }
	        elseif($option == 'announcements')
	        {
	        	$data = $this->list_announcements();
	        }
	        elseif($option == 'departments')
	        {
	            $data = $this->list_departments();
	        }
	        elseif($option == 'offices')
	        {
	            $data = $this->list_offices();
	        }
	        elseif($option == 'plantilla_items')
	        {
	            $data = $this->list_plantilla_items();
	        }
	        elseif($option == 'countries')
	        {
	            $data = $this->list_countries();
	        }
	        elseif($option == 'cities')
	        {
	         	$data = $this->list_cities();
	       	}
	        elseif ($option == 'occupations')
	        {
	            $data = $this->list_occupations();
	        }
	        elseif ($option == 'citizenships')
	        {
	            $data = $this->list_citizenships();
	        }    
	        elseif ($option == 'provinces')
	        {
	            $data = $this->list_provinces();
	        }
	        elseif ($option == 'schools')
	        {
	            $data = $this->list_schools();
	        }
	        elseif ($option == 'courses')
	        {
	            $data = $this->list_courses();
	        }
	        elseif ($option == 'eligibilities')
	        {
	            $data = $this->list_eligibilities();
	        }
	        elseif ($option == 'providers')
	        {
	            $data = $this->list_providers();
	        }
	        elseif ($option == 'organizations')
	        {
	            $data = $this->list_organizations();
	        }
	        elseif ($option == 'trainings')
	        {
	            $data = $this->list_trainings();
	        }
	        elseif ($option == 'training_types')
	        {
	            $data = $this->list_training_types();
	        }
	        elseif ($option == 'divisions')
	        {
	            $data = $this->list_divisions();
	        }
	        elseif ($option == 'positions')
	        {
	            $data = $this->list_positions();
	        }
	        elseif ($option == 'appointment_status')
	        {
	            $data = $this->list_appointment_status();
	        }
	        elseif ($option == 'employment_status')
	        {
	            $data = $this->list_employment_status();
	        }
	        elseif ($option == 'salary_grade')
	        {
	            $data = $this->list_salary_grade();
	        }
	        elseif ($option == 'branches')
	        {
	            $data = $this->list_branches();
	        }
	        elseif ($option == 'locations')
	        {
	            $data = $this->list_locations();
	        }
	        elseif ($option == 'sections')
	        {
	            $data = $this->list_sections();
	        }
	        elseif ($option == 'units')
	        {
	            $data = $this->list_units();
	        }
	        elseif ($option == 'benefits')
	        {
	        	$data = $this->list_benefits();
	        }
	        elseif ($option == 'adjustments')
	        {
	        	$data = $this->list_adjustments();
	        }
	        elseif ($option == 'deductions')
	        {
	        	$data = $this->list_deductions();
	        }
	        elseif ($option == 'loans')
	        {
	        	$data = $this->list_loans();
	        }
	        elseif ($option == 'philhealth_policy')
	        {
	        	$data = $this->list_philhealthpolicy();
	        }
	        elseif ($option == 'pagibig_policy')
	        {
	        	$data = $this->list_pagibigpolicy();
	        }
	        elseif ($option == 'gsis_policy')
	        {
	        	$data = $this->list_gsispolicy();
	        }
	        elseif ($option == 'wage_rate')
	        {
	        	$data = $this->list_wagerate();
	        }
	        elseif ($option == 'tax_table')
	        {
	        	$data = $this->list_taxtables();
	        }
	        elseif ($option == 'responsibility_centers')
	        {
	        	$data = $this->list_responsibilitycenters();
	        }
	        elseif ($option == 'banks')
	        {
	        	$data = $this->list_banks();
	        }
	        elseif ($option == 'absences')
	        {
	        	$data = $this->list_absences();
	        }
	        elseif ($option == 'leaves')
	        {
	        	$data = $this->list_leaves();
	        }
	        elseif ($option == 'holidays')
	        {
	        	$data = $this->list_holidays();
	        }
	        elseif ($option == 'office_suspensions')
	        {
	        	$data = $this->list_officesuspensions();
	        }
	        elseif ($option == 'work_schedules')
	        {
	        	$data = $this->list_workschedules();
	        }
	        elseif ($option == 'sectors')
	        {
	        	$data = $this->list_sectors();
	        }
	        elseif( $option == 'signatories')
	        {
	        	$data = $this->list_signatories();
	        }
	        elseif( $option == 'designations')
	        {
	        	$data = $this->list_designations();
	        }
	        elseif( $option == 'assignments')
	        {
	        	$data = $this->list_assignments();
	        }
	        elseif( $option == 'bank_accounts')
	        {
	        	$data = $this->list_bank_accounts();
	        }
	        else
	        {
	            $data = array();
	        }
    	}
    	catch(Exception $e)
    	{
    		return response(['errors' => $e->getMessage()], 201);
    	}

    	$datatables = Datatables::of($data)
    	->addColumn('action', function($data) use ($option) {

    		$id = $data->id;
 
    		if($option == 'signatories') $id = $data->signatory_id;

           	return '<div class="tools"><button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button><div role="menu" class="dropdown-menu" x-placement="bottom-start"><a href="'.url('/file_setup/'.$option.'/'.$id.'/edit').'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>'.trans('page.edit').'</a><a href="javascript:void(0);" class="dropdown-item" onclick="delete_record('.$id.');"><i class="icon icon-left mdi mdi-delete"></i>'.trans('page.delete').'</a></div></div>';
        });

        $rawColumns = ['action'];
       
        if($option == 'employment_status')
        {
        	$plantilla_items = config('params.plantilla_category');

        	$datatables->addColumn('category_name', function($data) use ($plantilla_items) {
        		if(!isset($data->category_status)) return null;

        		return strtoupper($plantilla_items[$data->category_status]);
        	});

        	$rawColumns[] = 'category_name';
        }
        elseif($option == 'office_suspensions')
        {
        	$datatables->addColumn('category_name', function($data) use ($plantilla_items) {
        		if(!isset($data->category_status)) return null;

        		return strtoupper($plantilla_items[$data->category_status]);
        	});
        }

        return $datatables->rawColumns($rawColumns)->make(true);
    }

    public function get_record_file_setup($option, $id)
    {
        try
        {	
        	$this->is_filesetup_option_exist($option);

            $data = $this->check_exist_table_record($option, $id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422);
        }

        return response(['data' => $data], 201);
    }

    public function get_salary_grade($salary_grade, $step)
   	{
   		try
        {	
        	$data = $this->check_exist_salary_grade($salary_grade, $step);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422);
        }

        return response(['data' => $data]);
   	}

   	public function get_salary_amount(request $request)
   	{
   		try
        {	
        	$salary_grade = $request->get('salary_grade');
        	$salary_step = $request->get('salary_step');

        	$data = array();

        	if($salary_grade && $salary_step)
        	{
        		$data = $this->check_exist_salary_grade($salary_grade, $salary_stepsalary_step);	
        	}
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422);
        }

        return response(['data' => $data]);
   	}


   	public function get_plantilla_item($id)
   	{
   		try
        {	
        	$data = $this->check_exist_plantilla_item($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422);
        }

        return response(['data' => $data], 201);
   	}

   	public function get_training_by($id)
    {
        try
        {
            $training = $this->check_exist_table_record('trainings', $id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['training' => $training]);
    }

    public function get_all_record_file_setup($option)
   	{
   		try
        {
        	$records = array();


           	if($option == 'position')
	        {
	            $records = $this->list_positions();
	        }
	        elseif($option == 'sector')
	        {
	        	$records = $this->list_sectors();
	        }
	        elseif($option == 'department')
	        {
	        	$records = $this->list_departments();
	        }
	        elseif($option == 'division')
	        {
	        	$records = $this->list_divisions();
	        }
	        elseif($option == 'travel_tax_unit')
	        {
	        	$records = $this->list_units();
	        }
	        elseif($option == 'entities')
	        {
	        	$records = $this->list_offices();
	        }
	        elseif($option == 'detailed')
	        {
	        	$records = $this->list_designations();
	        }
	        elseif($option == 'employment_status')
	        {
	        	$records = $this->list_employment_status();
	        }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['records' => $records]);
   	}
}