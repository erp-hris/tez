<?php

namespace App\Http\Controllers;

use App\Http\Traits\Application;
use DB;
use Exception;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class ApplicationController extends Controller
{
    use Application;

    protected $location_permit_fields = [
        'proponent_name',
        'representative',
        'business_name',
        'business_line',
        'location',
        'contact_number',
        'fax_number',
        'email_add',
        'website',
        'application_type',
        'other_application',
        'project_type',
        'other_project',
        'units',
        'storey',
        'lot_area',
        'floor_area',
        'ownership_type',
        'other_ownership',
        'investment_cost',
    ];

    protected $business_permit_fields = [
        'enterprise_classification',
        'primary_enterprise_type',
        'secondary_enterprise_type',
        'tourist_transport_type',
        'application_type',
        'payment_mode',
        'tin',
        'registration_number',
        'business_type',
        'amendment_from',
        'amendment_to',
        'tax_incentive_from_govt',
        'govt_entity',
        'last_name',
        'first_name',
        'middle_name',
        'business_name',
        'trade_name',
        'business_address',
        'business_postal_code',
        'business_tel_no',
        'business_email_add',
        'business_mobile_no',
        'owner_address',
        'owner_postal_code',
        'owner_tel_no',
        'owner_email_add',
        'owner_mobile_no',
        'contact_person',
        'contact_person_number',
        'business_area',
        'total_employees',
        'residing_with_lgu',
        'lessor_full_name',
        'lessor_address',
        'lessor_contact_number',
        'lessor_email_add',
        'monthly_rental',
    ];

    protected $building_permit_fields = [
        'application_type' => 'application_type',
        'last_name' => 'last_name',
        'first_name' => 'first_name',
        'middle_name' => 'middle_name',
        'tin' => 'tin',
        'enterprise_name' => 'enterprise_name',
        'ownership_form' => 'ownership_form',
        'house_number' => 'house_number',
        'street' => 'street',
        'barangay' => 'barangay',
        'city' => 'city',
        'zip_code' => 'zip_code',
        'telephone_number' => 'telephone_number',
        'work_scope' => 'work_scope',
        'other_work_scope' => 'other_work_scope',
        'occupancy_character' => 'occupancy_character',
        'other_occupancy_character' => 'other_occupancy_character',
        'occupancy_classification' => 'occupancy_classification',
        'estimated_cost' => 'estimated_cost',
        'units' => 'units',
        'construction_date' => 'construction_date',
        'floor_area' => 'floor_area',
        'expected_completion_date' => 'expected_completion_date',
        'architect_name' => 'architect_name',
        'architect_address' => 'architect_address',
        'architect_prc_no' => 'architect_prc_no',
        'architect_prc_validity' => 'architect_prc_validity',
        'architect_ptr_no' => 'architect_ptr_no',
        'architect_ptr_issued_date' => 'architect_ptr_issued_date',
        'architect_id_issued_at' => 'architect_id_issued_at',
        'architect_tin' => 'architect_tin',
        'lot_owner_name' => 'lot_owner_name',
        'lot_owner_address' => 'lot_owner_address',
        'lot_owner_ctc_no' => 'lot_owner_ctc_no',
        'lot_owner_ctc_issued_date' => 'lot_owner_ctc_issued_date',
        'lot_owner_ctc_issued_place' => 'lot_owner_ctc_issued_place',
    ];

    protected $tct_fields = [
        'lot_number' => 'lot_number',
        'block_no' => 'block_no',        
        'tct_no' => 'tct_no',
        'tax_declaration_no' => 'tax_declaration_no',
        'construction_street' => 'construction_street',
        'construction_barangay' => 'construction_barangay',
        'construction_city' => 'construction_city'
    ];
    protected $user_account_fields = [
        'first_name',
        'last_name',
        'middle_name',
        'username',
        'position',
        'permit_type'

    ];
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'business_permit';
    }

    public function index($module, $option)
    {
        $json_url = '';
        $columns  = '';
        if($module == 'business_permit')
        {
            $json_url = url('/'.$module.'/'.$option.'/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'filed_date'],
                ['data' => 'issued_date'],
                ['data' => 'business_name'],
                ['data' => 'status'],
                ['data' => 'file_lock'],
            );
        }
        else if ($module == 'location_permit')
        {
            $json_url = url('/'.$module.'/'.$option.'/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'filed_date'],
                ['data' => 'issued_date'],
                ['data' => 'business_name'],
                ['data' => 'status'],
                ['data' => 'file_lock'],
            );   
        }
        else if ($module == 'building_permit')
        {
            $json_url = url('/'.$module.'/'.$option.'/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'filed_date'],
                ['data' => 'issued_date'],
                ['data' => 'applicant_name'],
                ['data' => 'status'],
            );   
        }
        else if ($module == 'file_setup')
        {
            $json_url = url('/'.$module.'/'.$option.'/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'name'],
                ['data' => 'permit_type'],
                ['data' => 'required'],
            );
        }
        else if ($module == 'system_config')
        {
            if($option == 'user_access')
            {
                $json_url = url('/'.$module.'/'.$option.'/datatables');
                $columns = array(
                    ['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'username'],
                    ['data' => 'position'],
                );
            }
            else
            {
                $json_url = url('/'.$module.'/'.$option.'/datatables');
                $columns = array(
                    ['data' => 'action', 'sortable' => false],
                    ['data' => 'name'],
                    ['data' => 'position'],
                    ['data' => 'permit_type'],
                );    
            }
            
        }
        $icon = 'mdi mdi-plus';
        if($module == 'css' || $module == 'axios')
        {
            return Redirect::to('/');
        }
    	$data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'json_url' => $json_url, 'columns' => $columns];
        return view($module.'.'.$option.'.index', $data);
    }

    public function create($module, $option)
    {   
        $array = array();
        if($module == 'location_permit')
        {
            $fields = $this->location_permit_fields;
            $files = $this->list_files(2);
            $array = ['files' => $files];
        }
        else if($module == 'business_permit')
        {
            $fields = $this->business_permit_fields;
            $files = $this->list_files(1);
            $array = ['files' => $files, 'fields' => $fields];

        }
        else if($module == 'building_permit')
        {
            $building_permit_fields = $this->building_permit_fields;
            $files = $this->list_files(3);
            $array = ['files' => $files, 'building_permit_fields' => $building_permit_fields, 'tct_fields' => $this->tct_fields];
        }
        else if($module == 'system_config')
        {
            $fields = $this->user_account_fields;
            $files = '';
            $array = ['files' => $files, 'fields' => $fields];
        }
        else if($module == 'system_config')
        {
            $fields = $this->user_account_fields;
            $files = '';
            $array = ['files' => $files, 'fields' => $fields];
        }
        else
        {
            $fields = '';
        }
        $icon = 'mdi mdi-plus';
        $application_url  = '';
        $file = $module.'.'.$option.'.add';
        $data = ['module' => $module, 'option' => $option, 'file' => $file, 'icon' => $icon, 'application_url' => $application_url, 'array' => $array, 'fields' => $fields];
        return view($module.'.'.$option.'.create', $data);
    }

    public function edit($module, $id)
    {   
        $array = array();
        if($module == 'building_permit')
        {
            $fields = $this->building_permit_fields;
            $array = ['id' => $id, 'tct_fields' => $this->tct_fields, 'building_permit_fields' => $fields];
            $folder = 'building_permit_application_form';
            $option = $folder;

        }
        else if($module == 'user_access')
        {
            $module = 'system_config';
            $folder = 'user_access';
            $fields = '';
            $option = $folder;
            $array  = ['id' => $id, 'info' => $this->get_record('users', $id)];
        }
        else
        {
            $fields = '';
        }
        $icon = 'mdi mdi-edit';
        $application_url  = '';
        $file = $module.'.'.$folder.'.add';
        $data = ['module' => $module, 'option' => $option, 'file' => $file, 'icon' => $icon, 'application_url' => $application_url, 'fields' => $fields, 'array' => $array, 'id' => $id];
        return view($module.'.'.$option.'.edit', $data);
    }

    /*public function view_application($module, $option, $id)
    {
        if($module == 'location_permit')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->location_permit_fields;
            $files      = $this->list_files(2);
            $attachment_data = $this->get_attachments($user_id, $id, 2);
            $array = ['files' => $files, 'id' => $id, 'attachments' => $attachment_data];
        }
        else if($module == 'business_permit')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->business_permit_fields;
            $files      = $this->list_files(1);
            $attachment_data = $this->get_attachments($user_id, $id, 1);
            $array = ['files' => $files, 'id' => $id, 'attachments' => $attachment_data];
        }
        else if($module == 'building_permit')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->building_permit_fields;
            $files      = $this->list_files(3);
            $attachment_data = $this->get_attachments($user_id, $id, 3);
            $array = ['files' => $files, 'id' => $id, 'attachments' => $attachment_data];
        }
        else
        {
            $fields = '';
        }
        $icon = 'mdi mdi-plus';
        $application_url  = '';
        $file = $module.'.'.$option.'.view';
        $data = ['module' => $module, 'option' => $option, 'file' => $file, 'icon' => $icon, 'application_url' => $application_url, 'fields' => $fields, 'array' => $array];
        return view($module.'.'.$option.'.create', $data);
    }*/

    public function view_attachment($module, $option, $id)
    {
        
        if($module == 'location_permit')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->location_permit_fields;
            $files      = $this->list_files(2);
            $attachment_data = $this->get_attachments($id, 2);
            $array = ['files' => $files, 'id' => $id, 'attachments' => $attachment_data];
        }
        else if($module == 'business_permit')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->business_permit_fields;
            $files      = $this->list_files(1);
            $attachment_data = $this->get_attachments($id, 1);
            $array = ['files' => $files, 'id' => $id, 'attachments' => $attachment_data];
        }
        else if($module == 'building_permit')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->building_permit_fields;
            $files      = $this->list_files(3);
            $attachment_data = $this->get_attachments($id, 3);
            $array = ['files' => $files, 'id' => $id, 'attachments' => $attachment_data, 'pre_eval' => $this->get_record('building_permit', $id), 'tct_informations' => $this->get_tct_informations($id), 'tct_fields' => $this->tct_fields, 'building_permit_fields' => $this->building_permit_fields];
            

        }
        else if($module == 'pre_evaluation')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->building_permit_fields;
            $module = 'building_permit';
            $option = 'pre_evaluation';
            $array = ['id' => $id, 'tct_informations' => $this->get_tct_informations($id), 'record' => $this->get_record('pre_evaluation_building_permit', $id)];
        }   
        else
        {
            $fields = '';
        }
        $file = $module.'.'.$option.'.view_form';

        $icon = 'mdi mdi-plus';
        $application_url  = '';
        
        $data = ['module' => $module, 'option' => $option, 'file' => $file, 'icon' => $icon, 'application_url' => $application_url, 'fields' => $fields, 'array' => $array];

        return view($module.'.'.$option.'.view', $data);
    }

    public function datatables($module, $option)
    {  

        $user_id    = 0;
        $evaluator  = 0;
        $approver   = 0;
        $issuer     = 0;

        if(Auth::user()->level == 1)
        {
            //client
            $user_id    = Auth::user()->id;
            $position   = Auth::user()->position;
        }
        else
        {
            //admins
            $position   = Auth::user()->position;
        }


        if ($module == 'business_permit')
        {
            $data = $this->list_business_permits($user_id, $evaluator, $approver, $issuer);
        }
        else if($module == 'location_permit')
        {
            $data = $this->list_location_permits($user_id, $evaluator, $approver, $issuer);
        }
        else if($module == 'building_permit')
        {
            $arr = array();
            if($position)
            {
                if(str_contains($position, '|'))   
                {
                    $position_arr = explode('|', $position);
                    /*
                        1. On process.
                        2. For Assessment.
                        3. Recommendation for Evaluation
                        4. For Evaluation (Evaluator & Licensing)
                        5. For Recommendation.
                        *. For Supplemental Docs
                        6. For Reviewing.
                        7. For Approval.
                        8. For Payment.
                        9. For Issuance.
                        10. Issued.
                        
                    */
                    //value = access control
                    //$arr = status
                    
                    foreach($position_arr as $value)
                    {
                        switch ($value) {
                            case '7':
                                array_push($arr, 2);
                                break;
                            case '8':
                                array_push($arr, 3);
                                break;
                            case '9':
                                array_push($arr, 4);
                                break;
                            case '10'://payment personnel
                                array_push($arr, 7);
                                break;
                            case '11':
                                array_push($arr, 5);
                                array_push($arr, 6);
                                break;
                            case '12':
                                array_push($arr, 8);
                                break;
                            case '13':
                                array_push($arr, 9);
                                array_push($arr, 10);
                                break;
                            /*case '14':
                                array_push($arr, 6);
                                break;
                            case '15':
                                array_push($arr, 7);
                                break;
                            case '16':
                                array_push($arr, 10);
                                break;*/
                            
                            default:
                                
                                break;
                        }
                    }
                }
            }
            //$data = $this->list_pre_evaluation_by($arr);
            $data = $this->list_building_permits($user_id, $arr);
        }
        else if($module == 'file_setup')
        {
            $data = $this->list_files();
        }
        else if ($module == 'system_config')
        {
            $data = $this->list_employees();
        }
        else if ($module == 'pre_evaluation')
        {
            $arr = array();
            if($position)
            {
                if(str_contains($position, '|'))   
                {
                    $position_arr = explode('|', $position);
                    foreach($position_arr as $value)
                    {
                        switch ($value) {
                            case '1':
                                array_push($arr, 2);
                                break;
                            case '2':
                                array_push($arr, 3);
                                break;
                            case '3':
                                array_push($arr, 2);
                                array_push($arr, 3);
                                array_push($arr, 4);
                                break;
                            case '4':
                                array_push($arr, 4);
                                break;
                            case '5':
                                array_push($arr, 5);
                                break;
                            case '6':
                                array_push($arr, 6);
                                break;
                            
                            default:
                                
                                break;
                        }
                    }
                }
            }
            if(Auth::user()->viewer == 1)
            {
                array_push($arr, 2);
                array_push($arr, 3);
            }
            $data = $this->list_pre_evaluation_by($arr);
        }
        else
        {
            $data = array();
        }


        if(in_array($option, ['location_permit_attachment', 'business_permit_attachment', 'building_permit_attachment']))
        {
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($module, $option) {

                if($data->file_lock)
                {
                    return '<div class="tools">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <a href="'.url('/'.$module.'/'.$option.'/'.$data->id.'/view-attachment').'" class="dropdown-item">
                                <i class="icon icon-left mdi mdi-eye"></i>View Application
                            </a>
                        </div>
                    </div>';
                }
                else
                {
                    return '<div class="tools">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <a href="'.url($module.'/'.$option.'/'.$data->id.'/view-attachment').'" class="dropdown-item">
                                <i class="icon icon-left mdi mdi-eye"></i>View Application
                            </a>
                        </div>
                    </div>';
                }
                
            });
        }
        else
        {
            if($module != 'system_config')
            {

                $datatables = Datatables::of($data)
                ->addColumn('action', function($data) use ($module, $option) {
                    $str = '<div class="tools">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">                          
                            
                    ';
                    if($module != 'file_setup' && $module != 'pre_evaluation')
                    {
                        if($data->file_lock != 1)
                        {
                            $str .= '
                                <a href="'.url('/'.$module.'/'.$data->id.'/edit').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-edit"></i>Edit
                                </a>
                                <span class="dropdown-item" onclick="delete_record(\''.$module.'\','.$data->id.')">
                                    <i class="icon icon-left mdi mdi-delete"></i>Remove
                                </span>
                            ';
                        }
                        $str .= '
                            <span class="dropdown-item" onclick="print_form(\''.$module.'\','.$data->id.')">
                                <i class="icon icon-left mdi mdi-print"></i>Print Form
                            </span>
                        ';
                        $upload_suplemental_2 = '<a href="'.url('building_permit/building_permit_application/'.$data->id.'/add-suplemental-proper').'" class="dropdown-item">
                            <i class="icon icon-left mdi mdi-upload"></i>Upload Supplemental
                        </a>';
                        if($data->fsec_request == 1)
                        {
                            /*$str .= '
                                <span class="dropdown-item" onclick="attach_fsec(\''.$module.'\','.$data->id.')">
                                    <i class="icon icon-left mdi mdi-attachment"></i>Attach FSEC
                                </span>
                            ';*/
                        }
                        if($data->status == 1)
                        {
                            $str .= '
                            <span class="dropdown-item" onclick="to_assessor(\''.$module.'\','.$data->id.')">
                                <i class="icon icon-left mdi mdi-skip-next"></i>Submit for Assessment
                            </span>
                            <span class="dropdown-item" onclick="window.open(\''.url('building_permit/building_permit_attachment/'.$data->id.'/view-attachment').'\')">
                                <i class="icon icon-left mdi mdi-upload"></i>Upload Attachments
                            </span>
                            ';
                        }
                        else if ($data->status == 10)
                        {
                            $str .= '
                            <span class="dropdown-item" onclick="show_building_permit('.$data->id.')">
                                <i class="icon icon-left mdi mdi-file"></i>Show Building Permit
                            </span>
                            ';
                        }
                        else if ($data->status == 12)
                        {
                            $str .= '<span class="dropdown-item" onclick="to_assessor(\''.$module.'\','.$data->id.')">
                                <i class="icon icon-left mdi mdi-skip-next"></i>Submit for Assessment
                            </span>';
                            $str .= $upload_suplemental_2;
                        }
                        else if ($data->status == 14)
                        {
                            $str .= '<span class="dropdown-item" onclick="to_assessor(\''.$module.'\','.$data->id.')">
                                <i class="icon icon-left mdi mdi-skip-next"></i>Submit for Assessment
                            </span>';
                            $str .= $upload_suplemental_2;
                        }
                        if(Auth::user()->level != 1)
                        {
                            if($data->status != 1)
                            {
                                $str .= '
                                    <a href="'.url($module.'/'.$option.'/'.$data->id.'/view-attachment').'" class="dropdown-item">
                                        <i class="icon icon-left mdi mdi-eye"></i>View Application
                                    </a>
                                ';
                            }    
                        }
                        
                    }
                    else
                    {
                        $str .= '
                                <a href="'.url($module.'/'.$option.'/'.$data->id.'/view-attachment').'" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-eye"></i>View Application
                                </a>
                            ';
                    }
                    
                    $str .= '
                        </div>
                    </div>';

                    return $str;
                    
                });
            }
            else
            {

                $datatables = Datatables::of($data)
                ->addColumn('action', function($data) use ($module, $option) {

                    return '<div class="tools">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <a href="javascript:void(0);" class="dropdown-item" onclick="reset_pw('.$data->id.');"><i class="icon icon-left mdi mdi-8tracks"></i>Reset Password</a>
                            <a href="'.url($option.'/'.$data->id.'/edit').'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>'.trans('page.edit').'</a>
                        </div>
                    </div>';
                });

                $datatables
                ->editColumn('position', function($data){
                    /*if ($data->position == 0)
                    {
                        $data->position = 'Administrator';
                    }
                    elseif ($data->position == 1)
                    {
                        $data->position = 'Applicant';
                    }
                    elseif ($data->position == 2)
                    {
                        $data->position = 'Assessor';
                    }
                    elseif ($data->position == 3)
                    {
                        $data->position = 'Evaluator';
                    }
                    elseif ($data->position == 4)
                    {
                        $data->position = 'For Recommendation';
                    }
                    elseif ($data->position == 5)
                    {
                        $data->position = 'Approver';
                    }*/
                    return $data->position;
                });

                /*$datatables
                ->editColumn('permit_type', function($data){
                    if ($data->permit_type == 1)
                    {
                        $data->permit_type = 'Business Permit';
                    }
                    elseif ($data->permit_type == 2)
                    {
                        $data->permit_type = 'Location Permit';
                    }
                    elseif ($data->permit_type == 3)
                    {
                        $data->permit_type = 'Building Permit';
                    }
                    elseif ($data->permit_type == 4)
                    {
                        $data->permit_type = 'Occupancy Permit';
                    }
                    return $data->permit_type;
                });*/
            }
            
        }
        if($module != 'system_config')
        {
            if($module != 'file_setup')
            {
                if($module == 'pre_evaluation')
                {
                    $datatables
                    ->editColumn('status', function($data){
                        if ($data->status == 1)
                        {
                            $data->status = 'For Submission';
                            //On Process
                        }
                        elseif ($data->status == 2)
                        {
                            $data->status = 'For Assessment';
                        }
                       elseif ($data->status == 3)
                        {
                            $data->status = 'On Going Pre-Evaluation';
                        }
                        elseif ($data->status == 4)
                        {
                            $data->status = 'For Building Permit Application';
                        }
                        elseif ($data->status == 5)
                        {
                            $data->status = 'For Review of Supplemental Initial Plan Evaluation Report';
                        }
                        elseif ($data->status == 6)
                        {
                            $data->status = 'Supplemental Initial Plan Evaluation Report to Client';
                        }
                        elseif ($data->status == 7)
                        {
                            $data->status = 'For Building Permit Application';
                        }
                        elseif ($data->status == 8)
                        {
                            $data->status = 'Incomplete Requirements';
                        }
                        elseif ($data->status == 9)
                        {
                            $data->status = 'For Submission of Supplemental Requirements';
                        }
                        return $data->status;


                    });
                }
                else
                {
                    $datatables
                    ->editColumn('status', function($data){
                        if ($data->status == 1)
                        {
                            $data->status = 'For Submission';
                            //On Process
                        }
                        elseif ($data->status == 2)
                        {
                            $data->status = 'For Assessment';
                        }
                        elseif ($data->status == 3)
                        {
                            //$data->status = 'For Evaluation';
                            $data->status = 'On Going Evaluation';
                            //Recommendation for Evaluation
                        }
                        elseif ($data->status == 4)
                        {
                            $data->status = 'For FSEC Request';
                            //For Endorsement to BFP
                            //For Evaluation
                        }
                        elseif ($data->status == 5)
                        {
                            $data->status = 'Application Endorsed to OBO';
                        }
                        elseif ($data->status == 6)
                        {
                            $data->status = 'For Payment Approval';
                        }
                        elseif ($data->status == 7)
                        {
                            $data->status = 'Payment Request';
                        }
                        elseif ($data->status == 8)
                        {
                            $data->status = 'Payment Accepted';
                        }
                        elseif ($data->status == 9)
                        {
                            $data->status = 'For Issuance';
                        }
                        elseif ($data->status == 10)
                        {
                            $data->status = 'Issued Building Permit';
                        }
                        elseif ($data->status == 11)
                        {
                            $data->status = $data->permit_no;
                        }
                        elseif ($data->status == 12)
                        {
                            $data->status = 'FPER Transmitted to Client - For Appropriate Action';
                        }
                        elseif ($data->status == 13)
                        {
                            $data->status = $data->permit_no;
                        }
                        elseif ($data->status == 14)
                        {
                            $data->status = 'Incomplete Requirements';
                        }
                        return $data->status;
                    });
                    /*
                    1. On process.
                    2. For Assessment.
                    3. For Evaluation
                    4. For Recommendation.
                    5. For Approval.
                    6. For Payment.
                    7. For Issuance.
                    8. Issued.
                    */
                }
                

                $datatables
                ->editColumn('file_lock', function($data){
                    if ($data->file_lock == 1)
                    {
                        $data->file_lock = 'YES';
                    }
                    else
                    {
                        $data->file_lock = 'NO';
                    }
                    
                    return $data->file_lock;
                });
            }
            else
            {
                /*$datatables
                ->editColumn('permit_type', function($data){
                    if ($data->permit_type == 1)
                    {
                        $data->permit_type = 'Business Permit';
                    }
                    elseif ($data->permit_type == 2)
                    {
                        $data->permit_type = 'Location Permit';
                    }
                    elseif ($data->permit_type == 3)
                    {
                        $data->permit_type = 'Building Permit';
                    }
                    elseif ($data->permit_type == 4)
                    {
                        $data->permit_type = 'Occupancy Permit';
                    }
                    return $data->permit_type;
                });*/

                $datatables
                ->editColumn('required', function($data){
                    if ($data->required == 1)
                    {
                        $data->required = 'YES';
                    }
                    else
                    {
                        $data->required = 'NO';
                    }
                    
                    return $data->required;
                });
            }
        }
        if($module == 'pre_evaluation')
        {
            $datatables
            ->editColumn('application_type', function($data){
                if($data->application_type == 0)
                {
                    $data->application_type = '';
                }
                else if ($data->application_type == 1)
                {
                    $data->application_type = 'New';
                }
                else if ($data->application_type == 2)
                {
                    $data->application_type = 'Renewal';
                }
                else if ($data->application_type == 3)
                {
                    $data->application_type = 'Amendatory';
                }
                return $data->application_type;
            });
        }

        if($module == 'building_permit')
        {
            /*$datatables
            ->editColumn('permit_type', function($data){
                if ($data->permit_type == 1)
                {
                    $data->permit_type = 'Architectural Permit';
                }
                else if ($data->permit_type == 2)
                {
                    $data->permit_type = 'Civil / Structural Permit';
                }
                else if ($data->permit_type == 3)
                {
                    $data->permit_type = 'Electrical Permit';
                }
                else if ($data->permit_type == 4)
                {
                    $data->permit_type = 'Mechanical Permit';
                }
                else if ($data->permit_type == 5)
                {
                    $data->permit_type = 'Electronics Permit';
                }
                else if ($data->permit_type == 6)
                {
                    $data->permit_type = 'Sanitary / Plumbing';
                }
                
                return $data->permit_type;
            });*/
        }

        if($module == 'file_setup')
        {
            $datatables
            ->editColumn('permit_type', function($data){
                if ($data->permit_type == 1)
                {
                    $data->permit_type = 'Business Permit';
                }
                else if ($data->permit_type == 2)
                {
                    $data->permit_type = 'Location Permit';
                }
                else if ($data->permit_type == 3)
                {
                    $data->permit_type = 'Building Permit';
                }
                else if ($data->permit_type == 4)
                {
                    $data->permit_type = 'Occupancy Permit';
                }
                else if ($data->permit_type == 5)
                {
                    $data->permit_type = 'Pre Evaluation';
                }
                
                
                return $data->permit_type;
            });
        }
        
        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }
    public function update(Request $request,$module, $option)
    {
        try {
            if($module == 'system_config')
            {
                $user_access = $request->get('user_access');
                $id = $request->get('id');

                $update_data = [
                    'username' => $request->get('username'),
                    'position' => $user_access,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
                DB::beginTransaction();
                DB::table('users')
                ->where('id', '=', $id)
                ->update($update_data);
                DB::commit();
            }
        } catch (Exception $e) {
            
        }
    }
    public function store(Request $request,$module, $option)
    {
        try {
            if($module == 'file_setup')
            {
                $insert_data = [
                    'code' => $request->get('code'),
                    'name' => $request->get('name'),
                    'permit_type' => $request->get('permit_type'),
                    'required' => $request->get('required'),
                    'remarks' => $request->get('remarks'),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                $table = 'files';
            }
            else if ($module == 'system_config')
            {
                $level = 0;
                if($request->get('position') != 0) $level = '2';
                $insert_data = [
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'middle_name' => $request->get('middle_name'),
                    'name' => $request->get('first_name').' '.$request->get('last_name'),
                    'permit_type' => $request->get('permit_type'),
                    'position' => $request->get('position'),
                    'password' => bcrypt(123456),
                    'user_status' => 1,
                    'level' => $level,
                    'username' => $request->get('username'),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                $table = 'users';
            }
            else
            {
                $user_id    = Auth::user()->id;
                $filed_date = date('Y-m-d H:i:s',time());
                $insert_data = [
                    'user_id' => $user_id,
                    'filed_date' => $filed_date,
                    'created_by' => Auth::user()->id,
                    'status' => 1,
                    'created_at' => DB::raw('now()')
                ];

                $update_data = [
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];


                if($module == 'location_permit')
                {
                    $table = 'location_permit';   
                    foreach ($this->location_permit_fields as $key => $value) {
                        $insert_data[$value] = $request->get($value);
                    }
                }
                else if($module == 'business_permit')
                {
                    $table = 'business_permit';   
                    foreach ($this->business_permit_fields as $key => $value) {
                        $insert_data[$value] = $request->get($value);
                    }
                }
                else if($module == 'building_permit')
                {
                    $table = 'building_permit';   
                    foreach ($this->building_permit_fields as $key => $value) {
                        $insert_data[$value] = $request->get($value);
                    }
                    //$insert_data['system_reference_no'] = 'BP'.time();
                    
                    if($request->get('id'))
                    {
                        foreach ($this->building_permit_fields as $key => $value) {
                            $update_data[$value] = $request->get($value);
                        }      
                    }
                }
                else if ($module == 'architectural_permit')
                {
                    $table                          = 'architectural_permit';
                    $conformance_str                = '';
                    $architectural_facilities_str   = '';
                    $conformance_object             = $request->get('conformance');
                    $architectural_facilities_object = $request->get('architectural_facilities');

                    $edit_building_permit = $request->get('edit_building_permit');

                    

                    if($conformance_object)
                    {
                        foreach ($conformance_object as $key => $value) {
                            $conformance_str .= $value.'|';
                        }
                    }

                    if($architectural_facilities_object)
                    {
                        foreach ($architectural_facilities_object as $key => $value) {
                            $architectural_facilities_str .= $value.'|';
                        }
                    }


                    $insert_data = [
                        'building_permit_id' => $request->get('building_permit_id'),
                        'architectural_facilities' => $architectural_facilities_str,
                        'other_architectural_facilities' => $request->get('other_architectural_facilities'),
                        'building_percentage' => $request->get('building_percentage'),
                        'impervious_surface_area' => $request->get('impervious_surface_area'),
                        'unpaved_surface_area' => $request->get('unpaved_surface_area'),
                        'others_sit_occupancy' => $request->get('others_sit_occupancy'),
                        'conformance' => $conformance_str,
                        'other_conformance' => $request->get('other_conformance'),
                        'civil_engineer' => $request->get('civil_engineer'),
                        'civil_engineer_address' => $request->get('civil_engineer_address'),
                        'civil_engineer_prc_no' => $request->get('civil_engineer_prc_no'),
                        'civil_engineer_prc_validity' => $request->get('civil_engineer_prc_validity'),
                        'civil_engineer_ptr_no' => $request->get('civil_engineer_ptr_no'),
                        'civil_engineer_ptr_date_issued' => $request->get('civil_engineer_ptr_date_issued'),
                        'civil_engineer_ptr_issued_at' => $request->get('civil_engineer_ptr_issued_at'),
                        'civil_engineer_tin' => $request->get('civil_engineer_tin'),
                        'architectural_supervisor' => $request->get('architectural_supervisor'),
                        'architectural_supervisor_address' => $request->get('architectural_supervisor_address'),
                        'architectural_supervisor_prc_no' => $request->get('architectural_supervisor_prc_no'),
                        'architectural_supervisor_prc_validity' => $request->get('architectural_supervisor_prc_validity'),
                        'architectural_supervisor_ptr_no' => $request->get('architectural_supervisor_ptr_no'),
                        'architectural_supervisor_ptr_date_issued' => $request->get('architectural_supervisor_ptr_date_issued'),
                        'architectural_supervisor_ptr_issued_at' => $request->get('architectural_supervisor_ptr_issued_at'),
                        'architectural_supervisor_tin' => $request->get('architectural_supervisor_tin'),
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];


                    $update_data = [
                        'architectural_facilities' => $architectural_facilities_str,
                        'other_architectural_facilities' => $request->get('other_architectural_facilities'),
                        'building_percentage' => $request->get('building_percentage'),
                        'impervious_surface_area' => $request->get('impervious_surface_area'),
                        'unpaved_surface_area' => $request->get('unpaved_surface_area'),
                        'others_sit_occupancy' => $request->get('others_sit_occupancy'),
                        'conformance' => $conformance_str,
                        'other_conformance' => $request->get('other_conformance'),
                        'civil_engineer' => $request->get('civil_engineer'),
                        'civil_engineer_address' => $request->get('civil_engineer_address'),
                        'civil_engineer_prc_no' => $request->get('civil_engineer_prc_no'),
                        'civil_engineer_prc_validity' => $request->get('civil_engineer_prc_validity'),
                        'civil_engineer_ptr_no' => $request->get('civil_engineer_ptr_no'),
                        'civil_engineer_ptr_date_issued' => $request->get('civil_engineer_ptr_date_issued'),
                        'civil_engineer_ptr_issued_at' => $request->get('civil_engineer_ptr_issued_at'),
                        'civil_engineer_tin' => $request->get('civil_engineer_tin'),
                        'architectural_supervisor' => $request->get('architectural_supervisor'),
                        'architectural_supervisor_address' => $request->get('architectural_supervisor_address'),
                        'architectural_supervisor_prc_no' => $request->get('architectural_supervisor_prc_no'),
                        'architectural_supervisor_prc_validity' => $request->get('architectural_supervisor_prc_validity'),
                        'architectural_supervisor_ptr_no' => $request->get('architectural_supervisor_ptr_no'),
                        'architectural_supervisor_ptr_date_issued' => $request->get('architectural_supervisor_ptr_date_issued'),
                        'architectural_supervisor_ptr_issued_at' => $request->get('architectural_supervisor_ptr_issued_at'),
                        'architectural_supervisor_tin' => $request->get('architectural_supervisor_tin'),
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];
                    
                }

                else if ($module == 'structural_permit')
                {
                    $table                          = 'structural_permit';
                    $structural_works_str           = '';
                    $structural_works_object        = $request->get('structural_works');

                    if($structural_works_object)
                    {
                        foreach ($structural_works_object as $key => $value) {
                            $structural_works_str .= $value.'|';
                        }
                    }

                    $insert_data = [
                        'building_permit_id' => $request->get('building_permit_id'),
                        'structural_works' => $structural_works_str,
                        'civil_engineer_name' => $request->get('civil_engineer_name'),
                        'civil_engineer_address' => $request->get('civil_engineer_address'),
                        'civil_engineer_prc_no' => $request->get('civil_engineer_prc_no'),
                        'civil_engineer_prc_validity' => $request->get('civil_engineer_prc_validity'),
                        'civil_engineer_ptr_no' => $request->get('civil_engineer_ptr_no'),
                        'civil_engineer_ptr_date_issued' => $request->get('civil_engineer_ptr_date_issued'),
                        'civil_engineer_ptr_issued_at' => $request->get('civil_engineer_ptr_issued_at'),
                        'civil_engineer_tin' => $request->get('civil_engineer_tin'),
                        'civil_supervisor' => $request->get('civil_supervisor'),
                        'civil_supervisor_address' => $request->get('civil_supervisor_address'),
                        'civil_supervisor_prc_no' => $request->get('civil_supervisor_prc_no'),
                        'civil_supervisor_prc_validity' => $request->get('civil_supervisor_prc_validity'),
                        'civil_supervisor_ptr_no' => $request->get('civil_supervisor_ptr_no'),
                        'civil_supervisor_ptr_date_issued' => $request->get('civil_supervisor_ptr_date_issued'),
                        'civil_supervisor_ptr_issued_at' => $request->get('civil_supervisor_ptr_issued_at'),
                        'civil_supervisor_tin' => $request->get('civil_supervisor_tin'),
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];

                    $update_data = [
                        'structural_works' => $structural_works_str,
                        'civil_engineer_name' => $request->get('civil_engineer_name'),
                        'civil_engineer_address' => $request->get('civil_engineer_address'),
                        'civil_engineer_prc_no' => $request->get('civil_engineer_prc_no'),
                        'civil_engineer_prc_validity' => $request->get('civil_engineer_prc_validity'),
                        'civil_engineer_ptr_no' => $request->get('civil_engineer_ptr_no'),
                        'civil_engineer_ptr_date_issued' => $request->get('civil_engineer_ptr_date_issued'),
                        'civil_engineer_ptr_issued_at' => $request->get('civil_engineer_ptr_issued_at'),
                        'civil_engineer_tin' => $request->get('civil_engineer_tin'),
                        'civil_supervisor' => $request->get('civil_supervisor'),
                        'civil_supervisor_address' => $request->get('civil_supervisor_address'),
                        'civil_supervisor_prc_no' => $request->get('civil_supervisor_prc_no'),
                        'civil_supervisor_prc_validity' => $request->get('civil_supervisor_prc_validity'),
                        'civil_supervisor_ptr_no' => $request->get('civil_supervisor_ptr_no'),
                        'civil_supervisor_ptr_date_issued' => $request->get('civil_supervisor_ptr_date_issued'),
                        'civil_supervisor_ptr_issued_at' => $request->get('civil_supervisor_ptr_issued_at'),
                        'civil_supervisor_tin' => $request->get('civil_supervisor_tin'),
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];
                }
                else if ($module == 'electrical_permit')
                {
                    $table                          = 'electrical_permit';
                    $electrical_application_str           = '';
                    $electrical_application_object        = $request->get('electrical_application');

                    if($electrical_application_object)
                    {
                        foreach ($electrical_application_object as $key => $value) {
                            $electrical_application_str .= $value.'|';
                        }
                    }

                    $insert_data = [
                        'building_permit_id' => $request->get('building_permit_id'),
                        'electrical_application' => $electrical_application_str,
                        'other_electrical_application' => $request->get('other_electrical_application'),
                        'total_connection_load' => $request->get('total_connection_load'),
                        'total_transformer_capacity' => $request->get('total_transformer_capacity'),
                        'total_generator_capacity' => $request->get('total_generator_capacity'),
                        'electrical_engineer' => $request->get('electrical_engineer'),
                        'electrical_engineer_address' => $request->get('electrical_engineer_address'),
                        'electrical_engineer_prc_no' => $request->get('electrical_engineer_prc_no'),
                        'electrical_engineer_prc_validity' => $request->get('electrical_engineer_prc_validity'),
                        'electrical_engineer_ptr_no' => $request->get('electrical_engineer_ptr_no'),
                        'electrical_engineer_ptr_date_issued' => $request->get('electrical_engineer_ptr_date_issued'),
                        'electrical_engineer_ptr_issued_at' => $request->get('electrical_engineer_ptr_issued_at'),
                        'electrical_engineer_tin' => $request->get('electrical_engineer_tin'),
                        'supervisor_classification' => $request->get('supervisor_classification'),
                        'electrical_supervisor' => $request->get('electrical_supervisor'),
                        'electrical_supervisor_address' => $request->get('electrical_supervisor_address'),
                        'electrical_supervisor_prc_no' => $request->get('electrical_supervisor_prc_no'),
                        'electrical_supervisor_prc_validity' => $request->get('electrical_supervisor_prc_validity'),
                        'electrical_supervisor_ptr_no' => $request->get('electrical_supervisor_ptr_no'),
                        'electrical_supervisor_ptr_date_issued' => $request->get('electrical_supervisor_ptr_date_issued'),
                        'electrical_supervisor_ptr_issued_at' => $request->get('electrical_supervisor_ptr_issued_at'),
                        'electrical_supervisor_tin' => $request->get('electrical_supervisor_tin'),
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];

                    $update_data = [
                        'electrical_application' => $electrical_application_str,
                        'other_electrical_application' => $request->get('other_electrical_application'),
                        'total_connection_load' => $request->get('total_connection_load'),
                        'total_transformer_capacity' => $request->get('total_transformer_capacity'),
                        'total_generator_capacity' => $request->get('total_generator_capacity'),
                        'electrical_engineer' => $request->get('electrical_engineer'),
                        'electrical_engineer_address' => $request->get('electrical_engineer_address'),
                        'electrical_engineer_prc_no' => $request->get('electrical_engineer_prc_no'),
                        'electrical_engineer_prc_validity' => $request->get('electrical_engineer_prc_validity'),
                        'electrical_engineer_ptr_no' => $request->get('electrical_engineer_ptr_no'),
                        'electrical_engineer_ptr_date_issued' => $request->get('electrical_engineer_ptr_date_issued'),
                        'electrical_engineer_ptr_issued_at' => $request->get('electrical_engineer_ptr_issued_at'),
                        'electrical_engineer_tin' => $request->get('electrical_engineer_tin'),
                        'supervisor_classification' => $request->get('supervisor_classification'),
                        'electrical_supervisor' => $request->get('electrical_supervisor'),
                        'electrical_supervisor_address' => $request->get('electrical_supervisor_address'),
                        'electrical_supervisor_prc_no' => $request->get('electrical_supervisor_prc_no'),
                        'electrical_supervisor_prc_validity' => $request->get('electrical_supervisor_prc_validity'),
                        'electrical_supervisor_ptr_no' => $request->get('electrical_supervisor_ptr_no'),
                        'electrical_supervisor_ptr_date_issued' => $request->get('electrical_supervisor_ptr_date_issued'),
                        'electrical_supervisor_ptr_issued_at' => $request->get('electrical_supervisor_ptr_issued_at'),
                        'electrical_supervisor_tin' => $request->get('electrical_supervisor_tin'),
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];
                }
                else if ($module == 'sanitary_permit')
                {
                    $table                          = 'sanitary_permit';
                    $sanitary_application_str           = '';
                    $sanitary_application_object        = $request->get('sanitary_application');

                    if($sanitary_application_object)
                    {
                        foreach ($sanitary_application_object as $key => $value) {
                            $sanitary_application_str .= $value.'|';
                        }
                    }

                    $insert_data = [
                        'building_permit_id' => $request->get('building_permit_id'),
                        'sanitary_application' => $sanitary_application_str,
                        'other_sanitary_application' => $request->get('other_sanitary_application'),
                        'sanitary_engineer' => $request->get('sanitary_engineer'),
                        'sanitary_engineer_address' => $request->get('sanitary_engineer_address'),
                        'sanitary_engineer_prc_no' => $request->get('sanitary_engineer_prc_no'),
                        'sanitary_engineer_prc_validity' => $request->get('sanitary_engineer_prc_validity'),
                        'sanitary_engineer_ptr_no' => $request->get('sanitary_engineer_ptr_no'),
                        'sanitary_engineer_ptr_date_issued' => $request->get('sanitary_engineer_ptr_date_issued'),
                        'sanitary_engineer_ptr_issued_at' => $request->get('sanitary_engineer_ptr_issued_at'),
                        'sanitary_engineer_tin' => $request->get('sanitary_engineer_tin'),
                        'sanitary_supervisor' => $request->get('sanitary_supervisor'),
                        'sanitary_supervisor_address' => $request->get('sanitary_supervisor_address'),
                        'sanitary_supervisor_prc_no' => $request->get('sanitary_supervisor_prc_no'),
                        'sanitary_supervisor_prc_validity' => $request->get('sanitary_supervisor_prc_validity'),
                        'sanitary_supervisor_ptr_no' => $request->get('sanitary_supervisor_ptr_no'),
                        'sanitary_supervisor_ptr_date_issued' => $request->get('sanitary_supervisor_ptr_date_issued'),
                        'sanitary_supervisor_ptr_issued_at' => $request->get('sanitary_supervisor_ptr_issued_at'),
                        'sanitary_supervisor_tin' => $request->get('sanitary_supervisor_tin'),
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];

                    $update_data = [
                        'sanitary_application' => $sanitary_application_str,
                        'other_sanitary_application' => $request->get('other_sanitary_application'),
                        'sanitary_engineer' => $request->get('sanitary_engineer'),
                        'sanitary_engineer_address' => $request->get('sanitary_engineer_address'),
                        'sanitary_engineer_prc_no' => $request->get('sanitary_engineer_prc_no'),
                        'sanitary_engineer_prc_validity' => $request->get('sanitary_engineer_prc_validity'),
                        'sanitary_engineer_ptr_no' => $request->get('sanitary_engineer_ptr_no'),
                        'sanitary_engineer_ptr_date_issued' => $request->get('sanitary_engineer_ptr_date_issued'),
                        'sanitary_engineer_ptr_issued_at' => $request->get('sanitary_engineer_ptr_issued_at'),
                        'sanitary_engineer_tin' => $request->get('sanitary_engineer_tin'),
                        'sanitary_supervisor' => $request->get('sanitary_supervisor'),
                        'sanitary_supervisor_address' => $request->get('sanitary_supervisor_address'),
                        'sanitary_supervisor_prc_no' => $request->get('sanitary_supervisor_prc_no'),
                        'sanitary_supervisor_prc_validity' => $request->get('sanitary_supervisor_prc_validity'),
                        'sanitary_supervisor_ptr_no' => $request->get('sanitary_supervisor_ptr_no'),
                        'sanitary_supervisor_ptr_date_issued' => $request->get('sanitary_supervisor_ptr_date_issued'),
                        'sanitary_supervisor_ptr_issued_at' => $request->get('sanitary_supervisor_ptr_issued_at'),
                        'sanitary_supervisor_tin' => $request->get('sanitary_supervisor_tin'),
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];
                }
                else if ($module == 'mechanical_permit')
                {
                    $table                          = 'mechanical_permit';
                    $mechanical_application_str           = '';
                    $mechanical_application_object        = $request->get('mechanical_application');

                    if($mechanical_application_object)
                    {
                        foreach ($mechanical_application_object as $key => $value) {
                            $mechanical_application_str .= $value.'|';
                        }
                    }

                    $insert_data = [
                        'building_permit_id' => $request->get('building_permit_id'),
                        'mechanical_application' => $mechanical_application_str,
                        
                        'other_mechanical_application' => $request->get('other_mechanical_application'),
                        'mechanical_engineer' => $request->get('mechanical_engineer'),
                        'mechanical_engineer_address' => $request->get('mechanical_engineer_address'),
                        'mechanical_engineer_prc_no' => $request->get('mechanical_engineer_prc_no'),
                        'mechanical_engineer_prc_validity' => $request->get('mechanical_engineer_prc_validity'),
                        'mechanical_engineer_ptr_no' => $request->get('mechanical_engineer_ptr_no'),
                        'mechanical_engineer_ptr_issued_date' => $request->get('mechanical_engineer_ptr_issued_date'),
                        'mechanical_engineer_ptr_issued_at' => $request->get('mechanical_engineer_ptr_issued_at'),
                        'mechanical_engineer_tin' => $request->get('mechanical_engineer_tin'),
                        'mechanical_supervisor_classification' => $request->get('mechanical_supervisor_classification'),
                        'mechanical_supervisor' => $request->get('mechanical_supervisor'),
                        'mechanical_supervisor_address' => $request->get('mechanical_supervisor_address'),
                        'mechanical_supervisor_prc_no' => $request->get('mechanical_supervisor_prc_no'),
                        'mechanical_supervisor_prc_validity' => $request->get('mechanical_supervisor_prc_validity'),
                        'mechanical_supervisor_ptr_no' => $request->get('mechanical_supervisor_ptr_no'),
                        'mechanical_supervisor_ptr_issued_date' => $request->get('mechanical_supervisor_ptr_issued_date'),
                        'mechanical_supervisor_ptr_issued_at' => $request->get('mechanical_supervisor_ptr_issued_at'),
                        'mechanical_supervisor_tin' => $request->get('mechanical_supervisor_tin'),
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];

                    $update_data = [
                        'mechanical_application' => $mechanical_application_str,
                        
                        'other_mechanical_application' => $request->get('other_mechanical_application'),
                        'mechanical_engineer' => $request->get('mechanical_engineer'),
                        'mechanical_engineer_address' => $request->get('mechanical_engineer_address'),
                        'mechanical_engineer_prc_no' => $request->get('mechanical_engineer_prc_no'),
                        'mechanical_engineer_prc_validity' => $request->get('mechanical_engineer_prc_validity'),
                        'mechanical_engineer_ptr_no' => $request->get('mechanical_engineer_ptr_no'),
                        'mechanical_engineer_ptr_issued_date' => $request->get('mechanical_engineer_ptr_issued_date'),
                        'mechanical_engineer_ptr_issued_at' => $request->get('mechanical_engineer_ptr_issued_at'),
                        'mechanical_engineer_tin' => $request->get('mechanical_engineer_tin'),
                        'mechanical_supervisor_classification' => $request->get('mechanical_supervisor_classification'),
                        'mechanical_supervisor' => $request->get('mechanical_supervisor'),
                        'mechanical_supervisor_address' => $request->get('mechanical_supervisor_address'),
                        'mechanical_supervisor_prc_no' => $request->get('mechanical_supervisor_prc_no'),
                        'mechanical_supervisor_prc_validity' => $request->get('mechanical_supervisor_prc_validity'),
                        'mechanical_supervisor_ptr_no' => $request->get('mechanical_supervisor_ptr_no'),
                        'mechanical_supervisor_ptr_issued_date' => $request->get('mechanical_supervisor_ptr_issued_date'),
                        'mechanical_supervisor_ptr_issued_at' => $request->get('mechanical_supervisor_ptr_issued_at'),
                        'mechanical_supervisor_tin' => $request->get('mechanical_supervisor_tin'),
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];
                }
            }
            

            DB::beginTransaction();
            if($request->get('id'))
            {
                DB::table($table)
                ->where('id', '=', $request->get('id'))
                ->update($update_data);

                $id = $request->get('id');
            }
            else
            {
                DB::table($table)
                ->insert($insert_data); 

                $id = DB::getPdo()->lastInsertId();    

                if($module == 'building_permit')
                {
                    $tct_data = [
                        'building_permit_id' => $id,
                        'created_by' => Auth::user()->id,
                        'created_at' => DB::raw('now()')
                    ];
                    $tct_count = $request->get('tct_count');
                    for ($i=1; $i <= $tct_count; $i++) { 
                        foreach ($this->tct_fields as $key => $value) {
                            $tct_data[$value] = $request->get($value)[$i-1];
                        }

                        DB::table('tct_informations')
                        ->insert($tct_data); 
                    }

                    
                    
                }
            }
            

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        
        return response(['last_insert_id' => $id], 200);
    }

    public function store_remarks(Request $request, $module, $option)
    {
        try {
            $id         = $request->get('id');
            $remarks    = $request->get('remarks');
            $status     = $request->get('status');
            $info       = $this->get_user_info('building_permit', $id);
            $email      = $info->email;
            $data = [
                'title' => 'Application Progress Notification',
                'content' => 'Application Progress Report',
                'full_name' => $info->first_name.' '.$info->last_name,
                'email' => $info->email
            ]; 
            $record     = $this->get_record('building_permit', $id);

            $folder_path = public_path().'/tieza/building_permit';

            if($module == 'building_permit')
            {
                $table = 'building_permit';

                $insert = [
                    'permit_type' => 3,
                    'record_id' => $id,
                    'remarks' => $remarks,
                    'status' => $status,
                    'user_id' => Auth::user()->id,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                if($status == 3)
                {
                    $update = [
                        'endorse_to_evaluation' => DB::raw('now()'),
                        'status' => $status,//4
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];    

                    if($record->final_eval_report_date)
                    {
                        $data['fer_date'] = $record->final_eval_report_date;

                        Mail::send('email.approval_of_assessment_supplemental', $data, function($message) use ($email) {
                            $message->to($email, 'TEZ Email')
                                    ->subject('Confirmation Receipt of Supplemental Requirements for Building Permit Application');
                        }); 
                    }
                    else
                    {
                        $check_list = $folder_path.'/checklist.pdf';
                        $data['status'] = 'Evaluation';
                        Mail::send('email.approval_of_assessment', $data, function($message) use ($email, $check_list) {
                            $message->to($email, 'TEZ Email')
                                    ->subject('Confirmation Receipt of Building Permit Application');
                        }); 
                    }
                    

                }
                else if($status == 4)
                {
                    $update = [
                        'evaluated_date' => DB::raw('now()'),
                        'status' => $status,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];   
                    //$data['status'] = 'FSEC Request';
                    /*Mail::send('email.application_status', $data, function($message) use ($email) {
                        $message->to($email, 'TEZ Email')
                                ->subject('Passed for Evaluation.');
                    });  
                    */
                }
                else if($status == 5)
                {
                    $update = [
                        'recommended_date' => DB::raw('now()'),
                        'status' => $status,
                        'fsec_no' => $request->get('fsec_no'),
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];    
                    /*$data['status'] = 'Endorsement to OBO';
                    Mail::send('email.application_status', $data, function($message) use ($email) {
                        $message->to($email, 'TEZ Email')
                                ->subject('Application is for Endorsement to OBO');
                    });*/ 
                    
                }
                else if($status == 6)
                {
                    
                    $update = [
                        'amount' => $request->get('amount'),
                        'reviewed_date' => DB::raw('now()'),
                        'status' => $status,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];    
                    /*$data['status'] = 'Reviewed';
                    Mail::send('email.application_status', $data, function($message) use ($email) {
                        $message->to($email, 'TEZ Email')
                                ->subject('Moved to Approval.');
                    }); */
                }
                else if($status == 7)
                {
                    $update = [
                        'system_reference_no' => 'TOPS'.date('mdY', time()).''.$id,
                        'approved_date' => DB::raw('now()'),
                        'status' => $status,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ];    
                    $data['status'] = 'Ready for Payment';
                    $record = $this->get_record('building_permit', $id);
                    $data['app_amount'] = $record->amount;
                    $data['amount_in_words'] = $this->number_to_words($record->amount);
                    $data['payment_link'] = 'https://stg-sanvic-payment.tieza.gov.ph/public/'.$id.'/make-payment';
                    $atap = $folder_path.'/'.$record->id.'/'.$record->atap;
                    Mail::send('email.request_for_payment', $data, function($message) use ($email, $atap) {
                        $message->to($email, 'TEZ Email')
                                ->subject('Payment Request');
                        //$message->attach($atap, ['as' => 'ATAP']);
                    });
                }
                else if($status == 9)
                {
                    $update = [
                        'issued_date' => DB::raw('now()'),
                        'status' => $status,
                        'permit_no' => 'BP2402'.str_pad($id,4,".",STR_PAD_LEFT),//last 4 digits sequence
                        'application_no' => '202402'.str_pad($id,4,".",STR_PAD_LEFT),
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ]; 
                    $data['status'] = 'issue permit';  
                    $subject = 'Approved Building Permit - '.$record->enterprise_name;
                    Mail::send('email.issuance_of_permit', $data, function($message) use ($email, $subject) {
                        $message->to($email, 'TEZ Email')
                                ->subject($subject);
                    });
                }
                else if($status == 10)
                {
                    $update = [
                        'issued_date' => DB::raw('now()'),
                        'status' => $status,
                        'permit_no' => 'BP'.$id.''.date('Ymd', time()),
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ]; 
                    $data['status'] = 'issue permit';
                    /*Mail::send('email.application_status', $data, function($message) use ($email) {
                        $message->to($email, 'TEZ Email')
                                ->subject('Issuance of Building Permit');
                    });*/    
                }
                else if($status == 11)
                {
                    $update = [
                        'date_of_issuance' => DB::raw('now()'),
                        'status' => $status,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => DB::raw('now()')
                    ]; 
                    $data['status'] = 'issuance';
                    /*Mail::send('email.application_status', $data, function($message) use ($email) {
                        $message->to($email, 'TEZ Email')
                                ->subject('Issuance of Building Permit');
                    });*/    
                }
            }

            DB::beginTransaction();

            DB::table($table)
            ->where('id', '=',$id)
            ->update($update);

            DB::table('application_remarks')
            ->insert($insert);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        
        return response('success', 201);
    }

    public function upload(Request $request, $module, $option)
    {
        try {
            $user_id        = Auth::user()->id;
            $record_id      = $request->get('record_id');
            $file_id        = $request->get('file_id');
            $file_data      = $this->get_record('files', $file_id);
            $file_code      = $file_data->code;
            $file           = Input::File('file');
            $token_code     = Str::random(50);
            $file_extension = $file->getClientOriginalExtension();
            $data_name      = $file_code.'_'.$token_code.'.'.$file_extension;
            $original_name  = $file->getClientOriginalName();

            $folder_path = public_path().'/tieza/'.$module.'/'.$record_id;

            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);


            //$checking       = $this->check_if_attachment_exist($user_id, $file_id, $record_id);

            $checking       = 0;

            if($module == 'business_permit')
            {
                $permit_type = 1;
            }
            else if($module == 'location_permit')
            {
                $permit_type = 2;
            }
            else if($module == 'building_permit')
            {
                $permit_type = 3;
            }
            else if($module == 'occupancy_permit')
            {
                $permit_type = 4;
            }
            else
            {
                $permit_type = 0;
            }

            if($checking)
            {
                $request->file('file')->move($folder_path, $data_name);

                $update_data = [
                    'user_id' => $user_id,
                    'record_id' => $record_id,
                    'permit_type' => $permit_type,
                    'file_id' => $file_id,
                    'original_name' => $original_name,
                    'attachment_name' => $data_name,
                    'lock_file' => 0,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
                DB::beginTransaction();

                DB::table('attachments')
                ->where('attachments.id', '=', $checking->id)
                ->update($update_data);

                DB::commit();
            }else
            {
                $request->file('file')->move($folder_path, $data_name);

                $insert_data = [
                    'user_id' => $user_id,
                    'record_id' => $record_id,
                    'permit_type' => $permit_type,
                    'file_id' => $file_id,
                    'attachment_name' => $data_name,
                    'original_name' => $original_name,
                    'lock_file' => 0,
                    'created_by' => $user_id,
                    'created_at' => DB::raw('now()')
                ];
                DB::beginTransaction();

                DB::table('attachments')
                ->insert($insert_data);

                DB::commit();
            }

            
            
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response(['status' => 200, 'file_name' => $data_name], 201);
    } 

    public function to_assess($permit, $id)
    {
        try {
            $table = $permit;
            $update_data = [
                'file_lock' => 1,
                'assessed_date' => date('Y-m-d H:i:s', time()),
                'status' => 2,
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
            ];

            $update_attachment = [
                'lock_file' => 1,
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
            ];
            DB::beginTransaction();

            DB::table($table)
            ->where($table.'.id', '=', $id)
            ->update($update_data);

            DB::table('attachments')
            ->where('attachments.record_id', '=', $id)
            ->update($update_attachment);            

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response(['status' => 200], 201);
    }

    public function create_account()
    {
        $list = [
            [
                'JAMAICA CHLOE TAGUINOD FERNANDO', '2015-0153-P', 'tez12345', '2'
            ],
            [
                'JESIEL DARASIN RUBRICO', '2019-0024-P', 'tez12345', '2'
            ]
        ];
        foreach ($list as $key => $value) {
            $insert_data = [
                'name' => $value[0],
                'username' => $value[1],
                'password' => bcrypt($value[2]),
                'position' => $value[3],
                'permit_type' => 3,
                'created_by' => 1,
                'created_at' => DB::raw('now()')
            ];
            DB::beginTransaction();

            DB::table('users')
            ->insert($insert_data);

            DB::commit();
            echo 'Save '.$value[0].'<br>';
        }
    }

    public function upload_requirements()
    {
        $building_permit_reqs = [
            'Environmental Compliance Certificate (ECC) or Certificate of Non-Coverage
            (CNC) by the DENR.',
            'Zoning/Locational Clearance from the FTEZ Administrator',
            'TIEZA Certificate of Designation or Special Permit to Locate',
            'Transfer Certificate of Title (TCT)',
            'Contract of Lease',
            'Deed of Sale',
            'Secretary\'s Certification',
            'TIEZA Permit Application Letter',
            'TIEZA Certification of the designated Designers and Project',
            'Engineers/Professionals in-charge of construction',
            'Building Permit Form',
            'Architectural Permit Form',
            'Civil/Structural Permit Form-',
            'Electrical Permit Form',
            'Plumbing Permit Form',
            'Sanitary Permit Form',
            'Mechanical Permit Form',
            'Electronics Permit Form ',
            'PRC License',
            'Professional Tax Receipt (PTR)',
            'Project Cost and Estimate',
            'Technical Specifications',
            'Structural Anaysis and Design',
            'Soil Boring Test',
            'Construction Log Book',
        ];
        foreach ($building_permit_reqs as $key => $value) {
            $building_permit = [
                'name' => $value,
                'permit_type' => 3,
                'created_by' => 1,
                'created_at' => DB::raw('now()')
            ];

            DB::beginTransaction();

            DB::table('files')
            ->insert($building_permit);

            DB::commit();
            echo 'Save '.$value.'<br>';
        }
        
    }

    public function add_requirements()
    {
        $arr = [
            'Proof of Business Registration ',
            'Securities and Exchange Commission (SEC), if Corporation',
            'Cooperative Development Authority (CDA), if Cooperative',
            'Department of Trade and Industry (DTI), if Single    Proprietorship',
            'Registration Certificate from LGU',
            'Accreditation Certificate from DOT',
            'Certified True Copy of Articles of Incorporation and By-Laws in case of Partnership, corporation, association',
            'Occupancy Permit',
            'Proof of Right over the business location',
            'Original/Transfer Certificate of Title), if owned by the applicant',
            'Contract of Lease, if not owned by the applicant',
            'Land Tax Clearance from LGU',
            'Zoning /Locational Clearance from TIEZA',
            'Sanitary Permit/Inspection Certificate from LGU',
            'Fire Safety Insurance Certificate from BFP',
            'Social Security System Identification and Clearance',
            'Phil Health Clearance',
            'PAG-IBIG/HDMF Clearance',
            'Valid visa and Labor Permit from DOLE for non-Filipino personnel',
            'Management Structure or list of names of Employer and Employees',
            'Insurance Coverage against accidents for passenger and loss of luggage',
            'List of vehicles owned by the agency',
            'Travel Agency Management Training Certificate or equivalent',
            'MARINA Certificate of Public Conveyance (all crews are duly licensed) for sea transport',
            'DOPT Franchise for land transport',
        ];
    }
    public function view_form($module, $option, $id)
    {
        if($module == 'building_permit')
        {
            $data = $this->get_record('building_permit', $id);
            $building_permit_id = $data->id;
            $data->architectural_permit = $this->get_ancillary_permit('architectural_permit', $building_permit_id);
        }
        //dd($data);
        $file = $module.'.'.$option.'.application_form';
        return view($file, ['data' => $data]);
    }

    public function get_application($option, $id)
    {
        if($option == 'building_permit')
        {
            $data = $this->get_record('building_permit', $id);
            $data = [
                'building_permit' => $this->get_record('building_permit', $id),
                'architectural_permit' => $this->get_ancillary_permit('architectural_permit', $id),

                'structural_permit' => $this->get_ancillary_permit('structural_permit', $id),

                'electrical_permit' => $this->get_ancillary_permit('electrical_permit', $id),

                'sanitary_permit' => $this->get_ancillary_permit('sanitary_permit', $id),

                'mechanical_permit' => $this->get_ancillary_permit('mechanical_permit', $id),

                'plumbing_permit' => '',

                'electronics_permit' => '',
            ];
        }
        else if($option == 'pre_evaluation')
        {
            $data = [
                'pre_eval' => $this->get_record('pre_evaluation_building_permit', $id),
                'tct_informations' => $this->get_tct_informations($id)
            ];
        }
        else if($option == 'user_access')
        {
            $data = [
                'info' => $this->get_record('users', $id)
            ];
        }
        return response(['status' => 200, 'data' => $data], 201);
    }

    public function create_payment($id)
    {
        $order_no = "BP0001";
        $datenow = Carbon::now();
        $result = $this->get_user_info('building_permit', $id);
        $data = [
            'amount' => 1,
            'currency' => 'PHP',
            'customer_email' => $result->email,
            'customer_name' => strtoupper($result->first_name).' '.strtoupper($result->last_name),
            'order_no' => $order_no,
            'product_description' => 'Building Permit',
            'remarks' => 'Building Permit Payment',
             'service_id' => 'SPH00000123',     //stg 
            //'service_id' => 'SPH00000036',      //live
            'transaction_time' => $datenow->toDateTimeString(),
        ];
        ksort($data);
        $data_string = '';
        foreach ($data as $key => $value) {
            $data_string .= $value . '|';
        }
        $data_string = substr($data_string, 0, -1);
        info($data_string);
        $private_key = openssl_pkey_get_private(File::get(public_path('pgw-key.pem')));
        openssl_sign($data_string, $signature, $private_key, OPENSSL_ALGO_SHA256);
        $signature = strtoupper(bin2hex($signature));
        
        info($signature);


        $view = 'payment.index';
        $data = ['module'=> 'fulltax', 'file' => 'payment.confirmation','details'=> $data,'checksum'=>$signature,'processing_fee'=>'1','amount_wo'=>1, 'option' => 'Building Permit', 'icon' => 'fa fa-close']; 
        return view($view,$data);
    }

    public function direct_payment(Request $request)
    {
        ini_set('memory_limit', '5024M');
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        info($request);   
        $status =  $request->status_code;
        $payment_channel = $request->payment_channel;
        $payment_reference_no = $request->reference_no;

        
        //$fulltax = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)->first();
        $details = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)->where('email_address',$request->customer_email)->get();
        $fulltax = $details->first();
        

        if($status == "00" &&  $fulltax->payment_status != "00" && $fulltax->ar_no == "") 
        {
            $amount = 0;
            $fee = 0;
            $total = 0;
            foreach ($details as $val) {
                $amount += $val->fulltax_amount;
                $fee += 50;
                $total += $val->fulltax_amount + 50;
            }
            
            $ar_no = self::getARReference();
            $data = array(
                'fulltax' => $fulltax,
                'details' => $details,
                'payment_channel' => $payment_channel,
                'payment_reference_no' => $payment_reference_no,
                'order_no' => $request->order_no,
                'ar_no' => $ar_no,
                'f_amount' => $amount,
                'f_fee' => $fee,
                'f_total' => $total,
            );
            if($data['ar_no'])
            {
                $ft = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)
                //->where('created_at',$fulltax->created_at)
                ->where('fulltax_no',$fulltax->fulltax_no)
                ->update([
                    'ar_no' => $data['ar_no'],
                    'payment_channel' => $data['payment_channel'],
                    'payment_status' => $request->status_code,
                    'payment_transaction_time' => $request->payment_transaction_time,
                    'payment_reference_no' => $data['payment_reference_no'],
                    'updated_at' => Carbon::now(),
                ]);    
            }
            
            $to_email =[$fulltax->email_address,'traveltaxonline@tieza.gov.ph'];
            $bcc_email = ['misd.traveltax.online@tieza.gov.ph', 'misd.traveltax.online@tieza.gov.ph'];
            $view = 'mail.ar_report';
            $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($view, $data)->setWarnings(false);
            $folder_path = storage_path('tieza/fulltax/ar_cert/'.$fulltax->fulltax_no);
            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);
            $filename = $fulltax->fulltax_no.'_arcert';
            $pdf->save($folder_path.'/'.$filename.'.pdf');
            $pdf = new SpatiePdf($folder_path.'/'.$filename.'.pdf');
            $pdf->saveImage($folder_path.'/'.$filename.'.jpeg');
            $generate_image = $folder_path.'/'.$filename.'.jpeg';
    
            info($data);
            $subject = 'TIEZA Acknowledgement Receipt #'.$data['ar_no'];
            Mail::send('mail.ar_main', $data, function($message) use ($to_email,$generate_image,$subject){
                $message->from("no-reply.traveltax@tieza.gov.ph",'no-reply.traveltax@tieza.gov.ph'); 
                $message->to($to_email)->subject($subject);
                /*$message->bcc($bcc_email)->subject($subject);*/
                $message->attach($generate_image, ['as' => 'AR CERTIFICATE']);
            });
        }
        else{
            $ft = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)
            ->update([
                'payment_channel' => $payment_channel,
                'payment_status' => $request->status_code,
                'payment_transaction_time' => $request->payment_transaction_time,
                'payment_reference_no' => $payment_reference_no,
                'updated_at' => Carbon::now(),
            ]);
        }
        
        
        $view = 'application.payment.index';
        $data = ['file' => 'application.payment.form','response'=>$request]; 
        return view($view,$data);
    }

    public function get_attachment($module, $file_id, $record_id)
    {
        $data = $this->get_attachment_by($file_id, $record_id);
        $option = $module;
        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) use ($module, $option) {
            if($data->lock_file)
            {
                return '
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="'.url('/tieza/building_permit').'/'.$data->record_id.'/'.$data->attachment_name.'" target="_blank" class="btn btn-success"><i class="icon icon-left mdi mdi-eye"></i></a>
                        </a>
                        </div>
                    </div>
                    
                ';
            }
            else
            {
                return '
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="button" class="btn btn-danger" onclick="delete_attachment('.$data->id.', '.$data->file_id.');">
                                <i class="icon icon-left mdi mdi-close"></i>
                            </button>
                            <a href="'.url('/tieza/building_permit').'/'.$data->record_id.'/'.$data->attachment_name.'" target="_blank" class="btn btn-success"><i class="icon icon-left mdi mdi-eye"></i></a>
                        </a>
                        </div>
                    </div>
                    
                ';
            }
            
            /*return '<div class="tools">
                <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                    <a href="'.url('/'.$module.'/'.$option.'/'.$data->id.'/view-attachment').'" class="dropdown-item">
                        <i class="icon icon-left mdi mdi-eye"></i>
                    </a>
                    <a href="'.url('/'.$module.'/'.$option.'/'.$data->id.'/view-attachment').'" class="dropdown-item">
                        <i class="icon icon-left mdi mdi-eye"></i>
                    </a>
                </div>
            </div>';*/
            
        });
        
        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }

    

    public function destroy($module, $option, $id)
    {
        try {
            $table = '';

            if ($option == 'attachments')
            {
                $table = 'attachments';
            }
            if($table)
            {
                DB::beginTransaction();
                $delete_data = [
                    'updated_by' => Auth::user()->id,
                    'is_deleted' => 1,
                    'updated_at' => DB::raw('now()')
                ];
                DB::table($table)
                ->where('id', '=', $id)
                ->update($delete_data);
                DB::commit();    
            }
            
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response('success', 201);
    }

    public function create_application($module, $option, $pre_eval_id)
    {   
        $array = array();
        if($module == 'location_permit')
        {
            $fields = $this->location_permit_fields;
            $files = $this->list_files(2);
            $array = ['files' => $files];
        }
        else if($module == 'business_permit')
        {
            $fields = $this->business_permit_fields;
            $files = $this->list_files(1);
            $array = ['files' => $files];
        }
        else if($module == 'building_permit')
        {
            $building_permit_fields = $this->building_permit_fields;
            $files = $this->list_files(3);
            $tct_informations = $this->get_tct_informations($pre_eval_id);
            $array = [
                'files' => $files, 
                'building_permit_fields' => $building_permit_fields, 
                'tct_fields' => $this->tct_fields, 
                'pre_eval' => $this->get_pre_evaluation($pre_eval_id),
                'tct_informations' => $tct_informations,
            ];
            

        }
        else if($module == 'system_config')
        {
            $fields = $this->user_account_fields;
            $files = '';
            $array = ['files' => $files, 'fields' => $fields];
        }
        else
        {
            $fields = '';
        }
        $icon = 'mdi mdi-plus';
        $application_url  = '';
        $file = $module.'.'.$option.'.add';
        $data = ['module' => $module, 'option' => $option, 'file' => $file, 'icon' => $icon, 'application_url' => $application_url, 'array' => $array];
        return view($module.'.'.$option.'.create', $data);
    }

    public function upload_file_for_fsec(Request $request)
    {
        $id                 = $request->get('id');
        $token_code         = Str::random(50);
        $supplemental_fer   = Input::File('supplemental_fer');
        $supplemental_transmittal_letter  = Input::File('supplemental_transmittal_letter');
        $order_payment      = Input::File('order_payment');
        $endorsement_to_bfp = Input::File('endorsement_to_bfp');
        $record             = $this->get_record('building_permit',$id);
        $user_data          = $this->get_record('users', $record->user_id);
        $fer                = Input::File('fer');
        $transmittal_letter = Input::File('transmittal_letter');

        

        if($supplemental_transmittal_letter)
        {
            $file_ext_supplemental_transmittal_letter  = $supplemental_transmittal_letter->getClientOriginalExtension();

            $supplemental_transmittal_letter_file      = 'supplemental_transmittal_letter_'.$token_code.'.'.$file_ext_supplemental_transmittal_letter;

            $supplemental_transmittal_letter_name      = $supplemental_transmittal_letter->getClientOriginalName();
        }
        else
        {
            $supplemental_transmittal_letter_file = '';
            $supplemental_transmittal_letter_name = '';
        }


        if($supplemental_fer)
        {
            $file_ext_supplemental_fer  = $supplemental_fer->getClientOriginalExtension();

            $supplemental_fer_file      = 'supplemental_fer_'.$token_code.'.'.$file_ext_supplemental_fer;

            $supplemental_fer_name      = $supplemental_fer->getClientOriginalName();
        }
        else
        {
            $supplemental_fer_file = '';
            $supplemental_fer_name = '';
        }

        if($order_payment)
        {
            $file_ext_order_payment  = $order_payment->getClientOriginalExtension();

            $order_payment_file      = 'order_payment_'.$token_code.'.'.$file_ext_order_payment;

            $order_payment_name      = $order_payment->getClientOriginalName();
        }
        else
        {
            $order_payment_file = '';
            $order_payment_name = '';
        }

        if($endorsement_to_bfp)
        {
            $file_ext_endorsement_to_bfp  = $endorsement_to_bfp->getClientOriginalExtension();

            $endorsement_to_bfp_file      = 'endorsement_to_bfp_'.$token_code.'.'.$file_ext_endorsement_to_bfp;

            $endorsement_to_bfp_name      = $endorsement_to_bfp->getClientOriginalName();
        }
        else
        {
            $endorsement_to_bfp_file = '';
            $endorsement_to_bfp_name = '';
        }

        if($fer)
        {
            $file_ext_fer  = $fer->getClientOriginalExtension();

            $fer_file      = 'fer_'.$token_code.'.'.$file_ext_fer;

            $fer_name      = $fer->getClientOriginalName();
        }
        else
        {
            $fer_file = '';
            $fer_name = '';
        }

        if($transmittal_letter)
        {
            $file_ext_transmittal_letter  = $transmittal_letter->getClientOriginalExtension();

            $transmittal_letter_file      = 'transmittal_letter_'.$token_code.'.'.$file_ext_transmittal_letter;

            $transmittal_letter_name      = $transmittal_letter->getClientOriginalName();
        }
        else
        {
            $transmittal_letter_file = '';
            $transmittal_letter_name = '';
        }

        $update_data = [
            'supplemental_transmittal_letter' => $supplemental_transmittal_letter_file,
            'supplemental_transmittal_letter_original_name' => $supplemental_transmittal_letter_name,
            'supplemental_final_eval_report' => $supplemental_fer_file,
            'supplemental_final_eval_report_original_name' => $supplemental_fer_name,
            'order_payment' => $order_payment_file,
            'order_payment_original_name' => $order_payment_name,
            'endorsement_to_bfp' => $endorsement_to_bfp_file,
            'endorsement_to_bfp_original_name' => $endorsement_to_bfp_name,
            'final_eval_report' => $fer_file,
            'final_eval_report_original_name' => $fer_name,
            'transmittal_letter' => $transmittal_letter_file,
            'transmittal_letter_original_name' => $transmittal_letter_name,
            'fsec_request' => 1,
            'status' => 4,
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
        ];
        $table = 'building_permit';
        DB::table($table)
        ->where($table.'.id', $id)
        ->update($update_data); 
        DB::commit();

        $folder_path = public_path().'/tieza/building_permit/'.$id;

        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

        if($supplemental_transmittal_letter_file)
        {
            $request->file('supplemental_transmittal_letter')->move($folder_path, $supplemental_transmittal_letter_file);    
        }
        if($supplemental_fer_file)
        {
            $request->file('supplemental_fer')->move($folder_path, $supplemental_fer_file);    
        }
        if($order_payment_file)
        {
            $request->file('order_payment')->move($folder_path, $order_payment_file);    
        }
        if($endorsement_to_bfp_file)
        {
            $request->file('endorsement_to_bfp')->move($folder_path, $endorsement_to_bfp_file);    
        }
        if($fer_file)
        {
            $request->file('fer')->move($folder_path, $fer_file);    
        }
        if($transmittal_letter_file)
        {
            $request->file('transmittal_letter')->move($folder_path, $transmittal_letter_file);    
        }
        $email = $user_data->email;

        $data = [
            'full_name' => $user_data->first_name.' '.$user_data->last_name,
        ];
        Mail::send('email.request_for_fsec', $data, function($message) use ($email, $folder_path, $supplemental_transmittal_letter_file, $supplemental_fer_file, $order_payment_file, $fer_file, $transmittal_letter_file) {
            $message->to($email, 'TEZ Email')
                    ->subject('Request for FSEC');
            if($supplemental_transmittal_letter_file)
            {
                $message->attach($folder_path.'/'.$supplemental_transmittal_letter_file, ['as' => 'Transmittal Letter (Supplemental)']);    
            }

            if($supplemental_fer_file)
            {
                $message->attach($folder_path.'/'.$supplemental_fer_file, ['as' => 'Supplemental  Final Evaluation Report']);  
            }

            if($fer_file)
            {
                $message->attach($folder_path.'/'.$fer_file, ['as' => 'Final Evaluation Report']);  
            }

            if($transmittal_letter_file)
            {
                $message->attach($folder_path.'/'.$transmittal_letter_file, ['as' => 'Transmittal Letter']);  
            }
            
            
            $message->attach($folder_path.'/'.$order_payment_file, ['as' => 'Order of Payment']);
        });
    }

    public function upload_fsec(Request $request)
    {

        $id                 = $request->get('building_permit_id');
        $token_code         = Str::random(50);
        $fsec               = Input::File('fsec_file');
        $obo_endorsement    = Input::File('obo_endorsement_letter');
        

        if($fsec)
        {
            $file_ext_fsec  = $fsec->getClientOriginalExtension();

            $fsec_file      = 'fsec_'.$token_code.'.'.$file_ext_fsec;

            $fsec_name      = $fsec->getClientOriginalName();
        }
        else
        {
            $fsec_file = '';
            $fsec_name = '';
        }
        if($obo_endorsement)
        {
            $file_ext_obo_endorsement  = $obo_endorsement->getClientOriginalExtension();

            $obo_endorsement_file      = 'obo_endorsement_'.$token_code.'.'.$file_ext_obo_endorsement;

            $obo_endorsement_name      = $obo_endorsement->getClientOriginalName();
        }
        else
        {
            $obo_endorsement_file = '';
            $obo_endorsement_name = '';
        }
        

        $update_data = [
            'fsec' => $fsec_file,
            'fsec_original_name' => $fsec_name,
            'fsec_request' => 0,
            'fsec_date' => DB::raw('now()'),
            'obo_endorsement_letter' => $obo_endorsement_file,
            'obo_endorsement_letter_original_name' => $obo_endorsement_name,
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
        ];
        $table = 'building_permit';
        DB::table($table)
        ->where($table.'.id', $id)
        ->update($update_data); 
        DB::commit();

        $folder_path = public_path().'/tieza/building_permit/'.$id;

        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

        if($fsec_file)
        {
            $request->file('fsec_file')->move($folder_path, $fsec_file);    
        }
        if($obo_endorsement_file)
        {
            $request->file('obo_endorsement_letter')->move($folder_path, $obo_endorsement_file);    
        }
        
    }

    public function reset_pw($id)
    {
        try
        {

            $update_data = [
            'first_login' => '0',
            'password' => bcrypt('123456'),
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
            ];

            DB::beginTransaction();

            DB::table('users')
            ->where('users.id', $id)
            ->update($update_data);

            DB::commit();
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response('success', 201);
    }

    public function upload_evaluation_attachments($module, $option, $id, Request $request)
    {
        $id                 = $id;
        $token_code         = Str::random(50);
        $fer                = Input::File('fer');
        $transmittal_letter = Input::File('transmittal_letter');
        $record             = $this->get_record('building_permit',$id);
        $user_data          = $this->get_record('users', $record->user_id);

        if($fer)
        {
            $file_ext_fer  = $fer->getClientOriginalExtension();

            $fer_file      = 'fer_'.$token_code.'.'.$file_ext_fer;

            $fer_name      = $fer->getClientOriginalName();
        }
        else
        {
            $fer_file = '';
            $fer_name = '';
        }
        if($transmittal_letter)
        {
            $file_ext_transmittal_letter  = $transmittal_letter->getClientOriginalExtension();

            $transmittal_letter_file      = 'transmittal_letter_'.$token_code.'.'.$file_ext_transmittal_letter;

            $transmittal_letter_name      = $transmittal_letter->getClientOriginalName();
        }
        else
        {
            $transmittal_letter_file = '';
            $transmittal_letter_name = '';
        }

        $update_data = [
            'status' => 12,
            'final_eval_report' => $fer_file,
            'final_eval_report_original_name' => $fer_name,
            'final_eval_report_date' => DB::raw('now()'),
            'transmittal_letter' => $transmittal_letter_file,
            'transmittal_letter_original_name' => $transmittal_letter_name,
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
        ];
        $table = 'building_permit';
        DB::table($table)
        ->where($table.'.id', $id)
        ->update($update_data); 
        DB::commit();

        $folder_path = public_path().'/tieza/building_permit/'.$id.'/';

        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

        if($transmittal_letter_file)
        {
            $request->file('transmittal_letter')->move($folder_path, $transmittal_letter_file);    
        }

        if($fer_file)
        {
            $request->file('fer')->move($folder_path, $fer_file);    
        }

        


        $email = $user_data->email;

        $data = [
            'full_name' => $user_data->first_name.' '.$user_data->last_name,
        ];
        $subject = 'Final Plan Evaluation Report (FPER) for the proposed development of Mr./Ms.'.$user_data->first_name.' '.$user_data->last_name;

        Mail::send('email.fer_transmittal_letter', $data, function($message) use ($email, $fer_file, $transmittal_letter_file, $folder_path, $subject) {
            $message->to($email, 'TEZ Email')
                    ->subject($subject);
            $message->attach($folder_path.''.$fer_file, ['as' => 'Final Plan Evaluation Report']);
            $message->attach($folder_path.''.$transmittal_letter_file, ['as' => 'Transmittal Letter']);
        });
    }


    public function return_to_client($module, $option, $id, Request $request)
    {
        $id                 = $id;
        $record             = $this->get_record('building_permit',$id);
        $user_data          = $this->get_record('users', $record->user_id);

        $update_data = [
            'status' => 14,
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
        ];
        $table = 'building_permit';
        DB::table($table)
        ->where($table.'.id', $id)
        ->update($update_data); 
        DB::commit();

        

        


        $email = $user_data->email;

        $data = [
            'full_name' => $user_data->first_name.' '.$user_data->last_name,
            'requirement_list' => $request->get('remarks')
        ];
        $subject = 'Incomplete Requirements for Evaluation of Building Permit Application';

        Mail::send('email.noncompliant_letter', $data, function($message) use ($email, $subject) {
            $message->to($email, 'TEZ Email')
                    ->subject($subject);
        });
    }

    public function add_suplemental_proper($module, $option, $id)
    {
        $array = array();
        $file = $module.'.'.$option.'_form.upload_supplemental';
        $array = [
            'requirements' => $this->list_files(3),
            'record' => $this->get_record('building_permit', $id)
        ];
        $icon = 'mdi mdi-plus';
        $data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'file' => $file, 'array' => $array, 'id' => $id];
        return view($module.'.'.$option.'_form.view', $data);
    }

    public function upload_supplemental($module, $option, Request $request)
    {
        $id                 = $request->get('record_id');
        $user_id            = Auth::user()->id;
        $token_code         = Str::random(50);
        $file               = Input::File('file');
        $file_id            = $request->get('file_id');
        $remarks            = $request->get('remarks');
        

        if($file)
        {
            $file_ext           = $file->getClientOriginalExtension();

            $attachment_name    = $token_code.'.'.$file_ext;

            $original_name      = $file->getClientOriginalName();
        }
        else
        {
            $attachment_name = '';
            $original_name = '';
        }


        

        $insert_data = [
            'user_id' => $user_id,
            'record_id' => $id,
            'permit_type' => 3,
            'file_id' => $file_id,
            'attachment_name' => $attachment_name,
            'original_name' => $original_name,
            'remarks' => $remarks,
            'lock_file' => 1,
            'created_by' => $user_id,
            'created_at' => DB::raw('now()')
        ];
        DB::beginTransaction();

        DB::table('attachments')
        ->insert($insert_data);

        DB::commit();

        $folder_path = public_path().'/tieza/building_permit/'.$id;

        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

        if($file)
        {
            $request->file('file')->move($folder_path, $attachment_name);    
        }
    }



    public function upload_atap($module, $option, $id, Request $request)
    {
        $id                 = $id;
        $token_code         = Str::random(50);
        $atap                = Input::File('atap');
        $record             = $this->get_record('building_permit',$id);
        $user_data          = $this->get_record('users', $record->user_id);

        if($atap)
        {
            $file_ext_atap  = $atap->getClientOriginalExtension();

            $atap_file      = 'atap_'.$token_code.'.'.$file_ext_atap;

            $atap_name      = $atap->getClientOriginalName();
        }
        else
        {
            $atap_file = '';
            $atap_name = '';
        }

        $update_data = [
            'status' => 6,
            'atap' => $atap_file,
            'amount' => $request->get('amount'),
            'atap_original_name' => $atap_name,
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
        ];
        $table = 'building_permit';
        DB::table($table)
        ->where($table.'.id', $id)
        ->update($update_data); 
        DB::commit();

        $folder_path = public_path().'/tieza/building_permit/'.$id.'/';

        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

        if($atap_file)
        {
            $request->file('atap')->move($folder_path, $atap_file);    
        }
    }

    public function upload_payment_details($module, $option, $id, Request $request)
    {
        $id                 = $id;
        $token_code         = Str::random(50);
        $payment_receipt    = Input::File('payment_receipt');
        $record             = $this->get_record('building_permit',$id);
        $user_data          = $this->get_record('users', $record->user_id);

        if($payment_receipt)
        {
            $file_ext_payment_receipt  = $payment_receipt->getClientOriginalExtension();

            $payment_receipt_file      = 'payment_receipt_'.$token_code.'.'.$file_ext_payment_receipt;

            $payment_receipt_name      = $payment_receipt->getClientOriginalName();
        }
        else
        {
            $payment_receipt_file = '';
            $payment_receipt_name = '';
        }

        $update_data = [
            'status' => 8,
            'payment_receipt' => $payment_receipt_file,
            'payment_receipt_original_name' => $payment_receipt_name,
            'or_number' => $request->get('or_number'),
            'or_date' => $request->get('or_date'),
            'payment_date' => DB::raw('now()'),
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
        ];
        $table = 'building_permit';
        DB::table($table)
        ->where($table.'.id', $id)
        ->update($update_data); 
        DB::commit();

        $folder_path = public_path().'/tieza/building_permit/'.$id.'/';

        if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

        if($payment_receipt_file)
        {
            $request->file('payment_receipt')->move($folder_path, $payment_receipt_file);    
        }
    }

    public function number_to_words($number) 
    {
        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ' ';
        $negative    = 'negative ';
        $decimal     = ' pesos and ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'forty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX) {
            // overflow
            return false;
        }

        if ($number < 0) {
            return $negative . $this->number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int)($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= ' '.$this->number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int)($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            $words[] = $fraction.'/100';
            foreach (str_split((string)$fraction) as $number) {
                //$words[] = $dictionary[$number];
                
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

    public function view_permit($id)
    {
        $data = $this->get_record('building_permit', $id);
        $file = 'building_permit.building_permit_application_form.building_permit_form';
        return view($file, ['data' => $data]);
    }

    public function test($id)
    {
        $data = $this->get_record('pre_evaluation_building_permit', $id);
        echo '<textarea rows="10" style="background: rgba(0, 0, 0, 0); border: none; outline: 0; cursor: text; resize: none; overflow: hidden;">'.$data->assessed_remarks.'</textarea>';
    }
}
