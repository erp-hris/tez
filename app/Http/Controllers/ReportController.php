<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\Report;


class ReportController extends Controller
{
    use Report;

    protected $reports = [
        'issued-business-permit' => 'ISSUED BUSINESS PERMIT',
        'issued-location-permit' => 'ISSUED LOCATION PERMIT',
        'issued-occupancy-permit' => 'ISSUED OCCUPANCY PERMIT',
        'issued-building-permit' => 'ISSUED BUILDING PERMIT',
        'permit-application' => 'LIST OF PERMIT APPLICATION',
        'business-permit' => 'TIEZA PERMIT',

    ];

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'report';

        $this->default_attribute = [
            'name' => strtolower(trans('page.name'))
        ];
    }

    public function index()
    {
        $option = 'report';
        $data = ['module' => $this->module, 'option' => $option];
        $icon = 'mdi mdi-file';
        return view('report.index', [
            'reports' => $this->reports,
            'module' => $this->module,
            'option' => $option,
            'icon' => $icon,

        ]);
    }

    public function view_report(request $request, $option) 
    {
        $data_array = array();
        $data       = array();
        $report_title = $this->reports[$option];
    	return view('report.'.$option, 
            [
                'data' => $data,
                'report_title' => $report_title,
                'data_array' => $data_array
            ]
        );
        //return response(['data' => $employee_list]);
    }

    public function view_service_record_rpt(request $request, $id)
    {
        $data       = $this->list_service_record($id);
        $employee   = $this->get_employee_by($id);

        $certify = $this->get_signatory_by($request->input('certified_by'));

        $first_name = array($certify->first_name);

        if($certify->extension_name) $first_name[] = $certify->extension_name;

        $certified_by = implode(' ', $first_name).' '.$certify->middle_name[0].'. '.$certify->last_name;



        return view('report.service-record-form',['data' => $data, 'employee' => $employee, 'certified_by' => $certified_by, 'position_name' => $certify->position, 'department_name' => $certify->department, 'division_name' => $certify->division]); 
    }
}
