<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

use Mail;
class RegisterController extends Controller
{
    public function __construct()
    {
        
    }

    public function index()
    {
       return view('register.index');
    }

    public function register(Request $request)
    {

        $first_name     = strtoupper($request->get('first_name'));
        $last_name      = strtoupper($request->get('last_name'));
        $middle_name    = strtoupper($request->get('middle_name'));
        $username       = $request->get('username');
        $email          = $request->get('email');
        $password       = bcrypt(trim($request->get('password')));
        $name           = $first_name.' '.$last_name;
        $level          = 1;
        $position       = 1;
        $user_status    = 1;
        $created_by     = 1;

        
        $query = DB::table('users')->where('username', $username);
        $result = $query->count();
        if($result)
        {
            return response(['data' => 'exist'], 200);
        }
        else
        {
            try {
                $otp = random_int(100000, 999999);
                $insert_data = [
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'middle_name' => $middle_name,
                    'name' => $name,
                    'username' => $username,
                    'email' => $email,
                    'password' => $password,
                    'level' => $level,
                    'position' => $position,
                    'first_login' => 1,
                    'otp' => $otp,
                    'user_status' => $user_status,
                    'created_by' => $created_by,
                    'created_at' => DB::raw('now()')
                ];
                $client_name = $first_name.' '.$last_name;

                DB::beginTransaction();
                DB::table('users')
                ->insert($insert_data);

                $id = DB::getPdo()->lastInsertId();    

                DB::commit();

                $data = [
                    'title' => 'OTP',
                    'content' => 'This is your OTP '.$otp.'.',
                    'otp' => $otp,
                    'email' => $email,
                    'client_name' => $client_name,
                ]; 
                Mail::send('email.welcome', $data, function($message) use ($email) {
                    $message->to($email, 'TEZ Email')
                            ->subject('TIEZA Online Permitting System OTP');
                });   
                
                return response($id, 201);
            } catch (Exception $e) {
                DB::rollback();

                $data = json_decode($e->getMessage(), true);

                if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
                
                return response(['errors' => $data], 422);
            }
        }
        
        
        
        
        
    }

    public function sendEmail()
    {
        
    }

    public function check_otp($id)
    {
        $row = DB::table('users')
        ->where('id', $id)
        ->first();

        return view('register.otp', ['email' => $row->email, 'user_id' => $id]);

    }

    public function validate_otp(Request $request)
    {
        $user_id = $request->get('user_id');
        $otp = $request->get('otp');

        $result = DB::table('users')
        ->where('id', $user_id)
        ->where('otp', $otp)
        ->first();

        if($result)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
