<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function direct_payment(Request $request)
    {
        ini_set('memory_limit', '5024M');
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        info($request);   
        $status =  $request->status_code;
        $payment_channel = $request->payment_channel;
        $payment_reference_no = $request->reference_no;
        dd($request);

        
        //$fulltax = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)->first();
        $details = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)->where('email_address',$request->customer_email)->get();
        $fulltax = $details->first();
        

        if($status == "00" &&  $fulltax->payment_status != "00" && $fulltax->ar_no == "") 
        {
            $amount = 0;
            $fee = 0;
            $total = 0;
            foreach ($details as $val) {
                $amount += $val->fulltax_amount;
                $fee += 50;
                $total += $val->fulltax_amount + 50;
            }
            
            $ar_no = self::getARReference();
            $data = array(
                'fulltax' => $fulltax,
                'details' => $details,
                'payment_channel' => $payment_channel,
                'payment_reference_no' => $payment_reference_no,
                'order_no' => $request->order_no,
                'ar_no' => $ar_no,
                'f_amount' => $amount,
                'f_fee' => $fee,
                'f_total' => $total,
            );
            if($data['ar_no'])
            {
                $ft = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)
                //->where('created_at',$fulltax->created_at)
                ->where('fulltax_no',$fulltax->fulltax_no)
                ->update([
                    'ar_no' => $data['ar_no'],
                    'payment_channel' => $data['payment_channel'],
                    'payment_status' => $request->status_code,
                    'payment_transaction_time' => $request->payment_transaction_time,
                    'payment_reference_no' => $data['payment_reference_no'],
                    'updated_at' => Carbon::now(),
                ]);    
            }
            
            $to_email =[$fulltax->email_address,'traveltaxonline@tieza.gov.ph'];
            $bcc_email = ['misd.traveltax.online@tieza.gov.ph', 'misd.traveltax.online@tieza.gov.ph'];
            $view = 'mail.ar_report';
            $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($view, $data)->setWarnings(false);
            $folder_path = storage_path('tieza/fulltax/ar_cert/'.$fulltax->fulltax_no);
            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);
            $filename = $fulltax->fulltax_no.'_arcert';
            $pdf->save($folder_path.'/'.$filename.'.pdf');
            $pdf = new SpatiePdf($folder_path.'/'.$filename.'.pdf');
            $pdf->saveImage($folder_path.'/'.$filename.'.jpeg');
            $generate_image = $folder_path.'/'.$filename.'.jpeg';
    
            info($data);
            $subject = 'TIEZA Acknowledgement Receipt #'.$data['ar_no'];
            Mail::send('mail.ar_main', $data, function($message) use ($to_email,$generate_image,$subject){
                $message->from("no-reply.traveltax@tieza.gov.ph",'no-reply.traveltax@tieza.gov.ph'); 
                $message->to($to_email)->subject($subject);
                /*$message->bcc($bcc_email)->subject($subject);*/
                $message->attach($generate_image, ['as' => 'AR CERTIFICATE']);
            });
        }
        else{
            $ft = DB::table('fulltax_application')->where('fulltax_no',$request->order_no)
            ->update([
                'payment_channel' => $payment_channel,
                'payment_status' => $request->status_code,
                'payment_transaction_time' => $request->payment_transaction_time,
                'payment_reference_no' => $payment_reference_no,
                'updated_at' => Carbon::now(),
            ]);
        }
        
        
        $view = 'application.payment.index';
        $data = ['file' => 'application.payment.form','response'=>$request]; 
        return view($view,$data);
    }
}

