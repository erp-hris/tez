<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Traits\Dashboard;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    use Dashboard;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'home';
    }

    public function index()
    {
        //dd(2);
        if(Auth::user()->level == 1)
        {
            $pre_eval = $this->get_pre_evaluation_by(Auth::user()->id);
            $permit_record = $this->get_permit(Auth::user()->id, 'building_permit');
            if(!empty($permit_record))
            {
                $permit_remarks = $this->get_permit_remarks(3, $permit_record->id);    
            }
            else
            {
                $permit_remarks = array();
            }
            
            $data = ['module' => $this->module, 'permit_record' => $permit_record, 'permit_remarks' => $permit_remarks, 'pre_eval' => $pre_eval]; 
            return view('home.user-index', $data);
        }
        else
        {
            $process        = 0;
            $assessment     = 0;
            $evaluation     = 0;
            $recommendation = 0;
            $approval       = 0;
            $payment        = 0;
            $issuance       = 0;
            $issued         = 0;

            $building_permit = $this->list_building_permits();
            foreach($building_permit as $key => $val)
            {
                $status = $val->status;
                switch ($status) {
                    case '1':
                        $process++;
                        break;
                    case '2':
                        $assessment++;
                        break;
                    case '3':
                        $evaluation++;
                        break;
                    case '4':
                        $recommendation++;
                        break;
                    case '5':
                        $approval++;
                        break;
                    case '6':
                        $payment++;
                        break;
                    case '7':
                        $issuance++;
                        break;
                    case '8':
                        $issued++;
                        break;
                    default:
                        // code...
                        break;
                }
            }
            $building_permit_rpt = [
                'process' => $process,
                'assessment' => $assessment,
                'evaluation' => $evaluation,
                'recommendation' => $recommendation,
                'approval' => $approval,
                'payment' => $payment,
                'issuance' => $issuance,
                'issued' => $issued,
            ];
            $data = ['module' => $this->module, 'building_permit_rpt' => $building_permit_rpt]; 
            return view('home.index', $data);
        }
        
    }
}
