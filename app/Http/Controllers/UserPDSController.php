<?php

namespace App\Http\Controllers;

use App\Http\Traits\User_PDS;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class UserPDSController extends Controller
{
    use User_PDS;

    private $personal_info_attribute;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'user_pds_file';
    
        $this->personal_info_attribute = [
        'last_name' => strtolower(trans('page.surname')),
        'first_name' => strtolower(trans('page.first_name')),
        'middle_name' => strtolower(trans('page.middle_name')),
        'extension_name' => strtolower(trans('page.ext_name')),
        'sex' => strtolower(trans('page.sex')),
        'civil_status' => strtolower(trans('page.civil_status')),
        'other_civil_status' => strtolower(trans('page.please_specify')),
        'birth_date' => strtolower(trans('page.birth_date')),
        'birth_place' => strtolower(trans('page.birth_place')),
        'height' => strtolower(trans('page.height')),
        'blood_type' => strtolower(trans('page.blood_type')),
        'gsis' => strtolower(trans('page.gsis_id_no')),
        'pagibig' => strtolower(trans('page.pagibig_id_no')),
        'philhealth' => strtolower(trans('page.philhealth_no')),
        'sss' => strtolower(trans('page.sss_no')),
        'tin' => strtolower(trans('page.tin_no')),
        'telepone_no' => strtolower(trans('page.tel_no')),
        'mobile_no' => strtolower(trans('page.mobile_no')),
        'email_address' => strtolower(trans('page.email_address')),
        'is_filipino' => strtolower(trans('page.is_filipino')),
        'dual_citizenship' => strtolower(trans('page.dual_citizenship')),
        'residential_house_no' => strtolower(trans('page.house_blk_lot_no')),
        'residential_street' => strtolower(trans('page.street')),
        'residential_subdivision' => strtolower(trans('page.subdivision')),
        'residential_barangay' => strtolower(trans('page.barangay')),
        'same_as_above' => strtolower(trans('page.same_as_above')),
        'permanent_house_no' => strtolower(trans('page.house_blk_lot_no')),
        'permanent_street' => strtolower(trans('page.street')),
        'permanent_subdivision' => strtolower(trans('page.subdivision')),
        'permanent_barangay' => strtolower(trans('page.barangay')),
        ];
    }

    public function index($option)
    {
        $employee_id    = Auth::user()->employee_id;

        $children_columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'child_name'],
            ['data' => 'child_birthday']
        );
        $education_columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'education_level'],
            ['data' => 'school_name'],
            ['data' => 'course_name'],
            ['data' => 'year_graduated']
        );
        $eligibility_columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'eligibility_name'],
            ['data' => 'rating'],
            ['data' => 'license_number']
        );
        $workexp_columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'workexp_start_date'],
            ['data' => 'workexp_end_date'],
            ['data' => 'position_name'],
            ['data' => 'appointment_status_name'],
            ['data' => 'government_service']
        );
        $voluntary_work_columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'organization_name'],
            ['data' => 'voluntarywork_start_date'],
            ['data' => 'voluntarywork_end_date']
        );
        $training_columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'training_name'],
            ['data' => 'training_start_date'],
            ['data' => 'training_end_date'],
            ['data' => 'training_total_hours']
        );
        $other_info_columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'hobbies'],
            ['data' => 'recognition'],
            ['data' => 'membership']
        );
        $reference_columns = array(
            ['data' => 'action', 'sortable' => false],
            ['data' => 'name'],
            ['data' => 'address'],
            ['data' => 'telephone_number']
        );

        $children_json_url  = url('/user-pds-file/load_tables/children');
        $education_json_url = url('/user-pds-file/load_tables/education');
        $eligibility_json_url = url('/user-pds-file/load_tables/eligibility');
        $workexp_json_url = url('/user-pds-file/load_tables/workexp');
        $voluntary_work_json_url = url('/user-pds-file/load_tables/voluntary_work');
        $training_json_url = url('/user-pds-file/load_tables/training');
        $other_info_json_url = url('/user-pds-file/load_tables/other_info');
        $reference_json_url = url('/user-pds-file/load_tables/reference');
        
        return view('user-pds-file.index', 
            [
                'module' => $this->module, 
                'option' => '201_file', 
                'list_cities' => $this->list_cities(), 
                'list_provinces' => $this->list_provinces(), 
                'employee_id' => $employee_id,
                'children_tbl_id' => 'children_tbl',
                'children_columns' => $children_columns,
                'children_json_url' => $children_json_url,
                'education_tbl_id' => 'education_tbl',
                'education_columns' => $education_columns,
                'education_json_url' => $education_json_url,
                'eligibility_tbl_id' => 'eligibility_tbl',
                'eligibility_columns' => $eligibility_columns,
                'eligibility_json_url' => $eligibility_json_url,
                'workexp_tbl_id' => 'workexp_tbl',
                'workexp_columns' => $workexp_columns,
                'workexp_json_url' => $workexp_json_url,
                'voluntary_work_tbl_id' => 'voluntary_work_tbl',
                'voluntary_work_columns' => $voluntary_work_columns,
                'voluntary_work_json_url' => $voluntary_work_json_url,
                'training_tbl_id' => 'training_tbl',
                'training_columns' => $training_columns,
                'training_json_url' => $training_json_url,
                'other_info_tbl_id' => 'other_info_tbl',
                'other_info_columns' => $other_info_columns,
                'other_info_json_url' => $other_info_json_url,
                'reference_tbl_id' => 'reference_tbl',
                'reference_columns' => $reference_columns,
                'reference_json_url' => $reference_json_url,
                
            ]
        );
    }

    public function get_employee_info($id) 
    {
        $data = $this->get_employee($id);
        return response(['data' => $data]);
    }

    public function employee_datatables($option)
    {
        $employee_id    = Auth::user()->employee_id;
        if (in_array($option,
            [
                'children', 'education', 'eligibility','workexp','voluntary_work', 'training', 'other_info', 'reference'
            ]
        ))
        {
            
            if($option == 'children')
            {
                $data = $this->list_employee_children($employee_id);
            }
            elseif($option == 'education')
            {
                $data = $this->list_employee_education($employee_id);
                //dd($data);
            }
            elseif($option == 'eligibility')
            {
                $data = $this->list_employee_eligibility($employee_id);
            }
            elseif($option == 'workexp')
            {
                $data = $this->list_employee_workexperience($employee_id);
            }
            elseif($option == 'voluntary_work')
            {
                $data = $this->list_employee_voluntary_work($employee_id);
            }
            elseif($option == 'training')
            {
                $data = $this->list_employee_training($employee_id);
            }
            elseif($option == 'other_info')
            {
                $data = $this->list_employee_other_info($employee_id);
            }
            elseif($option == 'reference')
            {
                $data = $this->list_employee_reference($employee_id);
            }
            else
            {
                $data = array();
            }
            //dd($data);
            
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($option){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>
                                <span class="dropdown-item" onclick="delete_record(\''.$option.'\','.$data->id.');">
                                    <i class="icon icon-left mdi mdi-delete"></i>Delete
                                </span>
                            </div>
                        </div>';
            });

            if ($option == 'workexp')
            {
                $datatables
                ->editColumn('government_service', function($data){
                    return $data->government_service == 1 ? 'Yes' : 'No';
                });
            }
            elseif ($option == 'education')
            {
                $datatables
                ->editColumn('education_level', function($data){
                    $level = $data->education_level;
                    if ($level == 1)
                    {
                        $level = 'Elementary';
                    }
                    elseif ($level == 2)
                    {
                        $level = 'Secondary';
                    }
                    elseif ($level == 3)
                    {
                        $level = 'Vocational';
                    }
                    elseif ($level == 4)
                    {
                        $level = 'College';
                    }
                    elseif ($level == 5)
                    {
                        $level = 'Graduate Studies';
                    }
                    return $data->education_level = $level;
                });
            }

            return $datatables
            ->rawColumns(['action'])
            ->make(true);


            return Datatables::of($data)
            ->addColumn('action', function($data){
                return '<div class="tools text-center">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="" class="dropdown-item">
                                    <i class="icon icon-left mdi mdi-view"></i>View
                                </a>
                            </div>
                        </div>';
            })
            ->rawColumns(['action'])
            ->make(true);
        } 
    }

    public function get_city_by($id)
    {   
        try
        {
            if($this->count_city_by($id) == 0) throw new Exception();

            $city = $this->select_city_by($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => 'no data found'], 422); 
        }

        return response(['city' => $city]);
    }


    public function add_entry($option)
    {
        $view = 'user-pds-file.create';
        if ($option == 'educ')
        {
            $courses = $this->list_courses();   
            $schools = $this->list_schools();   
            return view($view, 
                [
                    'module' => $this->module,
                    'option' => $option,
                    'schools' => $schools,
                    'courses' => $courses
                ]
            );
        } 
        elseif ($option == 'eligibility') {
            $eligibilities = $this->list_eligibilities();   
            return view($view, 
                [
                    'module' => $this->module,
                    'option' => $option,
                    'eligibilities' => $eligibilities
                ]
            );
        }
        else
        {
            return view($view, ['module' => $this->module, 'option' => $option]);    
        }
        
        
    }

    public function store_entry(request $request, $option)
    {
        try
        {
            if ($option == 'child')
            {
                $table          = 'employee_children';
                $child_name     = $request->get('child_name');
                $child_birthday = $request->get('child_birthday');

                $insert_data    = [
                    'employee_id' => Auth::user()->employee_id,
                    'child_name' => strtoupper($child_name),
                    'child_birthday' => $child_birthday,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
            }
            elseif ($option == 'educ')
            {
                $table                  = 'employee_education';
                $education_level        = $request->get('education_level');
                $school_id              = $request->get('school_id');
                $course_id              = $request->get('course_id');
                $start_year             = $request->get('start_year');
                $end_year               = $request->get('end_year');
                $education_present      = $request->get('education_present');
                $year_graduated         = $request->get('year_graduated');
                $highest_year_level     = $request->get('highest_year_level');
                $academic_honors        = $request->get('academic_honors');

                $insert_data    = [
                    'employee_id' => Auth::user()->employee_id,
                    'education_level' => $education_level,
                    'school_id' => $school_id,
                    'course_id' => $course_id,
                    'start_year' => $start_year,
                    'end_year' => $end_year,
                    'education_present' => $education_present,
                    'year_graduated' => $year_graduated,
                    'highest_year_level' => $highest_year_level,
                    'academic_honors' => $academic_honors,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];

            }
            elseif ($option == 'eligibility')
            {
                $table                  = 'employee_eligibility';
                $eligibility_id         = $request->get('eligibility_id');
                $rating                 = $request->get('rating');
                $examination_date       = $request->get('examination_date');
                $examination_place      = $request->get('examination_place');
                $license_number         = $request->get('license_number');
                $released_date          = $request->get('released_date');

                $insert_data    = [
                    'employee_id' => Auth::user()->employee_id,
                    'eligibility_id' => $eligibility_id,
                    'rating' => $rating,
                    'examination_date' => $examination_date,
                    'examination_place' => $examination_place,
                    'license_number' => $license_number,
                    'released_date' => $released_date,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
            }
            /*dd($insert_data);
            return false;*/

            DB::beginTransaction();

            DB::table($table)
            ->insert($insert_data); 

            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }
        return response('success', 204);
            

    }

    public function store_201_file(request $request)
    {
        //dd($request);
        try
        {
            $validate_cities = implode(',', $this->get_value_by($this->list_cities(), 'id'));

            $rule = [
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'middle_name' => 'required|max:255',
            'extension_name' => 'sometimes|nullable|max:255',
            'sex' => 'in:M,F',
            'civil_status' => 'in:1,2,3,4,5',
            'other_civil_status' => 'sometimes|nullable|max:255',
            'birth_date' => 'required',
            'birth_place' => 'required|max:255',
            'weight' => 'required|max:255',
            'height' => 'required|max:255',
            'blood_type' => 'in:AB+,AB-,A+,A-,B+,B-,O+,O-',
            'gsis' => 'sometimes|nullable|max:255',
            'pagibig' => 'sometimes|nullable|max:255',
            'philhealth' => 'sometimes|nullable|max:255',
            'sss' => 'sometimes|nullable|max:255',
            'tin' => 'sometimes|nullable|max:255',
            'employee_number' => 'required|max:255',
            'telepone_no' => 'sometimes|nullable|max:255',
            'mobile_no' => 'sometimes|nullable|max:255',
            'email_address' => 'required|email',
            'is_filipino' => 'in:0,1',
            'dual_citizenship' => 'in:0,1',
            'residential_house_no' => 'required|max:255',
            'residential_street' => 'sometimes|nullable|max:255',
            'residential_subdivision' => 'sometimes|nullable|max:255',
            'residential_barangay' => 'required|max:255',
            'residential_city' => 'in:'.$validate_cities,
            'same_as_above' => 'in:0,1',
            'permanent_house_no' => 'required|max:255',
            'permanent_street' => 'sometimes|nullable|max:255',
            'permanent_subdivision' =>  'sometimes|nullable|max:255',
            'permanent_barangay' => 'required|max:255',
            'permanent_city' => 'in:'.$validate_cities,
            ];

            //$this->validate_request($request->all(), $rule, $this->personal_info_attribute);

            $residential_house_no = $request->get('residential_house_no');
            $residential_street = $request->get('residential_street');
            $residential_subdivision = $request->get('residential_subdivision');
            $residential_barangay = $request->get('residential_barangay');
            $residential_city = $request->get('residential_city');
            if ($residential_city == '')
            {
                $residential_province_id = 0;
                $residential_zip_code = '';
            }
            else
            {
                $residential_city_info = $this->select_city_by($residential_city);
                $residential_province_id = $residential_city_info->province_id;
                $residential_zip_code = $residential_city_info->zip_code;    
            }
            

            $same_as_above = $request->get('same_as_above');

            $permanent_house_no = $request->get('permanent_house_no');
            $permanent_street = $request->get('permanent_street');
            $permanent_subdivision = $request->get('permanent_subdivision');
            $permanent_barangay = $request->get('permanent_barangay');
            $permanent_city = $request->get('permanent_city');
            

            if ($permanent_city == '')
            {
                $permanent_province = 0;
                $permanent_zip_code = '';
            }
            else
            {
                $permanent_city_info =  $this->select_city_by($permanent_city);
                $permanent_province = $permanent_city_info->province_id;
                $permanent_zip_code = $permanent_city_info->zip_code;    
            }
            

            if($same_as_above == 1)
            {
                $permanent_house_no = $residential_house_no;
                $permanent_street = $residential_street;
                $permanent_subdivision = $residential_subdivision;
                $permanent_barangay = $residential_barangay;
                $permanent_city = $residential_city;
                $permanent_province = $residential_city_info->province_id;
                $permanent_zip_code = $residential_city_info->zip_code;
            }

            $civil_status = $request->get('civil_status');
            $other_civil_status = $request->get('other_civil_status');

            if($civil_status == 5) 
            {
                $civil_status = 0;
            }
            else
            {
                $other_civil_status = null;
            }

            $update_data = [
            'last_name' => strtoupper($request->get('last_name')),
            'first_name' => strtoupper($request->get('first_name')),
            'middle_name' => strtoupper($request->get('middle_name')),
            'extension_name' => strtoupper($request->get('extension_name')),
            'birthday' => date("Y-m-d", strtotime($request->get('birth_date'))),
            'birth_place' => $request->get('birth_place'),
            'email_address' => $request->get('email_address'),
            'gender' => $request->get('sex'),
            'civil_status_id' => $civil_status,
            'other_civil_status' => $other_civil_status,
            'filipino' => $request->get('is_filipino'),
            'height' => $request->get('height'),
            'weight' => $request->get('weight'),
            'blood_type_id' => $request->get('blood_type'),
            'gsis' => $request->get('gsis'),
            'pagibig' => $request->get('pagibig'),
            'philhealth' => $request->get('philhealth'),
            'sss' => $request->get('sss'),
            'tin' => $request->get('tin'),
            'employee_number' => $request->get('employee_number'),
            'telephone_number' => $request->get('telepone_no'),
            'mobile_number' => $request->get('mobile_no'),
            'residential_house_number' => $residential_house_no,
            'residential_street' => $residential_street,
            'residential_subdivision' => $residential_subdivision,
            'residential_brgy' => $residential_barangay,
            'residential_city_id' => $residential_city,
            'residential_province_id' => $residential_province_id,
            'residential_zip_code' => $residential_zip_code,
            'residential_country_id' => 1,
            'permanent_house_number' => $permanent_house_no,
            'permanent_street' => $permanent_street,
            'permanent_subdivision' => $permanent_subdivision,
            'permanent_brgy' => $permanent_barangay,
            'permanent_city_id' => $permanent_city,
            'permanent_province_id' => $permanent_province,
            'permanent_zip_code' => $permanent_zip_code,
            'permanent_country_id' => 1,
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
            ];

            DB::beginTransaction();

            DB::table('employees')
                ->where('id', '=',$request->get('employee_id'))
                ->update($update_data); 
            DB::commit();

        }
        catch(Exception $e)
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);

            return response(['errors' => $data], 422);
        }

        // session()->flash('success', trans('page.added_successfully', ['attribute' => strtolower(trans('page.201-file'))]));

        return response('success', 204);
    }
}
