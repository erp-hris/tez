<?php

namespace App\Http\Controllers;

use App\Http\Traits\Attendance;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class DTRController extends Controller
{
    use Attendance;

    public function __construct()
    {
        $this->middleware('auth');
        $this->module = 'attendance';
    }

    public function attendance_logs($id, $month, $year)
    {
        $present_count              = 0;
        $restday_count              = 0;
        $leave_count                = 0;
        $no_lunch_count             = 0;
        $special_holiday_count      = 0;
        $legal_holiday_count        = 0;
        $undertime_count            = 0;
        $tardy_count                = 0;
        $absent_count               = 0;
        $total_tardy                = 0;
        $total_undertime            = 0;
        $tardy_count_per_week       = array(0,0,0,0,0,0,0);
        $tardy_hour_per_week        = array(0,0,0,0,0,0,0);
        $undertime_count_per_week   = array(0,0,0,0,0,0,0);
        $undertime_hour_per_week    = array(0,0,0,0,0,0,0);

        $dtr_logs           = array();
        $calendar_logs      = array();
        $employee           = $this->get_employee_information($id);
        $work_schedule      = $this->get_data('work_schedules',$employee->work_schedule_id);
        $assumption_date    = $employee->assumption_date;
        $resign_date        = $employee->resign_date;

        $curr_date          = date("Y-m-d",time());
        $num_of_days        = cal_days_in_month(CAL_GREGORIAN,$month,$year);
        $exact_end_days     = $num_of_days;
        $new_week           = '';
        $week_count         = 0;
        $month_start        = $year."-".$month."-01";
        $month_end          = $year."-".$month."-".$num_of_days;

        if(!$work_schedule)
        {
            $work_schedule_name = 'No Work Schedule';
        }
        else
        {
            $work_schedule_name = $work_schedule->name;
        }


        //if (strtotime($assumption_date) >= strtotime($month_start)) $month_start = $assumption_date;

        if (strtotime($curr_date) <= strtotime($month_end))
        {
            $month_end = $curr_date;
        }
        else
        {
            $month_end = $month_end;
        }

        $num_of_days         = date("d",strtotime($month_end));
        $period              = $month."/01/".$year." - ".$month."/".$num_of_days."/".$year;

        for($d=1;$d<=$num_of_days;$d++)
        {
            if ($d <= 9) $d = "0".$d;
            $y      = $year."-".$month."-".$d;
            $temp_date = date("D",strtotime($y)).", ".$month."-".$d."-".$year;
            $dtr_date = date("D",strtotime($y)).", ".date('m-d-y',strtotime($y));
            $week   = date("W",strtotime($y));
            if ($week != $new_week) $week_count++;
            $dtr = [
                'attendance_date' => $y,
                'day' => $d,
                'day_name' => strtolower(date("l",strtotime($y))),
                'week' => $week_count,
                'entry_kind' => '',
                'time_in' => 0,
                'lunch_out' => 0,
                'lunch_in' => 0,
                'time_out' => 0,
                'office_authority_code' => '',
                'office_authority_name' => '',
                'office_authority_start_time' => 0,
                'office_authority_end_time' => 0,
                'office_authority_whole_day' => 0,
                'leave_code' => '',
                'leave_name' => '',
                'forced_leave' => 0,
                'holiday_name' => '',
                'holiday_type' => 0,
                'offsus_name' => '',
                'offsus_whole_day' => 0,
                'offsus_time_start' => 0,
                'offsus_required_time_in' => 0,
                'overtime_type' => '',
                'overtime_classification' => '',
                'overtime_time' => 0,
                'overtime_time_in' => 0,
                'overtime_time_out' => 0,
                'cto_hours' => 0
            ];
            $calendar = [
                'attendance_date' => $y,
                'temp_date' => $temp_date,
                'dtr_date' => $dtr_date,
                'day' => intval($d),
                'day_name' => strtolower(date("l",strtotime($y))),
                'week' => $week_count,
                'time_in' => 0,
                'lunch_out' => 0,
                'lunch_in' => 0,
                'time_out' => 0,
                'tardy' => 0,
                'undertime' => 0,
                'excess_time' => 0,
                'consume_time' => 0,
                'total_tardy_ut' => 0,
                'remarks' => ''
            ];


            $calendar_logs['summary'] = [
                'present_count' => 0,
                'restday_count' => 0,
                'leave_count' => 0,
                'no_lunch_count' => 0,
                'special_holiday_count' => 0,
                'legal_holiday_count' => 0,
                'undertime_count' => 0,
                'tardy_count' => 0,
                'absent_count' => 0,
                'total_tardy' => 0,
                'total_undertime' => 0,
                'tardy_equivalent' => 0,
                'undertime_equivalent' => 0
            ];


            $dtr_logs["attendance"][$y] = $dtr;
            $calendar_logs["attendance"][$y] = $calendar;
            $new_week = $week;
        }
        $calendar_logs["employee_info"] = [
            'employee_name' => $employee->employee_name,
            'work_schedule' => $work_schedule_name,
            'biometrics_id' => $employee->employee_number,
            'position_name' => $employee->position_name,
            'division_name' => $employee->division_name,
            'department_name' => $employee->department_name,
            'printed_by' => $this->get_employee_name(Auth::user()->employee_id),
        ];
        if (!empty($employee->biometrics))
        {

            $logs       = $this->get_attendance_logs($employee->biometrics, $month, $year);  
            $count      = 0;
            $bio_arr    = array();

            $adjusted_logs = $this->get_adjusted_attendance_logs($id, $month, $year);  

            foreach ($logs as $key => $value)
            {
                $attendance_date = $value->attendance_date;
                $time_in_minutes = strtotime($value->utc);

                $count++;
                $bio_arr['logs'][$attendance_date][$count] = $time_in_minutes;
            }

            
            foreach ($adjusted_logs as $key => $value) {
                $attendance_date = $value->attendance_date;
                $entry_kind      = $value->entry_kind;
                $minutes         = $value->attendance_time;
                if ($entry_kind == 1)
                {
                    $fld = 'time_in';
                }
                else if ($entry_kind == 2)
                {
                    $fld = 'lunch_out';
                }
                else if ($entry_kind == 3)
                {
                    $fld = 'lunch_in';
                }
                else if ($entry_kind == 4)
                {
                    $fld = 'time_out';
                }
                else
                {
                    $fld = '';
                }
                if ($fld != '')
                {
                    if (isset($dtr_logs["attendance"][$attendance_date]['attendance_date']))
                    {

                        if ($dtr_logs["attendance"][$attendance_date]['attendance_date'] == $attendance_date)
                        {
                            $dtr_logs["attendance"][$attendance_date][$fld] = $minutes;
                        }
                    }    
                }
            }

            /*foreach ($logs as $key => $value) {
                $attendance_date = $value->attendance_date;
                $time_in_minutes = strtotime($value->utc_date);
                $entry_kind      = $value->entry_kind;
                $minutes         = $this->get_today_minute($time_in_minutes);
                if ($entry_kind == 1)
                {
                    $fld = 'time_in';
                }
                else if ($entry_kind == 2)
                {
                    $fld = 'lunch_out';
                }
                else if ($entry_kind == 4)
                {
                    $fld = 'lunch_in';
                }
                else if ($entry_kind == 5)
                {
                    $fld = 'time_out';
                }
                else
                {
                    $fld = '';
                }
                if ($fld != '')
                {
                    if (isset($dtr_logs["attendance"][$attendance_date]['attendance_date']))
                    {
                        if ($dtr_logs["attendance"][$attendance_date]['attendance_date'] == $attendance_date)
                        {
                            if ($fld != 'time_out')
                            {
                                if ($dtr_logs["attendance"][$d][$fld] == "") $dtr_logs["attendance"][$d][$fld] = $minutes;    
                            }
                            else
                            {
                                $dtr_logs["attendance"][$d][$fld] = $minutes;
                            }
                            
                        }
                    }    
                }
                
            }*/

            foreach ($bio_arr['logs'] as $key => $value) {
                foreach ($value as $nkey => $nvalue) {
                    $time = $this->get_today_minute($nvalue);
                    if(isset($dtr_logs["attendance"][$key]['attendance_date']))
                    {
                        if($time <= 720)
                        {
                            if ($dtr_logs["attendance"][$key]["time_in"] == "") $dtr_logs["attendance"][$key]["time_in"] = $time;
                        }
                        else if ($time >= 720 && $time <= 780)
                        {
                            if ($dtr_logs["attendance"][$key]["lunch_out"] == "") $dtr_logs["attendance"][$key]["lunch_out"] = $time;

                            if ($dtr_logs["attendance"][$key]["lunch_in"] == "") $dtr_logs["attendance"][$key]["lunch_in"] = $time;
                        } 
                        else if ($time >= 780)
                        {
                            if ($dtr_logs["attendance"][$key]["time_out"] == "") $dtr_logs["attendance"][$key]["time_out"] = $time;
                        }
                    }
                    
                }
            }
            /*foreach ($bio_arr as $key => $value) {
                foreach ($value as $nkey => $nvalue) {
                    $first  = reset($nvalue);
                    $last   = end($nvalue);
                    $utc_f  = $first;
                    $val_f  = $this->get_today_minute($utc_f);

                    $utc_l  = $last;
                    $val_l  = $this->get_today_minute($utc_l);
                    
                    $utc    = $first;
                    $d      = date("Y-m-d",$utc);
                    if (count($nvalue) > 1) {
                        if (isset($dtr_logs["attendance"][$d]['attendance_date']))
                        {
                            if ($dtr_logs["attendance"][$d]['attendance_date'] == $d)
                            {
                                if ($this->get_today_minute($utc_f) <= 720 || $this->get_today_minute($utc_l) <= 720)
                                {
                                    if ($dtr_logs["attendance"][$d]["time_in"] == "") $dtr_logs["attendance"][$d]["time_in"] = $val_f;
                                }
                                if ($this->get_today_minute($utc_f) >= 721 || $this->get_today_minute($utc_l) >= 721)
                                {
                                    if ($dtr_logs["attendance"][$d]["time_out"] == "") $dtr_logs["attendance"][$d]["time_out"] = $val_l;
                                }   
                            }   
                        }   
                    } else {
                        $new_value = reset($nvalue);
                        $new_utc = strtotime($new_value);
                        $new_value = $this->get_today_minute($new_utc);
                        if ($new_value <= 720)
                        {
                            if (isset($dtr_logs["attendance"][$d]['attendance_date']))
                            {
                                if ($dtr_logs["attendance"][$d]['attendance_date'] == $d)
                                {  
                                    if ($dtr_logs["attendance"][$d]["time_in"] == "") $dtr_logs["attendance"][$d]["time_in"] = $new_value;
                                }
                            }
                        }
                        else
                        {
                            if (isset($dtr_logs["attendance"][$d]['attendance_date']))
                            {
                                if ($dtr_logs["attendance"][$d]['attendance_date'] == $d)
                                {  
                                    if ($dtr_logs["attendance"][$d]["time_out"] == "") $dtr_logs["attendance"][$d]["time_out"] = $val_l;
                                }
                            }
                        }
                    }       
                }
            }*/

            //Getting Holidays
            $holiday = $this->list_holidays();
            if (!empty($holiday))
            {
                foreach ($holiday as $key => $value)
                {
                    $start_date     = $value->start_date;
                    $end_date       = $value->end_date;
                    $legal_holiday  = $value->legal_holiday;
                    $yearly         = $value->yearly;
                    $date_diff      = $this->date_difference($start_date, $end_date);
                    $name           = $value->code;
                    $temp_arr       = explode("-", $start_date);
                    $temp_date      = $year."-".$temp_arr[1]."-".$temp_arr[2];
                    for ($i=0; $i <= $date_diff ; $i++)
                    { 
                        $new_date = date('Y-m-d', strtotime($temp_date. ' + '.$i.' days'));
                        if ($yearly == 1)
                        {
                            if(isset($dtr_logs["attendance"][$new_date])) {
                                
                                $dtr_logs["attendance"][$new_date]["holiday_name"] = $name;
                                $dtr_logs["attendance"][$new_date]["holiday_type"] = $legal_holiday;
                            }
                        }
                        else
                        {
                            if(isset($dtr_logs["attendance"][$start_date])) {
                                $dtr_logs["attendance"][$start_date]["holiday_name"] = $name;
                                $dtr_logs["attendance"][$start_date]["holiday_type"] = $legal_holiday;
                            }
                        }
                    }
                }
            }

            //Getting Office Suspensions
            $office_suspension = $this->list_office_suspensions();
            if (!empty($office_suspension))
            {
                foreach ($office_suspension as $key => $value)
                {
                    $start_date     = $value->start_date;
                    $end_date       = $value->end_date;
                    $date_diff      = $this->date_difference($start_date, $end_date);
                    $name           = $value->code;
                    $whole_day      = $value->whole_day;
                    $start_time     = $value->start_time;
                    $time_in_required = $value->time_in_required;
                    $temp_arr       = explode("-", $start_date);
                    $temp_date      = $year."-".$temp_arr[1]."-".$temp_arr[2];
                    for ($i=0; $i <= $date_diff ; $i++)
                    { 
                        $new_date = date('Y-m-d', strtotime($temp_date. ' + '.$i.' days'));
                        if(isset($dtr_logs["attendance"][$new_date])) {
                            $dtr_logs["attendance"][$new_date]["offsus_name"] = $name;
                            $dtr_logs["attendance"][$new_date]["offsus_time_start"] = $start_time;
                            $dtr_logs["attendance"][$new_date]["offsus_whole_day"] = $whole_day;
                            $dtr_logs["attendance"][$new_date]["offsus_required_time_in"] = $time_in_required;
                        }
                    }
                }
            }

            //Getting Employee's Leaves
            $leaves = $this->get_application_employees_leave($id);
            if (!empty($leaves))
            {
                foreach ($leaves as $key => $value) {
                    $start_date     = $value->start_date;
                    $end_date       = $value->end_date;
                    $date_diff      = $this->date_difference($start_date, $end_date);
                    $name           = $value->code;
                    $code           = $value->code;
                    $temp_arr       = explode("-", $start_date);
                    $temp_date      = $year."-".$temp_arr[1]."-".$temp_arr[2];
                    $forced_leave   = $value->force_leave;
                    for ($i=0; $i <= $date_diff ; $i++)
                    { 
                        $new_date = date('Y-m-d', strtotime($temp_date. ' + '.$i.' days'));
                        if(isset($dtr_logs["attendance"][$new_date])) {
                            $dtr_logs["attendance"][$new_date]["leave_name"] = $name;
                            $dtr_logs["attendance"][$new_date]["leave_code"] = $code;
                            $dtr_logs["attendance"][$new_date]["forced_leave"] = $forced_leave;
                        }
                    }   
                }
            }

            //Getting Emplyoee's Office Authorities
            $office_authority = $this->get_application_employees_absence($id);
            if (!empty($office_authority))
            {
                foreach ($office_authority as $key => $value) {
                    $start_date     = $value->start_date;
                    $end_date       = $value->end_date;
                    $date_diff      = $this->date_difference($start_date, $end_date);
                    $name           = $value->code;
                    $code           = $value->code;
                    $start_time     = $value->start_time;
                    $end_time       = $value->end_time;
                    $whole_day      = $value->whole_day;

                    $temp_arr       = explode("-", $start_date);
                    $temp_date      = $year."-".$temp_arr[1]."-".$temp_arr[2];
                    for ($i=0; $i <= $date_diff ; $i++)
                    { 
                        $new_date = date('Y-m-d', strtotime($temp_date. ' + '.$i.' days'));
                        if(isset($dtr_logs["attendance"][$new_date])) {
                            $dtr_logs["attendance"][$new_date]["office_authority_name"] = $name;
                            $dtr_logs["attendance"][$new_date]["office_authority_code"] = $code;
                            $dtr_logs["attendance"][$new_date]["office_authority_start_time"] = $start_time;
                            $dtr_logs["attendance"][$new_date]["office_authority_end_time"] = $end_time;
                            $dtr_logs["attendance"][$new_date]["office_authority_whole_day"] = $whole_day;
                        }
                    }   
                }
            }

            //Getting Employee's Overtime
            $overtime = $this->get_application_employees_overtime($id);
            if (!empty($overtime))
            {
                foreach ($overtime as $key => $value) {
                    $start_date     = $value->start_date;
                    $end_date       = $value->end_date;
                    $date_diff      = $this->date_difference($start_date, $end_date);
                    $start_time     = $value->start_time;
                    $end_time       = $value->end_time;
                    $with_pay       = $value->with_pay;
                    $overtime_classification = $value->overtime_classification;

                    $temp_arr       = explode("-", $start_date);
                    $temp_date      = $year."-".$temp_arr[1]."-".$temp_arr[2];

                    if ($overtime_classification == 2) {
                        if ($end_time != "") {
                            if ($end_time > $start_time) {
                                $overtime_time = $end_time - $start_time;
                                if ($overtime_time > 480) {
                                    $excess_ot_time = $overtime_time - 480;
                                    for ($y=60; $y <= $excess_ot_time ; $y+=60) {
                                        if ($y == 180) {
                                            $dummy = $excess_ot_time - 180;
                                            if ($dummy > 60) {
                                                $excess_ot_time -= 60;  
                                            } else {
                                                $excess_ot_time -= $dummy;
                                            }
                                        }
                                    }
                                    $overtime_time = $excess_ot_time + 480;
                                } else {
                                    if ($start_time < 720 && $end_time > 780) {
                                        $overtime_time -= 60;
                                    }   
                                }

                            } else {
                                $overtime_time = (1440 - $start_time) + $end_time;
                                for ($x=60; $x < $overtime_time ; $x+=60) { 
                                    if ($x == 180) {
                                        $dummy = $overtime_time - 180;
                                        if ($dummy > 60) {
                                            $overtime_time -= 60;  
                                        } else {
                                            $overtime_time -= $dummy;
                                        }
                                    }
                                }
                            }   
                        }

                    }
                    for ($i=0; $i <= $date_diff ; $i++)
                    { 
                        $new_date = date('Y-m-d', strtotime($temp_date. ' + '.$i.' days'));
                        if(isset($dtr_logs["attendance"][$new_date])) {
                            $dtr_logs["attendance"][$new_date]["overtime_type"] = $with_pay;
                            $dtr_logs["attendance"][$new_date]["overtime_classification"] = $overtime_classification;
                            $dtr_logs["attendance"][$new_date]["overtime_time"] = $overtime_time;
                            $dtr_logs["attendance"][$new_date]["overtime_time_in"] = $start_time;
                            $dtr_logs["attendance"][$new_date]["overtime_time_out"] = $end_time;
                        }
                    }   
                }   
            }

            //Getting Employee's CTO
            $cto = $this->get_application_employees_cto($id);
            if (!empty($cto))
            {
                foreach ($cto as $key => $value) {
                    $start_date     = $value->start_date;
                    $end_date       = $value->end_date;
                    $date_diff      = $this->date_difference($start_date, $end_date);
                    $hours          = $value->hours;
                    $temp_arr       = explode("-", $start_date);
                    $temp_date      = $year."-".$temp_arr[1]."-".$temp_arr[2];
                    for ($i=0; $i <= $date_diff ; $i++)
                    { 
                        $new_date = date('Y-m-d', strtotime($temp_date. ' + '.$i.' days'));
                        if(isset($dtr_logs["attendance"][$new_date])) {
                            $dtr_logs["attendance"][$new_date]["cto_hours"] = $hours;
                        }
                    }   
                }
            }

        }
        //dd($dtr_logs);
        if($work_schedule)
        {
            foreach ($dtr_logs as $key => $value) {
                foreach ($value as $nkey => $dtr) {
                    $tardy                          = 0;
                    $undertime                      = 0;
                    $consume_time                   = 0;
                    $excess_time                    = 0;
                    $remarks                        = '';
                    $attendance_date                = $dtr['attendance_date'];
                    $day                            = $dtr['day'];
                    $day_name                       = $dtr['day_name'];
                    $week                           = $dtr['week'];
                    $entry_kind                     = $dtr['entry_kind'];
                    $time_in                        = $dtr['time_in'];
                    $lunch_out                      = $dtr['lunch_out'];
                    $lunch_in                       = $dtr['lunch_in'];
                    $time_out                       = $dtr['time_out'];
                    $office_authority_code          = $dtr['office_authority_code'];
                    $office_authority_name          = $dtr['office_authority_name'];
                    $office_authority_start_time    = $dtr['office_authority_start_time'];
                    $office_authority_end_time      = $dtr['office_authority_end_time'];
                    $office_authority_whole_day     = $dtr['office_authority_whole_day'];
                    $leave_code                     = $dtr['leave_code'];
                    $leave_name                     = $dtr['leave_name'];
                    $forced_leave                   = $dtr['forced_leave'];
                    $holiday_name                   = $dtr['holiday_name'];
                    $holiday_type                   = $dtr['holiday_type'];
                    $offsus_name                    = $dtr['offsus_name'];
                    $offsus_whole_day               = $dtr['offsus_whole_day'];
                    $offsus_time_start              = $dtr['offsus_time_start'];
                    $offsus_required_time_in        = $dtr['offsus_required_time_in'];
                    $overtime_type                  = $dtr['overtime_type'];
                    $overtime_classification        = $dtr['overtime_classification'];
                    $overtime_time                  = $dtr['overtime_time'];
                    $overtime_time_in               = $dtr['overtime_time_in'];
                    $overtime_time_out              = $dtr['overtime_time_out'];
                    $cto_hours                      = $dtr['cto_hours'];

                    $data_time_in                   = $work_schedule->{$day_name.'_time_in'};
                    $data_lunch_out                 = $work_schedule->{$day_name.'_lunch_out'};
                    $data_lunch_in                  = $work_schedule->{$day_name.'_lunch_in'};
                    $data_time_out                  = $work_schedule->{$day_name.'_time_out'};
                    $data_flexi_time                = $work_schedule->{$day_name.'_flexi_time'};
                    $data_restday                   = $work_schedule->{$day_name.'_restday'};
                    $data_schedule_type             = $work_schedule->schedule_type;

                    

                    $working_hours                  = ($data_time_out - $data_time_in) - ($data_lunch_in - $data_lunch_out);
                    $lunch_break                    = $data_lunch_in - $data_lunch_out;

                    $core_time                      = $data_flexi_time + ($data_time_out - $data_time_in);

                    if ($time_in <= $data_time_in && $data_restday != 1 && $time_in > 0) $time_in = $data_time_in;

                    if ($data_schedule_type == 1) $data_flexi_time = $data_time_in;

                    if ($leave_name != '' && $data_restday != 1)
                    {
                        $time_in = $data_time_in;
                        $time_out = $data_time_out;
                    }

                    if ($office_authority_name != '' && $data_restday != 1)
                    {
                        if ($office_authority_whole_day == 1)
                        {
                            $time_in = $data_time_in;
                            $time_out = $data_time_out;
                        }
                        else
                        {
                            if ($time_in > 0)
                            {
                                if ($time_in >= $office_authority_start_time) $time_in = $office_authority_start_time;
                            }

                            if ($time_out > 0)
                            {
                                if ($time_out <= $office_authority_end_time) $time_out = $office_authority_end_time;
                            }
                        }
                    }

                    if ($offsus_name != '')
                    {
                        if ($offsus_whole_day == 1)
                        {
                            $time_in = $data_time_in;
                            $time_out = $data_time_out;    
                        }
                        else
                        {
                            if ($offsus_required_time_in == 1)
                            {
                                if ($time_in == 0)
                                {
                                    $time_in    = 0;
                                    $time_out   = 0;
                                }
                            }
                            else
                            {
                                if ($time_in == 0)
                                {
                                    $time_in    = 0;
                                    $time_out   = 0;
                                }
                            }
                            if ($time_out > 0)
                            {
                                if ($offsus_time_start >= $time_out) $time_out = $offsus_time_start;    
                            }
                            
                        }
                    }

                    if ($time_in > 0 && $data_restday != 1) $present_count++;

                    //Getting the late
                    if ($time_in > 0 && $data_restday != 1)
                    {

                        if ($time_in >= $data_lunch_out && $time_in <= $data_lunch_in)
                        {
                            $tardy += $data_lunch_out - $data_flexi_time;
                            $tardy_hour_per_week[$week] += $tardy;
                            $tardy_count_per_week[$week]++;

                        }
                        else if ($time_in > $data_flexi_time)
                        {

                            if ($time_in > $data_lunch_in)
                            {
                                $tardy += $time_in - $data_flexi_time;
                                $tardy_hour_per_week[$week] += $tardy;
                                $tardy_count_per_week[$week]++;
                            }
                            else
                            {
                                $tardy += $time_in - $data_flexi_time;
                                $tardy_hour_per_week[$week] += $tardy;
                                $tardy_count_per_week[$week]++;    

                            }
                            
                        }

                    }

                    //Getting the undertime
                    if ($time_out > 0 && $data_restday != 1)
                    {
                        if ($time_out >= $data_lunch_out && $time_out <= $data_lunch_in)
                        {
                            $undertime += $data_time_out - $data_lunch_in;
                            $undertime_hour_per_week[$week] += $undertime;
                            $undertime_count_per_week[$week]++;
                        }
                        else if ($time_out < $data_time_out)
                        {
                            $undertime += $data_time_out - $time_out;
                            $undertime_hour_per_week[$week] += $undertime;
                            $undertime_count_per_week[$week]++;   
                        }
                    }

                    //Getting the regular hours
                    if ($data_restday != 1 && $time_in > 0 && $time_out > 0)
                    {
                        $consume_time   = $working_hours - ($tardy + $undertime);    
                        $excess_time    = ($time_out - $time_in) - ($lunch_break) - ($tardy + $undertime);
                        $excess_time   -= $consume_time;
                        if ($excess_time < 0) $excess_time = 0; 
                    }
                    

                    if (isset($calendar_logs["attendance"][$attendance_date]['attendance_date']))
                    {
                        if ($calendar_logs["attendance"][$attendance_date]['attendance_date'] == $attendance_date)
                        {
                            $calendar_logs['attendance'][$attendance_date]['time_in']           = $this->time_format($dtr['time_in'],2);
                            $calendar_logs['attendance'][$attendance_date]['time_out']          = $this->time_format($dtr['time_out'],2);
                            $calendar_logs['attendance'][$attendance_date]['lunch_out']         = $this->time_format($dtr['lunch_out'],2);
                            $calendar_logs['attendance'][$attendance_date]['lunch_in']          = $this->time_format($dtr['lunch_in'],2);
                            $calendar_logs['attendance'][$attendance_date]['tardy']             = $this->time_format($tardy);
                            $calendar_logs['attendance'][$attendance_date]['undertime']         = $this->time_format($undertime);
                            $calendar_logs['attendance'][$attendance_date]['excess_time']       = $this->time_format($excess_time);
                            $calendar_logs['attendance'][$attendance_date]['consume_time']      = $this->time_format($consume_time);
                            $calendar_logs['attendance'][$attendance_date]['total_tardy_ut']    = $this->time_format($tardy + $undertime);
                            $total_tardy += $tardy;
                            $total_undertime += $undertime;
                            
                            
                            if ($data_restday == 1) {
                                $restday_count++;
                                //$calendar_logs['attendance'][$attendance_date]['remarks'] = '<span style="color:red;">'.date("l",strtotime($attendance_date)).'</span>';
                            }
                            if ($holiday_name != '')
                            {
                                $calendar_logs['attendance'][$attendance_date]['remarks']  .= $holiday_name.'<br>';
                            }
                            if ($offsus_name != '' && $data_restday == 0)
                            {
                                $calendar_logs['attendance'][$attendance_date]['remarks']  .= $offsus_name;
                            }
                            if ($leave_code != '')
                            {
                                $calendar_logs['attendance'][$attendance_date]['remarks']  = $leave_code;
                            }
                            else if ($office_authority_name != '')
                            {
                                $calendar_logs['attendance'][$attendance_date]['remarks']  = $office_authority_name;
                            }
                            else if ($overtime_type != '')
                            {
                                if ($overtime_type == 1)
                                {
                                    $calendar_logs['attendance'][$attendance_date]['remarks']  = 'OT Pay';    
                                }
                                else if ($overtime_type == 0)
                                {
                                    $calendar_logs['attendance'][$attendance_date]['remarks']  = 'COC';    
                                }
                            }
                            if ($consume_time == 0 && $data_restday != 1) 
                            {
                                $calendar_logs['attendance'][$attendance_date]['remarks']  = 'Absent';    
                                $absent_count++;
                            }
                            if(
                                $holiday_name == '' &&
                                $offsus_name == '' &&
                                $leave_name == ''
                            )
                            {
                                if($time_in != '')
                                {
                                    if($lunch_out == 0 || $lunch_in == 0)
                                    {
                                        $no_lunch_count++;
                                    }    
                                }
                                    
                            }
                        }   
                    }   
                }
            }

            for ($i=1; $i <= 6; $i++) { 
                $undertime_count += $undertime_count_per_week[$i];
                $tardy_count += $tardy_count_per_week[$i];
            }
            //dd($calendar_logs);
            $calendar_logs['summary'] = [
                'present_count' => $present_count,
                'restday_count' => $restday_count,
                'leave_count' => $leave_count,
                'no_lunch_count' => $no_lunch_count,
                'special_holiday_count' => $special_holiday_count,
                'legal_holiday_count' => $legal_holiday_count,
                'undertime_count' => $undertime_count,
                'tardy_count' => $tardy_count,
                'absent_count' => $absent_count,
                'total_tardy' => $this->time_format($total_tardy),
                'total_undertime' => $this->time_format($total_undertime),
                'tardy_equivalent' => $this->get_equivalent($total_tardy),
                'undertime_equivalent' => $this->get_equivalent($total_undertime)
            ];    
        }
        
        return $calendar_logs;
    }

    public function preview_dtr($id, $month, $year)
    {
        $calendar_logs = $this->attendance_logs($id, $month, $year);
        $file = 'attendance.daily-time-record.dtr-report';
        $month = date("F",strtotime($year.'-'.$month.'-01'));
        return view($file, ['data' => $calendar_logs, 'month' => $month, 'year' => $year]);
    }
}
