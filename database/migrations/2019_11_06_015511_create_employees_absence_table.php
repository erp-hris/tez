<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesAbsenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employees_absence')) {
            Schema::create('employees_absence', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->integer('absence_id');
                $table->date('filed_date')->nullable();
                $table->date('start_date')->nullable();
                $table->date('end_date')->nullable();
                $table->boolean('whole_day')->nullable();
                $table->integer('start_time')->nullable();
                $table->integer('end_time')->nullable();
                $table->string('control_number')->nullable();
                $table->string('destination')->nullable();
                $table->integer('signatory1')->nullable();
                $table->integer('signatory2')->nullable();
                $table->integer('signatory3')->nullable();
                $table->integer('status')->nullable();
                $table->integer('approved_by')->nullable();
                $table->string('reason')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_absence');
    }
}
