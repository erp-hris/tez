<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employee_education')) {
            Schema::create('employee_education', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->integer('education_level')->nullable();
                $table->integer('school_id')->nullable();
                $table->integer('course_id')->nullable();
                $table->integer('start_year')->nullable();
                $table->integer('end_year')->nullable();
                $table->boolean('education_present')->nullable();
                $table->integer('year_graduated')->nullable();
                $table->string('highest_year_level')->nullable();
                $table->string('academic_honors')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_education');
    }
}
