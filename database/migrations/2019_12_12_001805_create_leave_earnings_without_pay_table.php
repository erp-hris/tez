<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveEarningsWithoutPayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('leave_earnings_without_pay')) {
            Schema::create('leave_earnings_without_pay', function (Blueprint $table) {
                $table->increments('id');
                $table->date('effectivity_date')->nullable();
                $table->decimal('days_present', 10, 3);
                $table->decimal('days_lwop', 10, 3);
                $table->decimal('earnings', 10, 3);
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_earnings_without_pay');
    }
}
