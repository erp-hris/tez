-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.25-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for tez
DROP DATABASE IF EXISTS `tez`;
CREATE DATABASE IF NOT EXISTS `tez` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `tez`;

-- Dumping structure for table tez.application_remarks
DROP TABLE IF EXISTS `application_remarks`;
CREATE TABLE IF NOT EXISTS `application_remarks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `permit_type` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.application_remarks: ~20 rows (approximately)
INSERT INTO `application_remarks` (`id`, `user_id`, `permit_type`, `record_id`, `status`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, 1214, 3, 1, 3, 'dalin mo daw hard copies', 1214, NULL, '2024-02-08 01:37:27', NULL, NULL),
	(2, 1214, 3, 1, 3, NULL, 1214, NULL, '2024-02-08 01:50:24', NULL, NULL),
	(3, 1214, 3, 1, 3, NULL, 1214, NULL, '2024-02-08 01:58:39', NULL, NULL),
	(4, 1214, 3, 1, 3, NULL, 1214, NULL, '2024-02-08 02:09:01', NULL, NULL),
	(5, 1214, 3, 1, 3, NULL, 1214, NULL, '2024-02-08 02:12:04', NULL, NULL),
	(6, 1214, 3, 1, 3, NULL, 1214, NULL, '2024-02-08 03:27:36', NULL, NULL),
	(7, 1214, 3, 1, 3, NULL, 1214, NULL, '2024-02-08 03:51:06', NULL, NULL),
	(8, 1214, 3, 1, 3, NULL, 1214, NULL, '2024-02-08 03:53:08', NULL, NULL),
	(9, 1214, 3, 1, 5, NULL, 1214, NULL, '2024-02-08 06:26:06', NULL, NULL),
	(10, 1290, 3, 1, 6, NULL, 1290, NULL, '2024-02-08 06:38:09', NULL, NULL),
	(11, 1290, 3, 1, 7, NULL, 1290, NULL, '2024-02-08 06:39:43', NULL, NULL),
	(12, 1270, 3, 1, 8, 'sir john\'s remarks', 1270, NULL, '2024-02-08 08:26:45', NULL, NULL),
	(13, 1270, 3, 1, 7, 'agasdgasdgasdggasdgasdgass', 1270, NULL, '2024-02-08 08:32:36', NULL, NULL),
	(14, 1270, 3, 1, 7, 'agasdgasdgasdggasdgasdgass', 1270, NULL, '2024-02-08 08:32:42', NULL, NULL),
	(15, 1270, 3, 1, 7, NULL, 1270, NULL, '2024-02-08 08:43:56', NULL, NULL),
	(16, 1270, 3, 1, 7, NULL, 1270, NULL, '2024-02-08 08:45:22', NULL, NULL),
	(17, 1270, 3, 1, 7, NULL, 1270, NULL, '2024-02-08 08:54:26', NULL, NULL),
	(18, 1270, 3, 1, 7, NULL, 1270, NULL, '2024-02-08 09:02:09', NULL, NULL),
	(19, 1270, 3, 1, 7, NULL, 1270, NULL, '2024-02-08 09:06:43', NULL, NULL),
	(20, 1270, 3, 1, 7, NULL, 1270, NULL, '2024-02-08 09:43:45', NULL, NULL),
	(21, 1270, 3, 1, 7, NULL, 1270, NULL, '2024-02-08 10:20:30', NULL, NULL),
	(22, 1270, 3, 1, 9, NULL, 1270, NULL, '2024-02-08 10:28:10', NULL, NULL),
	(23, 1270, 3, 1, 9, NULL, 1270, NULL, '2024-02-08 10:30:39', NULL, NULL);

-- Dumping structure for table tez.architectural_permit
DROP TABLE IF EXISTS `architectural_permit`;
CREATE TABLE IF NOT EXISTS `architectural_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `building_permit_id` int(11) DEFAULT NULL,
  `architectural_facilities` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_architectural_facilities` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `building_percentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `impervious_surface_area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unpaved_surface_area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `others_sit_occupancy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conformance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_conformance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_ptr_date_issued` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architectural_supervisor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architectural_supervisor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architectural_supervisor_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architectural_supervisor_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architectural_supervisor_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architectural_supervisor_ptr_date_issued` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architectural_supervisor_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architectural_supervisor_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.architectural_permit: ~1 rows (approximately)
INSERT INTO `architectural_permit` (`id`, `building_permit_id`, `architectural_facilities`, `other_architectural_facilities`, `building_percentage`, `impervious_surface_area`, `unpaved_surface_area`, `others_sit_occupancy`, `conformance`, `other_conformance`, `civil_engineer`, `civil_engineer_address`, `civil_engineer_prc_no`, `civil_engineer_prc_validity`, `civil_engineer_ptr_no`, `civil_engineer_ptr_date_issued`, `civil_engineer_ptr_issued_at`, `civil_engineer_tin`, `architectural_supervisor`, `architectural_supervisor_address`, `architectural_supervisor_prc_no`, `architectural_supervisor_prc_validity`, `architectural_supervisor_ptr_no`, `architectural_supervisor_ptr_date_issued`, `architectural_supervisor_ptr_issued_at`, `architectural_supervisor_tin`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, 1, '1|2|3|4|5|6|9|11|15|16|', NULL, 'Itaque sit ipsum ma', 'Pariatur Sit paria', 'Molestias illum har', 'Eligendi ut porro a', '1|3|7|', NULL, 'Molestias natus labo', 'Irure dolorem incidi', 'Cum non illum ullam', '2024-02-08', 'Facilis nulla dolor', '1997-10-22', 'Mollitia sit ea dig', 'Sed asperiores enim', 'Molestias natus labo', 'Irure dolorem incidi', 'Cum non illum ullam', '2024-02-08', 'Facilis nulla dolor', '1997-10-22', 'Mollitia sit ea dig', 'Sed asperiores enim', NULL, 1289, NULL, '2024-02-08 01:17:43', NULL, NULL);

-- Dumping structure for table tez.attachments
DROP TABLE IF EXISTS `attachments`;
CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `permit_type` int(11) DEFAULT NULL,
  `file_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lock_file` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.attachments: ~17 rows (approximately)
INSERT INTO `attachments` (`id`, `user_id`, `record_id`, `permit_type`, `file_id`, `attachment_name`, `original_name`, `lock_file`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, 1288, 1, 5, '30', 'gXNixwTuvpSeStOV0jtpeBpijuphWuvREJIfha3n7DkMgpR8rR.txt', 'sample1.txt', 1, NULL, 1288, 1289, '2024-02-07 02:44:53', '2024-02-08 03:40:51', NULL),
	(2, 1288, 1, 5, '51', 'FutJtyBwzE7YgkQOAjAoLLiSP9T8GI7xbd26uCb3Se5zRhlZrY.txt', 'sample1.txt', 1, '1', 1288, 1289, '2024-02-07 06:29:27', '2024-02-08 03:40:51', NULL),
	(3, 1288, 1, 5, '53', 'gpJ0wghG2Mw1gbkyzqmJlUL3qs58I0G8j6wc8ArDgICqjrFthX.txt', 'obo_endorsement.txt', 1, '1', 1288, 1289, '2024-02-07 06:42:14', '2024-02-08 03:40:51', NULL),
	(4, 1288, 1, 5, '56', 'IYLUhxTSZawJq1ZN4DHHc9hNhIPg47wSCxfJbMXKT2AcyVZKbF.txt', 'transmittal.txt', 1, '1', 1288, 1289, '2024-02-07 06:59:09', '2024-02-08 03:40:51', NULL),
	(5, 1288, 1, 5, '56', '0soTUl1sjULckfmHkAIHZzH9v6vJDaN2V0udi2oTKhp7GZxsxZ.txt', 'obo_endorsement.txt', 1, '1', 1288, 1289, '2024-02-07 08:12:48', '2024-02-08 03:40:51', NULL),
	(6, 1289, 7, 5, '30', '72xpjKCnaayvhvSqKvsjDJGcKPfkCmSIJqeEs7rdY1Vv9xxsq8.txt', 'obo_endorsement.txt', 1, NULL, 1289, NULL, '2024-02-07 09:56:30', NULL, NULL),
	(7, 1289, 7, 5, '55', 'bsGoKzwet2RpHcT3zfKXiGg3IL3sjnsVmRR0eUqWGaGd4EDALe.txt', 'order_payment.txt', 1, '1', 1289, NULL, '2024-02-07 10:01:34', NULL, NULL),
	(8, 1289, 7, 5, '56', 'x9AwmUqX8OeuhcoqmQB583OEBarGdll6yZl2DT57WT8YqjNtVg.txt', 'sample1.txt', 1, '1', 1289, NULL, '2024-02-07 10:08:55', NULL, NULL),
	(9, 1289, 1, 3, '1', '_64ZdQCnLFHtFqm7P9ud2gxV6MmsqlNY6RZw4oa3AMesyuQ9yN8.txt', 'stl.txt', 1, NULL, 1289, 1289, '2024-02-08 01:19:34', '2024-02-08 03:40:51', NULL),
	(10, 1289, 1, 3, '2', '_fg2VWY4COfzpQI2fbVUOnSMS9VwYWzRO7mrguIE11kKXVXZbxu.txt', 'signed_iper.txt', 1, NULL, 1289, 1289, '2024-02-08 01:19:37', '2024-02-08 03:40:51', NULL),
	(11, 1289, 1, 3, '3', '_IhmtcGlB5gYALGB9mavKE8sJLbIp8dPi5BbQXvw2QQUubdzwmy.txt', 'fsec.txt', 1, NULL, 1289, 1289, '2024-02-08 01:19:41', '2024-02-08 03:40:51', NULL),
	(12, 1289, 1, 5, '57', 'tWTbVwUp0goZg7wpKYw2DWEgunzrwaPAUk9DdKIyztV4fEi6qr.txt', 'signed_iper.txt', 1, '1', 1289, 1289, '2024-02-08 03:11:20', '2024-02-08 03:40:51', NULL),
	(13, 1289, 1, 5, '57', 'hSLZ1P0Eor2M8XEiYMLZ7oyqCcOTSWZ9cPB4h6CFpJVeZoe9Bn.txt', 'transmittal.txt', 1, '1', 1289, 1289, '2024-02-08 03:12:05', '2024-02-08 03:40:51', NULL),
	(14, 1289, 1, 5, '57', 'puRo0uxqE0cF5JmwtZ7W8wngqJLYF9V19KlQsPlBHANrnqY4qF.txt', 'signed_iper.txt', 1, '1', 1289, 1289, '2024-02-08 03:14:06', '2024-02-08 03:40:51', NULL),
	(15, 1289, 1, 5, '57', 'NQQqjTc9rSzXtAzvCuKOnbf3Rydxtur2hI338f1kTSPX3xBA8t.txt', 'payment_receipt.txt', 1, '1', 1289, 1289, '2024-02-08 03:15:38', '2024-02-08 03:40:51', NULL),
	(16, 1289, 1, 5, '57', '8J6ZO3Wg5bgMc9Z9GUO8prOhZItB7ZQLIxzCJAM7MnWRM92CFx.txt', 'siper.txt', 1, '1', 1289, 1289, '2024-02-08 03:21:40', '2024-02-08 03:40:51', NULL),
	(17, 1289, 1, 3, '57', 'vqgSBPvTKs5JUQjh8zFiJZRrZSor3KmjhDMCup8B7bBkPKDBdX.txt', 'transmittal.txt', 1, '1', 1289, 1289, '2024-02-08 03:24:15', '2024-02-08 03:40:51', NULL);

-- Dumping structure for table tez.building_permit
DROP TABLE IF EXISTS `building_permit`;
CREATE TABLE IF NOT EXISTS `building_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permit_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filed_date` datetime DEFAULT NULL,
  `transmittal_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transmittal_letter_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplemental_transmittal_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplemental_transmittal_letter_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `final_eval_report` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `final_eval_report_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplemental_final_eval_report` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplemental_final_eval_report_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `final_eval_report_date` timestamp NULL DEFAULT NULL,
  `order_payment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_payment_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `endorsement_to_bfp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `endorsement_to_bfp_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `obo_endorsement_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `obo_endorsement_letter_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atap_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fsec` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fsec_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_receipt_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `or_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `or_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `system_reference_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(20,2) DEFAULT NULL,
  `payment_channel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_trn_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_reference_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fsec_request` int(11) DEFAULT NULL,
  `assessed_date` datetime DEFAULT NULL COMMENT '2',
  `endorse_to_evaluation` datetime DEFAULT NULL COMMENT '3',
  `evaluated_date` datetime DEFAULT NULL COMMENT '4',
  `recommended_date` datetime DEFAULT NULL COMMENT '5',
  `supplemental_docs` datetime DEFAULT NULL,
  `reviewed_date` datetime DEFAULT NULL COMMENT '6',
  `approved_date` datetime DEFAULT NULL COMMENT '7',
  `payment_date` datetime DEFAULT NULL COMMENT '8',
  `issued_date` datetime DEFAULT NULL COMMENT '9',
  `date_of_issuance` date DEFAULT NULL COMMENT '10',
  `status` int(11) DEFAULT NULL,
  `file_lock` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `final_approver_id` int(11) DEFAULT NULL,
  `application_type` int(11) DEFAULT NULL,
  `fsec_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enterprise_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ownership_form` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_scope` int(10) DEFAULT NULL,
  `other_work_scope` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupancy_character` int(10) DEFAULT NULL,
  `other_occupancy_character` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupancy_classification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estimated_cost` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `units` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_date` date DEFAULT NULL,
  `floor_area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expected_completion_date` date DEFAULT NULL,
  `architect_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architect_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architect_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architect_prc_validity` date DEFAULT NULL,
  `architect_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architect_ptr_issued_date` date DEFAULT NULL,
  `architect_id_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `architect_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lot_owner_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lot_owner_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lot_owner_ctc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lot_owner_ctc_issued_date` date DEFAULT NULL,
  `lot_owner_ctc_issued_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.building_permit: ~1 rows (approximately)
INSERT INTO `building_permit` (`id`, `permit_no`, `application_no`, `filed_date`, `transmittal_letter`, `transmittal_letter_original_name`, `supplemental_transmittal_letter`, `supplemental_transmittal_letter_original_name`, `final_eval_report`, `final_eval_report_original_name`, `supplemental_final_eval_report`, `supplemental_final_eval_report_original_name`, `final_eval_report_date`, `order_payment`, `order_payment_original_name`, `endorsement_to_bfp`, `endorsement_to_bfp_original_name`, `obo_endorsement_letter`, `obo_endorsement_letter_original_name`, `atap`, `atap_original_name`, `fsec`, `fsec_original_name`, `payment_receipt`, `payment_receipt_original_name`, `or_number`, `or_date`, `system_reference_no`, `ar_no`, `amount`, `payment_channel`, `payment_status`, `payment_trn_time`, `payment_reference_no`, `fsec_request`, `assessed_date`, `endorse_to_evaluation`, `evaluated_date`, `recommended_date`, `supplemental_docs`, `reviewed_date`, `approved_date`, `payment_date`, `issued_date`, `date_of_issuance`, `status`, `file_lock`, `user_id`, `evaluator_id`, `approver_id`, `final_approver_id`, `application_type`, `fsec_no`, `last_name`, `first_name`, `middle_name`, `tin`, `enterprise_name`, `ownership_form`, `house_number`, `street`, `barangay`, `city`, `zip_code`, `telephone_number`, `work_scope`, `other_work_scope`, `occupancy_character`, `other_occupancy_character`, `occupancy_classification`, `estimated_cost`, `units`, `construction_date`, `floor_area`, `expected_completion_date`, `architect_name`, `architect_address`, `architect_prc_no`, `architect_prc_validity`, `architect_ptr_no`, `architect_ptr_issued_date`, `architect_id_issued_at`, `architect_tin`, `lot_owner_name`, `lot_owner_address`, `lot_owner_ctc_no`, `lot_owner_ctc_issued_date`, `lot_owner_ctc_issued_place`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, 'BP24020001', '2024020001', '2024-02-08 09:17:29', '', '', 'supplemental_transmittal_letter_ruU3laKmNQ1oQSUPmTycylbUeux9adyNy8RQNB04t9FBaPzG5w.txt', 'stl.txt', '', '', 'supplemental_fer_ruU3laKmNQ1oQSUPmTycylbUeux9adyNy8RQNB04t9FBaPzG5w.txt', 'signed_iper.txt', '2024-02-08 04:30:39', 'order_payment_ruU3laKmNQ1oQSUPmTycylbUeux9adyNy8RQNB04t9FBaPzG5w.txt', 'iper.txt', 'endorsement_to_bfp_ruU3laKmNQ1oQSUPmTycylbUeux9adyNy8RQNB04t9FBaPzG5w.txt', 'sample1.txt', 'obo_endorsement_8iaU74Be7ppKwtehBZEgUMD4EdsWJX9MY3yLRUTyGX2GUiqKn2.txt', 'obo_endorsement.txt', 'atap_7bSpnikmbjLkMhbS7LsC0Hq3bJvSOt1k31O8OS6KWJi05c63yJ.txt', 'atap.txt', 'fsec_8iaU74Be7ppKwtehBZEgUMD4EdsWJX9MY3yLRUTyGX2GUiqKn2.txt', 'fsec.txt', 'payment_receipt_qRgKgQ3wZLBm3v0xcKnPinVk8sNPMehWAc2acPP2INxiCii0OF.txt', 'payment_receipt.txt', '555', '2024-02-14', 'TOPS020820241', NULL, 350486.75, NULL, NULL, NULL, NULL, 0, '2024-02-08 11:40:51', '2024-02-08 11:53:08', NULL, '2024-02-08 14:26:06', NULL, '2024-02-08 14:38:09', '2024-02-08 18:20:30', '2024-02-08 17:45:19', '2024-02-08 18:30:39', NULL, 9, 1, 1289, NULL, NULL, NULL, 1, '12345', 'SANCHEZ', 'RAM JAE', 'X', 'OFFICIA UNDE OFFICIA', 'HADASSAH WOOD', 'CORPORATION', '550', 'SINT DOLORE CULPA O', 'INCIDUNT AUT SIT ET', 'EIUS AUTE REPUDIANDA', '71815', NULL, 8, NULL, 0, NULL, 'CILLUM ALIQUID NON L', 'CORRUPTI MOLESTIAE', 'NON QUIS DO QUAERAT', '1991-03-24', 'RATIONE DUIS NATUS R', '1993-04-28', 'IFEOMA SHERMAN', 'ULLAM SOLUTA AD ATQU', 'ADIPISICING RERUM UT', '2024-02-08', 'LABORUM AT REM NON', '1975-10-04', 'QUI AUT NOSTRUD PARI', 'ET DOLORE EST VOLUPT', 'VELMA BASS', 'PERSPICIATIS POSSIM', 'ENIM IN EXERCITATION', '1974-06-28', 'EA IN QUAE COMMODO A', NULL, 1289, 1270, '2024-02-08 01:17:29', '2024-02-08 10:30:39', NULL);

-- Dumping structure for table tez.business_permit
DROP TABLE IF EXISTS `business_permit`;
CREATE TABLE IF NOT EXISTS `business_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filed_date` date DEFAULT NULL,
  `evaluated_date` date DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `file_lock` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `final_approver_id` int(11) DEFAULT NULL,
  `enterprise_classification` int(11) DEFAULT NULL,
  `primary_enterprise_type` int(11) DEFAULT NULL,
  `secondary_enterprise_type` int(11) DEFAULT NULL,
  `tourist_transport_type` int(11) DEFAULT NULL,
  `application_type` int(11) DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_type` int(11) DEFAULT NULL,
  `amendment_from` int(11) DEFAULT NULL,
  `amendment_to` int(11) DEFAULT NULL,
  `tax_incentive_from_govt` int(11) DEFAULT NULL,
  `govt_entity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trade_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_tel_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_email_add` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_mobile_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_tel_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_email_add` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_mobile_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_area` decimal(10,2) DEFAULT 0.00,
  `total_employees` int(11) DEFAULT NULL,
  `residing_with_lgu` int(11) DEFAULT NULL,
  `lessor_full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lessor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lessor_contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lessor_email_add` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_rental` decimal(10,2) DEFAULT 0.00,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.business_permit: ~0 rows (approximately)

-- Dumping structure for table tez.electrical_permit
DROP TABLE IF EXISTS `electrical_permit`;
CREATE TABLE IF NOT EXISTS `electrical_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `building_permit_id` int(11) DEFAULT NULL,
  `electrical_application` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_electrical_application` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_connection_load` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_transformer_capacity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_generator_capacity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_engineer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_engineer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_engineer_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_engineer_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_engineer_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_engineer_ptr_date_issued` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_engineer_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_engineer_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supervisor_classification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_supervisor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_supervisor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_supervisor_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_supervisor_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_supervisor_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_supervisor_ptr_date_issued` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_supervisor_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_supervisor_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.electrical_permit: ~0 rows (approximately)

-- Dumping structure for table tez.files
DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permit_type` int(11) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.files: ~54 rows (approximately)
INSERT INTO `files` (`id`, `code`, `name`, `permit_type`, `required`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, NULL, 'Environmental Compliance Certificate (ECC) or Certificate of Non-Coverage\r\n            (CNC) by the DENR.', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(2, NULL, 'Zoning/Locational Clearance from the FTEZ Administrator', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(3, NULL, 'TIEZA Certificate of Designation or Special Permit to Locate', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(4, NULL, 'Transfer Certificate of Title (TCT)', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(5, NULL, 'Contract of Lease', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(6, NULL, 'Deed of Sale', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(7, NULL, 'Secretary\'s Certification', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(8, NULL, 'TIEZA Permit Application Letter', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(9, NULL, 'TIEZA Certification of the designated Designers and Project', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(10, NULL, 'Engineers/Professionals in-charge of construction', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(11, NULL, 'Building Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(12, NULL, 'Architectural Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(13, NULL, 'Civil/Structural Permit Form-', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(14, NULL, 'Electrical Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(15, NULL, 'Plumbing Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(16, NULL, 'Sanitary Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(17, NULL, 'Mechanical Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(18, NULL, 'Electronics Permit Form ', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(19, NULL, 'PRC License', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(20, NULL, 'Professional Tax Receipt (PTR)', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(21, NULL, 'Project Cost and Estimate', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(22, NULL, 'Technical Specifications', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(23, NULL, 'Structural Anaysis and Design', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(24, NULL, 'Soil Boring Test', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(25, NULL, 'Construction Log Book', 3, NULL, NULL, 1, NULL, '2022-04-04 02:00:41', NULL, NULL),
	(26, 'SAMP1', 'TINDAHAN NAME', 1, 0, NULL, 1150, NULL, '2023-03-21 09:15:12', NULL, NULL),
	(27, NULL, 'BUILDING PLAN', 3, 0, NULL, 1218, NULL, '2023-03-22 06:12:30', NULL, NULL),
	(30, NULL, 'Transfer Certificate of Title (TCT)', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(31, NULL, 'Original Certificate of Title (OCT)', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(32, NULL, 'DENR Certification (Status of Land)', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(33, NULL, 'Brgy. Certificate as the rightful claimant/occupant', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(34, NULL, 'Relocation Survey', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(35, NULL, 'Survey Plan', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(36, NULL, 'Architectural Plan', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(37, NULL, 'Civil/Structural Plan', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(38, NULL, 'Plumbing Plan', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(39, NULL, 'Electrical Plan', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(40, NULL, 'Sanitary Plan', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(41, NULL, 'Mechanical Plan', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(42, NULL, 'Electronics Plan', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(43, NULL, 'Structural analysis and design for two (2) storey buildings/structures', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(44, NULL, 'Seismic analysis for three (3) storey buildings/structures', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(45, NULL, 'Boring Test', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(46, NULL, 'Load Test for three (3) storey buildings/structures and higher', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(47, NULL, 'Plumbing analysis and design for two (2) storey buildings and complex development', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(48, NULL, 'Short circuit analysis and voltage drop calculation for 70AT and above load', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(49, NULL, 'Mechanical design computations', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(50, NULL, 'Electronics analysis and design', 5, 0, NULL, 1, NULL, '2023-07-06 03:10:13', NULL, NULL),
	(51, 'samp', 'sample', 5, 0, NULL, 1214, NULL, '2024-02-07 06:26:16', NULL, NULL),
	(52, 'asdadsf', 'adfasdf', 5, 0, NULL, 1214, NULL, '2024-02-07 06:31:37', NULL, NULL),
	(55, 'IR', 'Supplemental requirements per initial assessment (Merged File)', 5, 0, NULL, 1214, NULL, '2024-02-07 06:49:00', NULL, NULL),
	(56, 'SD', 'Supplemental requirements per IPER (Merged File)', 5, 0, NULL, 1214, NULL, '2024-02-07 06:49:20', NULL, NULL),
	(57, 'SDP', 'Supplemental requirements per FPER (Merged File)', 3, 0, NULL, 1214, NULL, '2024-02-08 03:10:45', NULL, NULL);

-- Dumping structure for table tez.location_permit
DROP TABLE IF EXISTS `location_permit`;
CREATE TABLE IF NOT EXISTS `location_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filed_date` date DEFAULT NULL,
  `evaluated_date` date DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `file_lock` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `final_approver_id` int(11) DEFAULT NULL,
  `proponent_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `representative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_line` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_add` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_type` int(11) DEFAULT NULL,
  `other_application` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_type` int(11) DEFAULT NULL,
  `other_project` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `units` int(11) DEFAULT NULL,
  `storey` int(11) DEFAULT NULL,
  `lot_area` int(11) DEFAULT NULL,
  `floor_area` int(11) DEFAULT NULL,
  `ownership_type` int(11) DEFAULT NULL,
  `other_ownership` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `investment_cost` decimal(20,2) DEFAULT 0.00,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.location_permit: ~0 rows (approximately)

-- Dumping structure for table tez.mechanical_permit
DROP TABLE IF EXISTS `mechanical_permit`;
CREATE TABLE IF NOT EXISTS `mechanical_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `building_permit_id` int(11) DEFAULT NULL,
  `mechanical_application` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_mechanical_application` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_engineer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_engineer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_engineer_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_engineer_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_engineer_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_engineer_ptr_issued_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_engineer_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_engineer_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_supervisor_classification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_supervisor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_supervisor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_supervisor_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_supervisor_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_supervisor_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_supervisor_ptr_issued_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_supervisor_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanical_supervisor_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.mechanical_permit: ~0 rows (approximately)

-- Dumping structure for table tez.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.migrations: ~0 rows (approximately)

-- Dumping structure for table tez.occupancy_permit
DROP TABLE IF EXISTS `occupancy_permit`;
CREATE TABLE IF NOT EXISTS `occupancy_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filed_date` date DEFAULT NULL,
  `evaluated_date` date DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `file_lock` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `final_approver_id` int(11) DEFAULT NULL,
  `application_type` int(11) DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lot_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `block_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tct_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_declaration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.occupancy_permit: ~0 rows (approximately)

-- Dumping structure for table tez.plumbing_permit
DROP TABLE IF EXISTS `plumbing_permit`;
CREATE TABLE IF NOT EXISTS `plumbing_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `building_permit_id` int(11) DEFAULT NULL,
  `plumbing_application` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_plumbing_application1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_plumbing_application2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_plumbing_application3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `water_system` int(11) DEFAULT NULL,
  `sewage_system` int(11) DEFAULT NULL,
  `septic_tank` int(11) DEFAULT NULL,
  `drainage` int(11) DEFAULT NULL,
  `plumbing_engineer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_engineer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_engineer_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_engineer_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_engineer_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_engineer_ptr_issued_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_engineer_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_engineer_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_supervisor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_supervisor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_supervisor_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_supervisor_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_supervisor_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_supervisor_ptr_issued_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_supervisor_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plumbing_supervisor_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.plumbing_permit: ~0 rows (approximately)

-- Dumping structure for table tez.pre_evaluation_building_permit
DROP TABLE IF EXISTS `pre_evaluation_building_permit`;
CREATE TABLE IF NOT EXISTS `pre_evaluation_building_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `filed_date` datetime DEFAULT NULL,
  `for_assessment` datetime DEFAULT NULL,
  `assessed_by` int(11) DEFAULT NULL,
  `assessed_remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_administrator` datetime DEFAULT NULL,
  `confirm_by` int(11) DEFAULT NULL,
  `confirmed_remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `for_evaluation` datetime DEFAULT NULL,
  `evaluated_by` int(11) DEFAULT NULL,
  `evaluation_remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `back_to_administrator` datetime DEFAULT NULL,
  `accepted_by` int(11) DEFAULT NULL,
  `accepted_remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `back_to_assessor` datetime DEFAULT NULL,
  `final_assessed_by` int(11) DEFAULT NULL,
  `final_assessed_remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `returned_date` datetime DEFAULT NULL,
  `cancelled_date` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `return_status` int(1) DEFAULT NULL,
  `file_lock` int(1) DEFAULT NULL,
  `application_type` int(11) DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enterprise_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ownership_form` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `evaluator_remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transmittal_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transmittal_letter_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplemental_transmittal_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplemental_transmittal_letter_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iper_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplemental_iper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplemental_iper_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noncompliant_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noncompliant_letter_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `system_reference_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  `supplemental` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.pre_evaluation_building_permit: ~2 rows (approximately)
INSERT INTO `pre_evaluation_building_permit` (`id`, `user_id`, `filed_date`, `for_assessment`, `assessed_by`, `assessed_remarks`, `to_administrator`, `confirm_by`, `confirmed_remarks`, `for_evaluation`, `evaluated_by`, `evaluation_remarks`, `back_to_administrator`, `accepted_by`, `accepted_remarks`, `back_to_assessor`, `final_assessed_by`, `final_assessed_remarks`, `approved_date`, `returned_date`, `cancelled_date`, `status`, `return_status`, `file_lock`, `application_type`, `last_name`, `first_name`, `middle_name`, `tin`, `project_name`, `enterprise_name`, `ownership_form`, `house_number`, `street`, `barangay`, `city`, `zip_code`, `contact_number`, `evaluator_remarks`, `transmittal_letter`, `transmittal_letter_original_name`, `supplemental_transmittal_letter`, `supplemental_transmittal_letter_original_name`, `iper`, `iper_original_name`, `supplemental_iper`, `supplemental_iper_original_name`, `noncompliant_letter`, `noncompliant_letter_original_name`, `remarks`, `system_reference_no`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`, `supplemental`) VALUES
	(1, 1288, '2024-02-07 10:44:46', '2024-02-07 16:14:22', 1214, '1. test \r\n2. test', '2024-02-07 10:50:28', 1210, 'passed.', '2024-02-07 10:52:43', 1212, 'passed', '2024-02-07 11:09:00', 1210, NULL, '2024-02-07 11:21:59', NULL, NULL, NULL, '2024-02-07 17:49:09', NULL, 3, NULL, NULL, 1, 'MARSHALL', 'HAYLEY', 'SADE BEAN', 'DOLORIBUS ISTE AUTE', 'MELVIN RIGGS', 'JORDAN ROBBINS', 'SINGLE PROPRIETOR', '527', 'ELIGENDI SUSCIPIT EO', 'QUO EST LABORUM SED', 'CUMQUE BEATAE EUM DO', '51369', '317', NULL, 'transmittal_letter_TjD6qL7VsZrxLZD9DgxrvbMY5mfoVssNylGqtH5PebvT4kmsEr.txt', 'transmittal.txt', 'supplemental_transmittal_letter_LyTQ8vDQBsAwqwnUNOk0mxZv0mLn2CTzskkXhX5bUsD9LKFwU1.txt', 'siper.txt', 'iper_TjD6qL7VsZrxLZD9DgxrvbMY5mfoVssNylGqtH5PebvT4kmsEr.txt', 'iper.txt', 'supplemental_iper_LyTQ8vDQBsAwqwnUNOk0mxZv0mLn2CTzskkXhX5bUsD9LKFwU1.txt', 'stl.txt', '', '', NULL, NULL, 1288, 1214, '2024-02-07 02:44:46', '2024-02-07 09:49:09', NULL, NULL),
	(7, 1289, '2024-02-07 17:55:59', '2024-02-07 18:09:09', 1214, '1. plan 1\r\n2. plan 2.\r\n3. plan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2024-02-07 18:10:23', NULL, 3, NULL, NULL, 1, 'SANCHEZ', 'RAM JAE', 'X', 'OFFICIA UNDE OFFICIA', 'DARRYL KIM', 'HADASSAH WOOD', 'CORPORATION', '550', 'SINT DOLORE CULPA O', 'INCIDUNT AUT SIT ET', 'EIUS AUTE REPUDIANDA', '71815', '368', NULL, 'transmittal_letter_jAgNa5FMeYZlQzUUcsexB9Q6oaBOAWQIZBxgirgBZGwvTvKnm0.txt', 'transmittal.txt', 'supplemental_transmittal_letter_pJ3wMO2Rfr5s31DJK2v91l9JjL9BwuDHobf3RmFEqTt0qIM8nU.txt', 'stl.txt', 'iper_jAgNa5FMeYZlQzUUcsexB9Q6oaBOAWQIZBxgirgBZGwvTvKnm0.txt', 'iper.txt', 'supplemental_iper_pJ3wMO2Rfr5s31DJK2v91l9JjL9BwuDHobf3RmFEqTt0qIM8nU.txt', 'siper.txt', NULL, NULL, NULL, NULL, 1289, 1214, '2024-02-07 09:55:59', '2024-02-07 10:10:23', NULL, NULL);

-- Dumping structure for table tez.sanitary_permit
DROP TABLE IF EXISTS `sanitary_permit`;
CREATE TABLE IF NOT EXISTS `sanitary_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `building_permit_id` int(11) DEFAULT NULL,
  `sanitary_application` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_sanitary_application` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_engineer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_engineer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_engineer_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_engineer_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_engineer_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_engineer_ptr_date_issued` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_engineer_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_engineer_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_supervisor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_supervisor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_supervisor_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_supervisor_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_supervisor_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_supervisor_ptr_date_issued` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_supervisor_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitary_supervisor_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.sanitary_permit: ~0 rows (approximately)

-- Dumping structure for table tez.structural_permit
DROP TABLE IF EXISTS `structural_permit`;
CREATE TABLE IF NOT EXISTS `structural_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `building_permit_id` int(11) DEFAULT NULL,
  `structural_works` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_ptr_date_issued` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_engineer_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_supervisor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_supervisor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_supervisor_prc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_supervisor_prc_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_supervisor_ptr_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_supervisor_ptr_date_issued` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_supervisor_ptr_issued_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_supervisor_tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.structural_permit: ~1 rows (approximately)
INSERT INTO `structural_permit` (`id`, `building_permit_id`, `structural_works`, `civil_engineer_name`, `civil_engineer_address`, `civil_engineer_prc_no`, `civil_engineer_prc_validity`, `civil_engineer_ptr_no`, `civil_engineer_ptr_date_issued`, `civil_engineer_ptr_issued_at`, `civil_engineer_tin`, `civil_supervisor`, `civil_supervisor_address`, `civil_supervisor_prc_no`, `civil_supervisor_prc_validity`, `civil_supervisor_ptr_no`, `civil_supervisor_ptr_date_issued`, `civil_supervisor_ptr_issued_at`, `civil_supervisor_tin`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, 1, '1|8|10|11|12|14|16|', 'Halee Casey', 'Vel id assumenda ear', 'Praesentium est in', '2024-02-08', 'Proident quidem tem', '1990-06-27', 'Earum dolore obcaeca', 'Ipsum nostrud hic r', 'Halee Casey', 'Vel id assumenda ear', 'Praesentium est in', '2024-02-08', 'Proident quidem tem', NULL, 'Earum dolore obcaeca', 'Ipsum nostrud hic r', NULL, 1289, NULL, '2024-02-08 01:17:58', NULL, NULL);

-- Dumping structure for table tez.tct_informations
DROP TABLE IF EXISTS `tct_informations`;
CREATE TABLE IF NOT EXISTS `tct_informations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `building_permit_id` int(11) DEFAULT NULL,
  `pre_evaluation_id` int(11) DEFAULT NULL,
  `land_ownership` int(11) DEFAULT NULL,
  `lot_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `block_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tct_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_declaration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `land_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `land_title_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_declaration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_declaration_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `denr_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `denr_certificate_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay_certificate_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flagt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flagt_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authorization_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authorization_letter_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lease_contract` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lease_contract_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deed_of_sale` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deed_of_sale_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_to_sell` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_to_sell_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secretary_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secretary_certificate_original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.tct_informations: ~2 rows (approximately)
INSERT INTO `tct_informations` (`id`, `building_permit_id`, `pre_evaluation_id`, `land_ownership`, `lot_number`, `block_no`, `tct_no`, `tax_declaration_no`, `construction_street`, `construction_barangay`, `construction_city`, `land_title`, `land_title_original_name`, `tax_declaration`, `tax_declaration_original_name`, `denr_certificate`, `denr_certificate_original_name`, `barangay_certificate`, `barangay_certificate_original_name`, `flagt`, `flagt_original_name`, `authorization_letter`, `authorization_letter_original_name`, `lease_contract`, `lease_contract_original_name`, `deed_of_sale`, `deed_of_sale_original_name`, `contract_to_sell`, `contract_to_sell_original_name`, `secretary_certificate`, `secretary_certificate_original_name`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, NULL, 1, 3, '505', 'MAGNAM UT EX EOS PER', 'OMNIS MODI EOS VOLU', 'PARIATUR NEQUE NUMQ', 'ET DOLOR QUI QUIS OF', 'MAXIME QUO AD PORRO', 'ILLO CORPORIS OMNIS', 'land_title_hvNNq89q7IbiTnRoyX56y5WlJn51rfDQl56pyywVc7Y1AM1nC7.txt', 'sample1.txt', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, 1288, NULL, '2024-02-07 02:45:09', NULL, NULL),
	(2, NULL, 7, 3, '349', 'DISTINCTIO REM ELIG', 'SAPIENTE PROIDENT N', 'EIUS DUIS IMPEDIT O', 'QUI MINIMA REM IN UL', 'EXERCITATIONEM LABOR', 'VENIAM EXPEDITA ITA', 'land_title_Ccupq6gkmC5vtRwaagFzeZF4hJRz3W9KE5al9PKCrWbvSW27IA.txt', 'payment_receipt.txt', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, 1289, NULL, '2024-02-07 09:57:42', NULL, NULL);

-- Dumping structure for table tez.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `evaluator` int(11) DEFAULT NULL,
  `approver` int(11) DEFAULT NULL,
  `issuer` int(11) DEFAULT NULL,
  `permit_type` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL DEFAULT 1,
  `first_login` tinyint(4) NOT NULL DEFAULT 0,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1291 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.users: ~76 rows (approximately)
INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `middle_name`, `email`, `username`, `password`, `otp`, `level`, `evaluator`, `approver`, `issuer`, `permit_type`, `remember_token`, `user_status`, `first_login`, `position`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `is_deleted`) VALUES
	(1, 'admin', '', '', '', '', 'admin', '$2y$10$5FiMRgOPGD20oPTyHDdOAem4OmZg89Et2bL9NYOdB7CVMbgdJVPYC', NULL, 0, NULL, NULL, NULL, NULL, 'QzEwqcrcmYStIRr1gKyk3YfWRxxLjQZEy2z4du2vqsGwlmrPDi5tiexZ0KBs', 1, 0, '0', '2019-10-01 17:45:17', '2019-10-01 17:45:17', NULL, 0, NULL, NULL),
	(1148, 'Emmanuel Placido', '', '', '', '', 'applicant', '3OPGh.UXlW7iI', NULL, 1, NULL, NULL, NULL, 3, 'rDPYcLhjorbxSOdZrJWCzoiM0kgQum7MxCZRnIgRcpiYTfJbJ3iFmABnVajQ', 1, 0, '1', '2022-04-04 10:15:21', NULL, NULL, 1, NULL, NULL),
	(1149, 'Rommel Salvador', '', '', '', '', 'assessor', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, '8WNy5I2OErZbW1CuhViBmpeQ6CVKkxxkanubwjQwevEyBpjg78bIrkosFksK', 1, 0, '2', '2022-04-04 10:15:21', NULL, NULL, 1, NULL, NULL),
	(1150, 'Maria Angelita Simbulan', '', '', '', '', 'evaluator', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, 'vOvrzv1DNNsemrWyLE5CwECORoLwXLcwZdK9oeAGTWUQokpTNM2I6M6ShPku', 1, 0, '3', '2022-04-04 10:15:21', NULL, NULL, 1, NULL, NULL),
	(1151, 'Raquel Dela Cruz', '', '', '', '', 'recommendator', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, '4DsrJ3XAJJhmhWQMXWSCIZSVZ0E0UGeQ5mH7AoKWJKV8HMBNGGYaAzgFrNYy', 1, 0, '4', '2022-04-04 10:15:21', NULL, NULL, 1, NULL, NULL),
	(1152, 'John Domingo', '', '', '', '', '1991-0003-P', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, '7I1uMimFk2Pmaa7WvzNTaittyMZjk5rotYOHNVwDnUZiJKNR25dvHMuZN4fJ', 1, 0, '12|', '2022-04-04 10:15:21', '2024-02-01 19:17:42', NULL, 1, 1279, NULL),
	(1208, 'emmanuel placido', 'emmanuel', 'placido', 'santiago', 'emmanplacido.me@gmail.com', 'emman', '3OPGh.UXlW7iI', NULL, 1, NULL, NULL, NULL, NULL, '', 1, 0, '1', '2023-03-20 17:48:47', NULL, NULL, 1, NULL, NULL),
	(1209, 'DARYL JERUZ', 'DARYL', 'JERUZ', '', 'emmanplacido.me@gmail.com', 'daryl', '3OPGh.UXlW7iI', NULL, 1, NULL, NULL, NULL, NULL, '9xPBXkdHgjuptZA7oCa5ZvezstNOhrzO1DQ1MiEFT6y9VUqBbVOpq7q8866v', 1, 0, '1', '2023-03-21 06:55:00', NULL, NULL, 1, NULL, NULL),
	(1210, 'BERNARDO CASTELLANO ALARILLA', '', '', '', '', '2015-0151-P', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, 'LIbvQbslZ5SiYHL0JTpVbGWFafvFLHnS0PEYtLkIQLnUvmRDg3LtNVu9jLa0', 1, 0, '2|5|8|11|', '2023-03-21 17:17:59', '2023-10-18 00:08:12', NULL, 1, 1217, NULL),
	(1211, 'JONNA GRACE SALCEDO BACUEL', '', '', '', '', '2022-0061-NP', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, 'QWWd7sBDBfb7GN66omkzhSxqea8pN8nDtsOvzRsrw2JtQbA2q5SfuYcWTcll', 1, 0, '2', '2023-03-21 17:17:59', NULL, NULL, 1, NULL, NULL),
	(1212, ' JOVEN SALMORIN BIGOY', '', '', '', '', '2019-0036-P', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, 'G0kVxeY6Aq7RhsVfgAoQIghBcmxKFtbP4nlDUpFcU2UAY1QQMmxGEN5AZQH3', 1, 0, '3|4|9|10|', '2023-03-21 17:17:59', '2023-10-18 00:07:55', NULL, 1, 1217, NULL),
	(1213, 'RICHARD RAMOS VEGA', '', '', '', '', '2022-0087-NP', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, '', 1, 0, '5', '2023-03-21 17:17:59', NULL, NULL, 1, NULL, NULL),
	(1214, 'GRACE RODRIGUEZ CINCO', '', '', '', '', '2019-0035-P', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, 'HZ33F5a4s3EgQApXnmHmXhTMtKr5DLshFlK1xYP5W3CnzNk4Pi0yIkUY4jhw', 1, 0, '1|6|7|8|9|10|', '2023-03-21 17:17:59', '2024-02-07 02:12:43', NULL, 1, 1, NULL),
	(1215, 'IVAN JASFER MACOLA PABLICO', '', '', '', '', '2021-0002-P', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, 'HWeQM7Xz28DsicVxVt2uq7vCKicnwZI6bFluQu31uDGO26NKGfkaAxSMPzr2', 1, 0, '5', '2023-03-21 17:17:59', NULL, NULL, 1, NULL, NULL),
	(1216, 'ROYTHER PALMA PABLICO', '', '', '', '', '2020-0041-NP', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, 'dQXim4ZH7tM9A0nbdlKZWUtFG1bYLN5YuSBWxdireq9ZL0VnHo4ovnL6KWMJ', 1, 0, '3|9|', '2023-03-21 17:17:59', '2023-10-18 00:10:30', NULL, 1, 1217, NULL),
	(1217, 'MERICRIS CAYABYAB TONIO', '', '', '', '', '2017-0020-P', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, 'zrzIDakQEVuFw6qeMkY4IOjm88t5SHQd7otFA1ufdoB4SP0rSfI2lJmHB9Q7', 1, 0, '5', '2023-03-21 17:17:59', NULL, NULL, 1, NULL, NULL),
	(1218, 'RANDY RULF GACAYAN RADAM', '', '', '', '', '2020-0056-NP', '$2y$10$ucuSKN9gv0RjdkQwt2k06.CK6o9YN38tqiOvNqu7KpPzXj9io1hK6', NULL, 2, NULL, NULL, NULL, 3, '0YBZKMQZla0gcOgQJa9REjVAeeHm0WOSS3g5NqyAyZewkdr7gm1MK0Uw2sW9', 1, 0, '3|7|9|', '2023-03-21 17:17:59', '2023-10-18 01:11:32', NULL, 1, 1218, NULL),
	(1219, 'GRACE CINCO', 'GRACE', 'CINCO', 'RODRIGUEZ', 'gracercinco@yahoo.com', 'missgracey', '3OPGh.UXlW7iI', NULL, 1, NULL, NULL, NULL, NULL, 'Vpu6QNOZC1eF78rxlDT8a2LXfeNvV5knRANgm6TUyly6beWASPmGyE6HCkg8', 1, 0, '1', '2023-03-21 18:19:31', NULL, NULL, 1, NULL, NULL),
	(1220, 'JAMAICA CHLOE FERNANDO', '', '', '', '', '2015-0153-P', '3OPGh.UXlW7iI', NULL, 2, NULL, NULL, NULL, 3, '9IBGQwN5Zh6i8oyj7CvmrkUZKky3qkUyaa2EXaY4vIurDa0Vh5C68XelfZE3', 1, 0, '9|13|', '2023-03-21 17:17:59', '2024-01-31 17:37:04', NULL, 1, 1256, NULL),
	(1221, 'RESALYN PIOJO', 'RESALYN', 'PIOJO', 'ASAA', 'resalynpiojo@yahoo.com', 'resalynpiojo@', '3OPGh.UXlW7iI', NULL, 1, NULL, NULL, NULL, NULL, 'h3626Z3mj48sWV8ZfXNdY50HDzTgcwFnm4dU0eqlrItAotv0k8a85ksB1zBW', 1, 0, '1', '2023-03-22 22:44:06', NULL, NULL, 1, NULL, NULL),
	(1222, 'MERICRIS TONIO', 'MERICRIS', 'TONIO', 'CAYABYAB', 'mctonio2.tieza@gmail.com', '123456', '3OPGh.UXlW7iI', NULL, 1, NULL, NULL, NULL, NULL, '', 1, 0, '1', '2023-04-02 00:33:39', NULL, NULL, 1, NULL, NULL),
	(1223, 'JAJA AGUSTA', 'JAJA', 'AGUSTA', 'C', 'cyber-terror187@protonmail.com', 'Jaja', '3OPGh.UXlW7iI', NULL, 1, NULL, NULL, NULL, NULL, '', 1, 0, '1', '2023-05-09 18:50:33', NULL, NULL, 1, NULL, NULL),
	(1228, 'ALLAN QUEZON', 'ALLAN', 'QUEZON', '', 'allanquezon890@gmail.com', 'allan10k', '3OPGh.UXlW7iI', NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 0, '1', '2023-06-13 20:56:08', NULL, NULL, 1, NULL, NULL),
	(1229, 'MERICRIS TONIO', 'MERICRIS', 'TONIO', 'CAYABYAB', 'mctonio2.tieza@gmail.com', 'Mericris_12', '3OPGh.UXlW7iI', NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 0, '1', '2023-06-13 21:11:31', NULL, NULL, 1, NULL, NULL),
	(1230, 'HS\' SGA\'', 'HS\'', 'SGA\'', 'HAHA\'', 'g@gmail.com', 'Nnjaj\'', '3OPGh.UXlW7iI', '532288', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-13 23:18:15', NULL, NULL, 1, NULL, NULL),
	(1231, 'KATHLEEN MCGUIRE', 'KATHLEEN', 'MCGUIRE', 'ESPENIDA', 'gracercinco@yahoo.com', 'ms. kathleen', '3OPGh.UXlW7iI', '314117', 1, NULL, NULL, NULL, NULL, '4c8PIbC9Ih9w75uCVoUNjDhbWeeOxly1KRxOuseyhn4DloZy6LHh6onFT41d', 1, 1, '1', '2023-06-13 23:24:52', NULL, NULL, 1, NULL, NULL),
	(1232, 'KK K', 'KK', 'K', 'K', 'gabobolbol@gmail.com', 'okok99', '3OPGh.UXlW7iI', '470986', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-14 00:01:32', NULL, NULL, 1, NULL, NULL),
	(1233, 'NENEVE VURUM', 'NENEVE', 'VURUM', 'KENAN', 'eamilfeyziyev@gmail.com', 'r4m1l', '3OPGh.UXlW7iI', '539373', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-14 00:43:19', NULL, NULL, 1, NULL, NULL),
	(1234, 'ZOSUMISKI CAPITLE', 'ZOSUMISKI', 'CAPITLE', 'NOENA', 'usminyakapitle@gmail.com', 'usminyakap2152', '3OPGh.UXlW7iI', '529001', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-14 07:34:29', NULL, NULL, 1, NULL, NULL),
	(1235, 'IRON MAN', 'IRON', 'MAN', 'TO', 'fopsinusta@gufum.com', 'iron', '3OPGh.UXlW7iI', '100819', 1, NULL, NULL, NULL, NULL, 'DUyO2TlVKYtDLoYXHS849cY3a2cUxtCnqfUE692OUZFKh1Edds31YD1iLLRj', 1, 1, '1', '2023-06-14 11:55:05', NULL, NULL, 1, NULL, NULL),
	(1236, 'MERICRIS TONIO', 'MERICRIS', 'TONIO', 'CAYABYAB', 'mctonio.tieza@gmail.com', 'MERICRISTONIO', '3OPGh.UXlW7iI', '623421', 1, NULL, NULL, NULL, NULL, 'm4JpgTcYUwOltJRLmfLNnaONQLOJsYG89lesuSO8uvdC4O040t6G7p84pPIf', 1, 1, '1', '2023-06-19 00:11:45', NULL, NULL, 1, NULL, NULL),
	(1237, 'KATHLEEN MCGUIRE', 'KATHLEEN', 'MCGUIRE', 'ESPENIDA', 'gracercinco@yahoo.com', 'ms.kathleen2', '$2y$10$BJG.BBQeqbkiGFVpotNOFuEB5eLZzNizbLHZAd.UVJRnCfuApQkNO', '129034', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-25 20:35:40', NULL, NULL, 1, NULL, NULL),
	(1238, 'KATHLEEN MCGUIRE', 'KATHLEEN', 'MCGUIRE', 'ESPENIDA', 'gracercinco@yahoo.com', 'ms.kathleen2', '$2y$10$4wwNXmGa8lN6uzAwF43jR.QLbHA/cqiEmNoyFmpcBZUeuHLyDlX3q', '126014', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-25 20:35:44', NULL, NULL, 1, NULL, NULL),
	(1239, 'KATHLEEN MCGUIRE', 'KATHLEEN', 'MCGUIRE', 'ESPENIDA', 'gracercinco@yahoo.com', 'ms.kathleen2', '$2y$10$DYdTpL6.eTvW8Xr34piZCeU1xFZOMJjRm8L7b7hh7FxtmZ6hXB11e', '786531', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-25 20:36:04', NULL, NULL, 1, NULL, NULL),
	(1240, 'KATH MCGUIRE', 'KATH', 'MCGUIRE', 'ESPENIDA', 'gracercinco@yahoo.com', 'ms.kathleen2', '$2y$10$spK2dUHx/dnxhfHBzcrF7.Ol6im2gdu3TgSwJm.nihWtbqZxbUMZC', '632228', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-25 20:36:37', NULL, NULL, 1, NULL, NULL),
	(1241, 'KATH MCGUIRE', 'KATH', 'MCGUIRE', 'ESPENIDA', 'gracercinco@yahoo.com', 'ms.kathleen2', '$2y$10$V7eQQi7pjZ.SPGJOJr/hv.4Ls0QSSl0YJAyLjrgq/IQUad.kxVQy2', '206180', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-25 20:36:45', NULL, NULL, 1, NULL, NULL),
	(1242, 'KATH MCGUIRE', 'KATH', 'MCGUIRE', 'ESPENIDA', 'gracercinco@yahoo.com', 'ms.kathleen2', '$2y$10$cMFT6fhpueNYjeFVdDXvMewCQCo5GrTVXJx2OL9VcUK4Q0.FMqzSi', '888962', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-25 20:38:00', NULL, NULL, 1, NULL, NULL),
	(1243, 'JAN PAUL RODRIGUEZ', 'JAN PAUL', 'RODRIGUEZ', 'CASTRO', 'gracercinco@yahoo.com', 'JP RODRIGUEZ', '$2y$10$n1Na84z7eCcFo/iHItiGc.0lbOyUEgp1sxjqsk7BsgrHgqTQH91j2', '114963', 1, NULL, NULL, NULL, NULL, 'XYaxuxxVNQGhQ3fG9SecC1HhA7tQ7tPpYShFZckIHawjqT18vl7eBxbKBp5s', 1, 1, '1', '2023-06-25 20:53:30', NULL, NULL, 1, NULL, NULL),
	(1244, 'JAN PAUL RODRIGUEZ', 'JAN PAUL', 'RODRIGUEZ', 'CASTRO', 'gracercinco@yahoo.com', 'JP RODRIGUEZ', '$2y$10$3boK3jVEG3KVI84vwT1Z9e9SSP5jL6awNZnrKO1b1IAdXNbKNTvGG', '499560', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-25 20:55:45', NULL, NULL, 1, NULL, NULL),
	(1245, 'JAN PAUL RODRIGUEZ', 'JAN PAUL', 'RODRIGUEZ', 'CASTRO', 'gracercinco@yahoo.com', 'JP RODRIGUEZ', '$2y$10$oFEAw7E/5VAchpe.DT28ZObYGPMi2FVzemXQSft0tFyqrVxRU6fDK', '404768', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-25 21:00:19', NULL, NULL, 1, NULL, NULL),
	(1246, 'JAN PAUL RODRIGUEZ', 'JAN PAUL', 'RODRIGUEZ', 'CASTRO', 'gracercinco@yahoo.com', 'JP RODRIGUEZ', '$2y$10$usztD3Fj9EvJMaFD6sZt5OeErGyfgOy0NMtGzc4KdrBCpacoqOUKe', '664109', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-25 21:15:38', NULL, NULL, 1, NULL, NULL),
	(1247, 'RANDY RULF RADAM', 'RANDY RULF', 'RADAM', 'GACAYAN', 'rulf95@gmail.com', 'Rasheed', '$2y$10$ztUYlu2BA2H/.5fGZCQf5eTzwg8IlvFyUXJdYPmgFb8stbEfLcbEy', '931014', 1, NULL, NULL, NULL, NULL, 'sYhzfcr7WwqqCRRdtqV3ADf3mrPXCMDhnOdr4shv0FSBcQnetCQDownJDfCm', 1, 1, '1', '2023-06-25 22:38:22', NULL, NULL, 1, NULL, NULL),
	(1248, 'EMMANUEL PLACIDO', 'EMMANUEL', 'PLACIDO', 'SANTIAGO', 'emmanplacido.me@gmail.com', 'emman', '$2y$10$MHTZfMWyxoKUepE815AnQOcnqk5Gq4WVEgavSd.TKCQaVBLrK5ndi', '656280', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-26 18:13:09', NULL, NULL, 1, NULL, NULL),
	(1249, 'EMMANUEL PLACIDO', 'EMMANUEL', 'PLACIDO', 'SANTIAGO', 'emmanplacido.me@gmail.com', 'placido', '$2y$10$hNRD8OCbMR/10haFIJRK9eIPiV8vXGWrIZa7m0FyVsGlO5kjEcnFq', '495337', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-26 18:14:13', NULL, NULL, 1, NULL, NULL),
	(1250, 'RENZ PLACIDO', 'RENZ', 'PLACIDO', 'SANTIAGO', 'emmanplacido.me@gmail.com', 'renz', '$2y$10$ngZF2uoSkWScxjDZmcmqF.dqHpzfVvgstr8mpONGpk3EeuefTfEma', '657118', 1, NULL, NULL, NULL, NULL, '8VHpJZrDtlz0JvHibQ1D16PWwVUx5UdK3MIfFFVBvA1dFgu2nog8oqYbWU56', 1, 1, '1', '2023-06-26 21:21:33', NULL, NULL, 1, NULL, NULL),
	(1251, 'JOHN DOE', 'JOHN', 'DOE', '', 'emmanplacido.tieza@gmail.com', 'tez', '$2y$10$lNMhXnL8wCuAImYJw.3Wu.oGat9EXG03zDzFFNNxi0Fo4nEOnPVm6', '790580', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-26 23:17:06', NULL, NULL, 1, NULL, NULL),
	(1252, 'GRACE QUATRO', 'GRACE', 'QUATRO', 'RAMIREZ', 'mctonio2.tieza@gmail.com', 'mctonio2.tieza@gmail.com', '$2y$10$yVbVAD/npm.k8KSvh4TdS.pKEzK8fjgJhf2ea8YrTDBf4ZtrJOBeW', '533851', 1, NULL, NULL, NULL, NULL, 'B6XycPm8nCzjHGiubAussm1PtdzK1l3yk6q0UR6XdpPULDZPJIXIceBUJtpQ', 1, 1, '1', '2023-06-27 00:48:57', NULL, NULL, 1, NULL, NULL),
	(1253, 'MERICRISSY TONIO', 'MERICRISSY', 'TONIO', 'CAYABYAB', 'mctonio2.tieza@gmail.com', 'tez12345', '$2y$10$tkPZ2qj3Ss4Yg4/7bcDMe.SFWYBqT2k/EUZKMHTmHvLEgYUjC8Az6', '186777', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-06-27 00:56:10', NULL, NULL, 1, NULL, NULL),
	(1254, 'MERICRIS TONIO', 'MERICRIS', 'TONIO', 'CAYABYAB', 'mctonio2.tieza@gmail.com', 'mericris.tonio', '$2y$10$UHvtedRljVheVetxuDyUvO9jiFYGgCb1QInDchpS654K/9.s2qn/O', '200618', 1, NULL, NULL, NULL, NULL, 'XoFJhBb8M2dEG1pTpmv18aFZX0WOsc6Je8b9zi1O4uuTTllau4wHatM5YVe2', 1, 1, '1', '2023-06-28 00:14:25', NULL, NULL, 1, NULL, NULL),
	(1255, 'JAMAICA CHLOE TAGUINOD FERNANDO', NULL, NULL, NULL, NULL, '2015-0153-P', '$2y$10$B5lJVO7JnYFVvckGL2XVy.9iEv9ERHSX8.xfZTRsetF60iC5WgCA.', NULL, NULL, NULL, NULL, NULL, 3, NULL, 1, 0, '2', '2023-06-28 21:13:09', NULL, NULL, 1, NULL, NULL),
	(1257, 'EMMAN PLACIDO', 'EMMAN', 'PLACIDO', '', 'emmanplacido.tieza@gmail.com', 'emman1', '$2y$10$ZyIHPaHjLIlu9YEBR.qtSOiy3hEC39AbnNAT/1ZcZgrmdaMlRO0yG', '742167', 1, NULL, NULL, NULL, NULL, 'eTt3WWTQEfDXeG2AAbb4mpTn4EVfsRQdUSGGTGsDF1FqgloxOVdp01Ki7Gqg', 1, 1, '1', '2023-07-10 23:09:47', NULL, NULL, 1, NULL, NULL),
	(1258, 'ANAIS BONAMY', 'ANAIS', 'BONAMY', 'J.', 'gracercinco@yahoo.com', 'Bonamy', '$2y$10$KA4QBh6X0qdY6Bfvmn3IieW6uFXToy2pXTluDomJJkS3LBf3g4yFm', '372945', 1, NULL, NULL, NULL, NULL, 'ayIjGfF3HjgUm8EdNioU2IBWfwFMJ1X5u9XMWitVsmHrW0VnMrZZwgFrd7CP', 1, 1, '1', '2023-09-04 19:00:20', NULL, NULL, 1, NULL, NULL),
	(1259, 'ZANNE SALCEDO', 'ZANNE', 'SALCEDO', 'GO', 'PHOTOZANNETHESIS@GMAIL.COM', 'ZANNESALCEDO', '$2y$10$GDTWx3LZIcXd8w9.TX1N9.dnzr3n5tvbJWu4.g5KIVgKFkv.ockGi', '989714', 1, NULL, NULL, NULL, NULL, 'nGwhXfHYpuMcWMiQwjL23oBZ23UdbFKwcKo1limTDBCQPX4eWtEVQFJjBaKM', 1, 1, '1', '2023-09-06 00:06:04', NULL, NULL, 1, NULL, NULL),
	(1260, 'ANAIS BONAMY', 'ANAIS', 'BONAMY', 'C.', 'gracercinco@yahoo.com', 'anais', '$2y$10$WxGg2mdXpmUMQfg.3Cxrf.oCL4AShpIUbty7JY4X.pug.oKvNfOQu', '742258', 1, NULL, NULL, NULL, NULL, 'qHgUzbtaz9BHNmQ7ysTgzUBSwYo7rr08lwET6EIcdbOVObX9u3g7tpE5FeDy', 1, 1, '1', '2023-09-19 22:22:33', NULL, NULL, 1, NULL, NULL),
	(1261, 'VICTOR PANLILIO', 'VICTOR', 'PANLILIO', 'C', 'znpermittingtriala@gmail.com', 'VICTOR PANLILIO', '$2y$10$NTvxwjt0gA4/TKStNiT0XOtsdCHiUmRieAMP985wNd.u0yKWlEenG', '710443', 1, NULL, NULL, NULL, NULL, 'j93rsd0YpzIVZJ5yTrvOivphWNZ1zri7wO1mGEP8nlLYVI3bA7IBpAdisY3k', 1, 1, '1', '2023-10-08 22:40:05', NULL, NULL, 1, NULL, NULL),
	(1262, 'MERICRIS TONIO', 'MERICRIS', 'TONIO', 'CAYABYAB', 'mctonio.tieza@gmail.com', 'mctonio.tieza@gmail.com', '$2y$10$T2nZs4pb.4YHZYLX6o9kHeaTDCxwbgiNlzxVe2ioB.YoDjmyl6SY6', '322100', 1, NULL, NULL, NULL, NULL, '7u5Yr3PJDYZsNtYnERPlN0jQRkp67iHnDcr1e1mhvjziIPXtm9RlmOqRuRMh', 1, 1, '1', '2023-10-16 19:29:55', NULL, NULL, 1, NULL, NULL),
	(1263, ' ', '', '', '', NULL, NULL, '$2y$10$cdSG0RLwWmFVASODia1stOhWckAhsPO.U/7OuJYEcEmToCiFL.h5q', '151542', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-10-30 15:51:47', NULL, NULL, 1, NULL, NULL),
	(1264, 'ANAIS BONAMY', 'ANAIS', 'BONAMY', 'E', 'gracercinco@yahoo.com', 'Ms. Bonamy', '$2y$10$StKK.jUN2JCrkrDvkc2Pzuwrz/HLry/sYZM.cwFf0DxAVk2KyoGEC', '949582', 1, NULL, NULL, NULL, NULL, 'nJFnI6n8UZkZjNennB7V2chmZUIbOTnWZsrrmG5gy2d9Livx6NKmfWEIxlT3', 1, 1, '1', '2023-11-14 00:43:52', NULL, NULL, 1, NULL, NULL),
	(1265, 'MERCY JOY PANGADLO', 'MERCY JOY', 'PANGADLO', 'COLLANTES', 'pangadlomercyjoy@gmail.com', 'MJ@121896', '$2y$10$PfXqp58.x.ISaVzfjQfuou8PapwBd9tUxr..aXWneODUPkqZUSIBe', '240132', 1, NULL, NULL, NULL, NULL, 'REQeR91vvPeHSuUGYVGWUhtUSHZo6Xj0U2yJuGuWkLYm67JLu5MuJ2LxR5cJ', 1, 1, '1', '2023-11-19 20:27:59', NULL, NULL, 1, NULL, NULL),
	(1266, 'CEZANNE GO', 'CEZANNE', 'GO', '', 'photozannethesis@gmail.com', 'Cezanne Go', '$2y$10$LCElEEURBHt2yuVxqtcJN.GZF1PrownttQlepeplyBFgtdo11OhPa', '685302', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-11-19 20:49:15', NULL, NULL, 1, NULL, NULL),
	(1267, 'MURAGYAW ASD', 'MURAGYAW', 'ASD', 'ASDAWD', 'lieutenantunix@gmail.com', 'muragyawa', '$2y$10$uLpF0P4tDQXsdrfrC3M4P.3z4s2S4vGQzh6hfd38jRiLmkdKgad5i', '787179', 1, NULL, NULL, NULL, NULL, 'sqmbVCnadRwfo5bQeMnzumusrXgJJewr56bED7xGW05oALAEOOqcV5JdsieO', 1, 1, '1', '2023-11-25 01:36:48', NULL, NULL, 1, NULL, NULL),
	(1268, 'WILLIAM BUNGALSO', 'WILLIAM', 'BUNGALSO', 'G', 'gracercinco@yahoo.com', 'bungalso', '$2y$10$AS64lBMGOPP/oNvBzu3J5eSSjpF.q2MK6jZ6yOVpt7pjhqT.jWeIG', '819452', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-11-30 22:52:11', NULL, NULL, 1, NULL, NULL),
	(1269, 'Mercy Joy Pangadlo', 'Mercy Joy', 'Pangadlo', NULL, NULL, '2023-0106-NP', '$2y$10$Q7/Ys1WqQcigeVj3wiS96ehGLtwCiX6PwtPmomW61Jh1F36i5AWoy', NULL, 0, NULL, NULL, NULL, 1, NULL, 1, 0, '3|', '2023-12-03 22:38:40', '2023-12-03 22:39:04', NULL, 1, 1, NULL),
	(1270, 'Jesiel Rubrico', 'Jesiel', 'Rubrico', 'Darasin', NULL, '2019-0024-P', '3OPGh.UXlW7iI', NULL, 0, NULL, NULL, NULL, 1, NULL, 1, 0, '10|11|12|13|', '2023-12-03 22:39:37', '2024-02-08 09:17:58', NULL, 1, 1270, NULL),
	(1271, 'FEW ERGE', 'FEW', 'ERGE', 'WGQEW', 'asd@asd.com', 'asdasd', '$2y$10$NeqwyAlKGMJnuvqkjjwlhOsDpQBGKv06gDYbW06jAckh1x6smRYme', '815234', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-12-15 05:11:00', NULL, NULL, 1, NULL, NULL),
	(1272, 'KURUMI TOKISAKI', 'KURUMI', 'TOKISAKI', 'NIGHTMARE', 'nightmarenbsec@gmail.com', 'nightmare', '$2y$10$8tBA7ripD.PCuSIVBDRf3Oyf7YO0UiVw7x5RhL2t8dmNe5Marz43O', '850817', 1, NULL, NULL, NULL, NULL, 'DLEiLwZF1VJd1soScrMHAX2L4JGHA1VEzIs3yJu5uIlOMudbL9K03jYXNGZG', 1, 1, '1', '2023-12-24 20:22:45', NULL, NULL, 1, NULL, NULL),
	(1273, 'NNJ NNN', 'NNJ', 'NNN', 'NNN', 'alexander.govph@gmail.com', 'Pesty.gov', '$2y$10$zQjjF1RCGfbe9l4E.OLz8uaE8XAFgr0V9tomr62gNGHNa20KuwpLu', '900241', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-12-26 13:28:03', NULL, NULL, 1, NULL, NULL),
	(1274, 'GGG VVVV', 'GGG', 'VVVV', 'VVVVVVCC', 'mangblat@gmail.com', 'mangblat', '$2y$10$7wTnZZNQJr3Ab6bL1k8ErePT2X2etyrQROFcYrYLk8A19mLaqOtey', '558700', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2023-12-26 13:30:46', NULL, NULL, 1, NULL, NULL),
	(1275, 'ADMINISTRATOR ADMINISTRATOR', 'ADMINISTRATOR', 'ADMINISTRATOR', 'ADMINISTRASI', 'combetohct@yahoo.com', 'combetohct', '$2y$10$GY8iOIDAEs/qP25yxUx3RerimnVrzUdnYujeYQ0MCCrN4ClNd34k6', '113329', 1, NULL, NULL, NULL, NULL, 'q5is7RSBL059F46K9eLnMSFlZMKUx5XpU8co30egAmxo8d3UNmLnuqMuOE8f', 1, 1, '1', '2024-01-21 12:03:02', NULL, NULL, 1, NULL, NULL),
	(1276, 'Rhona Herrera', 'Rhona', 'Herrera', 'Shelby Stewart', NULL, 'sv_assessor', '$2y$10$0Jh5ylkmUGa1hreIYs836uhZaB.Zfh9B4hukMQJEXyAW2YrAj7NDK', NULL, 0, NULL, NULL, NULL, 3, '3F1O2u9Wct0SCOazEWqS4I3Jsmbtlq8WXiTGbjKnb0tWGsp6bUwuO4rirXzd', 1, 0, '1|6|7|8|9|10|', '2024-02-01 18:33:27', '2024-02-01 19:36:21', NULL, 1, 1276, NULL),
	(1277, 'EMMAN PLACIDO', 'EMMAN', 'PLACIDO', 'S', 'emmanplacido.tieza@gmail.com', 'tez_emman', '$2y$10$O37SUpPjsC02xfpnPRRCDONc7zfq7B3X4VwaKbWEWv3LiU9h8s052', '994900', 1, NULL, NULL, NULL, NULL, 'XbcNWSAdg7WpZCdO29CLuYkl0zKGkww5RkBmCn8Wo11dcvtcSM41lu7fQQ23', 1, 1, '1', '2024-02-01 18:34:58', NULL, NULL, 1, NULL, NULL),
	(1278, 'Ali Mejia', 'Ali', 'Mejia', 'Hall Jarvis', NULL, 'obo_assessor', '$2y$10$pTy40ALO8dXQo7zj2gW2yON2/N88OF8Rzvhx.wRASYkgXYlykMqwC', NULL, 0, NULL, NULL, NULL, 2, 'Vl2VNkPmdZCfwushfi2chPTk4OZORokibZVMYuy1CUQMm5uRTZc0nQw7E7kb', 1, 0, '11|13|', '2024-02-01 19:11:59', '2024-02-01 19:12:17', NULL, 1276, 1276, NULL),
	(1279, 'Keely Fernandez', 'Keely', 'Fernandez', 'Jesse Barber', NULL, 'obo_approver', '$2y$10$W5km/Oy2yMHEDMX0.r3fi.yCwTVQiA3bYdAS0HhZJ3B12rbtAp1Jy', NULL, 0, NULL, NULL, NULL, 4, 'LEHnbDyVa1JSi9uXKeREsEGSasDb4TyY9tF0R2s3x5cY0OPMBT4Zut7Phes7', 1, 0, '12|', '2024-02-01 19:15:01', '2024-02-01 19:15:13', NULL, 1278, 1278, NULL),
	(1288, 'HAYLEY MARSHALL', 'HAYLEY', 'MARSHALL', 'SADE BEAN', 'emmanplacido.tieza@gmail.com', 'tez1', '$2y$10$JeuLXT.0VdaJMbt8P8jUbeG4lm.iENTm448CS6ufsc1UZ61mfZQv.', '214187', 1, NULL, NULL, NULL, NULL, 'bAlIqWyy2OYd0qMmx3u0gqszLUgTve7iJ9G1FnPm4oogWoHlkkgAGhZ1VCaw', 1, 1, '1', '2024-02-07 02:36:09', NULL, NULL, 1, NULL, NULL),
	(1289, 'RAM JAE SANCHEZ', 'RAM JAE', 'SANCHEZ', 'X', 'emmanplacido.tieza@gmail.com', 'ram', '$2y$10$XAdoHM0pdM9qmy.6l4MbdezVnn5nWPAneB3WnPLpUCvEtCRoOW.Ru', '879142', 1, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '2024-02-07 09:54:41', NULL, NULL, 1, NULL, NULL),
	(1290, 'Emman Placido', 'Emman', 'Placido', 'a', NULL, 'obo_reviewer', '$2y$10$DqMuVvA9P5q2fsPFUSWFO.1xBmK8.RNMpMsyT.TFUXNPl7XIS7oVy', NULL, 0, NULL, NULL, NULL, 1, 'AvV9ZDtBHENmnBlBlZmcGIWfKCzIbFIePA704dHfop6NqPtDUM1YFEq8D0SI', 1, 0, '11|12|13|', '2024-02-08 06:30:22', '2024-02-08 06:38:56', NULL, 1214, 1290, NULL);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
