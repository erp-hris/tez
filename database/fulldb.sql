-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.20-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for tez
DROP DATABASE IF EXISTS `tez`;
CREATE DATABASE IF NOT EXISTS `tez` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `tez`;

-- Dumping structure for table tez.application_remarks
DROP TABLE IF EXISTS `application_remarks`;
CREATE TABLE IF NOT EXISTS `application_remarks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `permit_type` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.application_remarks: ~6 rows (approximately)
/*!40000 ALTER TABLE `application_remarks` DISABLE KEYS */;
INSERT INTO `application_remarks` (`id`, `user_id`, `permit_type`, `record_id`, `status`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, 1144, 3, 1, 3, 'it\'s good and nice', 1144, NULL, '2022-04-04 14:51:54', NULL, NULL),
	(2, 1145, 3, 1, 4, 'well done', 1145, NULL, '2022-04-04 14:52:19', NULL, NULL),
	(3, 1146, 3, 1, 5, 'I will recommend it to sir John', 1146, NULL, '2022-04-04 14:53:03', NULL, NULL),
	(4, 1147, 3, 1, 6, 'Please proceed to payment request', 1147, NULL, '2022-04-04 14:53:28', NULL, NULL),
	(5, 1143, 3, 1, 7, NULL, 1143, NULL, '2022-04-04 14:54:12', NULL, NULL),
	(6, 1147, 3, 1, 8, ' Thank you', 1147, NULL, '2022-04-04 14:54:46', NULL, NULL);
/*!40000 ALTER TABLE `application_remarks` ENABLE KEYS */;

-- Dumping structure for table tez.attachments
DROP TABLE IF EXISTS `attachments`;
CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `permit_type` int(11) DEFAULT NULL,
  `file_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lock_file` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.attachments: ~2 rows (approximately)
/*!40000 ALTER TABLE `attachments` DISABLE KEYS */;
INSERT INTO `attachments` (`id`, `user_id`, `record_id`, `permit_type`, `file_id`, `attachment_name`, `lock_file`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, 1143, 1, 3, '1', '_HLrFr8t27VTTNvcqTHaQnekweAuk2r1XJYGPhOcbgDSYgulseD.pdf', 1, NULL, 1143, 1143, '2022-04-04 14:50:03', '2022-04-04 14:50:16', NULL),
	(2, 1143, 1, 3, '2', '_CtIbbqVvVwN9qTZqgp9b9Rr112T7YXq0PE6Z4eFuJX9DoxVuTx.pdf', 1, NULL, 1143, 1143, '2022-04-04 14:50:07', '2022-04-04 14:50:16', NULL),
	(3, 1143, 1, 3, '3', '_Y27ORn6yywAh2yI8j4FfPsvUVp5kF0Hqa8oS8pYNT2AcFeXPZq.pdf', 1, NULL, 1143, 1143, '2022-04-04 14:50:09', '2022-04-04 14:50:16', NULL);
/*!40000 ALTER TABLE `attachments` ENABLE KEYS */;

-- Dumping structure for table tez.building_permit
DROP TABLE IF EXISTS `building_permit`;
CREATE TABLE IF NOT EXISTS `building_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permit_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filed_date` date DEFAULT NULL,
  `assessed_date` date DEFAULT NULL,
  `evaluated_date` date DEFAULT NULL,
  `recommended_date` date DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  `date_of_issuance` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `file_lock` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `final_approver_id` int(11) DEFAULT NULL,
  `application_type` int(11) DEFAULT NULL,
  `permit_type` int(11) DEFAULT NULL,
  `area_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lot_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `block_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tct_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_declaration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_scope` int(11) DEFAULT NULL,
  `other_work_scope` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupancy_character` int(11) DEFAULT NULL,
  `other_occupancy_character` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupancy_classification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estimated_cost` decimal(20,2) DEFAULT 0.00,
  `units` int(11) DEFAULT NULL,
  `construction_date` date DEFAULT NULL,
  `floor_area` int(11) DEFAULT NULL,
  `expected_completion_date` date DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.building_permit: ~0 rows (approximately)
/*!40000 ALTER TABLE `building_permit` DISABLE KEYS */;
INSERT INTO `building_permit` (`id`, `permit_no`, `filed_date`, `assessed_date`, `evaluated_date`, `recommended_date`, `approved_date`, `payment_date`, `issued_date`, `date_of_issuance`, `status`, `file_lock`, `user_id`, `evaluator_id`, `approver_id`, `final_approver_id`, `application_type`, `permit_type`, `area_no`, `last_name`, `first_name`, `middle_name`, `tin`, `house_number`, `street`, `barangay`, `city`, `zip_code`, `telephone_number`, `lot_number`, `block_no`, `tct_no`, `tax_declaration_no`, `construction_street`, `construction_barangay`, `construction_city`, `work_scope`, `other_work_scope`, `occupancy_character`, `other_occupancy_character`, `occupancy_classification`, `estimated_cost`, `units`, `construction_date`, `floor_area`, `expected_completion_date`, `remarks`, `payment_receipt`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, '21080001', '2022-04-04', '2022-04-06', '2022-04-10', '2022-04-14', '2022-04-15', '2022-04-16', '2022-04-20', '2022-04-21', 8, 1, 1143, NULL, NULL, NULL, 1, 1, NULL, 'PLACIDO', 'EMMANUEL', 'SANTIAGO', '123-346-161', '813', 'PAG-ASA', 'MALIBO MATANDA', 'PANDI', '3014', '09367179649', '213', '3', '123456', '123456', 'ISAAC', 'MASUSO', 'PANDI', 2, NULL, 1, NULL, '5', 500000.00, 5, '2022-04-04', 500, '2022-04-29', NULL, 'receipt.png', 1143, 1147, '2022-04-04 14:43:46', '2022-04-04 14:54:46', NULL);
/*!40000 ALTER TABLE `building_permit` ENABLE KEYS */;

-- Dumping structure for table tez.business_permit
DROP TABLE IF EXISTS `business_permit`;
CREATE TABLE IF NOT EXISTS `business_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filed_date` date DEFAULT NULL,
  `evaluated_date` date DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `file_lock` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `final_approver_id` int(11) DEFAULT NULL,
  `enterprise_classification` int(11) DEFAULT NULL,
  `primary_enterprise_type` int(11) DEFAULT NULL,
  `secondary_enterprise_type` int(11) DEFAULT NULL,
  `tourist_transport_type` int(11) DEFAULT NULL,
  `application_type` int(11) DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_type` int(11) DEFAULT NULL,
  `amendment_from` int(11) DEFAULT NULL,
  `amendment_to` int(11) DEFAULT NULL,
  `tax_incentive_from_govt` int(11) DEFAULT NULL,
  `govt_entity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trade_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_tel_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_email_add` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_mobile_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_tel_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_email_add` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_mobile_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_area` decimal(10,2) DEFAULT 0.00,
  `total_employees` int(11) DEFAULT NULL,
  `residing_with_lgu` int(11) DEFAULT NULL,
  `lessor_full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lessor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lessor_contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lessor_email_add` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_rental` decimal(10,2) DEFAULT 0.00,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.business_permit: ~0 rows (approximately)
/*!40000 ALTER TABLE `business_permit` DISABLE KEYS */;
/*!40000 ALTER TABLE `business_permit` ENABLE KEYS */;

-- Dumping structure for table tez.files
DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permit_type` int(11) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.files: ~25 rows (approximately)
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` (`id`, `code`, `name`, `permit_type`, `required`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
	(1, NULL, 'Environmental Compliance Certificate (ECC) or Certificate of Non-Coverage\r\n            (CNC) by the DENR.', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(2, NULL, 'Zoning/Locational Clearance from the FTEZ Administrator', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(3, NULL, 'TIEZA Certificate of Designation or Special Permit to Locate', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(4, NULL, 'Transfer Certificate of Title (TCT)', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(5, NULL, 'Contract of Lease', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(6, NULL, 'Deed of Sale', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(7, NULL, 'Secretary\'s Certification', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(8, NULL, 'TIEZA Permit Application Letter', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(9, NULL, 'TIEZA Certification of the designated Designers and Project', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(10, NULL, 'Engineers/Professionals in-charge of construction', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(11, NULL, 'Building Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(12, NULL, 'Architectural Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(13, NULL, 'Civil/Structural Permit Form-', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(14, NULL, 'Electrical Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(15, NULL, 'Plumbing Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(16, NULL, 'Sanitary Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(17, NULL, 'Mechanical Permit Form', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(18, NULL, 'Electronics Permit Form ', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(19, NULL, 'PRC License', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(20, NULL, 'Professional Tax Receipt (PTR)', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(21, NULL, 'Project Cost and Estimate', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(22, NULL, 'Technical Specifications', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(23, NULL, 'Structural Anaysis and Design', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(24, NULL, 'Soil Boring Test', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL),
	(25, NULL, 'Construction Log Book', 3, NULL, NULL, 1, NULL, '2022-04-04 10:00:41', NULL, NULL);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;

-- Dumping structure for table tez.location_permit
DROP TABLE IF EXISTS `location_permit`;
CREATE TABLE IF NOT EXISTS `location_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filed_date` date DEFAULT NULL,
  `evaluated_date` date DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `file_lock` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `final_approver_id` int(11) DEFAULT NULL,
  `proponent_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `representative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_line` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_add` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_type` int(11) DEFAULT NULL,
  `other_application` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_type` int(11) DEFAULT NULL,
  `other_project` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `units` int(11) DEFAULT NULL,
  `storey` int(11) DEFAULT NULL,
  `lot_area` int(11) DEFAULT NULL,
  `floor_area` int(11) DEFAULT NULL,
  `ownership_type` int(11) DEFAULT NULL,
  `other_ownership` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `investment_cost` decimal(20,2) DEFAULT 0.00,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.location_permit: ~0 rows (approximately)
/*!40000 ALTER TABLE `location_permit` DISABLE KEYS */;
/*!40000 ALTER TABLE `location_permit` ENABLE KEYS */;

-- Dumping structure for table tez.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.migrations: ~0 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table tez.occupancy_permit
DROP TABLE IF EXISTS `occupancy_permit`;
CREATE TABLE IF NOT EXISTS `occupancy_permit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filed_date` date DEFAULT NULL,
  `evaluated_date` date DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `file_lock` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `final_approver_id` int(11) DEFAULT NULL,
  `application_type` int(11) DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lot_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `block_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tct_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_declaration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `construction_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.occupancy_permit: ~0 rows (approximately)
/*!40000 ALTER TABLE `occupancy_permit` DISABLE KEYS */;
/*!40000 ALTER TABLE `occupancy_permit` ENABLE KEYS */;

-- Dumping structure for table tez.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `evaluator` int(11) DEFAULT NULL,
  `approver` int(11) DEFAULT NULL,
  `issuer` int(11) DEFAULT NULL,
  `permit_type` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL DEFAULT 1,
  `first_login` tinyint(4) NOT NULL DEFAULT 0,
  `position` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1148 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tez.users: ~6 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `middle_name`, `email`, `username`, `password`, `level`, `evaluator`, `approver`, `issuer`, `permit_type`, `remember_token`, `user_status`, `first_login`, `position`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`) VALUES
	(1, 'admin', NULL, NULL, NULL, NULL, 'admin', '$2y$10$5FiMRgOPGD20oPTyHDdOAem4OmZg89Et2bL9NYOdB7CVMbgdJVPYC', 0, NULL, NULL, NULL, NULL, 'EQe3w7jpCguMkyZPrdWlPET8NRniYmdEc84rSbzNEDdn8iT5XZUkitbJfQKv', 1, 0, 0, '2019-10-02 17:45:17', '2019-10-02 17:45:17', NULL, 0, NULL),
	(1143, 'Mr. Applicant', NULL, NULL, NULL, NULL, 'applicant', '$2y$10$mb4QzU9L1jUAbIBmNBq5xeZxlj4YuIpvM9lSqof/oCtYxbTULisJu', 1, NULL, NULL, NULL, 3, '8HKNmh4u4iGQz5kU3yu9mqsKAHPcEOYxJrwcNiIzlQk8MxH6dZdM3c4Aq2OF', 1, 0, 1, '2022-04-04 09:59:29', NULL, NULL, 1, NULL),
	(1144, 'Mr. Assessor', NULL, NULL, NULL, NULL, 'assessor', '$2y$10$tNn4u.9eL9nDTpSP1EPKwOBAQUBGWzajVcss9.6IrVtX5OFqraodK', 2, NULL, NULL, NULL, 3, '5bxIEFGOPehK7RNbwMa8KTxXZOeijn8pirl3EzEiZDXcFHyQVRgp4Plcs5mR', 1, 0, 2, '2022-04-04 09:59:29', NULL, NULL, 1, NULL),
	(1145, 'Mr. Evaluator', NULL, NULL, NULL, NULL, 'evaluator', '$2y$10$We3au97R3leN6oAMhknx4O7m96AKf4pbbdHXfx8oUJJ9reHsDv95K', 2, NULL, NULL, NULL, 3, 'k4qIk6E4KgrJOK7wVbY1QoSce70435Ju5dt7p6Rkmk8gzVYVJ2lgAlSRwwWA', 1, 0, 3, '2022-04-04 09:59:29', NULL, NULL, 1, NULL),
	(1146, 'Mr. Recommendator', NULL, NULL, NULL, NULL, 'recommendator', '$2y$10$Zjcigozhvb1NjdhOtT6EqOZXJlpQ7RAOM6KZnNsq1/cLHuidj.4Yu', 2, NULL, NULL, NULL, 3, 'ROvnI3VEXWcPcx9tCqWR5HQHgP9VDtV97H9wXpUrqjdv0ZLTX3Boyah2GsUn', 1, 0, 4, '2022-04-04 09:59:29', NULL, NULL, 1, NULL),
	(1147, 'Mr. Approver/Issuer', NULL, NULL, NULL, NULL, 'approver', '$2y$10$Zsx4XB82dCWgNqvYoRN6vOSZ0ymb6VZs17mEWhzmjbyReYoK1g2KG', 2, NULL, NULL, NULL, 3, 'qZSTL3wdedgiP60wRq9zXEQIMMQtyPkuks8AcC2k2LTj2Zi9Bly07GBWi1qV', 1, 0, 5, '2022-04-04 09:59:29', NULL, NULL, 1, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
