<?php

use Illuminate\Database\Seeder,
    App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        //User::truncate();

        // And now, let's create a few articles in our database:
        /** **/
        User::create([
            'name' => '',
            'username' => '1997-0010-P',
            'email' => 'storemalt@gmail.com',
            'password' => bcrypt(123123),
            'employee_id' => '118',
            'level' => 1
        ]);

        /**

        $users = array(
        ['name' => 'Azores, Maricar L.', 'username' => 'mlazores', 'password' => '123456'],
        ['name' => 'Bagtas, Julia V.', 'username' => 'jvbagtas', 'password' => '123456'],
        ['name' => 'Borromeo, Sheena C.', 'username' => 'scborromeo', 'password' => '123456'],
        ['name' => 'Cariaga, Emily T.', 'username' => 'etcariaga', 'password' => '123456'],
        ['name' => 'De Castro, Cathrin Margot M.', 'username' => 'cmmdecastro', 'password' => '123456'],
        ['name' => 'Escuadra, Ken Jefte B.', 'username' => 'kjbescuadra', 'password' => '123456'],
        ['name' => 'Esguerra, Maria Aloha C.', 'username' => 'macesguerra', 'password' => '123456'],
        ['name' => 'Gatchalian, Leizl S.', 'username' => 'lsgatchalian', 'password' => '123456'],
        ['name' => 'Irizare, Iriz Mae C.', 'username' => 'imcirizare', 'password' => '123456'],
        ['name' => 'Merino, Kate Bernadette A.', 'username' => 'kbamerino', 'password' => '123456'],
        ['name' => 'Paz, Lory May S.', 'username' => 'lmspaz', 'password' => '123456'],
        ['name' => 'Peco, Glory Hope M.', 'username' => 'ghmpeco', 'password' => '123456'],
        ['name' => 'Reyes, Mary Jane M.', 'username' => 'mjmreyes', 'password' => '123456'],
        ['name' => 'Rosal, Charess R.', 'username' => 'crrosal', 'password' => '123456'],
        ['name' => 'Sampayan, Jenylyn I.', 'username' => 'jisampayan', 'password' => '123456'],
        ['name' => 'Ventura, Justine Joy V.', 'username' => 'jjvventura', 'password' => '123456']);


        foreach ($users as $key => $value) {
            User::create([
                'name' => $value['name'],
                'username' => $value['username'],
                'email' =>  null,
                'password' => bcrypt(123123),
                'level' => 0
            ]);
        }
         **/

    }
}
